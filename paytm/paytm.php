<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">

    <script src="../Bootstrap/js/respond.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>

<body>
	<div class="container">
		<div class="jumbotron">
		<form id="paytm_payment" name="paytm_payment" method="POST" action="https://secure.paytm.in/oltp-web/processTransaction">
			<div id="paytm_payment_div">
			</div>

			</form>
			<form class="form-horizontal" id="paytm_payment1" name="paytm_payment1" role="form" method="POST" >
			<input type='hidden' name='phone' value='7503523923'>
			<input type='hidden' name='email' value='a.sharma@indiareads.com'> 
			<div class="form-group">
				<label class="control-label col-sm-2" for="user_id">User Id:</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="user_id" name="user_id" placeholder="User ID" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="amount">Enter Amount:</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="amount" name="amount" placeholder="Enter Amount" required>
				</div>
			</div>
			  
			  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="button" class="btn btn-default" name="pay_now" id="pay_now">Pay Now</button>
				    </div>
			  </div>
			</form>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click',"#pay_now", function(){
			
			if($("#amount").val().trim()=="")
			{
				alert("Please enter amount");
				$("#amount").focus();
				return false;
			}
			var str = $("#paytm_payment1").serialize();
			$.ajax({
                url: "create-checksum.php", 
                method: 'POST',
                data: str,
                dataType: 'json', 
                success: function(response){
                  if(response.code==1)
                  {
					$("#paytm_payment_div").empty().html(response.html);
					$("#paytm_payment").submit();
                  }
                }
            });
			
		});
	});
</script>
