 <?php

include_once('../config/config.php');

include_once('../config/functions.php');
 include_once('../mail/mail.php'); 
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

$cor_name_v="-----------------------------select-------------------------";

if(isset($_POST['submit1']) && !empty($_POST['emails'])){
	$body='<table>
				<tbody style="font-size: 18px;">
					<tr>
						<td><a href="#"><img src="http://www.indiareads.com/images/logo.png" alt="INDIAREADS"></a></td>
					</tr>
					<tr>
						<td><br>Hello, Reader!</td>
					</tr>
					<tr>
						<td>
						<br>
							 This is to kindly inform you that the time span to keep the book has been over, so it\'s a humble request to you to kindly return the book.	
						</td>
					</tr>
				</tbody></table>
					 
					<table style="font-size: 18px;"><tbody>
					<tr>
						<td>
						<br><br>
							<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you read books and return them after reading.<br/></p>
						</td>
					</tr>
					<tr>
						<td>
							<h4>Here is how we make reading so easy for you.</h4>
							<li>Just Login, Browse Books and Add them to your Bookshelf</li>
							<li>Select the Books you want to rent and checkout through our Rental Cart</li>

							<li>Books are Delivered at your Address</li>
							<li>Done Reading ? Request to return the books</li>
							
						</td>
					</tr>
					<tr>
						<td><p>Need Assistance?<br><a href="#">Contact us</a> any time.</p></td>
					</tr>
					<tr>
						<td>
							Or, <br> Write to us @
							customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-4206323
						</td>

					</tr>
					<br>
					<tr>
						<td>
							<p>Happy Reading!<br>Team IndiaReads.</p>
						</td>
					</tr>
				</tbody>
			</table>';
	 foreach ($_POST['emails'] as $key => $value1) {
	 	 $gett=sendMail($value1, 'Time Span For Returning Book',$body);
	 	 // $gett=sendMail("ritesh7kumar7@gmail.com", 'Time Span For Returning Book',$body);
	 	 if($gett){
	 	 	print_r($value1);
	 	 	print_r('email sent');
	 	 }
	 }
}

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='more';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
       
        <li class="active"><a href="../reminder/reminder.php">Send Reminder</a></li>
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-7">
            <div class="form-group">
              <label class="control-label col-sm-3" for="cor_name">Select Corporate:</label>
              <div class="col-sm-5 no-left-pad">
                  <select required class="form-control"  name="cor_name" id="cor_name" >
                   <?php 
                      $res_cor = $fun_obj->get_all_corporates();
                      foreach ($res_cor as $key1) {
                          echo '<option value="'.$key1['company_id'].'">'.$key1['company_name'].'</option>'; 
                      }
                       echo '<option selected>'.$cor_name_v.'</option>'; 
                    ?>
                  </select>
                    <div id="loader" class="sp sp-circle"></div>
              </div>
              <div class="col-sm-4 no-left-pad">
              		<input type="submit" name="submit" class="btn btn-danger iread-btn iread-btn-white" value="Submit"/>
                   
              </div>
            </div>
          </div>
      </form><Br><Br>
       
      
    </div>
    <div class="col-md-12">
    	<?php 
    		 if(isset($_POST['submit']) && isset($_POST['cor_name']))
			 {
			 	 $result = $fun_obj->get_orders_to_send_reminder($_POST['cor_name']);
			 	 if(!empty($result)){
			 	 	?>
			 	 	 
			 	 	<form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
			 	 		<?php foreach($result as $value)
							{
							  echo '<input type="hidden" name="emails[]" value="'. $value['user_email']. '">';
							}
						?>
			 	 		<input type="submit" name="submit1" class="btn btn-danger iread-btn iread-btn-white" value="Send email to all"/>
              
			 	 	</form>
			 	 	<h3>Total orders <?php echo count($result); ?></h3>
			 	 	<table class="table table-bordered table-condensed font-new-del">
					    <thead>
					      <tr>
					        <th>Order ID</th>
					        <th>Email</th>
					        <th>Dispatched Date</th>
					        <th>Order Status</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php
					    	foreach ($result as $key => $value) {
					    	?>
					    	<tr>
					    		<td><?php echo $value['bookshelf_order_id']; ?></td>
					    		<td><?php echo $value['user_email']; ?></td>
					    		<td><?php echo $value['dispatched']; ?></td>
					    		<td><?php echo $value['order_status']; ?></td>
					    	</tr>
					    	<?php 
					    	}
					    	?>
					    </tbody>
					</table>
			 	 	<?php
			 	 }
			 	 else{
			 	 	echo '<h4>No orders found</h4>';
			 	 }
			 }
    	?>
    </div>

</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>
