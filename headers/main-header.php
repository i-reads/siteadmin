<!-- <script src="http://code.jquery.com/jquery-latest.min.js"></script> -->
<?php
session_start();
if(!$_SESSION['user']){
header('Location: /siteadmin/siteadmin/logout.php');
}
$con1 = connect($config);
$fun_obj1 = new ireads($con1);
$count = $fun_obj1->license_expired_count();
?>
<script src="<?php echo base_url;?>/Bootstrap/js/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url;?>/Bootstrap/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url;?>/Bootstrap/js/bootstrap.min.js"></script>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img src="../headers/logo.png" style="width:140px;margin-top:-7px;" class="img-responsive"></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="<?php if($page_class=='rent-requests'){ echo 'active';} ?>"><a href="../rent-request/rent-requests.php">Rent requests</a></li>
      <li class="<?php if($page_class=='add-data'){ echo 'active';} ?>"><a href="../add-data/add-data.php">Add Books</a></li>
      <li class="<?php if($page_class=='merchant'){ echo 'active';} ?>"><a href="../merchant/add-merchant.php">Merchant</a></li>
      <li class="<?php if($page_class=='wishlist'){ echo 'active';} ?>"><a href="../wishlist/corporate-w.php">Wishlist</a></li>
      <!-- <li class="<?php if($page_class=='bulk-mail'){ echo 'active';} ?>"><a href="../bulk-mail/bulk_mail.php"></a></li> -->
      <li class="<?php if($page_class=='user'){ echo 'active';} ?>"><a href="../user/user_pass.php">User Panel</a></li>
      <li class="<?php if($page_class=='License'){ echo 'active';} ?>"><a href="../license/license.php">License <span class="badge" style="color:red;background:white;"><?php if(!empty($count)){echo $count;}else{echo "";}?></span></a></li>
      <li class="<?php if($page_class=='search'){ echo 'active';} ?>"><a href="../search/search.php">Search <span class="badge" style="color:red;background:white;"><?php if(!empty($count)){echo $count;}else{echo "";}?></span></a></li>
      <li class="<?php if($page_class=='reissue'){ echo 'active';} ?>"><a href="../reissue/reissue.php">Reissue </a></li>
      <li class="<?php if($page_class=='proc'){ echo 'active';} ?>"><a href="../proc_management/proc_management.php">Proc Management</a></li>

      <li class="<?php if($page_class=='kota'){ echo 'active';} ?>"><a href="../kota-main/add-kota.php"  >Kota </a></li>
      
   
      <li class="<?php if($page_class=='more'){ echo 'active';} ?>" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">More
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
           
          <li><a href="../pending-pickup/pending-pickup.php">Pending Pickups</a></li>
          <li><a href="../bulk-mail/bulk_mail.php">Bulk Mail</a></li>
          <li><a href="../verification-email/verification-email.php">Verification Mail Corporate</a></li>
          <li><a href="../get-users/get-users.php">Get Users</a></li>
          <li><a href="../mahindra/mahindra.php">Mahindra Update</a></li>
          <li><a href="../reminder/reminder.php">Send Reminder</a></li>
          <li><a href="#">Coming Soon</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/siteadmin/siteadmin/logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>