<?php

include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj 	= new ireads($con);
$count_tot   	= 0;
$company_data 	= "";
//$fun_obj = $fun_obj->fetch_bshelf_order_tracking('30608', '5629');

//print_r($fun_obj);die;
if((isset($_REQUEST['Search']) && $_REQUEST['Search']== 'Search') && (isset($_REQUEST['company_id']) && $_REQUEST['Search']!= ''))
{
	$company_data 	= $fun_obj->get_bshelf_orders($_REQUEST['company_id']);
	$object_array	= $fun_obj->array_to_object1($company_data);
	$count_tot		= count($company_data);
	

}
if(isset($_REQUEST['REISSUE_ALL']) && $_REQUEST['REISSUE_ALL']=='REISSUE ALL')
{
	foreach ($object_array as $value)
	{
		
		$fun_objj = $fun_obj->reissue_bshelf_order($value);
	}
	echo "Orders has been reissed.";
}
$all_data = $fun_obj->get_company_details();
//print_r($all_data);die;


?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
	 <?php 
	    $page_class='reissue';  
	    include_once('../headers/main-header.php');
	  ?>	
	  	<div class="well well-sm" align="center">
  		  	<form class="form-inline" method="GET">
  			    <div class="form-group">
  			      	<select class="form-control" id="company_id" name="company_id">
  			      		<option>SELECT COMPANY</option>
  			      		<?php 
  			      		foreach ($all_data as $key) {
  			      		
  			      		?>
  				        	<option value="<?php echo $key['company_id']; ?>" <?php if(isset($_REQUEST['company_id'])){ if($_REQUEST['company_id']==$key['company_id']){ echo "selected";} } ?>><?php echo $key['name']; ?></option>
  				        <?php } ?>
  			      	</select>
  			    </div>
  			    <button type="submit" name="Search" id="Search" value="Search" naclass="btn btn-default">Search</button>
  		  	</form>
  		  	<?php if($count_tot>0){ ?>
  		  	<form class="form-inline" method="POST">
	  		  	<div align="right">
	  		  		<span class="badge">TOTAL ORDERS FOUND : <?php echo $count_tot; ?></span>
	  		  		<input type="submit" name="REISSUE_ALL" id="REISSUE_ALL" value="REISSUE ALL" class="btn btn-lg btn-danger">
	  		  	</div>
  		  	</form>
  		  	<?php } ?>
	  	</div>
	  	<table class="table table-striped table-bordered">
	  		<thead>
	  			<tr>
	  				<th>Order ID</th>
	  				<th>ISBN</th>
	  				<th>Order Status</th>
	  				<th>Initial Pay</th>
	  				<th>Issue Date</th>
	  				<th>Unique Book Id</th>
	  			</tr>
	  		</thead>
	  		<tbody>
	  			<?php 
	  			if(!empty($object_array))
	  			{
	  				foreach ($object_array as $value) {
	  					
	  			?>
	  			<tr>
	  				<td><?php echo $value->bookshelf_order_id; ?></td>
	  				<td><?php echo $value->ISBN13;?></td>
	  				<td><?php echo $value->order_status;?></td>
	  				<td><?php echo $value->init_pay;?></td>
	  				<td><?php echo $value->issue_date;?></td>
	  				<td><?php echo $value->unique_book_id;?></td>
	  			</tr>
	  			<?php } } ?>
	  		</tbody>
	  	</table>
	</body>
</html>