  <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);
?>



<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>siteadmin-Add books</title>
        <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../style.css" rel="stylesheet">
        <script src="../Bootstrap/js/respond.js"></script>
     
    </head>
    <body>
        <?php 
            $page_class='License'; 
            $res = $fun_obj->license_check();
            include_once('../headers/main-header.php');
        ?>
        <div class="container">
            <h2>License Expiry</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><h3>Company ID</h3></th>
                        <th><h3>Company Name</h3></th>
                        <th><h3>No. of License</h3></th>
                        <th><h3>Total Request Pending</h3></th>
                        <th><h3>Total Request Delivered</h3></th>
                        <th><h3>License Status</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($res as $value) {
                        $counter = $fun_obj->order_counts($value['company_id']);
                    ?>
                    <tr>
                        <td><h4><?php echo $value['company_id']; ?></h4></td>
                        <td><h4><?php echo $value['company_name']; ?></h4></td>
                        <td><h4><?php echo $value['license_count']; ?></h4></td>
                        <td><h4><?php echo $counter[0][0]?></h4></td>
                        <td><h4><?php echo $counter[1][0]?></h4></td>
                        <td style="color: green;"><h4><b>Expired</b></h4></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php
        if(!empty($i_msg)){
        echo '<div class="alert alert-success i-alert" id="i-alert">
            <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
           '.$i_msg.'  
        </div>';
        }
        else{
         
        }
        ?>
        <!-- container ends-->
	</body>
     
    <script type="text/javascript"> 
    $(document).ready(function() {
    	 $("#loader").hide();
      $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
          $("#i-alert").alert('close');
      });
    });
    </script>
</html>
