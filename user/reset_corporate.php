<?php

include_once('../config/config.php');
 
include_once('../config/functions.php');
 
$con = connect($config);
$fun_obj = new ireads($con);
        $email =$_POST['email1'];
        $new_password = $_POST['new_password1'];
        $dup_password=  $_POST['dup_password1'];
        $salt =getToken(32);
       $msg='';
       if($dup_password== $new_password) {
            $new_pass_hash = hash("sha256",$new_password.$salt);
           
            $check_user=$fun_obj->corp_check_if_user_email($email);
            if(!empty($check_user)) {
                 if($fun_obj->corp_update_password($email,$salt,$new_pass_hash)){
                    $msg.='Password changed';
                 }
                 else{
                    $msg.="Oops,some error";
                 }
            
            }
            else{
                $msg.='user not found';
            }
        }
        else{
            $msg.="Password does not match, Please try again";
        }
        header("Location: user_pass.php?message=$msg");

     function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $token;
    }
     function crypto_rand_secure($min, $max) 
    {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

?>