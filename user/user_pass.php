   <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

if(!empty($_GET['message'])) {
    $i_msg.= $_GET['message'];
}
if(isset($_POST['send'])){
	$user1=$_POST['email1'];
	
    header("Location: http://indiareads.com/get-login-from-admin?email=$user1");
}
  

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='user';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
	    <div class="container-fluid">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="../user/user_pass.php">Password Reset / User Login</a></li>
	        <li  ><a href="../user/store.php">Store Credit</a></li>
 
	      </ul>
	    </div>
  	</nav>	
	<div class="col-md-4 col-md-offset-1">	
	 <h3>Password Reset RETAIL</h3>
	  <form role="form" action="reset.php" method="post">
	    <div class="form-group">
	      <input type="email" name="email" required class="form-control" id="email" placeholder="Enter email">
	    </div>
	    <div class="form-group">
	      <input type="password" name="new_password" required class="form-control" id="pwd" placeholder="Enter new password">
	    </div>
	    <div class="form-group">
	      <input type="password" name="dup_password" required class="form-control" id="pwd1" placeholder="Confirm password">
	    </div>
	    <button type="submit" class="btn btn-danger iread-btn iread-btn-white">Submit</button>
	  </form>
	  <br>
	  <h3>Password Reset CORPORATE</h3>
	  <form role="form" action="reset_corporate.php" method="post">
	    <div class="form-group">
	      <input type="email" name="email1" required class="form-control" id="email1" placeholder="Enter email">
	    </div>
	    <div class="form-group">
	      <input type="password" name="new_password1" required class="form-control" id="pwd1" placeholder="Enter new password">
	    </div>
	    <div class="form-group">
	      <input type="password" name="dup_password1" required class="form-control" id="pwd11" placeholder="Confirm password">
	    </div>
	    <button type="submit" class="btn btn-danger iread-btn iread-btn-white">Submit</button>
	  </form>
	</div>
	<div class="col-md-4 col-md-offset-2">
	 <h3>User Login</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="email" class="form-control" id="email" name="email1" placeholder="Enter email">
	    </div>    	
	    <input type="submit" name="send" class="btn btn-danger iread-btn iread-btn-white" value="Submit"/>
	  </form>
	</div>
 

<div class="col-md-12"><hr></div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
 
<script type="text/javascript"> 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
 
</script>
</html>
