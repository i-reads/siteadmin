    <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

if(isset($_POST['update'])){
    //$chk=$fun_obj->update_store_credit($_POST['email'],$_POST['credit']);
    $i_msg.="Get it done by technical team.";
}

  

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='user';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
	    <div class="container-fluid">
	      <ul class="nav navbar-nav">
	        <li ><a href="../user/user_pass.php">Password Reset / User Login</a></li>
	        <li class="active"><a href="../user/store.php">Store Credit</a></li>
 
	      </ul>
	    </div>
  	</nav>	
	<div class="col-md-4 col-md-offset-1">	
	 <h3>Check Store Credit</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="email" name="email" required class="form-control" id="email" placeholder="Enter email">
	    </div>
	    <button type="submit" name="check" class="btn btn-danger iread-btn iread-btn-white">Check</button>
	  </form>
	 	<?php
		 	if(isset($_POST['check'])){
			    $chk=$fun_obj->check_store_credit($_POST['email']);
			    if($chk !== null){
			    	echo "<h3>".$_POST['email']."</h3>";
			    	echo "<h3>Store Credits : ".$chk." </h3>";
			    }
			    else{
			    	$i_msg.="Oops something went wrong";
			    }
			}
		?>
	</div>
	<div class="col-md-4 col-md-offset-2">
	 <h3>Update Store Credit</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
	    </div>  
	    <div class="form-group">
	      <input type="text" class="form-control" id="credit" name="credit" placeholder="Enter credit">
	    </div>    	
	    <input type="submit" name="update" class="btn btn-danger iread-btn iread-btn-white" value="Update"/>
	  </form>
	</div>
 

<div class="col-md-12"><hr></div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
 
<script type="text/javascript"> 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(14000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
 
</script>
</html>
