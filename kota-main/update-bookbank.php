 <?php
 
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

 $mer_v="";
  $LibraryBookID_v=$ISBN13_v=$ShelfLocation_v=$InSub_v=$Title_v=$mer_v="";
$Price_v=$discount_v=$ProcPrice_v=1;
 
 
  if (isset($_POST['submit'])) 
  {
    
      $ISBN13=trim($_POST['ISBN13']);
       
      $sell=$_POST['sell'];
      $mrp=$_POST['mrp'];
      
     

      if(empty($ISBN13) || empty($sell) || empty($mrp)){
        $i_msg.="Please fill all fields";
      }
      else{
        $ins= $fun_obj->update_kota_book_price($ISBN13,$mrp,$sell);
        $i_msg.=$ins;
         
      }

  }
  
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='kota';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a target="_blank" href="../Kota-old/index.php">Kota Bookbank</a></li>
        <li  ><a target="_blank" href="../kota-landmark/allen/index.php">Kota Landmark</a></li>
        <li  ><a href="../kota-main/add-kota.php">Add to KOTA</a></li>
        <li  ><a href="../kota-main/view-inven.php">View Kota Inventory</a></li>
        <li class="active"><a href="../kota-main/update-bookbank.php">Update MRP</a></li>
         
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
             
            <div class="form-group">
              <label class="control-label col-sm-4" for="ISBN13">ISBN13:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control"  name="ISBN13" id="ISBN13" value="" placeholder="Enter ISBN13" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
 
            <div class="form-group">
              <label class="control-label col-sm-4" for="mrp">MRP:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="mrp" id="mrp" placeholder="Enter MRP" value="">
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-sm-4" for="ProcPrice">Selling Price:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="sell" value="" id="ProcPrice" placeholder="Enter Selling Price">
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">Update</button>
              </div>
              <!-- <div class=" col-sm-4  text-right">
                    <button type="submit" name="update_book" class="btn btn-default iread-btn-white update-btn">Update Existing Book</button>
              </div> -->
            </div>
          </div>
      </form>
    </div>
     

</div>
<?php
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>