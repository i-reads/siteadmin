<?php
 
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

$LibraryBookID_v=$ISBN13_v=$ShelfLocation_v=$InSub_v=$Title_v=$mer_v="";
$Price_v=$discount_v=$ProcPrice_v=1;
 

  if (isset($_POST['submit'])) 
  {
      $LibraryBookID=trim($_POST['LibraryBookID']);
      $ISBN13=$_POST['ISBN13'];
       
      $ProcPrice=$_POST['ProcPrice'];
       $mrp=$_POST['mrp'];
      $discount=$mrp-$ProcPrice;
      
      $Mer_Name=$_POST['Mer_Name'];

      if(empty($ISBN13) || empty($LibraryBookID) || empty($Mer_Name)  || empty($mrp) || empty($ProcPrice)){
        $i_msg.="Please fill all fields";
      }
      else{
        $ins= $fun_obj->add_to_kota_inven($LibraryBookID,$ISBN13,$ProcPrice,$Mer_Name,$discount,$mrp);
        $i_msg.=$ins;
         
      }

  }
 
  
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='kota';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a target="_blank" href="../Kota-old/index.php">Kota Bookbank</a></li>
        <li  ><a target="_blank" href="../kota-landmark/allen/index.php">Kota Landmark</a></li>
        <li class="active"><a href="../add-data/add-kota.php">Add to KOTA</a></li>
        <li><a href="../kota-main/view-inven.php">View Kota Inventory</a></li>
         <li><a href="../kota-main/update-bookbank.php">Update MRP</a></li>
         
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="LibraryBookID">Library Book ID:</label>
              <div class="col-sm-5 no-left-pad input-group req-fi">
                <input type="text" class="form-control" required name="LibraryBookID" id="LibraryBookID" value="<?php echo $LibraryBookID_v; ?>" placeholder="Enter Library Book ID" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
              <div class="col-sm-offset-0 col-sm-3 text-left no-left-pad">
                <button type="submit" name="check_book" class="btn btn-default iread-btn-white isbn-btn">Check Book</button>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ISBN13">ISBN13:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control"  name="ISBN13" id="ISBN13" value="<?php echo $ISBN13_v; ?>" placeholder="Enter ISBN13" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
 
         
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Name">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad">
                  <select required class="form-control"  name="Mer_Name" id="Mer_Name"  onchange="javascript: this.form.submit();$('#loader').show();"  >
                   <?php 
                      $all_mer=$fun_obj->get_all_merchant_kota();
                      foreach ($all_mer as $key) {
                          echo '<option value="'.$key['mid'].'">'.$key['name'].'</option>'; 
                      }
                       echo '<option selected>'.$mer_v.'</option>'; 
                    ?>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="mrp">MRP:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="mrp" id="mrp" placeholder="Enter MRP" value="<?php echo $Price_v; ?>">
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-sm-4" for="ProcPrice">Procurement Price:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="ProcPrice" value="<?php echo $ProcPrice_v; ?>" id="ProcPrice" placeholder="Enter Proc Price">
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">Add New Book</button>
              </div>
              <!-- <div class=" col-sm-4  text-right">
                    <button type="submit" name="update_book" class="btn btn-default iread-btn-white update-btn">Update Existing Book</button>
              </div> -->
            </div>
          </div>
      </form>
    </div>

</div>
<?php
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>