<?php
 
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

 $mer_v="";
 $result_for_mer="";
  if (isset($_POST['submit'])) 
  {
      $Mer_Name=trim($_POST['Mer_Name']);
      

      if(empty($Mer_Name)){
        $i_msg.="Please select atleast one merchant";
      }
      else{
        $result_for_mer= $fun_obj->get_books_by_mer_kota($Mer_Name);
        // print_r($ins);
        // $i_msg.=$ins;
         
      }

  }
 if (isset($_POST['get_proc'])) 
  {
      
      
      if($_POST['date_start']!='' && $_POST['date_end']!=''){
           $result_for_mer= $fun_obj->get_books_by_date_kota($_POST['date_start'],$_POST['date_end']);

      }
      else{
        $i_msg.="Please select both dates";
      }
       
     
       
              
      

  }
  
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='kota';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a target="_blank" href="../Kota-old/index.php">Kota Bookbank</a></li>
        <li  ><a target="_blank" href="../kota-landmark/allen/index.php">Kota Landmark</a></li>
        <li  ><a href="../kota-main/add-kota.php">Add to KOTA</a></li>
        <li class="active"><a href="../kota-main/view-inven.php">View Kota Inventory</a></li>
         <li  ><a href="../kota-main/update-bookbank.php">Update MRP</a></li>
         
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-12">
 
            <div class="form-group">
              <label class="control-label col-sm-2" for="Mer_Name">Merchant Name:</label>
              <div class="col-sm-3 no-left-pad">
                  <select required class="form-control"  name="Mer_Name" id="Mer_Name"  onchange="javascript: this.form.submit();$('#loader').show();"  >
                   <?php 
                      $all_mer=$fun_obj->get_all_merchant_kota();
                      foreach ($all_mer as $key) {
                          echo '<option value="'.$key['mid'].'">'.$key['name'].'</option>'; 
                      }
                       echo '<option selected>'.$mer_v.'</option>'; 
                    ?>
                  </select>
              </div>
               <div class="col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">View Books</button>
              </div>
            </div>
             
            
          </div>
      </form>
      <div class="col-md-6">
        <h5 class="text-center">OR</h5>
      </div>
       <form role="form" class="col-md-8 form-inline" method="post">
            <div class="form-group">
              <label for="date_start">Start Date:</label>
              <input type="date" class="form-control" name="date_start" id="date_start">
            </div>
            <div class="form-group">
              <label for="date_end">End Date:</label>
              <input type="date" class="form-control" name="date_end" id="date_end">
            </div>
            <input type="submit" name="get_proc" class="btn btn-danger iread-btn iread-btn-white" value="Get Books"/>
          </form>
    </div>
    <div class="col-md-12">
      <?php 
      if($result_for_mer){
      ?>
      <h4>Total books procured : <?php echo count($result_for_mer);?></h4>
       <table class="table table-bordered table-condensed font-new-del ">
            <thead>
              <tr>
                <th>Book ID</th>
                <th>ISBN</th>
                <th>Merchant</th>
                <th>MRP</th>
                
                <th>Proc Price</th>
                <th>Discount</th>
                <th>Date of Procurement</th>
                 
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($result_for_mer as $key => $value) {
                 
              ?>
              <tr>
                <td><?php echo $value['library_id']; ?></td>
                <td><?php echo $value['isbn']; ?></td>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo 'Rs. '.$value['mrp']; ?></td>
                <td><?php echo 'Rs. '.$value['pro_price']; ?></td>
                <td><?php echo 'Rs. '.$value['discount']; ?></td>
                <td><?php echo $value['date']; ?></td>
                 
                
              </tr>
              <?php }?>
            </tbody>
          </table>  

          <?php }?> 
    </div>

</div>
<?php
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>