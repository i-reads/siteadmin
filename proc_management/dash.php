<?php
include_once('../config/config.php');
include_once('../config/functions.php');

global $i_msg;
$i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
    
  </head>
  <body>
    <?php
    $page_class='proc';
    include_once('../headers/main-header.php');
    ?>
    <div class="container">
      <nav class="sub-head navbar navbar-default">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
           <li class="active"><a href="../proc_management/dash.php">Dash Board</a></li>
            <li  ><a href="../proc_management/proc_management.php">New Procurements</a></li>
            
          </ul>
        </div>
      </nav>
      <div class="col-md-5 margintop">
            <?php 
               $circ_books = $fun_obj->get_books_for_every_circulation();
            ?> 
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Circulation</th>
                  <th>Total Books</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                $total_of_circulations=0;
                foreach ($circ_books as $key) {
                  $total_of_circulations+=$key['circulation']*$key['number'];
               ?>
                  <tr>
                    <td><?php echo $key['circulation'];?></td>
                    <td><?php echo $key['number'];?></td>
                  </tr>
              <?php } ?>
            </tbody>
          </table>
      </div>
      <div class="col-md-7">
          <h2 class="text-center">Overview</h2>
           <?php 
               $circ_books = $fun_obj->get_total_books_in_library();
               $total_books=count($circ_books);
               $avg_of_circulations=round($total_of_circulations/$total_books,2);
               $proc_prices=0;
               $init_pay =0;
               foreach ($circ_books as $key1) {
                $proc_prices+=$fun_obj->get_proc_price_of_book($key1['ISBN13']);
                $init_pay += $fun_obj->get_initial_payable($key1['book_id'],$key1['ISBN13']);
               }
               
               // print_r($proc_prices);
               // echo '<br>';
               // print_r($init_pay);
                $result_for_profit = $fun_obj->get_total_profit();
                $result_for_profit_corp = $fun_obj->get_total_profit_corporate();
                $total_corp_profit=(count($result_for_profit_corp))*(-20);
                $total_retail_profit=(($result_for_profit[0]['total_init']-$result_for_profit[0]['total_refund'])-(($result_for_profit[0]['total_orders'])*110));
           
            ?>
            <h4>Average Circulations : <?php echo $avg_of_circulations;?> </h4>
            <h4>Total Price of all books : <?php echo $proc_prices;?> </h4>
            <h4>Current value value of Library : <?php echo $init_pay;?> </h4>
            <h4>Total Profit Retail : <?php echo $total_retail_profit;?> </h4>
            <h4>Total Profit Corporate : <?php echo $total_corp_profit;?> </h4>
            <h4>Net Profit : <?php echo ($total_corp_profit+$total_retail_profit);?> </h4>
            
      </div>
    </div><!-- container ends-->

      <?php
      if(!empty($i_msg)){
      echo '<div class="alert alert-success i-alert" id="i-alert">
      <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
      '.$i_msg.'
      </div>';
      }
      else{

      }
      ?>
  </body>
  <script type="text/javascript"> 
  $(document).ready(function() {
  	 $("#loader").hide();
    $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
  });
  </script>
</html>
