 <?php
include_once('../config/config.php');
include_once('../config/functions.php');

global $i_msg;
$i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
    
  </head>
  <body>
    <?php
    $page_class='proc';
    include_once('../headers/main-header.php');
    ?>
    <div class="container">
      <nav class="sub-head navbar navbar-default">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            <li ><a href="../proc_management/dash.php">Dash Board</a></li>
            <li  ><a href="../proc_management/proc_management.php">New Procurements</a></li>
            <li class="active"><a href="../proc_management/delivery.php">Delivery</a></li>
            
          </ul>
        </div>
      </nav>
      <div class="col-md-12">
        <div class="margintop">
          <form role="form" class="col-md-8 form-inline" method="post">
            <div class="form-group">
              <label for="date_start">Start Date:</label>
              <input type="date" class="form-control" name="date_start" id="date_start">
            </div>
            <div class="form-group margin-left-15">
              <label for="date_end">End Date:</label>
              <input type="date" class="form-control " name="date_end" id="date_end">
            </div><br>
             <div class="form-group margintop">
              <label for="date_end">Corporate/Retail:</label>
              <select class="form-control" name="corp_flag" id="corp_flag">
                <option value="1">Corporate</option>
                <option value="2">Retail</option>
              </select>
            </div>
            <input type="submit" name="get_proc" class="margintop margin-left-15 btn btn-danger iread-btn iread-btn-white" value="Get Books"/>
          </form>
          <br><br>
          <table class="   table table-bordered table-condensed font-new-del margin-top-40">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>ISBN</th>
                <th>Book ID</th>
                <th>User ID</th>
                <th>Dispatch Date</th>
                 
              </tr>
            </thead>
            <tbody>
              <?php
              if(isset($_POST['get_proc'])){
              if($_POST['date_start']!='' && $_POST['date_end']!=''){
              echo '<h4 class="text-right">Deliveries from '.$_POST['date_start'].' to '.$_POST['date_end'].'</h4>';
              
               $books = $fun_obj->get_delivery_between_dates($_POST['date_start'],$_POST['date_end'],$_POST['corp_flag']);
                
               

                 
                if($books){
                   $record_count   = count($books);
                    echo '<h4 class="text-right">'.$record_count.' Total Deliveries</h4>';
                  
                   foreach ($books as $key) {
                  ?>
                  <tr>
                    <td><?php echo $key['order_id']; ?></td>
                    <td><?php echo $key['isbn']; ?></td>
                    <td><?php echo $key['book_id']; ?></td>
                    <td><?php echo $key['user_id']; ?></td>
                    <td><?php echo $key['d_date']; ?></td>
                     
     
                  </tr>
                  <?php }  }
                else{
                  $i_msg.= "No Delivery found";
                  }}
              }else{
              echo '<h4>Please enter both dates</h4>';
              }
               ?>
            </tbody>
          </table>
 
          <!--table for details end-->

        </div>
      </div><!-- container ends-->
      <?php
      if(!empty($i_msg)){
      echo '<div class="alert alert-success i-alert" id="i-alert">
      <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
      '.$i_msg.'
      </div>';
      }
      else{

      }
      ?>
  </body>
  <script type="text/javascript"> 
  $(document).ready(function() {
  	 $("#loader").hide();
    $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
  });
  </script>
</html>
