 <?php
include_once('../config/config.php');
include_once('../config/functions.php');

global $i_msg;
$i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
    
  </head>
  <body>
    <?php
    $page_class='proc';
    include_once('../headers/main-header.php');
    ?>
    <div class="container">
      <nav class="sub-head navbar navbar-default">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            <li ><a href="../proc_management/dash.php">Dash Board</a></li>
            <li  class="active"><a href="../proc_management/proc_management.php">New Procurements</a></li>
            <li ><a href="../proc_management/delivery.php">Delivery</a></li>
            
          </ul>
        </div>
      </nav>
      <div class="col-md-12">
        <div class="margintop">
          <form role="form" class="col-md-8 form-inline" method="post">
            <div class="form-group">
              <label for="date_start">Start Date:</label>
              <input type="date" class="form-control" name="date_start" id="date_start">
            </div>
            <div class="form-group">
              <label for="date_end">End Date:</label>
              <input type="date" class="form-control" name="date_end" id="date_end">
            </div>
            <input type="submit" name="get_proc" class="btn btn-danger iread-btn iread-btn-white" value="Get Books"/>
          </form>
          <br>
          <table class="table table-bordered table-condensed font-new-del margin-top-40">
            <thead>
              <tr>
                <th>Book ID</th>
                <th>ISBN</th>
                <th>Source</th>
                <th>MRP</th>
                <th>Discount</th>
                <th>Proc Price</th>
                <th>Date of Procurement</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(isset($_POST['get_proc'])){
              if($_POST['date_start']!='' && $_POST['date_end']!=''){
              echo '<h4 class="text-right">Procurements from '.$_POST['date_start'].' to '.$_POST['date_end'].'</h4>';
               $books = $fun_obj->get_books_from_procurement($_POST['date_start'],$_POST['date_end']);
                $record_count   = count($books);
                echo '<h4 class="text-right">'.$record_count.' Books are procured</h4>';
              if($books) {
                 foreach ($books as $key) {
                  $lib_status=$fun_obj->get_book_id_booklibrary($key['book_id']);
              ?>
              <tr>
                <td><?php echo '<b>'.$key['book_id'].'</b><br>';
                      if(($lib_status[0]['status'])==1){
                          echo  '<small style="color:#64DD17;">Inside</small>';
                      }
                      elseif(($lib_status[0]['status'])==2){
                          echo  '<small style="color:#DD2C00;">Outside</small>';
                      }
                      elseif(($lib_status[0]['status'])==-1){
                          echo  '<small style="color:#d50000;">Lost</small>';
                      }
                      elseif(($lib_status[0]['status'])==0){
                          echo  '<small style="color:#d50000;">Damaged</small>';
                      }
                      else{
                        echo  "";
                      }
                        
                  ?>
                </td>
                <td><?php echo $key['ISBN']; ?></td>
                <td><?php echo $key['merchant_name']; ?></td>
                <td><?php echo 'Rs. '.$key['Mrp']; ?></td>
                <td><?php echo 'Rs. '.$key['discount']; ?></td>
                <td><?php echo 'Rs. '.$key['proc_price']; ?></td>
                <td><?php echo $key['when_proc']; ?></td>
                <td>
                  <form role="form" class="col-md-8 form-inline" method="post">
                    <input type="hidden" name="book_id" value="<?php echo $key['book_id']; ?>">
                    <input type="submit" name="get_detail" class="btn btn-danger iread-btn iread-btn-white" value="Details"/>
                  </form>
                </td>
                
              </tr>
              <?php } }
              }else{
              echo '<h4>Please enter both dates</h4>';
              }
              }?>
            </tbody>
          </table>

          <!--table for details-->
          <?php
              if(isset($_POST['get_detail'])){
                echo '<hr>';
                  // echo '<h4 class="text-right">Procurements from '.$_POST['date_start'].' to '.$_POST['date_end'].'</h4>';
                  $details = $fun_obj->get_details_from_circulation($_POST['book_id']);
                  $record_count   = count($details);
                  echo '<h4 class="text-center">Details for Book ID : '.$_POST['book_id'].'</h4>'; 
            
          if($details) { 
          ?>  
          <table class="table table-bordered table-condensed font-new-del ">
            <thead>
              <tr>
                <th>Book ID</th>
                <th>ISBN</th>
                <th>Source</th>
                <th>MRP</th>
                <th>Discount</th>
                <th>Proc Price</th>
                <th>Date of Procurement</th>
                <th>Total Circulations</th>
                <th>Total Profit</th>
                <th>Current Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $details[0]['book_id']; ?></td>
                <td><?php echo $details[0]['ISBN']; ?></td>
                <td><?php echo $details[0]['merchant_name']; ?></td>
                <td><?php echo 'Rs. '.$details[0]['Mrp']; ?></td>
                <td><?php echo 'Rs. '.$details[0]['discount']; ?></td>
                <td><?php echo 'Rs. '.$details[0]['proc_price']; ?></td>
                <td><?php echo $details[0]['when_proc']; ?></td>
                <td><?php echo $record_count; ?></td>
                <td>
                    <?php 
                      $total_profit=0;
                      foreach ($details as $key) { 
                        $total_profit += ($key['sell_price']-$key['buy_price'])-(120);
                      }
                      echo 'Rs. '.$total_profit;
                    ?>
                </td>
                <td><?php  
                  $init_pay = $fun_obj->get_initial_payable($details[0]['book_id'],$details[0]['ISBN']);
                  echo 'Rs. '.$init_pay;
                 ?></td>
              </tr>
            </tbody>
          </table>   
          <table class="table table-bordered table-condensed font-new-del margin-top-40">
            <thead>
              <tr>
                <th>Circulation</th>
                <th>Circulated from to</th>
                <th>Sell Price</th>
                <th>Buy Price</th>
                <th>Profit</th>
                <th>Net Profit</th>
              
                <!-- <th>Discount</th>
                <th>Proc Price</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
                 foreach ($details as $key) {
              ?>
              <tr> 
                <td><?php echo $key['circulation']; ?></td>
                <td><?php echo date('d-m-Y',strtotime($key['sell_date'])).' to '.date('d-m-Y',strtotime($key['buy_date'])); ?></td>
                <td><?php echo $key['sell_price']; ?></td>
                <td><?php echo $key['buy_price']; ?></td>
                <td><?php echo ($key['sell_price']-$key['buy_price']); ?></td>
                <td><?php echo (($key['sell_price']-$key['buy_price'])-(120)); ?></td>
              </tr>
              <?php } 
            } else{ echo '<h4 class="text-center">No data found for Book ID : '.$_POST['book_id'].'</h4>';}
              }
              ?>
            </tbody>
          </table>
          <!--table for details end-->

        </div>
      </div><!-- container ends-->
      <?php
      if(!empty($i_msg)){
      echo '<div class="alert alert-success i-alert" id="i-alert">
      <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
      '.$i_msg.'
      </div>';
      }
      else{

      }
      ?>
  </body>
  <script type="text/javascript"> 
  $(document).ready(function() {
  	 $("#loader").hide();
    $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
  });
  </script>
</html>
