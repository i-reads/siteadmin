  <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='more';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">	
	<div class="col-md-8 col-md-offset-2">	
<?php
 
include_once('../mail/mail.php'); 
if(isset($_POST['send'])){
			
	$emailarray = explode(',', $_POST['email']);
	foreach($emailarray as $info){
		//$to      = 'nobody@example.com';
		$gett=sendMail($info, $_POST['subject'],$_POST['body']);
		
	}
	if($gett){
			$i_msg.="Mail is sent";
	}
	else{
		$i_msg.="Oops some error,try again";
	}

}	

?>
<script src="ckeditor/ckeditor.js"></script>
<script>
  window.onload = function(){
    CKEDITOR.replace('body_email');
  };
</script>
<style type="text/css">
#progress-bar
{
	width: 400px;
}
</style>

 <form  method="post" action="">
  <div class="form-group">
    <label for="email">Email address:</label>
   <textarea class="form-control" id="exampleTextarea" rows="10" name="email" required></textarea>
  </div>
  <div class="form-group">
    <label for="email">Subject:</label>
   <textarea class="form-control" id="exampleTextarea1" rows="2" name='subject' required></textarea>
  </div>  
  <div class="form-group" id="body_id">
    <label for="email">Body:</label>
   <textarea class="form-control" id="body_email" rows="10" name='body' required></textarea>
  </div>

  <button type="submit" class="btn btn-danger iread-btn" name ='send'>Send</button>
   <button type="button" id="preview_btn" class="btn btn-danger iread-btn iread-btn-white" data-toggle="modal" data-target="#modal_mail"  name ='preview'>Preview</button>
</form>
 

</div>

  <!-- Modal -->
  <div class="modal fade" id="modal_mail" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<div class="col-md-12"><hr></div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
 
<script type="text/javascript"> 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
$("#preview_btn").click(function() {
	 $('.modal-body').html(CKEDITOR.instances.body_email.getData());
	 var sub=$('textarea#exampleTextarea1').val();
	 $('.modal-title').html(sub);
 
});
</script>
</html>
