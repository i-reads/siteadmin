 <?php

include_once('../config/config.php');
 
include_once('../config/functions.php');
 include_once('../mail/mail.php'); 
$con = connect($config);
 
$fun_obj = new ireads($con);
function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
}	
function main_func_email($emailarray,$subject,$fun_obj){
		$i_msg='';
 		 
		foreach($emailarray as $to_addr){
		$npass = generateRandomString();
		$res1=   $fun_obj->update_newpassword_corp($to_addr,$npass);
		$url = 'http://indiareads.com/set-password?email='.$to_addr.'&h='.$npass;
	    $body_mail='<!DOCTYPE html>
								<html>
									<head>
										<title>IndiaReads</title>
									</head>
									<body>
										<table>
											<tbody style="font-size: 14px;">
												<tr>
													<td><a href="http://indiareads.com/"><img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/logo.png" alt="INDIAREADS"></a></td>
												</tr>
												<tr>
													<td><br>Hello, Reader!</td>
												</tr>
												<tr>
													<td><p>Thanks for signing up for <a href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a><br>Try it out and let us know what you think!</p></td>
												</tr>
												<tr>
													<td>
														<a href="'.$url.'" style="background-color: #bc3526;color: white;padding: 10px; font-size: 16px; font-family:arial;border-radius: 5px; border: 2px solid #bc3526;letter-spacing: 0.1em; margin-left: 30px;text-decoration: none;">Confirm Your Email</a>
													</td>
												</tr>
												<tr>
													<td><p>If it was not you, you can safely ignore this email.</p></td>
												</tr>
												<tr>
													<td>
														<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you rent books and return them after reading.<br/></p>
													</td>
												</tr>
												<tr>
													<td>
														<h4>Here is how we make reading so easy for you.</h4>
														<li>Just Login, Browse Books and Add them to your Bookshelf</li>
														<li>Select the Books you want to rent and checkout through our Rental Cart</li>
							
														<li>Books are Delivered at your Address</li>
														<li>Done Reading ? Request to return the books</li>
														
													</td>
												</tr>
												<tr>
													<td><p>Need Assistance?<br><a href="http://indiareads.com/contactus">Contact us</a> any time.</p></td>
												</tr>
												<tr>
													<td>
														Or, <br> Write to us @
														customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-2424233
													</td>

												</tr>
												<br>
												<tr>
													<td>
														<p>Happy Reading!<br>Team IndiaReads.</p>
													</td>
												</tr>
											</tbody>
										</table>
									</body>
								</html>';	

			 $gett=1;
			// $gett=sendMail($to_addr,$subject,$body_mail);
			if($gett){
				$i_msg.="Mail is sent for ".$to_addr.' <br> ';
			}
			else{
				$i_msg.="Failed for ".$to_addr.' <br> ';
			}
		}
		return $i_msg;
}
 
 
$arr_emails=$_POST['arr_emails'];
$subject=$_POST['subject'];

 //$res1= $fun_obj->get_unverified_users_corp($start);
if($arr_emails){
	$res[0]=main_func_email($arr_emails,$subject,$fun_obj);
}
else{
	$res[0]="false";
}

 echo json_encode($res[0]);
?>