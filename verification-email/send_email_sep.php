<?php
include_once('../config/config.php');
include_once('../config/functions.php');
$con = connect($config);
global $i_msg;
 $i_msg="";
$fun_obj = new ireads($con);
include_once('../rent-request/pagination2.php');
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>siteadmin</title>
		<link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="../style.css" rel="stylesheet">
		<script src="../Bootstrap/js/respond.js"></script>
	</head>
	<body>
		<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>
		<?php
		$page_class='more';
		include_once('../headers/main-header.php');
		?>
		<div class="container">
			<nav class="sub-head navbar navbar-default">
				<div class="container-fluid">
					<ul class="nav navbar-nav">
						<li class="active"><a href="verification-email.php">Verification Email Corporate</a></li>
					</ul>
				</div>
			</nav>
			<div class="col-md-12">
			 <?php
			 	if(isset($_POST['send'])){
					//$i='';
					
					$emailarray = explode(',', $_POST['email']);
					$total_email = count($emailarray);
					$limit=$_POST['limit'];
					$sub_post=$_POST['subject'];
					 
					for($i=0;$i<$total_email-1;$i=$i+$limit){
						?>
						<form method="post" action="">
							<div id="loader1" class="sp sp-circle loader"></div>
						<?php
						 for ($j=$i; $j < $i+$limit; $j++) { 
						 	if($j<$total_email-1){
						 		//echo $emailarray[$j].'<br>';
								//echo $j.'<br>';
						 ?>
						 	<input type="hidden" name="sep_email[]" value="<?php echo $emailarray[$j];?>"/>
						 <?php
						 }}
						?><br>
							Subject : <input type="text" id="subject" value="<?php echo $sub_post;?>"/>
							<input type="submit" id="send_final" class="send_final btn btn-danger iread-btn" value="Send<?php echo ($i+1);?> to <?php echo ($i+$limit);?>"/>
						</form><br><hr>
						<?php
						  
					}
				}
			 ?>
			</div><hr><Hr>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}else{}
?> 
<script type="text/javascript">
 
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(10000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});
/*send mails*/
$(".send_final").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     $(this).removeClass("btn-danger").addClass("btn-success");
    $(this).removeClass("iread-btn").addClass("disabled");
    
     var subject=$(this).siblings('#subject').val();
     var form=$(this).parent('form');
	     inputs = form.children("input");
	          arr_emails = [];
	      for(var i=0, len=inputs.length; i<len; i++){
	        if(inputs[i].type === "hidden"){
	          arr_emails.push(inputs[i].value);
	        }
	      }
	      
	      // console.log(arr);
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/verification-email/ajax_send_final.php',
            data: {  
                    arr_emails : arr_emails,
                    subject : subject
                  },
            dataType: 'json',
            success: function(res){  
                                    $('.loader').hide(); 
                                   html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                                    $('body').append(html);
                                     $("#i-alert").fadeTo(9000000, 500).slideUp(500, function(){
                                       $("#i-alert").alert('close');
                                     });
            }
     });
     return false; 
});
</script>
 
	</body>
</html>