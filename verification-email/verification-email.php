<?php
include_once('../config/config.php');
include_once('../config/functions.php');
$con = connect($config);
global $i_msg;
 $i_msg="";
$fun_obj = new ireads($con);
include_once('../rent-request/pagination2.php');
include_once('../mail/mail.php'); 

 	
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>siteadmin</title>
		<link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="../style.css" rel="stylesheet">
		<script src="../Bootstrap/js/respond.js"></script>
	</head>
	<body>
		<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>
		<?php
		$page_class='more';
		include_once('../headers/main-header.php');
		?>
		<div class="container">
			<nav class="sub-head navbar navbar-default">
				<div class="container-fluid">
					<ul class="nav navbar-nav">
						<li class="active"><a href="verification-email.php">Verification Email Corporate</a></li>
					</ul>
				</div>
			</nav>
			<div class="col-md-12">
				<form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-sm-4" for="cor_name">Select Corporate:</label>
							<div class="col-sm-8 no-left-pad">
								<select required class="form-control"  name="cor_name" id="cor_name" >
									<?php
									$res_cor = $fun_obj->get_all_corporates();
									foreach ($res_cor as $key1) {
									echo '<option value="'.$key1['company_id'].'">'.$key1['company_name'].'</option>';
									}
									echo '<option selected>'.$cor_name_v.'</option>';
									?>
								</select>
								<div id="loader" class="sp sp-circle loader"></div>
							</div>
						</div>
					</div>
				</form>
				<div class="col-md-10 col-md-offset-1">
					<form  method="post" action="send_email_sep.php">
						<div class="form-group">
							<label for="email">Email address:</label>
							<textarea class="form-control" id="textarea_to" rows="10" name="email" required></textarea>
						</div>
						<div class="form-group">
							<label for="email">Subject:</label>
							<textarea class="form-control" id="textarea1" rows="1" name='subject' required></textarea>
						</div>
						<div class="form-group">
							<label for="email">Limit for mails per button:</label>
							<input class="form-control" id="limit" rows="1" name='limit' value="1" required>
						</div>
						<button type="submit" class="btn btn-danger iread-btn" name ='send'>Send</button>
						<!-- <button type="button" id="preview_btn" class="btn btn-danger iread-btn iread-btn-white" data-toggle="modal" data-target="#modal_mail"  name ='preview'>Preview</button> -->
					</form>
					
				</div>
				<!-- Modal -->
				<div class="modal fade" id="modal_mail" role="dialog">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"></h4>
							</div>
							<div class="modal-body">
								<p></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div><hr><Hr>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}else{}
?> 
<script type="text/javascript">
$("#preview_btn").click(function() {
	 $('.modal-body').html(CKEDITOR.instances.body_email.getData());
	 var sub=$('textarea#exampleTextarea1').val();
	 $('.modal-title').html(sub);
 
});
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(10000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});
$("#cor_name").change(function() {
	$('.loader').show();
	 var cor_id = $.trim($('#cor_name').val());
	var html='';
  $.ajax({
    url: '<?php echo base_url; ?>/verification-email/ajax-get-users.php',
    method: "POST",
    data: {
      cor_id: cor_id
    },
    dataType: 'json',
	   }).success(function(res) {
		    //alert(res.email);
		    if(res=="false"){
		    	$('.loader').hide();
		    	$('#textarea_to').text(html);
		    	html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>No users are unverified/some error</div>';
                 $('body').append(html);
	             $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
	               $("#i-alert").alert('close');
	             });
		    }
		    else{
		    	for (var i = 0; i < res.length; i++) {
			  	 	html+= res[i].user_email+',';
				}
		   		$('#textarea_to').text(html);
		   		$('.loader').hide();
		    }
		   
  	});
});	

/*script for save comment*/
$(".save_comment").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     var textval=$(this).siblings('textarea').val();
     var order=$(this).siblings('#order_id_cmmnt').val();
     var html='';
     $.ajax({
            type: "POST",

            url: '<?php echo base_url; ?>/rent-request/ajax/comment_save_pickup.php',
            data: { textval : textval,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
              $('.loader').hide(); 
           
              html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
              $('body').append(html);
               $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                 $("#i-alert").alert('close');
               });
            }
     });
     return false; 
});
 
</script>
 
	</body>
</html>