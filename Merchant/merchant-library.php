 <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);
 

  if (isset($_POST['add'])) 
  {
      $Mer_Name=trim($_POST['Mer_Name']);
      $Mer_Isbn=$_POST['Mer_Isbn'];
      $Mer_Count=$_POST['Mer_Count'];
      $Mer_Price=$_POST['Mer_Price'];
     
      $ins= $fun_obj->add_book_merchant($Mer_Name,$Mer_Isbn,$Mer_Count,$Mer_Price);
      if($ins)
      {
       $i_msg=$ins;
      }   
      else
      {}
  }
  if (isset($_POST['update'])) 
  {
      $Mer_Id=trim($_POST['Merchant_Id']);
      $Mer_Isbn=$_POST['Mer_Isbn1'];
      $Mer_Count=$_POST['Mer_Count1'];
      $Mer_Price=$_POST['Mer_Price1'];
     
      $ins= $fun_obj->update_book_merchant($Mer_Id,$Mer_Isbn,$Mer_Count,$Mer_Price);
      if($ins)
      {
       $i_msg.="Merchant Library is updated with ISBN ".$Mer_Isbn."";
      }   
      else
      {}
  }
  if (isset($_POST['delete'])) 
  {
      $Mer_Id=trim($_POST['Merchant_Id']);
      $Mer_Isbn=$_POST['Mer_Isbn1'];
      $del= $fun_obj->delete_book_merchant($Mer_Id,$Mer_Isbn);
      if($del)
      {
       $i_msg.="ISBN ".$Mer_Isbn." is deleted from Merchant Library";
      }   
      else
      {}
  }
  if (isset($_POST['download_file'])){
    $file1 = '../merchant/merchant_library_details.csv';

    if (file_exists($file1)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file1).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file1));
        readfile($file1);
        exit;
    }
  }
    if (isset($_POST['upload_file'])){
       $Mer_Name=trim($_POST['Mer_Name1']);
        $res= $fun_obj->update_from_file_book_merchant($Mer_Name);
        $i_msg.=$res;
  }
?>
 
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <link rel="stylesheet" href="../Bootstrap/js/select2.min.css">
    <script src="../Bootstrap/js/respond.js"></script>
     <style>
       .select2-container{color:white !important;margin: 0px !important;}
     </style>
</head>
<body>
  <?php 
    $page_class='merchant';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a href="../merchant/add-merchant.php">Add Merchant</a></li>
        <li ><a href="../merchant/update-merchant.php">Update :wq</a></li>
        <li class="active"><a href="../merchant/merchant-library.php">Merchant Library</a></li>
      </ul>
    </div>
  </nav>

    <div class="col-md-6">
    	<h4  >Add New ISBN</h4><Br>	
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Name">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
  				<select required class="form-control"  name="Mer_Name" id="Mer_Name">
                   <?php 
                      $all_mer=$fun_obj->get_all_merchant();
                      foreach ($all_mer as $key) {
                          echo '<option >'.$key['merchant_name'].'</option>'; 
                      }
                    ?>
                 </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Isbn">ISBN:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control" name="Mer_Isbn" id="Mer_Isbn" placeholder="Enter ISBN" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Count">Count:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control" name="Mer_Count" id="Mer_Count" placeholder="Enter Count" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Price">Supply Price:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control" name="Mer_Price" id="Mer_Price" placeholder="Enter Supply Price" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="add" class="btn btn-default iread-btn">Add to Merchant</button>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="col-md-6">
    	<h4  >Update Existing ISBN</h4><Br>	
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label col-sm-4" for="Merchant_Id">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
  				<select required class="form-control"  name="Merchant_Id" id="Merchant_Id">
                   <?php 
                      $all_mer=$fun_obj->get_all_merchant();
                      foreach ($all_mer as $key) {
                          echo '<option value="'.$key['merchant_id'].'">'.$key['merchant_name'].'</option>'; 
                      }
                    ?>
                 </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Isbn1">ISBN:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <select class="form-control" name="Mer_Isbn1" id="Mer_Isbn1">
                    <option value="0">Select a ISBN</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Count1">Count:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control" name="Mer_Count1" id="Mer_Count1" placeholder="Enter Count" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Price1">Supply Price:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control" name="Mer_Price1" id="Mer_Price1" placeholder="Enter Supply Price" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="update" class="btn btn-default iread-btn">Update</button>
              </div>
              <div class=" col-sm-4  text-right">
                    <button type="submit" name="delete" class="btn btn-default iread-btn-white  ">Delete ISBN</button>
              </div>
            </div>
           </div>
      </form>
    </div>
     <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6"><hr><h4>Add From File</h4>
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Name1">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                 <select required class="form-control"  name="Mer_Name1" id="Mer_Name1">
                   <?php 
                      $all_mer1=$fun_obj->get_all_merchant();
                      foreach ($all_mer1 as $key1) {
                          echo '<option >'.$key1['merchant_name'].'</option>'; 
                      }
                    ?>
                 </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="file"> </label>
              <div class="col-sm-8 no-left-pad">
                <input type="file" class=" " id="file" name="file" placeholder="Upload file"><span> 
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="upload_file" class="btn btn-default iread-btn">Upload</button>
              </div>
              <div class=" col-sm-4  text-right">
                    <button type="submit" name="download_file" class="btn btn-default iread-btn-white update-btn">Download Sample</button>
              </div>
            </div>
           </div>
      </form>
    </div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
  </body>
  <script src="../Bootstrap/js/select2.min.js"></script>
  <script>
      
  </script>
<script type="text/javascript">
 var count=0;
	$("#Merchant_Id").change(function() {
		  var Merchant_Id = $.trim($('#Merchant_Id').val());
		  var html = '';
 
		  $.ajax({
		    url: '<?php echo base_url; ?>merchant/ajax-get-isbn.php',
		    method: "POST",
		    data: {
		      Merchant_Id: Merchant_Id
		    }
	  	   }).success(function(resp) {
			    var obj = $.parseJSON(resp);
			    $.each(obj, function() { 
			      html += '<option value ="' + this['ISBN'] + '" >' + this['ISBN'] + '</option>';
            count+=1;
			});

	    if (html != ''){
        var html1 = document.getElementById("Mer_Isbn1").innerHTML;
        html = html1 + html;
	      $('#Mer_Isbn1').html(html);
        $("#Mer_Isbn1").select2({
            placeholder: "Select a ISBN",
            allowClear: true
        });
        if(count==1){
           var Mer_Isbn1 = $.trim($('#Mer_Isbn1').val());
            var Mer_id2 = $.trim($('#Merchant_Id').val());
            var html2 = '';
            var html3 = '';
             
            $.ajax({
              url: '<?php echo base_url; ?>merchant/ajax-get-detail.php',
              method: "POST",
              data: {
                Mer_Isbn1: Mer_Isbn1,
                Mer_id2: Mer_id2
              },
              dataType : 'json',

               }).success(function(resp) {
                $('#Mer_Count1').val(resp.count); 
                $('#Mer_Price1').val(resp.supply_price); 

                return false;    
            });
          }
	    }
	    else{}
	       
	  });
    
	});	

	$("#Mer_Isbn1").change(function() {
		  var Mer_Isbn1 = $.trim($('#Mer_Isbn1').val());
		  var Mer_id2 = $.trim($('#Merchant_Id').val());
		  var html2 = '';
		  var html3 = '';
 			 
		  $.ajax({
		    url: '<?php echo base_url; ?>merchant/ajax-get-detail.php',
		    method: "POST",
		    data: {
		      Mer_Isbn1: Mer_Isbn1,
		      Mer_id2: Mer_id2
		    },
		    dataType : 'json',

	  	   }).success(function(resp) {
          $('#Mer_Count1').val(resp.count); 
			    $('#Mer_Price1').val(resp.supply_price); 

			    return false;    
	  });
	});


$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>