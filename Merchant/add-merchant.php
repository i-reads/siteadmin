<?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

  if (isset($_POST['submit'])) 
  {
      $Mer_Name=trim($_POST['Mer_Name']);
      $Mer_Address=$_POST['Mer_Address'];
      $Mer_Phone=$_POST['Mer_Phone'];
      $Mer_Time=$_POST['Mer_Time'];
      $Mer_Discount=$_POST['Mer_Discount'];
     
      $ins= $fun_obj->add_merchant($Mer_Name,$Mer_Address,$Mer_Phone,$Mer_Time,$Mer_Discount);
      if($ins)
      {
       $i_msg=$ins;
      }   
      else
      {}
  }
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
   
</head>
<body>
 <?php 
    $page_class='merchant';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../merchant/add-merchant.php">Add Merchant</a></li>
        <li ><a href="../merchant/update-merchant.php">Update Merchant</a></li>
        <li ><a href="../merchant/merchant-library.php">Merchant Library</a></li>
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Name">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" required class="form-control"  name="Mer_Name" id="Mer_Name" placeholder="Enter Merchant Name" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Address">Merchant Address:</label>
              <Div class="col-sm-8 no-left-pad">
                 <textarea class="form-control" rows="3" name="Mer_Address"  id="Mer_Address" placeholder="Enter Merchant Address"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Phone">Merchant Phone:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Mer_Phone" id="Mer_Phone" placeholder="Enter Merchant Phone">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Time">Procurement Time:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Mer_Time" id="Mer_Time" placeholder="Enter Procurement Time">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Mer_Discount">Merchant Discount:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Mer_Discount" id="Mer_Discount" placeholder="Enter Merchant Discount ">
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">Add Merchant</button>
              </div>
            </div>
      </form>
    </div>

</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>