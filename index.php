<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <script src="Bootstrap/js/respond.js"></script>

</head>

<body>
	<?php
		session_start();
		include_once('config/config.php');
		include_once('config/functions.php');
		$con = connect($config);
		$fun_obj = new ireads($con);
		$fun_obj->license_expired_count();
		if(isset($_POST['btnlogin'])){

			$username = $_POST['username'];
			$password = $_POST['password'];
			if(($username == 'admin')&&($password == 'akash@123')){
				$_SESSION['user'] = $username;
				header("Location: rent-request/rent-requests.php");
			}
			else
			{
				echo '<div align="center" style="color:red;"><h3>Account is Invalid</h3></div>';
			}
		}

	?>
	<div class="container">
		<div class="jumbotron">
			<form class="form-horizontal" role="form" method="POST">
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="email">Email:</label>
				    <div class="col-sm-6">
				      <input type="text" class="form-control" id="email" placeholder="Enter email" name="username" required>
				    </div>
				  </div>
			  <div class="form-group">
				    <label class="control-label col-sm-2" for="pwd">Password:</label>
				    <div class="col-sm-6"> 
				      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password" required>
				    </div>
			  </div>
			  
			  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-default" name="btnlogin">Submit</button>
				    </div>
			  </div>
			</form>
		</div>
	</div>
</body>
</html>
