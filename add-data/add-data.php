<?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);
  
  $ISBN13_v="";
  $Author_v="";
  $ProductForm_v="----Select----";
  $PublisherName_v="";
  $PublicationPlace_v="";
  $PublicationCountry_v="";
   $PublishingStatus_v="----Select----";
  $Price_v="";
  $TextLaunguage_v="----Select----";
  $ShortDescription_v="";
  $LongDescription_v="";
  $AboutAuthor_v="";
  $ISBN13_v="";
  $Title_v="";
  $Image_v="";
  $category_v ="";

  if (isset($_POST['submit'])) 
  {
      $Author=htmlspecialchars($_POST['Author'],ENT_QUOTES);$Author = str_replace("'", "\\'", $Author);
      $ProductForm=$_POST['ProductForm'];
      $PublisherName=htmlspecialchars($_POST['PublisherName'],ENT_QUOTES);$PublisherName = str_replace("'", "\\'", $PublisherName);
      $PublicationPlace=$_POST['PublicationPlace'];
      $PublicationCountry=$_POST['PublicationCountry'];
      $PublishingStatus=$_POST['PublishingStatus'];
      $Price=$_POST['Price'];
      $TextLaunguage=$_POST['TextLaunguage'];

      $ShortDescription   = htmlspecialchars($_POST['ShortDescription'], ENT_QUOTES);$ShortDescription = str_replace("'", "\\'", $ShortDescription);
      $LongDescription    = htmlspecialchars($_POST['LongDescription'],ENT_QUOTES);$LongDescription = str_replace("'", "\\'", $LongDescription);
      $AboutAuthor        = htmlspecialchars($_POST['AboutAuthor'], ENT_QUOTES);$AboutAuthor = str_replace("'", "\\'", $AboutAuthor);

      $ISBN13 = trim($_POST['ISBN13']);
      $Title = htmlspecialchars($_POST['Title'], ENT_QUOTES);$Title = str_replace("'", "\\'", $Title);
      $supper = $_POST['supper_cat'];
      $parent_cat = $_POST['parent_cat'];
      $p_cat = explode('|', $parent_cat);
      $p_id = $p_cat[0];
      $p_name = $p_cat[1];
      $cat_level1 = $_POST['cat_level1'];
       $cat_level_one = explode('|', $cat_level1);
       $cat_one_id = $cat_level_one[0];
       $cat_one_name = $cat_level_one[1];
      $cat_level2 = $_POST['cat_level2'];
      $cat_level_two = explode('|', $cat_level2);
      $cat_level_two_ID = $cat_level_two[0];
      $cat_level_two_Name = $cat_level_two[1];
      if(!empty($cat_level_two)){
        $cat_level_two_ID = $cat_level_two[0];
      $cat_level_two_Name = $cat_level_two[1];

      }
      else{

      $cat_level_two_ID = "";
      $cat_level_two_Name = "";
      }
      $save_cat_in_submaster = $p_name.'/'.$cat_one_name.'/'.$cat_level_two_Name;



      if(empty($ISBN13) || empty($Title) || empty($Author) || empty($Price)){
        $i_msg.="Please fill required fields";
      }
      else{
        $img_flag=($fun_obj->upload_img());
        $img_flag5=$img_flag[0];
        $i_msg=$img_flag[1];
        $ins= $fun_obj->add_to_sub_mastersheet($ISBN13,$Title,$Author,$ProductForm,$PublisherName,$PublicationPlace,$PublicationCountry,$PublishingStatus,$Price,$TextLaunguage,$ShortDescription,$LongDescription,$AboutAuthor,$img_flag5, $save_cat_in_submaster,$p_name);
        $up = $fun_obj->add_category_in_book_category_mapping_new($ISBN13,$supper,$p_id,$cat_one_id,$cat_level_two_ID);

        if($ins && $up)
        {
          $i_msg.= "Book Successfully Added";
        }   
        else
        {
          $i_msg.= "ISBN13 ".$ISBN13." already exists";
        }
      }

  }

  if (isset($_POST['check_book'])) {
      $ISBN13 = $_POST['ISBN13'];
      $book_detail = $fun_obj->get_isbn_submastersheet($ISBN13);
      if(!empty($book_detail)){
        //print_r($book_detail);exit;
         foreach ($book_detail as $key){
        $ISBN13_v=$key['ISBN13'];
        $Title_v=$key['title'];
        $category_v=$key['category'];
        $Author_v=$key['contributor_name1'];
        $ProductForm_v=$key['product_form'];
        $PublisherName_v=$key['publisher_name'];
        $PublicationPlace_v=$key['publication_place'];
        $PublicationCountry_v=$key['publication_country'];
        $PublishingStatus_v=$key['publishing_status'];
        $Price_v=$key['price'];
        $TextLaunguage_v=$key['text_language'];
        $ShortDescription_v=$key['short_desc'];
        $LongDescription_v=$key['long_desc'];
        $AboutAuthor_v=$key['author_bio'];
        
        if($key['image']==0){
          $Image_v="Image is not available";
        }
        else{
          $Image_v="Image was Uploaded(uplaod image to replace)";
        }
      }
      }
      else{
        $i_msg.= "ISBN13 ".$ISBN13." does  not exists";
      }
  }
  
  if (isset($_POST['update_book'])) {

      foreach($_POST as $key=>$value)
      {
        $data[$key] = $fun_obj->quoteIdent($value);
      }
    
      // $Author             = trim(htmlentities(strip_tags($_POST['Author'])));
      // $ProductForm        = trim(htmlentities(strip_tags($_POST['ProductForm'])));
      // $PublisherName      = trim(htmlentities(strip_tags($_POST['PublisherName'])));
      // $PublicationPlace   = trim(htmlentities(strip_tags($_POST['PublicationPlace'])));
      // $PublicationCountry = trim(htmlentities(strip_tags($_POST['PublicationCountry'])));
      // $PublishingStatus   = trim(htmlentities(strip_tags($_POST['PublishingStatus'])));
      // $Price              = trim(htmlentities(strip_tags($_POST['Price'])));
      // $TextLaunguage      = trim(htmlentities(strip_tags($_POST['TextLaunguage'])));
      // $ShortDescription   = trim(htmlentities(strip_tags($_POST['ShortDescription'])));
      // $LongDescription    = trim(htmlentities(strip_tags($_POST['LongDescription'])));
      // $AboutAuthor        = trim(htmlentities(strip_tags($_POST['AboutAuthor'])));
      // $ISBN13             = trim(htmlentities(strip_tags($_POST['ISBN13'])));
      // $Title              = trim(htmlentities(strip_tags($_POST['Title'])));

      $Author             = htmlspecialchars($_POST['Author'],ENT_QUOTES);$Author = str_replace("'", "\\'", $Author);
      $ProductForm        = $_POST['ProductForm'];
      $PublisherName      = htmlspecialchars($_POST['PublisherName'],ENT_QUOTES);$PublisherName = str_replace("'", "\\'", $PublisherName);
      $PublicationPlace   = $_POST['PublicationPlace'];
      $PublicationCountry = $_POST['PublicationCountry'];
      $PublishingStatus   = $_POST['PublishingStatus'];
      $Price              = $_POST['Price'];
      $TextLaunguage      = $_POST['TextLaunguage'];
      $ShortDescription   = htmlspecialchars($_POST['ShortDescription'], ENT_QUOTES);$ShortDescription = str_replace("'", "\\'", $ShortDescription);
      $LongDescription    = htmlspecialchars($_POST['LongDescription'],ENT_QUOTES);$LongDescription = str_replace("'", "\\'", $LongDescription);
      $AboutAuthor        = htmlspecialchars($_POST['AboutAuthor'], ENT_QUOTES);$AboutAuthor = str_replace("'", "\\'", $AboutAuthor);
      
      $ISBN13             = $_POST['ISBN13'];
      $Title              = htmlspecialchars($_POST['Title'],ENT_QUOTES);$Title = str_replace("'", "\\'", $Title);


      $supper             = $_POST['supper_cat'];
      $parent_cat         = $_POST['parent_cat'];
      $p_cat              = explode('|', $parent_cat);
      if(!empty($_POST['parent_cat']))
      {
        $p_id               = $p_cat[0];
        $p_name             = $p_cat[1];
      }
      else
      {
        $p_id               = '';
        $p_name             = '';
      }
      
      $cat_level1         = $_POST['cat_level1'];
      $cat_level_one     = explode('|', $cat_level1);
      if(!empty($cat_level1))
      {
        $cat_one_id        = $cat_level_one[0];
        $cat_one_name      = $cat_level_one[1];
      }
      else
      {
        $cat_one_id        = '';
        $cat_one_name      = '';
      }
      
      $cat_level2         = $_POST['cat_level2'];
      $cat_level_two      = explode('|', $cat_level2);
      if(!empty($_POST['cat_level2']))
      {
        $cat_level_two_ID = $cat_level_two[0];
        $cat_level_two_Name = $cat_level_two[1];
      }
      else
      {
        $cat_level_two_ID = "";
        $cat_level_two_Name = "";
      }
      

      $save_cat_in_submaster = $p_name.'/'.$cat_one_name.'/'.$cat_level_two_Name;

      $img_flag5="";
      if(isset($_FILES["Image"]["name"]) && $_FILES["Image"]["name"]!=""){
         
        $file_del_img = $ISBN13.".*" ;
        array_map( "unlink", glob( $file_del_img) );
       $img_flag=$fun_obj->upload_img();
         $img_flag5=$img_flag[0];
        $i_msg.=$img_flag[1];
      }
      //echo $ISBN13." , ".$Title." , ".$Author." , ".$ProductForm." , ".$PublisherName." , ".$PublicationPlace." , ".$PublicationCountry." , ".$PublishingStatus." , ".$Price." , ".$TextLaunguage." , ".$ShortDescription." , ".$LongDescription." , ".$AboutAuthor." , ".$img_flag5." , ".$save_cat_in_submaster." , ".$p_name;die;

      
      $upd= $fun_obj->update_to_sub_mastersheet($ISBN13,$Title,$Author,$ProductForm,$PublisherName,$PublicationPlace,$PublicationCountry,$PublishingStatus,$Price,$TextLaunguage,$ShortDescription,$LongDescription,$AboutAuthor,$img_flag5,$save_cat_in_submaster,$p_name);
        $map_up = $fun_obj->update_book_category_mapping_new($ISBN13, $supper, $p_id, $cat_one_id, $cat_level_two_ID);

      if($upd)
      {
        $i_msg.= "Book Details Updated";
      }   
      else
      {
        $i_msg.= "ISBN13 ".$ISBN13." does not exists, Please Add new book";
      }
       
  }
  $supper_cat_data = $fun_obj->get_supper_cat();
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='add-data';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../add-data/add-data.php">Add to Mastersheet</a></li>
        <li><a href="../add-data/add-book-library.php">Add to Library</a></li>
           
        
      </ul>
    </div>
  </nav>

    <div class="col-md-12" id="final_div_id">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="ISBN13">ISBN13:</label>
              <div class="col-sm-5 no-left-pad input-group req-fi">
                <input type="text" class="form-control" required name="ISBN13" id="ISBN13" value="<?php echo $ISBN13_v; ?>" placeholder="Enter ISBN13" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
              <div class="col-sm-offset-0 col-sm-3 text-left no-left-pad">
                <button type="submit" name="check_book" class="btn btn-default iread-btn-white isbn-btn">Check Book</button>
                <?php 
                if(!empty($category_v))
                {
                 echo $category_v;
                ?>
                <input type="hidden" name="category_v" id="category_v" value="<?php echo $category_v; ?>">
                <?php
                } 
                ?>


              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="Title">Title:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control"  name="Title" id="Title" value="<?php echo $Title_v; ?>" placeholder="Enter Title" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
          
            <div class="form-group">
              <label class="control-label col-sm-4" for="Author">Author:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Author" id="Author" value="<?php echo $Author_v; ?>" placeholder="Enter Author" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ProductForm">Product Form:</label>
              <div class="col-sm-8 no-left-pad">
                  <select class="form-control" id="ProductForm" value="<?php echo $ProductForm_v; ?>" name="ProductForm" placeholder="Enter Product Form">
                    <?php echo '<option selected>'.$ProductForm_v.'</option>' ?>
                    <option>Paperback</option>
                    <option>Hard Cover</option>
                    <option>Mass Paperback</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="PublisherName">Publisher Name:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="PublisherName" value="<?php echo $PublisherName_v; ?>" id="PublisherName" placeholder="Enter Publisher Name">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="PublicationPlace">Publication Place:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="PublicationPlace" value="<?php echo $PublicationPlace_v; ?>" id="PublicationPlace" placeholder="Enter Publication Place">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="PublicationCountry">Publication Country:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="PublicationCountry" value="<?php echo $PublicationCountry_v; ?>" id="PublicationCountry" placeholder="Enter Publication Country">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="PublishingStatus">Publishing Status:</label>
              <div class="col-sm-8 no-left-pad">
                  <select class="form-control" name="PublishingStatus" value="<?php echo $PublishingStatus_v; ?>" id="PublishingStatus">
                    <?php echo '<option selected>'.$PublishingStatus_v.'</option>' ?>
                    <option>Active</option>
                    <option>Inactive</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Price">Price:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Price" value="<?php echo $Price_v; ?>" id="Price" placeholder="Enter Price" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="Category">Super Category:</label>
              <div class="col-sm-8 no-left-pad">
                  <select class="form-control" name="supper_cat"  id="supper_cat">
                  <?php 
                    
                   foreach ($supper_cat_data as $supper) {
                     # code...
                  ?>
                    <option value="<?php echo $supper['super_cat_id']; ?>"><?php echo $supper['category']; ?></option>
                    
                    <?php }?>
                  </select>  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-4" for="Category">Parent Category:</label>
              <div class="col-sm-8 no-left-pad">
                 <select class="form-control" name="parent_cat"  id="parent_cat">
                    <option></option>
                  </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-4" for="Category">Cat1 Category:</label>
              <div class="col-sm-8 no-left-pad">
                <select class="form-control" name="cat_level1"  id="cat_level1">
                    <option></option>
                    
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-4" for="Category">Cat2 Category:</label>
              <div class="col-sm-8 no-left-pad">
                 <select class="form-control" name="cat_level2"  id="cat_level2">
                    <option></option>
                  
                 </select>
              </div>
            </div>
           
            <div class="form-group">
              <label class="control-label col-sm-4" for="TextLaunguage">Text Launguage:</label>
              <div class="col-sm-8 no-left-pad">
                  <select class="form-control" name="TextLaunguage"  id="TextLaunguage">
                    <?php echo '<option selected>'.$TextLaunguage_v.'</option>' ?>
                    <option>English</option>
                    <option>Hindi</option>
                  </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-sm-4" for="Image">Image:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="file" class=" " id="Image" name="Image" placeholder="Upload Image"><span><?php echo $Image_v; ?></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ShortDescription">Short Description:</label>
              <Div class="col-sm-8 no-left-pad">
                 <textarea class="form-control" rows="3" name="ShortDescription"  id="ShortDescription" placeholder="Enter Short Description"><?php echo $ShortDescription_v; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="LongDescription">Long Description:</label>
              <div class="col-sm-8 no-left-pad">
                <textarea class="form-control" rows="6" name="LongDescription" id="LongDescription" placeholder="Enter Long Description"><?php echo $LongDescription_v; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="AboutAuthor">About Author:</label>
              <div class="col-sm-8 no-left-pad">
                <textarea class="form-control" rows="3" name="AboutAuthor" id="AboutAuthor" placeholder="Enter About Author"><?php echo $AboutAuthor_v; ?></textarea>
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">Add New Book</button>
              </div>
              <div class=" col-sm-4  text-right">
                    <button type="submit" name="update_book" class="btn btn-default iread-btn-white update-btn">Update Existing Book</button>
                  </div>
            </div>
      </form>
    </div>

</div>
<?php
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});

  $('#final_div_id').on("change", '#supper_cat', function() {
    $("#parent_cat").empty().append("<option value=''>Select Parent Category</option>");
    var supper_cat = $('#supper_cat').val();
     $.ajax({
        url: '<?php echo base_url; ?>add-data/ajax_cat_record.php',
        method: 'POST',
        data: {supper_cat:supper_cat},
        dataType: 'json',
     
  }).success(function(response) { 
        
        $("#parent_cat").empty().append(response.options); 
    });
});

$('#final_div_id').on("change", '#parent_cat', function() {
    $("#cat_level1").empty().append("<option value=''>Select Cat Level1 Category</option>");
    var parent_cat = $('#parent_cat').val();
    var parent_cat = parent_cat.split('|');
    var parent_cat = parent_cat[0];
   // alert(parent_cat);
     $.ajax({
        url: '<?php echo base_url; ?>add-data/ajax_cat_record.php',
        method: 'POST',
        data: {parent_cat:parent_cat},
        dataType: 'json',
     
  }).success(function(response) { 
        
        $("#cat_level1").empty().append(response.options); 
    });
});


$('#final_div_id').on("change", '#cat_level1', function() {
    $("#cat_level2").empty().append("<option value=''>Select Cat Level1 Category</option>");
    var cat_level1 = $('#cat_level1').val();
    //alert(cat_level1);
    var cat_level1 = cat_level1.split('|');
    var cat_level1 = cat_level1[0];
    //alert(cat_level1);
     $.ajax({
        url: '<?php echo base_url; ?>add-data/ajax_cat_record.php',
        method: 'POST',
        data: {cat_level1:cat_level1},
        dataType: 'json',
     
  }).success(function(response) { 
        
        $("#cat_level2").empty().append(response.options); 
    });
});




</script>
</html>