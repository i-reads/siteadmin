<?php
 
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

$LibraryBookID_v=$ISBN13_v=$ShelfLocation_v=$InSub_v=$Title_v=$mer_v="";
$Price_v=$discount_v=$ProcPrice_v=1;
$Circulation_v=$CopyDiscount_v=0;
$Status_v=1;
$InventoryID_v=101;

  if (isset($_POST['submit'])) 
  {
      $LibraryBookID=trim($_POST['LibraryBookID']);
      $ISBN13=$_POST['ISBN13'];
      $InventoryID=$_POST['InventoryID'];
      $ShelfLocation=$_POST['ShelfLocation'];
      $ProcPrice=$_POST['ProcPrice'];
      $Circulation=$_POST['Circulation'];
      $discount=$_POST['discount'];
      $Status=$_POST['Status'];
      $Mer_Name=$_POST['Mer_Name'];
      $mrp=$_POST['mrp'];
     $Title =$_POST['Title'];

      if(empty($ISBN13) || empty($LibraryBookID) || empty($ShelfLocation) || empty($discount) || empty($mrp) || empty($Mer_Name)){
        $i_msg.="Please fill required fields";
      }
      else{
        $ins= $fun_obj->add_to_booklibrary($LibraryBookID,$ISBN13,$InventoryID,$ShelfLocation,$ProcPrice,$Circulation,0,$Status,$Title);
        if($ins)
        {
          $ins1= $fun_obj->add_to_procurement_details($LibraryBookID,$ISBN13,$Mer_Name,$mrp,$discount,$ProcPrice);
          $i_msg=$ins;
        }   
        else
        {}
      }

  }

  if (isset($_POST['check_book'])) {
      $LibraryBookID = trim($_POST['LibraryBookID']);
      $book_detail = $fun_obj->get_book_id_booklibrary($LibraryBookID);
      $book_proc = $fun_obj->check_if_book_in_proc($LibraryBookID);

      if(!empty($book_detail)){
         foreach ($book_detail as $key){
        $ISBN13_v=$key['ISBN13'];
        $Title_v=$key['title'];
        $InventoryID_v=$key['inv_id'];
        $ShelfLocation_v=$key['shelf_location'];
        $Price_v=$book_proc[0]['Mrp'];
        $mer_v=$book_proc[0]['source'];
        $discount_v=$book_proc[0]['discount'];
        $ProcPrice_v=$book_proc[0]['proc_price'];
        $Circulation_v=$key['circulation'];
        //$CopyDiscount_v=$key['copy_discount'];
        $Status_v=$key['status'];
        $LibraryBookID_v=$key['book_id'];
        }
        if(($book_detail[0]['status'])==1){
            $i_msg.= "Book is present in the Library";
        }
        elseif(($book_detail[0]['status'])==2){
            $i_msg.= "Book is in circulation";
        }
        elseif(($book_detail[0]['status'])==-1){
            $i_msg.= "Book is lost";
        }
        elseif(($book_detail[0]['status'])==0){
            $i_msg.= "Book is damaged";
        }
        else{
          $i_msg.= "Oops,Some error";
        }
      }
      else{
        $i_msg.= "Library Book ID ".$LibraryBookID." doest not exists";
      }
  }
  if (isset($_POST['update_book'])) {
     $LibraryBookID=trim($_POST['LibraryBookID']);
      $ISBN13=$_POST['ISBN13'];
      $InventoryID=$_POST['InventoryID'];
      $ShelfLocation=$_POST['ShelfLocation'];
      $ProcPrice=$_POST['ProcPrice'];
      $Circulation=$_POST['Circulation'];
      $discount=$_POST['discount'];
      $Status=$_POST['Status'];
      $Mer_Name=$_POST['Mer_Name'];
      $mrp=$_POST['mrp'];
     $Title =$_POST['Title'];
      $upd= $fun_obj->update_to_booklibrary( $LibraryBookID,$ISBN13,$InventoryID,$ShelfLocation,$ProcPrice,$Circulation,0,$Status,$Title); 
      if($upd)
      {
        $upd1= $fun_obj->update_to_procurement_details($LibraryBookID,$ISBN13,$Mer_Name,$mrp,$discount,$ProcPrice);
        $i_msg.= "Book Details are Updated";
      }   
      else
      {
        $i_msg.= "Library Book ID ".$LibraryBookID." does not exists, Please Add new book";
      }
       
  }
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='add-data';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a href="../add-data/add-data.php">Add to Mastersheet</a></li>
        <li class="active"><a href="../add-data/add-book-library.php">Add to Library</a></li>
         
         
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="LibraryBookID">Library Book ID:</label>
              <div class="col-sm-5 no-left-pad input-group req-fi">
                <input type="text" class="form-control" required name="LibraryBookID" id="LibraryBookID" value="<?php echo $LibraryBookID_v; ?>" placeholder="Enter Library Book ID" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
              <div class="col-sm-offset-0 col-sm-3 text-left no-left-pad">
                <button type="submit" name="check_book" class="btn btn-default iread-btn-white isbn-btn">Check Book</button>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ISBN13">ISBN13:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control"  name="ISBN13" id="ISBN13" value="<?php echo $ISBN13_v; ?>" placeholder="Enter ISBN13" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
          
            <div class="form-group">
              <label class="control-label col-sm-4" for="InventoryID">Inventory ID:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="InventoryID" id="InventoryID" value="<?php echo $InventoryID_v; ?>" placeholder="Enter Inventory ID" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Title">Title:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Title" value="<?php echo $Title_v; ?>" id="Title" placeholder="Enter Title">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ShelfLocation">Shelf Location:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="ShelfLocation" id="ShelfLocation" value="<?php echo $ShelfLocation_v; ?>" placeholder="Enter Shelf Location" aria-describedby="basic-addon2">
                 <span class="input-group-addon" id="basic-addon2">*</span>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-sm-4" for="Circulation">Circulation:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="Circulation" value="<?php echo $Circulation_v; ?>" id="Circulation" placeholder="Enter Circulation">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 " for="Status">Status:</label>
              <div class="col-sm-8 no-left-pad input-group req-fi">
                <input type="text" class="form-control" name="Status" value="<?php echo $Status_v; ?>" id="Status" placeholder="Enter Status" >
              </div>
            </div>
            <!-- <div class="form-group">
              <label class="control-label col-sm-4" for="CopyDiscount">Copy Discount:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="CopyDiscount" value="<?php echo $CopyDiscount_v; ?>" id="CopyDiscount" placeholder="Enter Copy Discount">
              </div>
            </div> -->
            
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="Mer_Name">Merchant Name:</label>
              <div class="col-sm-8 no-left-pad">
                  <select required class="form-control"  name="Mer_Name" id="Mer_Name"  onchange="javascript: this.form.submit();$('#loader').show();"  >
                   <?php 
                      $all_mer=$fun_obj->get_all_merchant();
                      foreach ($all_mer as $key) {
                          echo '<option value="'.$key['merchant_id'].'">'.$key['merchant_name'].'</option>'; 
                      }
                       echo '<option selected>'.$mer_v.'</option>'; 
                    ?>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="mrp">MRP:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="mrp" id="mrp" placeholder="Enter MRP" value="<?php echo $Price_v; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="discount">Merchant Discount<br>(In Rupees):</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter Merchant Discount" value="<?php echo $discount_v; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="ProcPrice">Proc Price:</label>
              <div class="col-sm-8 no-left-pad">
                <input type="text" class="form-control" name="ProcPrice" value="<?php echo $ProcPrice_v; ?>" id="ProcPrice" placeholder="Enter Proc Price">
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-4 col-sm-4 text-left no-left-pad">
                <button type="submit" name="submit" class="btn btn-default iread-btn">Add New Book</button>
              </div>
              <div class=" col-sm-4  text-right">
                    <button type="submit" name="update_book" class="btn btn-default iread-btn-white update-btn">Update Existing Book</button>
              </div>
            </div>
          </div>
      </form>
    </div>

</div>
<?php
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
$(document).ready(function() {
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>