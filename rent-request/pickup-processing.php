     <?php

include_once('../config/config.php');

include_once('../config/functions.php');


$con = connect($config);
$fun_obj = new ireads($con);
if(isset($_POST['send'])){
  $res1 = $fun_obj->update_order_status_7($_POST['order_id']);
  if($res1){
   $i_msg.='Order '.$_POST['order_id'].' is moved to sent-to-fedex';
   // header('Location: sent-to-fedex.php');
  }
  else{
    echo "some error";
  } 
}
include_once('../rent-request/pagination2.php');
$Order = $fun_obj->get_bookshelf_order_6($start_from,$num_rec_per_page);
 


?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
</head>

<body>
<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>

	 <?php 
    $page_class='rent-requests';  
    include_once('../headers/main-header.php');
  ?>
  <div class="container">
		<nav class="sub-head navbar navbar-default">
  <div class="container-fluid">
 
    <ul class="nav navbar-nav">
      <li  ><a href="#">All</a></li>
      <li class="active"><a href="#">Retail</a></li>
      <li><a href="../corporate/rent-request.php">Corporate 1</a></li>
      <li><a href="#">Corporate 2</a></li> 
      <li><a href="#">Corporate 3</a></li> 
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">More
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Page 1-1</a></li>
          <li><a href="#">Page 1-2</a></li>
          <li><a href="#">Page 1-3</a></li></ul> </li>
    </ul>
  </div>
</nav> 
 
    <ul id="rent-index" class="nav nav-pills">
      <li><a href="#">Stats</a></li>
      <li><a href="deleted.php">Deleted Requests</a></li>
      <li><a href="incomplete.php">Incomplete Requests</a></li> 
      <li ><a href="rent-requests.php">New Delivery Requests</a></li> 
      <li ><a href="delivery-processing.php">Delivery Processing</a></li>
      <li ><a href="dispatched.php">Dispatched</a></li> 
      <li><a href="delivered.php">Delivered</a></li> 
      <li ><a href="new-pickup.php">New Pickup Requests</a></li>
      <li class="active"><a href="pickup-processing.php">Pickup Processing</a></li> 
      <li><a href="sent-to-fedex.php">Sent to Fedex</a></li> 
      <li><a href="returned.php">Returned</a></li>
    </ul>
 
<div class="margintop">
  <table class="table table-bordered table-condensed font-new-del">
    <thead>
      <tr>
      	 
        <th>Parent Order</th>
        <th>Order id</th>
        <th>User</th>
        <th>Order Date</th>
        <th>Pickup Date</th>
        <th>Issue Date</th>
        <th>Pickup Address</th>
        <th>Price Details</th>
        <th>Sub-Order Price Details</th>
        <th>Payment Mode and Type</th>
        <th>Book Details</th>
        <th>Assigned Book id</th>
        <th>Carrier Details</th>
        <th>Comment</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
   <?php 
   if($Order) {
   foreach ($Order as $key) {
     $check_pincode = $fun_obj->check_pincode_pickingo($key['pincode']);

   ?>
      <tr>
      	 
        <td><?php echo $key['p_order_id']; ?></td>
        <td><?php echo $key['bookshelf_order_id']; ?> <input type="checkbox" name="sub_order[]" value="<?php echo $key['bookshelf_order_id']; ?>">
            <?php
             if($check_pincode){
              echo "Pickingo";
             }
            ?>
        </td>
        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
        <td><p><?php echo $key['order_date']; ?></p></td>

        <td><p><?php echo $key['new_pickup']; ?></p></td>

        <td><p><?php echo $key['issue_date']; ?></p></td>
        <td><p><?php echo $key['fullname']; ?></p><p><?php echo $key['address_line1']; ?>,</p><p><?php echo $key['address_line2']; ?>, <?php echo $key['city']; ?></p><p><p><?php echo $key['state'] ?> <span><?php echo $key['pincode']; ?></span></p><p id="p7"><?php echo $key['phone']; ?></p></td>
        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
        <td><?php echo $key['unique_book_id']; ?></a>
 
          </td>
          <td>
            <?php
              if($key['carrier'] || $key['d_track_id']){
                 echo "Carrier:".$key['carrier'];
                 echo "<br>Track Id:".$key['d_track_id']."<br>";
                 ?>
                 <br>
                <a  class="pointer-a edit_carrier  ">Edit Carrier</a><br>
                
                
                 <form class="form" role="form" style="display:none;">
                    <div class="form-group carrier-select-form">
                      <label for="sel1">Select Carrier:</label>
                      <select class="form-control" id="sel1">
                      <?php
                       if($check_pincode){
                        ?>
                        <option>Pickingo</option>
                      <?php
                       }
                      ?>
                        <option>FedEx</option>
                        <option>Pickrr</option>
                        <option>GoJavas</option>
                        <option>Delhivery</option>
                        <option>Speed Post</option>
                        <option>Self-Logistics</option>
                      </select>
                    </div>
                    <input type="text" name="tracking_id" id="tracking_id" class="track_input" placeholder="Enter Tracking ID">
                   
                    <input type="hidden" id="parent" value="<?php echo $key['p_order_id'];  ?>">
                    <input type="button" value="Process" name="process" class="btn iread-btn iread-btn-white small-process-btn">
                  </form> 
                 <?php
              }
              else{
              ?>
                <form class="form" role="form">
                  <div class="form-group carrier-select-form">
                    <label for="sel1">Select Carrier:</label>
                    <select class="form-control" id="sel1">
                      <?php
                       if($check_pincode){
                        ?>
                        <option>Pickingo</option>
                      <?php
                       }
                      ?>
                      <option>FedEx</option>
                      <option>Pickrr</option>
                      <option>GoJavas</option>
                      <option>Delhivery</option>
                      <option>Speed Post</option>
                      <option>Self-Logistics</option>
                    </select>
                  </div>
                  <input type="text" name="tracking_id" id="tracking_id" class="track_input" placeholder="Enter Tracking ID">
                   
                  <input type="hidden" id="parent" value="<?php echo $key['p_order_id']; ?>">
              
                  <input type="button" value="Process" name="process" class="btn iread-btn iread-btn-white small-process-btn">
                </form> 
              <?php 
              }?>
        </td>
        <td>
	        <div class="form-group">
               <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['pickup_comments']; ?></textarea>
                <input type="hidden" id="order_id_cmmnt" value="<?php echo $key['bookshelf_order_id']; ?>">
                <a class="btn iread-btn iread-btn-white small-save-btn save_comment">Save</a> 
                <div id="loader1" class="sp sp-circle loader"></div>
            </div>    
		    </td>
        <td> 
        	<form  method="POST" role="form" class="margintop">
                <input  class="btn btn-danger iread-btn action-btn-2 fedex" name="fedex" value="Fedex Pickup">
               <?php
                 if($check_pincode){
                  ?>
                   <input type="submit" class="btn btn-danger iread-btn action-btn-2 margintop pickingo" name="pickingo" value="Pickingo Pickup">
               <?php   
                 }
               ?>
               
        	</form>
          <form action="" method="POST" role="form" class="margintop">
              <input type="hidden" value="<?php echo $key['bookshelf_order_id']; ?>" name="order_id">
          
           <?php 
              if($key['carrier']){ 
          ?>
            <div class="form-group">
              <input type="submit" class="btn btn-danger iread-btn action-btn-3 " name="send" value="Send to Fedex">
            </div>
            <?php
              }?>
            
          </form>
        </td>
      </tr>

      <?php } } ?>
      	
      
    </tbody>
  </table>

</div>
	</div>
  <?php
/*for pagination*/
$total=$fun_obj->get_bookshelf_order_2_page(6);
include_once('../rent-request/pagination.php');
?>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(8000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});
/*script for  fedex sheet*/
$(".pickingo").click(function() {
    $('.overlay1').show();
       var selected_orders = new Array();
      var n = $("input[type=checkbox]:checked").length;
      if (n > 0){
          $("input[type=checkbox]:checked").each(function(){
              selected_orders.push($(this).val());
          });}
 
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/generate_sheet_pickingo.php',
            data: { 
                    selected_orders : selected_orders
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
/*script for  fedex sheet*/
$(".fedex").click(function() {
    $('.overlay1').show();
       var selected_orders = new Array();
      var n = $("input[type=checkbox]:checked").length;
      if (n > 0){
          $("input[type=checkbox]:checked").each(function(){
              selected_orders.push($(this).val());
          });}
 
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/generate_sheet.php',
            data: { 
                    selected_orders : selected_orders
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
 
/*script for  save carrier details and process button*/
$(".small-process-btn").click(function() {
    $('.overlay1').show();
     var carrier=$(this).siblings('.carrier-select-form').children('select').val();
     var track_id=$(this).siblings('#tracking_id').val();
     var parent=$(this).siblings('#parent').val();

     var selected_orders = new Array();
        var n = $("input[type=checkbox]:checked").length;
        if (n > 0){
            $("input[type=checkbox]:checked").each(function(){
                selected_orders.push($(this).val());
            });}
          //  alert(selected_orders);
    console.log(selected_orders);
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/carrier_save_pickup.php',
            data: { carrier : carrier,
                    track_id : track_id,
                    parent : parent,
                    selected_orders : selected_orders
                  },
            dataType: 'json',
            success: function(res){   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status",res);
                      window.location.reload();
                   }
            }
     });
     return false; 
});
 
/*script for save comment*/
$(".save_comment").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     var textval=$(this).siblings('textarea').val();
     var order=$(this).siblings('#order_id_cmmnt').val();
     var html='';
     $.ajax({
            type: "POST",

            url: '<?php echo base_url; ?>/rent-request/ajax/comment_save_pickup.php',
            data: { textval : textval,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
              $('.loader').hide(); 
           
              html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
              $('body').append(html);
               $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                 $("#i-alert").alert('close');
               });
            }
     });
     return false; 
});
 
</script>
<script type="text/javascript">
  $('.edit_carrier').on('click',function(event) {
    $(this).siblings('form').toggle('fast'); 
  });
</script>
	</body>
</html>