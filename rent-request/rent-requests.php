<?php
include_once('../config/config.php');
include_once('../config/functions.php');
include_once('../mail/mail.php');
include_once('cancel/cancel.php');

global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);


if(isset($_POST['process'])){
    $res1 = $fun_obj->update_order_status_2($_POST['order_id'],$_POST['user_id']);
    if(!empty($res1)){
        // SMS script
        $msg= 'Your order with Order ID: '.$_POST['order_id'].' is being processed.';       
        // to replace the space in message with  '%20'
        $str = trim(str_replace(' ', '%20', $msg));
        $user_id = 'IndiaReads';
        $pass= 'ireads@987'; 
        $sender_id = 'INDREA';   
        $mob_no = $_POST['phone'];       
        $priority = 'ndnd';
        $type = 'normal';
        $url="http://bhashsms.com/api/sendmsg.php?user=".$user_id."&pass=".$pass."&sender=".$sender_id."&phone=".$mob_no."&text=".$str."&priority=".$priority."&stype=".$type;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_exec($ch);
        curl_close($ch);
        // header('Location:deleted.php');
       $i_msg.='Order '.$_POST['order_id'].' is moved to delivery-processing';
       // header('Location:delivery-processing.php');
    }
    else{
        echo "not updated";
    }
}
 
if(isset($_POST['delete'])){
  
  $res1 = $fun_obj->update_order_status_1del($_POST['order_id'],$_POST['book_id'],$_POST['user_id'],$_POST['ISBN13'],$_POST['buy']);

  
  if(!empty($res1)){
    $i_msg.="Order ".$_POST['order_id']." is cancelled,";
    $can_obj = new cancel();
    $i_msg.=$can_obj->cancel_retail($_POST['order_id']);

    $result = mailer('Deleted',$_POST['order_id'],$fun_obj);
    $i_msg .= $result;
    // SMS script
    $msg= 'Your order with Order ID: '.$_POST['order_id'].' has been deleted.';       
    // to replace the space in message with  '%20'
    $str = trim(str_replace(' ', '%20', $msg));
    $user_id = 'IndiaReads';
    $pass= 'ireads@987'; 
    $sender_id = 'INDREA';         
    $mob_no = $_POST['phone'];       
    $priority = 'ndnd';
    $type = 'normal';
    $url="http://bhashsms.com/api/sendmsg.php?user=".$user_id."&pass=".$pass."&sender=".$sender_id."&phone=".$mob_no."&text=".$str."&priority=".$priority."&stype=".$type;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_exec($ch);
    curl_close($ch);
  }
  else{
    $i_msg.="not cancelled";
  }
}
include_once('../rent-request/pagination2.php');
$Order= $fun_obj->get_bookshelf_order_1($start_from,$num_rec_per_page);

?>
  


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
  <!--   // <script src="../Bootstrap/js/respond.js"></script> -->
</head>

<body>
<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>
	<!-- javascript-->
	

	<!-- Body -->
  
  <?php 
    $page_class='rent-requests';  
    include_once('../headers/main-header.php');
  ?>
  <div class="container">
		<nav class="sub-head navbar navbar-default">
  <div class="container-fluid">
 
    <ul class="nav navbar-nav">
      <!-- <li  ><a href="#">All</a></li> -->
      <li class="active"><a href="rent-requests.php">Retail</a></li>
      <li><a href="../corporate/rent-requests.php">Corporate</a></li>
      
    </ul>
  </div>
</nav>
 
 
 
    <ul id="rent-index" class="nav nav-pills">
      <li><a href="#">Stats</a></li>
      <li><a href="deleted.php">Deleted Requests</a></li>
      <li><a href="incomplete.php">Incomplete Requests</a></li> 
      <li class="active"><a href="rent-requests.php">New Delivery Requests</a></li> 
      <li><a href="delivery-processing.php">Delivery Processing</a></li>
      <li><a href="dispatched.php">Dispatched</a></li> 
      <li><a href="delivered.php">Delivered</a></li> 
      <li><a href="new-pickup.php">New Pickup Requests</a></li>
      <li><a href="pickup-processing.php">Pickup Processing</a></li> 
      <li><a href="sent-to-fedex.php">Sent to Fedex</a></li> 
      <li><a href="returned.php">Returned</a></li>
        
    </ul>
 
<div class="margintop">
  <table class="table table-bordered table-condensed font-new-del">
    <thead>
      <tr>
      	 
        <th>Parent Order</th>
        <th>Order id</th>
        <th>User</th>
        <th>Order Date</th>
        <th>Delivery Address</th>
        <th>Price Details</th>
        <th>Sub-Order Price Details</th>
        <th>Payment Mode and Type</th>
        <th>Book Details</th>
        <th>Assigned Book id</th>
        <th>Comment</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
   <?php 
   if($Order) {
   foreach ($Order as $key) {
   ?>
      <tr>
      	 
        <td><?php echo $key['p_order_id']; ?><br> 
             <?php echo '(Items : '.$key['items_count'].')'; ?>
        </td>
        <td><?php echo $key['bookshelf_order_id']; ?></td>
        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
        <td><p><?php echo $key['order_date']; ?></p></td>
        <td id="<?php echo $key['bookshelf_order_id']; ?>">
            <a   class="edit-addr"><h4 class="margin-top-0 glyphicon glyphicon-edit pull-right "></h4></a>
            <p id="p1"><?php echo $key['fullname']; ?></p ><p id="p2"><?php echo $key['address_line1']; ?>,</p><p id="p3"><?php echo $key['address_line2']; ?> </p><p id="p4"><?php echo $key['city']; ?></p><p id="p5"><?php echo $key['state'] ?> <p id="p6"><?php echo $key['pincode']; ?></p> <p id="p7"><?php echo $key['phone']; ?></p>
            <form action="" role="form" class="form addr-form" style="display:none;">
              <input type="text" name="fullname" id="fullname" value="<?php echo $key['fullname']; ?>">
              <input type="text" name="addr1" id="addr1" value="<?php echo $key['address_line1']; ?>">
              <input type="text" name="addr2" id="addr2" value="<?php echo $key['address_line2']; ?>">
              <input type="text" name="city" id="city" value="<?php echo $key['city']; ?>">
              <input type="text" name="state" id="state" value="<?php echo $key['state']; ?>">
              <input type="text" name="pincode" id="pincode" value="<?php echo $key['pincode']; ?>">
              <input type="text" name="phone" id="phone" value="<?php echo $key['phone']; ?>">
              <input type="hidden" name="addr_id" id="addr_id" value="<?php echo $key['address_book_id']; ?>">
              <input  type="button" value="Save" name="save_addr" id="<?php echo $key['address_book_id']; ?>" class="save_addr btn iread-btn action-btn margintop margin-left-8">
            <div id="loader" class="sp sp-circle loader"></div>
            </form> 
        </td>
        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
       
        <td >
           
          <?php $book_id='';
            if($key['unique_book_id']){
                echo $key['unique_book_id'];  
                $book_id=$key['unique_book_id'];
              if($key['unique_book_id']){
                $res5=$fun_obj->get_shelf_location($key['unique_book_id']);
                foreach ($res5 as $key5) {
                   echo '<br>Shelf : '.$key5['shelf_location']; 
                   echo '<br>Circulation : '.$key5['circulation']; 
                   $circ= $key5['circulation'];
                }
              }
              ?>
              <hr>
    	        <div class="dropdown">
    	           <a class="dropdown-toggle" data-toggle="dropdown" href="#">Change Book
    	             <span class="caret"></span>
                 </a>
    	           <ul class="dropdown-menu">
                 <?php 
                  $res2=$fun_obj->get_all_book_library_id($key['ISBN13'],$key['unique_book_id'],$circ); 
                  $res6=$fun_obj->get_all_book_library_id_same_circulation($key['ISBN13'],$key['unique_book_id'],$circ);
                  if($res6 || $res2){
                    ?><p class="please-wait"></p>
                    <table class="table table-condensed font-new-del table-hover">
                     <thead>
                      <tr>
                        <th>Library Book ID</th>
                        <th>Shelf</th>
                        <th>Circulation</th>
                      </tr>
                     </thead>
                     <tbody> 
                    <?php if($res6){
                            foreach ($res6 as $key_isbn) {
                             echo '<tr class="other_book"><input type="hidden" id="order_for_book" value="'.$key['bookshelf_order_id'].'"><input id="book_current" type="hidden" value="'.$key['unique_book_id'].'"> <td id="other_id">'.$key_isbn['book_id'].'</td><td id="other_location">'.$key_isbn['shelf_location'].'</td><td id="other_circ">'.$key_isbn['circulation'].'</td></tr>';                           
                            }
                          }
                      if($res2){
                        foreach ($res2 as $key_isbn_circ) {
                           echo '<tr class="other_book"><input type="hidden" id="order_for_book" value="'.$key['bookshelf_order_id'].'"><input id="book_current" type="hidden" value="'.$key['unique_book_id'].'"> <td id="other_id">'.$key_isbn_circ['book_id'].'</td><td id="other_location">'.$key_isbn_circ['shelf_location'].'</td><td id="other_circ">'.$key_isbn_circ['circulation'].'</td></tr>';
                       }
                      }
                     echo '</tbody></table>';
                  }
                  else{
                     echo "No other book is available";
                  }
                 ?>
      	          </ul> 
              </div>
      <?php }
            else{
              $res_is=$fun_obj->get_all_book_library_id_isbn($key['ISBN13']);
              if($res_is){?>
                <div class="dropdown">
                 <a class="dropdown-toggle" data-toggle="dropdown" href="#">Change Book
                   <span class="caret"></span>
                 </a>
                 <ul class="dropdown-menu">
                  
                    <table class="table table-condensed font-new-del table-hover">
                     <thead>
                      <tr>
                        <th>Library Book ID</th>
                        <th>Shelf</th>
                        <th>Circulation</th>
                      </tr>
                     </thead>
                     <tbody> 
                     <?php
                        foreach ($res_is as $key_isbn) {
                         echo '<tr class="other_book1"><input type="hidden" id="order_for_book" value="'.$key['bookshelf_order_id'].'"><input id="book_current" type="hidden" value="'.$key['unique_book_id'].'"> <td id="other_id">'.$key_isbn['book_id'].'</td><td id="other_location">'.$key_isbn['shelf_location'].'</td><td id="other_circ">'.$key_isbn['circulation'].'</td></tr>';                           
                        }
                     echo '</tbody></table>';
                 ;
                  
                 ?>
                  </ul> 
              </div>
            <?php  }

            echo "Book is not locked,Please check with Merchant library<br><br>";
            ?>
            <input type="hidden" value="<?php echo $key['ISBN13'];?>" id="isbn">
            <input type="hidden" value="<?php echo $key['Title'];?>" id="title_mer">
            <input type="hidden" value="<?php echo $key['bookshelf_order_id'];?>" id="order_id_for_merchant">
            <input type="button" class="btn btn-danger iread-btn action-btn merchant_modal_btn action-btn-3" data-toggle="modal"  value="Check Merchant">
            <?php
          }  ?>
           
          </td>
          

        <td>
	           <div class="form-group">
      			   <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['order_comments']; ?></textarea>
                <input type="hidden" id="order_id_cmmnt" value="<?php echo $key['bookshelf_order_id']; ?>">
                <a class="btn iread-btn iread-btn-white small-save-btn save_comment">Save</a> 
                <div id="loader1" class="sp sp-circle loader"></div>
				     </div>
		    </td>
        <td>
          
        <form action="" method="POST" role="form">
            <input type="hidden" value="<?php echo $key['bookshelf_order_id']; ?>" name="order_id">
            <input type="hidden" value="<?php echo $key['unique_book_id']; ?>" name="book_id">
            <input type="hidden" value="<?php echo $key['amt_pay']; ?>" name="amt_pay">
            <input type="hidden" value="<?php echo $key['payment_option']; ?>" name="payment_option">
            <input type="hidden" value="<?php echo $key['store_pay']; ?>" name="store_pay">
            <input type="hidden" value="<?php echo $key['user_id']; ?>" name="user_id">
            <input type="hidden" value="<?php echo $key['ISBN13']; ?>" name="ISBN13">

            <input type="hidden" value="<?php echo $key['buy_book_status']; ?>" name="buy">

            <input type="hidden" value="<?php echo $key['phone']; ?>" name="phone">

            <div class="form-group">
                <input  type="submit" name="delete" class="btn btn-danger iread-btn action-btn" value="Delete">
            </div>
            <div class="form-group">
                <input type="submit" name="process" class="btn btn-danger iread-btn action-btn" value="Process Delivery"> 
            </div>
        </form>
        </td>
      </tr>

      <?php } }  ?>
      	
      
    </tbody>
  </table>

</div>

  <!-- Modal -->
  <div class="modal fade" id="merchant_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="modal_content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <table class="table">
            <thead>
              <tr>
                <th>Merchant</th>
                <th>Price</th>
                <th>Phone</th>
                <th>Add</th>
              </tr>
            </thead>
            <tbody class="m_list">

            </tbody>
          </table>
          
        </div>
      </div>
      
    </div>
  </div>


<?php
/*for pagination*/
$total=$fun_obj->get_bookshelf_order_1_page(1);
include_once('../rent-request/pagination.php');
 

?>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
	</div>
  
<script type="text/javascript">
 
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(10000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
    
});
/*for updation and see add book*/
$('#modal_content').on('click', ".merchant_plus",function(event) {
      $(this).siblings('.merchant_form').toggle('fast'); 
      $(this).children('.glyphicon-plus').toggleClass('glyphicon-minus');
       $(this).siblings('.merchant_form').children('.loader').toggle('fast');  
});
$('#modal_content').on('click', ".add_book",function(event) {
         $(this).siblings('.loader').toggle('fast');
          var book_id=$(this).siblings('div').children('#book_id').val();
          var location=$(this).siblings('div').children('#location').val();
          var order_for_merchant=$(this).siblings('#order_for_merchant').val();
          var title_mer=$(this).siblings('#title_mer').val();
          var price=$(this).siblings('#price').val();
             var discount=$(this).siblings('#discount').val();
          var proc_price=$(this).siblings('#proc_price').val();
          var isbn=$(this).siblings('#isbn').val();
         
           $.ajax({
                  type: "POST",
                  url: '<?php echo base_url; ?>/rent-request/ajax/add_book_lock.php',
                  data: { book_id: book_id,
                          location: location,
                          order_for_merchant: order_for_merchant,
                          title_mer: title_mer,
                          isbn: isbn,
                           discount: discount,
                          proc_price: proc_price,
                          price: price
                        },
                  dataType: 'json',
                  success: function(res){  
                         $('.modal-body').html(res);
                         if (typeof(Storage) !== "undefined") {
                        localStorage.setItem("Status",res);
                        window.location.reload();
                    }
                  }
           });
           return false; 
});
/*for merchant book available*/
$(".merchant_modal_btn").click(function() {
     $('.overlay1').show();
    var isbn=$(this).siblings('#isbn').val();
    var title_mer=$(this).siblings('#title_mer').val();
    var order=$(this).siblings('#order_id_for_merchant').val();
    $('.modal-title').html('');
    $('.m_list').html('');
     $('.modal-title').append('Merchants list for book '+isbn);
     $('.overlay1').hide();
     
    var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/get_merchant.php',
            data: { isbn: isbn
                  },
            dataType: 'json',
            success: function(res){  
                var i; 
                  for(i=0;i<res.length;i++){
                    var price=(res[i].supply_price)-(((res[i].supply_price)*(res[i].merchant_discount))/100);
                   $('.m_list').append('<tr><td>'+res[i].merchant_name+'</td><td>'+price+'</td><td>'+res[i].merchant_phone+
                    '</td><td><a class="merchant_plus"><span class="glyphicon glyphicon-plus"></span></a>'+
                    '<div class="merchant_form"><div class="form-group"><input type="text" id="book_id" class="margintop form-control" placeholder="Book ID"></div><div class="form-group">'+
                    '<input type="text" id="location" class="form-control" placeholder="Shelf Location"></div>'+
                    '<input type="hidden" id="order_for_merchant" value="'+order+'">'+
                    '<input type="hidden" id="title_mer" value="'+title_mer+'">'+
                    '<input type="text" id="price"  class="form-control" placeholder="MRP"><br>'+
                    '<input type="text" id="discount" class="form-control" placeholder="Discount"><br>'+
                    '<input type="text" id="proc_price"   class="form-control" placeholder="Proc Price"><br>'+
                    '<input type="hidden" id="isbn" value="'+isbn+'">'+
                    '<input type="button" class="btn iread-btn iread-btn-white add_book" value="Add and Lock"><div id="loader" class="sp sp-circle loader"></div></div>'+
                    '</td></tr><br><hr>');
                  }  
                  $(".merchant_modal_btn").attr("data-target","#merchant_modal");   
                  $(".merchant_modal_btn").attr("data-toggle","modal");   
                  $('#merchant_modal').modal('show');
                     
            }
     });
     return false; 
});
/*for change book*/
$(".other_book").click(function() {
     $('.overlay1').show();
    var book_id=$(this).children('#other_id').html();
    var shelf=$(this).children('#other_location').html();
    var circulation=$(this).children('#other_circ').html();
    var order=$(this).children('#order_for_book').val();
    var cur_book_id=$(this).children('#book_current').val();
     
    var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/change_book.php',
            data: { book_id: book_id,
                    order : order,
                    cur_book_id : cur_book_id
                  },
            dataType: 'json',
            success: function(res){  
                    if (typeof(Storage) !== "undefined") {
                        localStorage.setItem("Status",res);
                        window.location.reload();
                    }
                                  
            }
     });
     return false; 
});
/*for change book */
$(".other_book1").click(function() {
     $('.overlay1').show();
    var book_id=$(this).children('#other_id').html();
    var order=$(this).children('#order_for_book').val();
    var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/rent-request/ajax/lock_book_after_free.php',
            data: { book_id: book_id,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
                    if (typeof(Storage) !== "undefined") {
                        localStorage.setItem("Status",res);
                        window.location.reload();
                    }
                                  
            }
     });
     return false; 
});
/*script for save comment*/
$(".save_comment").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     var textval=$(this).siblings('textarea').val();
     var order=$(this).siblings('#order_id_cmmnt').val();
     var html='';
     $.ajax({
            type: "POST",
            url:  '<?php echo base_url; ?>/rent-request/ajax/comment_save.php',
            data: { textval : textval,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
              $('.loader').hide(); 
           
             html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
              $('body').append(html);
               $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                 $("#i-alert").alert('close');
               });
            }
     });
     return false; 
});
/*script for edit address*/
$(".save_addr").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
    var td_id=$(this).closest('td').attr('id');
    var html='';
    $.ajax({
           type: "POST",
           url: '<?php echo base_url; ?>/rent-request/ajax/addr_update.php',
           data: $(this).parent().serialize(),  
            dataType: 'json',
           success: function(res){
                    if(res[1]){
                       $("#"+td_id+" #p1").html(res['1']);
                          $("#"+td_id+" #p2").html(res['2']);
                          $("#"+td_id+" #p3").html(res['3']);
                          $("#"+td_id+" #p4").html(res['4']);
                          $("#"+td_id+" #p5").html(res['5']);
                          $("#"+td_id+" #p6").html(res['6']);
                          $("#"+td_id+" #p7").html(res['7']);
                          }
                         $('.loader').hide(); 
                         $('.addr-form').hide();
                         html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res['0']+' for order : '+td_id+'</div>';
                         $('body').append(html);
                         $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                            $("#i-alert").alert('close');
                    });
           }
    });
    return false; 
});
</script>
 
  <script type="text/javascript">
    $('.edit-addr').on('click',function(event) {
      $(this).siblings('form').toggle('fast'); 
    });
    
  </script>
	</body>
</html>