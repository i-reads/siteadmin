<?php
//include_once('../../config/config.php');

include_once('../../config/functions.php');

 
 class dispatch
{		

	 
		public function data_con(){
			 try {
			$conn = new PDO('mysql:host=ec2-52-77-84-135.ap-southeast-1.compute.amazonaws.com;dbname=ireads','root','ireads');

			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			 
			} catch(Exception $e) {
			 
			}
			
			$fun_obj = new ireads($conn);
			return $fun_obj;
		}

	// Generate Dispatch Info
		public function generate_dispatch_info($selected_orders,$parent,$address_id,$track_id){
		 
 
		$fun_obj=$this->data_con();
		 
		$add = $fun_obj->get_address($address_id);//function to get address
		$u_email=$fun_obj->user_email($add[0]['user_id']);
		$email = $u_email[0]['user_email']; 
		$name= $add[0]['fullname'];
		$add1 = $add[0]['address_line1'];
		$add2 = $add[0]['address_line2'];
		$city = $add[0]['city'];
		$state = $add[0]['state'];
		$pincode =$add[0]['pincode'];
		$phone = $add[0]['phone'];
		$html = '<html>
		<head>
		        <meta charset="utf-8">
		        <title>IndiaReads.com</title>
		        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
		        <style type="text/css">
		                body{font-family: Source Sans Pro, sans-serif; font-size: 14px;}
		                .clear{clear: both;}
		                .solCont {border-bottom: 1px solid grey;}
		                .genCont{border-bottom: 1px solid grey;}
		        </style>
		</head>
		<body>
		        <div class="mainCont" style="width:670px;margin: 0 auto;">
		                <div class="headCont" style="height:50px; margin: 10px 0;">
		                        <div class="logoCont" style="width: 30%; float: left; height:50px;"><img style="max-width: 100%;max-height: 100%;" src="../img/irlogo_black.png"></div>
		                        <div class="barcodeCont" style="width: 55%; float: right; height:50px;"><img src="../barcode/test_1D.php?code=code39&o=1&dpi=72&t=80&r=1&rot=0&text='.$track_id.'&f1=Arial.ttf&f2=10" alt="Barcode" /></div>
		                        <div class="clear"></div>
		                </div>
		                <div class="solCont">
		                        <div class="addCont" style="width: 50%; float: left">
		                                <p style="font-weight: bold;">To:</p>
		                                <p><span>'.$name.'</span><br>'.$add1.', '.$add2.',<br>'.$city.', '.$state.'- '.$pincode.', INDIA<br/>Ph. : '.$phone.'<br>Email: '.$email.'</p>
		                        </div>
		                        <div class="additionalDetail" style="float: right; width: 45%;padding-left: 15px;border-left: 1px solid;">
		                                <p><span style="font-weight: bold;">Library Membership Details :</span><br><span>Library Request No.:<span>'.$parent.'</span></p>
		                                <p><span style="font-weight: bold;">Books in Packet :</span><br>';

						foreach ($selected_orders as $key) {

                                $order = $fun_obj->get_book_isbn_bookshelf_order($key);
                                $isbn = $order[0]['ISBN13'];
                                $bname = $fun_obj->get_title_submastersheet($isbn);
                                $html .= '<span>'.$bname[0]['title'].' ('.$isbn.')</span><br>';
                                 $html .= '<span>Order ID: R-'.$key.'</span><br>';
		                }

		                $html .= '</p></div>
		                        <div class="clear"></div>
		                </div>
		                <div class="genCont">
		                        <p><span style="font-weight: bold;">If undelivered, please return to :</span></p>
		                        <p><span>IndiaReads.com</span><br>
		                        <span>C-30, First Floor, Sector- 7,</span><br>
		                        <span>Noida, Uttar Pradesh- 201301, INDIA</span><br>
		                        <span>Website: www.indiareads.com</span><br>
		                        <span>E-mail: customercare@indiareads.com</span><br>
		                        <span>Phone: 0120-2424233, 0120-4206323</span><br></p>
		                </div>';
		                foreach($selected_orders as $key)
		                {
		                       	$order = $fun_obj->get_book_isbn_bookshelf_order($key);
                                $bname = $fun_obj->get_title_submastersheet($order[0]['ISBN13']);

		               $html .= ' <div class="solCont">
		                        <p><span style="font-weight: bold;">To :</span></p>
		                        <p><span>IndiaReads.com</span><br>
		                        <span>C-30, First Floor, Sector- 7,</span><br>
		                        <span>Noida, Uttar Pradesh- 201301, INDIA</span><br>
		                        <span>Website: www.indiareads.com</span><br>
		                        <span>E-mail: customercare@indiareads.com</span><br>
		                        <span>Phone: 0120-2424233, 0120-4206323</span><br></p>
		                </div>
		                <div class="genCont">
		                                <p style="font-weight: bold;">if undelivered please return it to:</p>
		                                <p><span>'.$name.'</span><br>'.$add1.', '.$add2.',<br>'.$city.', '.$state.'- '.$pincode.', INDIA<br/>Ph. : '.$phone.'<br>Email: '.$email.'</p>
		               </div>

				
		                <div class="genCont">
		        	                <div style="margin:10px 0">
			                                <span style="float:left"><span styla="font-weight: bold">Date: </span>____/____/20___</span>
		                                	<span style="float: left;text-align: center;display: block;width: 100%;font-weight: bold; margin-left:30px;">To Whomsoever it may Concern</span>
		                        	        <div class="clear"></div>
		                	        </div>
		                	        <span>Order ID: R-'.$key.'</span><br>
		        	                <p>Subject: Declaration of Books in Package</P>
			                        <p>I, “'.$name.'”, hereby declare that I am sending a book “'.$bname[0]['title'].'” that I borrowed from IndiaReads.com It was for read and return purpose and carries no commercial value.<br>We request you to consider this and allow the shipment to be taken to its marked address.</p>
		                        	<p>Thanking You.<br>(Signature)</p>
		                	</div>';
				}

				$html .= '</div>
			</body>
			</html>';
				
		return $html;
	}


	//generate invoice

	public function generate_invoice($inv,$selected_orders,$parent,$address_id,$ship,$odate,$ship_flag)
	{
		$fun_obj=$this->data_con();
		 
		$add = $fun_obj->get_address($address_id);//function to get address
		$user=$add[0]['user_id'];
		
		$name= $add[0]['fullname'];
		$add1 = $add[0]['address_line1'];
		$add2 = $add[0]['address_line2'];
		$city = $add[0]['city'];
		$state = $add[0]['state'];
		$pin =$add[0]['pincode'];
		$phone = $add[0]['phone'];

		date_default_timezone_set('Asia/Calcutta');
		$this->dt = date('Y-m-d H:i:s');

		
		 
		
		 
		
	
		
		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>IndiaReads.com</title>
			</head>

			<body>
			<table width="1000" align="center">
			    <tr style="border-bottom:1px solid #000;">
			    	<td align="center">
			        	<img height="70px" src="../img/irlogo_black.png" alt="" />
			        </td>
			    </tr>
			    <tr>
			    	<td>
			        	<p style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">
			            	Service Provided by: <br />
							iRead Books Private Limited <br />
							C-30, First Floor, <br />
			                Sector 7 <br />
			                Noida – 201301 U.P. <br />

			            </p>
			        </td>
			    </tr>
			    <tr>
			    	<td>
			        <br/><p style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">VAT/TIN number: 07360379350</p>
			        <table cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
			            <tr>
			            	<td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:30%">Invoice No.: <span style="font-weight:bold">'.$inv.'</span></td>
			                <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:35%">Billing Address</td>
			                <td style="padding:10px 5px; border-bottom:1px solid #000; width:35%">Shipping address</td>
			            </tr>
			            
			            <tr>
			            	<td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">
			                	Order id: <span style="font-weight:bold">'.$parent.'</span>
			                </td>
			                <td rowspan="4" style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">
			                <span style="font-weight:bold">'.$name.'<br/>'.$add1.'<br/>'.$add2.'<br/>'.$city.' - '.$pin.' '.$state.'<br/>Ph.: '.$phone.'</span>
			                </td>
			                <td rowspan="4" style="padding:10px 5px; border-bottom:1px solid #000;">
			                <span style="font-weight:bold">'.$name.'<br/>'.$add1.'<br/>'.$add2.'<br/>'.$city.' - '.$pin.' '.$state.'<br/>Ph.: '.$phone.'</span>
			                </td>
			            </tr>
			            <tr>
			            	<td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">
			                	Order Date: <span style="font-weight:bold">'.$odate.'</span>
			                </td>
			                
			            </tr>
			            <tr>
			            	<td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">
			                	Invoice date: <span style="font-weight:bold">'.$this->dt.'</span>
			                </td>
			                
			            </tr>
			            <tr>
			            	<td style="padding:10px 5px; border-right:1px solid #000; border-bottom:1px solid #000;">
			                	Nature: <span style="font-weight:bold">Rent</span>
			                </td>
			                
			            </tr>
			        </table>
			        </td>
			    </tr>
			    <tr>
			    	<td height="30">
			        <hr />
			        </td>
			    </tr>
			    <tr>
			    	<td>
			        	<table cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
			            	<tr>
			            	    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:10%; font-weight:bold">Order No.</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:15%; font-weight:bold">ISBN</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:55%; font-weight:bold">Title</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:10%; font-weight:bold">Price</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000; width:10%; font-weight:bold">Initial payable</td>
			                </tr>';
			                
			                  
			                $total = 0; $totali = 0;
			                foreach($selected_orders as $ord)
			                {	$order = $fun_obj->get_book_isbn_bookshelf_order($ord);
                                $isbn = $order[0]['ISBN13'];
                                $title = $fun_obj->get_title_submastersheet($isbn);

			                	$oid =  $ord;
			                	$mrp = $order[0]['mrp'] ;
			                	$ipay = $order[0]['init_pay'];
			                	$total += $mrp;
			                	$totali += $ipay;
			                	
			                $html .= '<tr height="15">
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$oid.'</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$isbn.'</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$title[0]['title'].'</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$mrp.'</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$ipay.'</td>
			                </tr>';
			                }
			                
			                $html .= '<tr height="15">
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                </tr>
			                <tr height="15">
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">Total(+):</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$total.'</td>
			                </tr>
			                <tr height="15">
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">Discount(-):</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.($total-$totali).'</td>
			                </tr>';

					if($ship > 0 && $ship_flag==1)
					{
						$html .= '<tr height="15">
			                 		    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
					                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                		    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
					                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">Shipping(+):</td>
			                		    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.$ship.'</td>
					                  </tr>';
					}

					$html .= '
			                <tr height="15">
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">Net Amount:</td>
			                    <td style="padding:10px 5px; border-bottom:1px solid #000; border-right:1px solid #000;">'.($totali + $ship + $cod).'</td>
			                </tr>
			            </table>
			        </td>
			    </tr>
			    <tr>
			    	<td height="30">
			        </td>
			    </tr>
			    <tr>
			    	<td>
			        	<p style="font-size:12px; font-family:Arial, Helvetica, sans-serif; line-height:1.5;">This is a computer generated invoice. No signature is required.<br />
						Enjoy Reading!</p>
			        </td>
			    </tr>
			    
			    <tr>
			    	<td style="font-size:12px; padding:5px 0; text-align:center; color:#000; font-family:Arial, Helvetica, sans-serif;">
			        	<a style="color:#ee4026;" href="www.indiareads.com" target="_blank"> www.IndiaReads.com</a> | C-30, First Floor, Sector – 7, Noida – 201301 | info@indiareads.com
			        </td>
			    </tr>
			</table>
			</body>
			</html>';

		return $html;
	}

 

}
?>