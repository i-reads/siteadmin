<?php
include_once('/../config/functions.php');
class Cancel
{		

	 
		public function data_con(){
			 try {
			$conn = new PDO('mysql:host=ec2-52-77-84-135.ap-southeast-1.compute.amazonaws.com;dbname=ireads','root','ireads');

			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			 
			} catch(Exception $e) {
			 
			}
			
			$fun_obj = new ireads($conn);
			return $fun_obj;
		}
		public function cancel_retail($order_id){
			$fun_obj=$this->data_con();
			$refund='';
			$msg='';
			$order=$fun_obj->get_bookshelf_order($order_id);
			$shipping=$order[0]['shipping_charge'];
			$payment=$order[0]['payment_option'];
			$store_pay=$order[0]['store_pay'];
			$amt_pay=$order[0]['amt_pay'];
			$init_pay=$order[0]['init_pay'];
			$price=$order[0]['price'];
			$net_pay=$order[0]['net_pay'];
			$count=$order[0]['items_count'];
			$user=$order[0]['user_id'];
			$parent=$order[0]['p_order_id'];
			$store_discount=$order[0]['store_discount'];
			  
			if($shipping==50){//if shiping is there
				if($payment==1){//if cod
					$refund=$store_pay;
				}
				elseif ($payment==2 || $payment==3 || $payment==4) {//if payment is online
					if($count==1){//if count is 1
						$refund=$amt_pay+$store_pay+$shipping;
					}
					else{//if count is >1
						$refund=$amt_pay+$store_pay;
					}	
				}
				elseif ($payment==5) {//if payment mode is store credit
					 if($count==1){//if item count is 1
					 	$refund=$store_discount;
					 }
					 else{
					 	$refund=$store_pay;
					 }
				}
				else{//some error occured with payment
					$msg.="Oops some error occured";
				}
			}
			else{//else for shipping 0
				if($payment==2 || $payment==3 || $payment==4){//if payment is online
					if(($price-$init_pay)<350 && $count==1){//compare net pay with 350 and count od items is 1
						$refund=$net_pay;
					}
					elseif (($price-$init_pay)<350 && $count>1) {//if count of items is >1
						$refund=$amt_pay+$store_pay-50;
						$shipping=50;
					}
					else{//if net pay is not below 350
						$refund=$amt_pay+$store_pay;
					}
				}
				elseif ($payment==1) {//if cod
					if(($price-$init_pay)<350 && $count==1){//compare net pay with 350 and count od items is 1
						$refund=$store_pay;
					}
					elseif (($price-$init_pay)<350 && $count>1) {//if count of items is >1
						if($store_pay>50){
							$refund=$store_pay-50;
						}
						else{
							$refund=$store_pay;//add 50 in shiping
							 
						}
						$shipping=50;

					}
					else{//if net pay is not below 350
						$refund=$store_pay;
					} 
				}
				elseif ($payment==5) {//if payment mode is store credit
					 if(($price-$init_pay)<350 && $count==1){//if item count is 1
					 	$refund=$store_discount;
					 }
					 elseif(($price-$init_pay)<350 && $count>1){//if item count is 1
					 	$refund=$store_pay-50;
					 	$shipping=50;
					 }
					 else{
					 	$refund=$store_pay;
					 }
				}
				else{//if some payment error
					$msg.="Oops some error occured";
				}
			}
			if($refund!=null){
				$res2=$fun_obj->update_refund_oncancel($user,$refund,$order_id,$shipping,$parent);
				if($res2){
					$msg.='Refund is done of '.$refund;
				}
			}
			else{
				$msg.='some error';
			}
			return $msg;

		}
}



?>