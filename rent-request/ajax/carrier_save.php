 <?php

include_once('../../config/config.php');
 
include_once('../../config/functions.php');
include_once('../../mail/mail.php');
 
$con = connect($config);
$fun_obj = new ireads($con);
$msg='';
$new_parent='';
$same_parent=1;
$selected_orders=array();
$carrier=$_POST['carrier'];
$parent=$_POST['parent'];
$track_id=$_POST['track_id'];
$address_id=$_POST['address_id'];
$shipping=$_POST['shipping'];
$odate=$_POST['odate'];

if(empty($_POST['selected_orders'])){

      $msg.="Please select atleast one order";
}
else{
     $selected_orders=$_POST['selected_orders'];
     /*check if all orders are from same parent order*/
     foreach ($selected_orders as $key1) {
         $every_parent=$fun_obj->get_parent_order_for_every_order($key1);
         foreach ($every_parent as $key2) {$new_parent=$key2['p_order_id'];}
         if($new_parent==$parent){}
         else{$same_parent=0;}
     }
     /*if same parent order then only it will proceed the process*/
     if($same_parent==1){
        include("../MPDF57/mpdf.php");
        include_once("../invoice_class/dispatch.php");
         
        
        $html='';
        $html1='';
        $dispatch = new dispatch(); 
        $html=$dispatch->generate_dispatch_info($selected_orders,$parent,$address_id,$track_id);
        date_default_timezone_set('Asia/Calcutta');
        $prefix_name = "IRDIS".date('ymdHis').$parent;
        $fname = $prefix_name.'.pdf';

        $mpdf = new mPDF(); 
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
        
        $mpdf->WriteHTML($html);
        $mpdf->Output('../../dispatch-slips/'.$fname);
        


        /*create invoice*/
        $ship_flag='';
            $inv = "IRLIB".date('ymdHis').$parent;
            $check_parent=$fun_obj->get_parent_dispatch($parent);
            if($check_parent){
                $ship_flag=0;
            }
            else{
                $ship_flag=1;
            }
           
            foreach ($selected_orders as $key11) {
               $check_row=$fun_obj->get_dispatch_slip_name($parent,$key11);
               if($check_row){
                    $res2=$fun_obj->get_delete_slip_name($parent,$key11);
               }
            }
         
            $html1=$dispatch->generate_invoice($inv,$selected_orders,$parent,$address_id,$shipping,$odate,$ship_flag);
            $fname_inv = $inv.'.pdf';
             $mpdf1 = new mPDF(); 
            $mpdf1->SetDisplayMode('fullpage');
            $mpdf1->list_indent_first_level = 0; 
            $mpdf1->WriteHTML($html1);
            $mpdf1->Output('../../invoices/'.$fname_inv);


           

            


        /*update tracking id and carrier*/
         $add_orders='';
         $res1='';
        foreach ($selected_orders as $key) {
             $save_res= $fun_obj-> update_dispatch_invoice($parent,$key,$prefix_name,$inv);
             $res1= $fun_obj-> update_carrier_track_id($carrier,$track_id,$key);
             $add_orders.="-".$key;
        }

        $result = order_ids('Dispatched',$selected_orders,$fun_obj);
        $msg .= $result;
         
        if($res1){

            $msg.=" Carrier ".$carrier." is saved for order ".$add_orders.".<br>Dispatch slip and invoice are generated,<Br> Please handover the book to the carrier.";
        }
        else{
            $msg.="Oops,Something went wrong";
        }
     }
     else{
         $msg.="Please select all orders from same parent order";
     }
       
}


$res[0]=$msg;
 echo json_encode($res[0]);
?>
