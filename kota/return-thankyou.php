<?php 
require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Return Summary</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/main.css">
	</head>
	<body style="padding:50px;">
		<div class="row" id="return_main">
			<h3 style="margin-left:15px;">Return Summary</h3>
			<div class="col-md-10">
				<div id="response" style="display:none;font-size:20px;font-weight:800;color:#090;"></div>
				<!-- <div class="authcard fixeddiv" id="authcard" > -->
				<div class="authcard " id="authcard">					
					<div id="invoice_reload">
						<table class="table table-striped">
						 	<thead>
						 		<tr>
						 			<th>Name of the book</th>
						 			<th>Initial Pay</th>
						 			<th>Charge</th>
						 			<th>Refund</th>
						 			<th>Number Of Days</th>
						 		</tr>
						 	</thead>
						 	<tbody>
						 	<?php 
						 	$sql     = "SELECT return_cart.*, bookshelf_order.* FROM return_cart JOIN bookshelf_order ON return_cart.order_id=bookshelf_order.bookshelf_order_id WHERE return_cart.user_id='$user_id'";
							$all_result  = $kota_ob->select_return_cart_data_user($sql);
							//print_r($all_result);die;
							
						 	if(!empty($all_result))
						 	{
							 	$total = 0;
							 	foreach($all_result as $row)
							 	{	
							 		$dt1 = date_create($row->issue_date);
									$dt2 = date_create(date('Y-m-d h:i:s'));
									$diffd = date_diff($dt2,$dt1);
									$diff = $diffd->m;
									$number_of_day = $diffd->d;
									if($diffd->d > 0)
									$diff++;
									$mrp = $row->init_pay;
									if($diff>=0 && $diff<=1) { $cost =  (20/100)*$mrp;}
									if($diff>1 && $diff<=2) { $cost =  (25/100)*$mrp;}
									if($diff>2 && $diff<=3) { $cost =  (30/100)*$mrp;}
									if($diff>3 && $diff<=4) { $cost =  (35/100)*$mrp;}
									if($diff>4 && $diff<=6) { $cost =  (40/100)*$mrp;}
									if($diff>6 && $diff<=12) { $cost =  (50/100)*$mrp;}
									$amount_pay = $row->amt_pay+$row->store_pay;
							 	?>
							 		<tr>
							 			<td><?php echo substr($row->title, 0, 20).'....'; ?></td>
							 			<td><?php echo $row->init_pay; ?></td>
							 			<td><?php echo $cost; ?></td>
							 			<td><?php echo $mrp - ceil($cost); ?></td>
							 			<td><?php echo $number_of_day; ?></td>
							 		</tr>
						 		<?php }
						 		}
				 		 	?>
						 	</tbody>
						</table>
					</div>
					<hr>
					<div class="" align="right">
						<a class="btn btn-primary" style="background:#bc3526;">Back</a>
					</div>
					

				</div>
			</div>
		</div>
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	$('#return_main').on("click", '.btn-primary', function() {
		var action 		=	"return";
		$.ajax({
		        url: 'services/delete-return.php',
		        method: 'POST',
		        data: {action:action},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		                window.location.href = 'bookshelf.php';
		        	}

		        });
});
});

$(document).ready(function(){
setTimeout(function(){ 
  var action 		=	"return";
		$.ajax({
		        url: 'services/delete-return.php',
		        method: 'POST',
		        data: {action:action},
		        dataType: 'json', 
		      }).success(function(resp) {

		        });
}, 2000);
});
</script>