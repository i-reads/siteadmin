<!DOCTYPE html>
<html>
	<head>
		<title>KOTA</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/main.css">
	</head>
	<body>
	<!-- <div class="toast"></div> -->
	<div id="main_div">
		<div class="row container"  style="padding:50px;">
			<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 authcard">
			  	<!-- Nav tabs -->
			  	<ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="tab active" id="sign_in">
				    	<a class="btn-btn" href="#signin" aria-controls="signin" role="tab" data-toggle="tab">Sign In</a>
				    </li>
				    <li role="presentation" class="tab">
				    	<a class="btn-btn" href="#signup" aria-controls="signup" role="tab" data-toggle="tab">Sign Up</a>
				    </li>
			  	</ul>
			  	<!-- Tab panes -->
			  	<div class="tab-content">
				    <div role="tabpanel" class="tab-pane fade in active" id="signin">
				    	<form name="login_frm" id="login_frm" method="post">
                            <div id="error1" class="form-group" style="display:none;"></div>

				    	  	<div class="form-group">
				    		    <label for="email">Email address : </label>
				    		    <input type="email" id="email_log" name="email_log" class="form-control" placeholder="Email">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="user_id">User ID</label>
				    		    <input type="text" id="user_id" name="user_id" class="form-control" placeholder="User Id">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="Reso_ID">Reso ID</label>
				    		    <input type="text" id="Reso_ID" name="Reso_ID" class="form-control" placeholder="Reso ID">
				    	  	</div>
				    	  	<button type="button"  id="login" class="btn btn-btn">Submit</button>
				    	</form>
				    </div>
				    <div role="tabpanel" class="tab-pane fade in" id="signup">
				    	<form name="signup_frm" id="signup_frm" method="post">
                            <div id="error" class="form-group" style="display:none;"></div>
				    	  	<div class="form-group">
				    		    <label for="email">Email address : </label>
				    		    <input type="email" id="email" name="email" class="form-control" placeholder="Email">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="email">Bookbank Id : </label>
				    		    <input type="text" name="bookbank_id" id="bookbank_id" class="form-control" placeholder="Bookbank Id">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="email">Name : </label>
				    		    <input type="text" name="name" id="name" class="form-control" placeholder="Name">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="email">Phone :</label>
				    		    <input type="email" maxlength="10" name="phone" id="phone" class="form-control" placeholder="Phone">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="email">Password : </label>
				    		    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
				    	  	</div>
				    	  	<div class="form-group">
				    		    <label for="email">Confirm Password : </label>
				    		    <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm Password">
				    	  	</div>
                            <div id="password_error" class="form-group" style="display:none;color:#a94442;"></div>
				    	  	<button type="button" name="Submit" id="Submit" class="btn btn-btn">Submit</button>
				    	</form>

				    </div>
			  	</div>
			</div>
		</div>
	</div>
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</body>
</html>
<script type="text/javascript">
function isEmail(email) 
{
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

$('#Submit').click(function(){
    if(isEmail($("#email").val().trim())=="")
    {
        $("#email").closest('div').addClass('has-error');   
        $("#email").focus();
        return false;
    }else{
        $("#email").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    if($("#name").val().trim()=="")
    {
        $("#name").closest('div').addClass('has-error');   
        $("#name").focus();
        return false;
    }else{
        $("#name").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    if($("#phone").val().trim()=="")
    {
        $("#phone").closest('div').addClass('has-error');   
        $("#phone").focus();
        return false;
    }else{
        $("#phone").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    if($("#password").val().trim()=="")
    {
        $("#password").closest('div').addClass('has-error');   
        $("#password").focus();
        return false;
    }else{
        $("#password").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    if($("#cpassword").val().trim()=="")
    {
        $("#cpassword").closest('div').addClass('has-error');   
        $("#cpassword").focus();
        return false;
    }else{
        $("#cpassword").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    if($("#cpassword").val().trim()!=$("#password").val().trim())
    {
        $("#password, #cpassword").closest('div').addClass('has-error'); 
        $("#password_error").show().empty().text('Password and confirm password should be same.');  
        $("#password").focus();
        return false;
    }else{
        $("#cpassword, #password").closest('div').removeClass('has-error').addClass('has-success'); 
        $("#password_error").hide();
    }
    var str = $("#signup_frm").serialize();
    $.ajax({
        url: 'services/service-registration.php',

        method: 'POST',
        data: {str:str},
        dataType: 'json', 
      }).success(function(resp) {
            if(resp.code==1)
            {
                $('#error').show().css("color","#3c763d").empty().text(resp.message);
                $('#signup_frm')[0].reset();
        }
        else{
               $('#error').show().css("color","#a94442").empty().text(resp.message);
            } 

        });

 });
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#login").click(function(){
	if($("#email_log").val().trim()=="" && $("#user_id").val().trim()=="" && $("#Reso_ID").val().trim()=="")
	{
		$('#error1').show().css("color","#a94442").empty().text("Please enter anyone of these field.");
		return false;
	}
    if($("#email_log").val().trim()!="")
    {
        if(isEmail($("#email_log").val().trim())=="")
	    {
	        $("#email_log").closest('div').addClass('has-error');   
	        $("#email_log").focus();
	        return false;
	    }else{
	        $("#email_log").closest('div').removeClass('has-error').addClass('has-success'); 
	    }
    }else{
        $("#email_log").closest('div').removeClass('has-error').addClass('has-success'); 
    }
    // if($("#user_id").val().trim()!="")
    // {
    //     $("#user_id").closest('div').addClass('has-error');   
    //     $("#user_id").focus();
    //     return false;
    // }else{
    //     $("#user_id").closest('div').removeClass('has-error').addClass('has-success'); 
    // }
    // if($("#Reso_ID").val().trim()!="")
    // {
    //     $("#Reso_ID").closest('div').addClass('has-error');   
    //     $("#Reso_ID").focus();
    //     return false;
    // }else{
    //     $("#Reso_ID").closest('div').removeClass('has-error').addClass('has-success'); 
    // }
    var str = $("#login_frm").serialize();
    $.ajax({
        url: 'services/service-login.php',

        method: 'POST',
        data: {str:str},
        dataType: 'json', 
      }).success(function(resp) {
            if(resp.code==1)
            {
                window.location.href	=	"listing.php";
                // $('#error1').show().css("color","#3c763d").empty().text(resp.message);
                // $('#login_frm')[0].reset();
        }
        else{
               $('#error1').show().css("color","#a94442").empty().text(resp.message);
            } 

        });
            
        });
    });
</script>