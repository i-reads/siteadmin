<?php
class Kota
{
	private $_db, $dt;

	public function __construct($user=null)
	{
		session_start();
		// error_reporting(E_ALL);
		// ini_set("display_errors", "On");
		$this->_db = DB::getInstance();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}
	public function get_currently_reading($user_id)
	{
		$select_reading = "SELECT * FROM bookshelf WHERE user_id='$user_id' AND shelf_type=3";
		$result = $this->_db->query($select_reading);
		return $result->results();
	}

	public function filter($data) 
	{
		$data = trim(htmlentities(strip_tags($data)));
		if (get_magic_quotes_gpc())
		{
			$data = stripslashes($data);
		}
		//$data = mysqli_escape_string($data);

		if (strpos(strtolower($data), "union") !== false || strpos(strtolower($data), "select") !== false || strpos(strtolower($data), "update") !== false || strpos(strtolower($data), "delete") !== false) 
		{ 
			$postdata = http_build_query(	array( 'filename' => "unsion_select", 'mobile' => '0', 'name' => '0', 'p1' => $_SERVER['REQUEST_URI'], 'p2' => "1", 'p3' => "1", 'detail' => $data));
			
			$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
			$context  = stream_context_create($opts);
			//$data = json_decode(file_get_contents('http://www.indiareads.com/config/errorLog.php', false, $context),true);
			return "invalid inputs";
		}
		else
		{
			return $data;
		}
	}

	public function check_duplicate_return_cart($boid)
	{
		$select_ret = "SELECT * FROM return_cart WHERE order_id='$boid'";
		$data = $this->_db->query($select_ret);
		if($data->count())
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public function add_return_cart($boid, $user_id, $title)
	{
		$data = $this->_db->insert('return_cart', array('order_id' =>$boid, 'user_id'=>$user_id, 'title'=>$title));
		return $data;
	}
	public function delete_return_cart($query)
	{
		$data = $this->_db->query($query);
		return $data;
	}

	public function execute_query_only($query)
	{
		$data = $this->_db->query($query);
		return $data;
	}

	public function execute_query_with_inserted_id($query)
	{
		$data = $this->_db->query($query);
		return $data->last();
	}

	
	public function select_return_cart_data_user($query)
	{
		$data = $this->_db->query($query);
		return $data->results();
	}

	public function check_login($query)
	{
		$data = $this->_db->query($query);
		if($data->count())
		{
			return $data->results();
		}
		else
		{
			return 0;
		}
	}
	public function return_cart_update($query)
	{
		$data = $this->_db->query($query);
		if($data->count())
		{
			return $data->results();
		}
		else
		{
			return 0;
		}
	}
	public function reading_history($query)
	{
		$data = $this->_db->query($query);
		if($data->count())
		{
			return $data->results();
		}
		else
		{
			return 0;
		}
	}
	public function listing_page($query)
	{
		$data = $this->_db->query($query);
		if($data->count())
		{
			return $data->results();
		}
		else
		{
			return 0;
		}
	}

	public function user_store_credit($user_id=null)
	{
	    if($user_id)
	    {
	        $query = "SELECT store_credit FROM user_store_credit WHERE `user_id`= ".$user_id."";
	        $data 	= $this->_db->query($query);
	        $data 	= $data->results();
	        if (!empty($data))
	        {
	            return $data[0]->store_credit;
	        }
	        else
	        {
	            return 0;
	        }
	    }
	    return false;
	}
	public function get_user_store_credit($user_id)
	{
	    $UserCredit = 0;
	    $data 	= $this->_db->query("select * from user_store_credit where user_id='$user_id'");
        $data 	= $data->results();             
	    if(!empty($data))
	    {
	      $store_credit = $data[0]->store_credit;
	      $used_credit  = $data[0]->used_credit;
	      if($store_credit !=$used_credit)
	      {
	        $UserCredit = $store_credit-$used_credit;
	      }
	    }
	      return $UserCredit;
	}
	public function get_user_used_credit($user_id)
	{   
	    $used_credit = 0;      
	    $data = $this->_db->query("select * from user_store_credit where user_id='$user_id'");
	    $data 	= $data->results();             
	    if(!empty($data))
	    {
	     
	      if(!empty($data[0]->used_credit))
	      {
	           $used_credit = $data[0]->used_credit;
	      }
	    }
	    return $used_credit;
	}
	public function get_parent($oid=null)
	{
	    if($oid)
	    {
	        $query 	= "SELECT p_order_id FROM bookshelf_parent_order WHERE `bookshelf_order_id`= ".$oid."";
	        $data 	= $this->_db->query($query);
	        $data 	= $data->results();
	        if (!empty($data))
	        {
	            return $data[0]->p_order_id;
	        }
	        else
	        {
	            return 0;
	        }
	    }
	    return false;         
	}
	public function pdf_creator($user_id, $parent_order_id)
	{
		$date 	=	date('Y-m-d');
		$time 	=	date('h-i');
		$dated	=	date('Y-m-d h:i:s');
		$id 	=	$user_id;

		$data 	= $this->_db->query("SELECT * FROM user_info where user_id = '$id'");
        $data 	= $data->results();
		if(!empty($data))
		{
			$fname 	=	$data[0]->first_name;
			$lname 	=	$data[0]->last_name;
			$phone 	=	$data[0]->mobile;
		}
		else
		{
			$fname 	=	"Temp";
			$lname 	=	"Temp";
			$phone 	=	"8826359036";
		}

		$invoice_id=$id."_".$date."_".$time;

		$filename = $invoice_id.'.pdf';
		$html = $this->library_order_invoice($invoice_id, $date, $fname, $lname, $phone);
		$mpdf = new mPDF(); 
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list

		$mpdf->WriteHTML($html);
		$mpdf->Output('../invoice/'.$filename); 

		$boobank_invoice = "INSERT INTO boobank_invoice SET user_id='$user_id', parent_order_id='$parent_order_id', invoice_url='$filename'";
		$this->_db->query($boobank_invoice);
		return $filename;
	}
	public function library_order_invoice($invoice_id, $date, $fname=null, $lname, $phone)
	{
		$name 		=	$fname." ".$lname;
		$i 			=	0;
		$isbn 		= 	"";
		$title 		= 	"";
		$qty 		= 	1;
		$mrp 		= 	0;
		$disc 		= 	0;
		$x1			=	0;
		$x			=	0;
		$str 		=	"";
		$amount 	= ($mrp*$qty) - $disc;
		$user_id	=	$_SESSION['bookbank_user_id'];
		$totalAmount 		= 	$this->listing_page("select sum(initial_pay) as 'initial_pay', COUNT(*) as 'count' from cart where user_id='$user_id'");
		$count 				=	$totalAmount[0]->count;
		$total_amount		= 	$totalAmount[0]->initial_pay;
		
		$ordersummarydata 	= $this->_db->query("SELECT * from cart where user_id='$user_id'");
        $ordersummarydata 	= $ordersummarydata->results();

        $net_pay 		= 	$this->listing_page("select sum(amt_pay) as 'amt_pay' from cart where user_id='$user_id'");
		$net_pay 		=	$net_pay[0]->amt_pay;

	    foreach($ordersummarydata as $row)
	    {
	    	$x1=$x+1;
			$str=$str.$x1.". ".ucwords($row->title)." - Rs.".$row->initial_pay."<br>";
	    }
		$finalmsg='<!DOCTYPE html>
		<html>
		<head
		<title>Print Invoice</title>
		<style>
		*{
		margin:0;
		padding:0;
		font-family:Arial;
		font-size:10pt;
		color:#000;
		}
		body{
		width:100%;
		font-family:Arial;
		font-size:10pt;
		margin:0;
		padding:0;
		}
		.heads
		{
			font-size:17px;
			font-weight:bold;
			line-height:22px;
		}
		.smallheads
		{
			font-size:13px;
			font-weight:bold;
			line-height:20px;
		}
		#dates
		{
			position:absolute;
			right:0px;
			width:300px;
			display:inline-block;
			margin-left:500px;
		}
		</style>
		</head>
		<body>
		Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

		<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['bookbank_user_id'].'</h1>
		Ph. :'.$phone.' <br>

		<h3 class="heads"> Books in Packet : </h3>
		<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$total_amount.'</h3>

		--------------------------------------------------------------------------------------------------------------------------------------------------<br>
		Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

		<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['bookbank_user_id'].'</h1>
		Ph. :'.$phone.' <br>
		<h3 class="heads"> Books in Packet : </h3>
		<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$total_amount.'</h3>

		--------------------------------------------------------------------------------------------------------------------------------------------------<br>

		Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

		<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['bookbank_user_id'].'</h1>
		Ph. :'.$phone.' <br>
		<h3 class="heads"> Books in Packet : </h3>
		<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$total_amount.'</h3>

		</body>
		</html>';

		return $finalmsg;

	}
	public function generateRandomString($length = 6) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	public function getToken($length)
	{
	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	    $codeAlphabet.= "0123456789";
	    for($i=0;$i<$length;$i++)
	    {
	        $val    = $this->crypto_rand_secure(0,strlen($codeAlphabet));
	        $token .= $codeAlphabet[$val];
	    }
	    return $token;
	}
	public function crypto_rand_secure($min, $max) 
	{
	    $range = $max - $min;
	    if ($range < 0) return $min; // not so random...
	    $log = log($range, 2);
	    $bytes = (int) ($log / 8) + 1; // length in bytes
	    $bits = (int) $log + 1; // length in bits
	    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	    do 
	    {
	        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
	        $rnd = $rnd & $filter; // discard irrelevant bits
	    }
	    while($rnd >= $range);
	    return $min + $rnd;
	}



}
// function filter($data) 
// {
//    if(is_array($data))
//    {   
// 		foreach($data as &$v)
// 		{
// 		    $v = filter($v);
// 		}
// 		unset($v);
// 		return $data;
//     }
//     else
//     {
// 		$data = trim(htmlentities(strip_tags($data)));
// 		if (get_magic_quotes_gpc())
// 		{
// 			$data = stripslashes($data);
// 		}
// 		$data = mysql_escape_string($data);
	
// 		if (strpos(strtolower($data), "union") !== false || strpos(strtolower($data), "select") !== false || strpos(strtolower($data), "update") !== false || strpos(strtolower($data), "delete") !== false) 
// 		{ 
// 			$postdata = http_build_query(	array( 'filename' => "unsion_select", 'mobile' => '0', 'name' => '0', 'p1' => $_SERVER['REQUEST_URI'], 'p2' => "1", 'p3' => "1", 'detail' => $data));
			
// 			$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
// 			$context  = stream_context_create($opts);
// 			//$data = json_decode(file_get_contents('http://www.directpolicyindia.com/include/errorLog.php', false, $context),true);
// 			return "invalid inputs";
// 			die();
// 		}
// 		else
// 		{
// 			return $data;
// 		}
//     }
// }
?>