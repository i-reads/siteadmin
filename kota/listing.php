<?php 

require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

if(empty($_SESSION['bookbank_user_id']))
{
    header("location:index.php");
}

$user_id =	$_SESSION['bookbank_user_id'];

$query = "SELECT DISTINCT category FROM books"; 
$result = $kota_ob->listing_page($query);

$category = '';
foreach($result as $row)
{
	$category = $category.'<option value="'.$row->category.'">'.$row->category.'</option>';	
}

$query_author = "SELECT DISTINCT author FROM books";
$author_result = $kota_ob->listing_page($query_author);
$arr 		=	array("."," ","-",";",",");
$author 	=	'';
$a 			=	array();
$i 			=	0;
$p 			=	1;
foreach($author_result as $row)
{
	$a[$i] 		= 	$row->author;
	for($z=0;$z<$i;$z++)
	{
		$a[$z] 	=	str_replace($arr,"",$a[$z]);
		$x 		=	$row->author;
		$x 		= 	str_replace($arr,"",$x);
		$a[$z] 	=	strtolower($a[$z]);
		$x 		=	strtolower($x);
		$p 		= 	strcmp($x,$a[$z]);
	}
		if($p!=0)	
		
		$author = $author.'<option value="'.$row->author.'">'.$row->author.'</option>';
		$i++;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Product Page</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/main.css">
	</head>
	<body style="padding:50px;">
		<div class="panel panel-info" id="search_div">
		  	<div class="panel-body">
		  		<span style="font-size:24px;"><b>Filter your search</b></span>
			   	<form action="" class="form-inline" role="form">
		   		   	<div class="form-group">
		   		   	  	<label for="subjects">Select from subjects:</label>
		   		   	  	<select class="form-control" id="subjects">
		   		   	  		<option value="">Select From Subject</option>>
		   			   	    <?php echo $category; ?>
		   		   	  	</select>
		   		   	</div>
		   		   	<div></div> <div class="form-group">
		   		   	  	<label for="author">Select From Author:</label>
		   		   	  	<select class="form-control" id="author">
		   			   	    <option value="">Select From Author</option>
		   			   	    <?php echo $author; ?>
		   		   	  	</select>
		   		   	</div>
		   		   	<div class="input-group">	
	   	   		      	<span class="input-group-btn">
	   		   		        <button class="btn btn-default" type="button" id="Filter_Now" name="Filter_Now">Filter Now</button>
	   	   		      	</span>
	   	   		    </div>
	   	   		    <div align="right">
							<a class="btn btn-primary" href="bookshelf.php" style="background:#bc3526;">Bookshelf >></a>
						</div>
	   		   		<!-- <div class="input-group">
	   	   		      	<input type="text" class="form-control" placeholder="Search for...">
	   	   		      	<span class="input-group-btn">
	   		   		        <button class="btn btn-default" type="button">Go!</button>
	   	   		      	</span>
	   	   		    </div> -->
			   	</form>
		  	</div>
		</div>
		<div class="row">
			<h3 style="margin-left:15px;">Choose book to add</h3>
			<div class="col-md-6">
				<div class="list-group" id="booklist_div">
					<?php
						$books = $kota_ob->listing_page('SELECT * FROM books');
						foreach($books as $row)
						{
					?>
				  	<a href="#" title="<?php echo $row->ISBN; ?>" class="list-group-item" alt="<?php echo $row->ISBN.';'.$row->mrp.';'.$row->sp.';'.$row->title.';'.$row->author;?>">
				    	<p class="inline"><?php echo $row->title; ?></p>
				    	<p class="inline right">Rs. <s><?php echo $row->mrp; ?></s>  <?php echo $row->sp; ?></p>
				  	</a>
				  	<?php } ?>
				</div>
			</div>
			<div class="col-md-6">
				<div id="response" style="display:none;font-size:20px;font-weight:800;color:#090;"></div>
				<!-- <div class="authcard fixeddiv" id="authcard" > -->
				<div class="authcard " id="authcard" >
					<h3>Invoice</h3>
					
					<div id="invoice_reload">
						<table class="table table-striped">
						 	<thead>
						 		<tr>
						 			<th>Name of the book</th>
						 			<th>MRP</th>
						 			<th>Initial Pay</th>
						 			<th>Action</th>
						 		</tr>
						 	</thead>
						 	<tbody>
						 	<?php 
						 	$select_cart = "select * from cart where user_id='$_SESSION[bookbank_user_id]'";
						 	$select_cart = $kota_ob->listing_page($select_cart);
						 	$total = 0;
						 	if(!empty($select_cart))
						 	{
							 	foreach($select_cart as $row)
							 	{
							 		$total = $total+$row->initial_pay;
							 	?>
							 		<tr>
							 			<td><?php echo substr($row->title, 0, 15).'....'; ?></td>
							 			<td><?php echo $row->mrp; ?></td>
							 			<td><?php echo $row->initial_pay; ?></td>
							 			<td><p class="btn btn-link" alt="Remove-<?php echo $row->ISBN13; ?>" title="Remove">Remove</p></td>
							 		</tr>
					 		<?php } 
					 		}
					 		?>
						 		<tr>
						 			<th>Total Amount</th>
						 			<th></th>
						 			<th></th>
						 			<th>Rs. <?php echo $total; ?></th>
						 		</tr>
						 	</tbody>
						</table>
					</div>
					<!-- <div class="check-btn" align="right"> -->
					<div class="" align="right">
						<button class="btn btn-primary" style="background:#bc3526;" id="order_now">Checkout</button>
					</div>
				</div>
			</div>
		</div>
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
	$('#booklist_div').on("click", 'a', function() {

			var str 	= $( this ).attr('class');
			var details = $( this ).attr('alt');
			var active 	= str.search(/active/i);
			if(active>0)
			{
				$.ajax({
				        url: 'services/service-addto-cart.php',

				        method: 'POST',
				        data: {details:details, action:'delete-cart'},
				        dataType: 'json', 
				      }).success(function(resp) {
				            if(resp.code==1)
				            {
				                $("#invoice_reload").load(location.href + " #invoice_reload");
				        }
				        else{
				               $('#error1').show().css("color","#a94442").empty().text(resp.message);
				            } 

				        });

			}
			else
			{
				$.ajax({
				        url: 'services/service-addto-cart.php',

				        method: 'POST',
				        data: {details:details, action:'addto-cart'},
				        dataType: 'json', 
				      }).success(function(resp) {
				            if(resp.code==1)
				            {
				                $("#invoice_reload").load(location.href + " #invoice_reload");
				        }
				        else{
				               $('#error1').show().css("color","#a94442").empty().text(resp.message);
				            } 

				        });
			}

			$( this ).toggleClass( "active" );
	});
	});

$(document).ready(function(){
	$('#invoice_reload').on("click", '.btn-link', function() {

		var details = 	$( this ).attr('alt');
		var split 	=	details.split('-');
		var ISBN 	=	split[1];
		$.ajax({
		        url: 'services/service-addto-cart.php',
		        method: 'POST',
		        data: {ISBN13:ISBN, action:'delete-cart', invoice:'invoice'},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		                $("#invoice_reload").load(location.href + " #invoice_reload");
		                $("a[title="+ISBN+"]").removeClass('active');
		        }
		        else{
		               $('#error1').show().css("color","#a94442").empty().text(resp.message);
		            } 

		        });
});
});	

$(document).ready(function(){
	$('#search_div').on("change", '#subjects, #author', function() {
		var current_val =	$(this).val();
		var field_id	=	$(this).attr('id');
		var action 		=	"";
		if(field_id=="author")
		{
			var subjects	=	$('#subjects').val();
			action 			=	"author";

		}
		else
		{
			var subjects	=	"";
			action 			=	"subjects";
			var field_id_append	=	"author";
		}
		$.ajax({
		        url: 'services/service-get-books-by-category.php',
		        method: 'POST',
		        data: {current_val:current_val, action:action, field_id:field_id,subjects:subjects},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		                $("#"+field_id_append).empty().append(resp.option);
		        }
		        else{
		               $('#error1').show().css("color","#a94442").empty().text(resp.message);
		            } 

		        });
});
});	
$(document).ready(function(){
	$('#search_div').on("click", '#Filter_Now', function() {
		
		if($('#subjects').val().trim()=="" && $('#author').val().trim()=="")
		{
			alert("Please select Filter your search");
			$('#subjects').focus();
			return false;
		}
		var subjects	=	$('#subjects').val();
		var author		=	$('#author').val();
		var action 		=	"filter";
		$.ajax({
		        url: 'services/service-get-books-by-category.php',
		        method: 'POST',
		        data: {subjects:subjects, author:author, action:action},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		                if(resp.result=="" || resp.result==null)
		                {
		                	$("#booklist_div").empty().append("No result found.");
		                }
		                else
		                {
		                	$("#booklist_div").empty().append(resp.result);
		                }
		                
		        }

		        });
});
});

$(document).ready(function(){
	$('#authcard').on("click", '#order_now', function() {
		var data_order = "order";
		$.ajax({
		        url: 'services/service-place-order.php',
		        method: 'POST',
		        data: {data_order:data_order},
		        dataType: 'json', 
		      }).success(function(resp) {
				if(resp.code==1)
				{
					$("#response").css("color", "#090");
					window.location.href = 'listing.php';
					//$("#invoice_reload").load(location.href + " #invoice_reload");
					$("#response").empty().fadeOut().text(resp.message);
					get_val		=	resp.pdf_creator;
					invoice_id 	=	get_val.split('.');
					invoice_id	=	invoice_id[0];
					window.open("invoice/"+get_val, 'Invoice Details For Invoice ('+invoice_id+') id' , '_blank');
				}
				else
				{
					$("#response").css("color", "red");
					$("#response").empty().show().text(resp.message);
				}
				
				setTimeout(function(){
					$("#response").fadeOut();
				}, 4000);
        });
});
});

</script>