<section>
	<!-- book grids-->
	<div class="row">
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $inn->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" >They Hang: Twelve Women in My 
							 	Portrait Gallery (English) (Paperback)	
							</p> 
							<div class="author-book-trunc">By:Author Name Norman Vincent Peale</div>
							<div class="dashedborder-black" align="center" style="margin-top:10px;">
								<p style="margin-bottom:0px;">
									<i class="fa fa-truck"></i>
									 <span>Delivery in 3-5 days</span>
								</p>
							</div>
						 	<div style="margin-top:10px;"> 
						 		<a class="btn deli-btn btn-danger btn-sm" href="#" type="button">Rent Now</a>
				 				<span style="float:right;"><button class="btn deli-btn btn-danger btn-sm" value=""><i class="fa fa-trash"></i> Delete</button></span>
						 	</div>	
						</div>
					</div>
				</a>	
			</div>
		</div>		 
	</div>
	<!-- book grids-->
</section>