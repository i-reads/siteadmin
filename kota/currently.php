<?php
require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

$user_id 	=	$_SESSION['bookbank_user_id'];
$result 	= 	$kota_ob->get_currently_reading($user_id);
$count  	= 	count($result);

?>
<section>
	<!-- book grids-->
	<div class="row" id="main_div_currently">
		<?php if($count>0) { ?>
		<div class="col-md-12 clearfix" align="right">
			<span align="left">
				<a href="return-request.php" title="Return Book" class="btn btn-danger log-btn  action-btn" type="submit" >Return Book</a>
			</span>
		</div>
		<?php
		}
		
		foreach($result as $info)
		{
			//print_r($info);
		?>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $info->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" ><?php echo ucwords(substr($info->title, 0,15))."....";?></p> 
							<div class="author-book-trunc">By:<?php echo ucwords($info->contributor_name1);?></div>
							<br>
							<label><input type="checkbox" class="return" id="return-<?php echo $info->bookshelf_order_id.'-'.$info->init_pay;?>" alt="return-<?php echo $info->bookshelf_order_id.'-'.$info->init_pay.'-'.$info->title;?>"> Check To Return</label>	
							<p class="order-id" >Status : <span class="status-span">Dispatched</span></p>	
						</div>
					</div>
				</a>	
			</div>
		</div>	
	<?php } ?>
	</div>
	<!-- book grids-->
</section>
<script type="text/javascript">
$(document).ready(function(){
$('#main_div_currently').on("click", '.return', function() {
	field1 = $(this).attr('id');
	field = $(this).attr('alt');
	split = field.split('-');
	boid  = split[1];
	init_pay  = split[2];
	title  = split[3];
	if($('#'+field1).is(':checked'))
	{
		action = "add-return-cart";
		$.ajax({
		        url: 'services/service-return-cart.php',
		        method: 'POST',
		        data: {boid:boid, init_pay:init_pay, title:title, action:action},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		            }
		        });
	}
	else
	{
		action = "delete-return-cart";
		$.ajax({
		        url: 'services/service-return-cart.php',
		        method: 'POST',
		        data: {boid:boid, init_pay:init_pay, action:action},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		            }
		        });
	}
});
});
</script>

