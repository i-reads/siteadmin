<?php
require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

$select_reading = "SELECT bookshelf.*, bookshelf_order_tracking_details.* FROM bookshelf JOIN bookshelf_order_tracking_details On bookshelf_order_tracking_details.bookshelf_order_id=bookshelf.bookshelf_order_id WHERE bookshelf.user_id='$user_id' AND shelf_type=4 group by bookshelf.bookshelf_order_id";
$result = $kota_ob->reading_history($select_reading);

$count  = count($result);
?>
<section>
	<!-- book grids-->
	<div class="row" id="main_div_currently">
		
		<?php		
		foreach($result as $info)
		{
			//print_r($info);
		?>
		<div class="col-md-4 col-sm-4 book-grid">
			<div class="book-box">
				<a class="a-deco" href="#">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-5 no-left-padd">
							<!-- if image present -->
							<!-- <img src="assets/book.png" class="img-responsive book-img-grid static-img"> -->
							<!-- if image not present -->
							<img src="assets/static-2.png" class="img-responsive book-img-grid static-img">
							<h4 class="static-title pull-right"><?php echo strlen('Book title Book title Book title') > 12 ? substr('Book title Book title Book title',0,12).".." : $info->title; ?>
								<br><br><small>Author Name</small>
							</h4>
						</div>
						<div class="col-md-7 col-xs-7 col-sm-7 text-left pad-10 margin-left--8"  >
							 <p class="heading-book" ><?php echo ucwords(substr($info->title, 0,15))."....";?></p> 
							<div class="author-book-trunc">By:<?php echo ucwords($info->contributor_name1);?></div>
							<br>	
							<p>Rent from : <?php echo date("jS F, Y", strtotime($info->new_pickup));  ?> to <?php echo date("jS F, Y", strtotime($info->returned)) ?></p>		
						</div>
					</div>
				</a>	
			</div>
		</div>	
	<?php } ?>
	</div>
	<!-- book grids-->
</section>