<?php 
require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Return Summary</title>
	</head>
	<body style="padding:50px;">
		<div class="row" id="return_main">
			<h3 style="margin-left:15px;">Return Summary</h3>
			<div class="col-md-10">
				<div id="response" style="display:none;font-size:20px;font-weight:800;color:#090;"></div>
				<!-- <div class="authcard fixeddiv" id="authcard" > -->
				<div class="authcard " id="authcard">					
					<div id="invoice_reload">
						<table class="table table-striped">
						 	<thead>
						 		<tr>
						 			<th>Order ID</th>
						 			<th>Order Date</th>
						 			<th>Invoice PDF</th>
						 		</tr>
						 	</thead>
						 	<tbody>
						 	<?php 
						 	$sql     = "SELECT * FROM boobank_invoice WHERE user_id='$user_id'";
							$all_result  = $kota_ob->select_return_cart_data_user($sql);							
						 	if(!empty($all_result))
						 	{
							 	$total = 0;
							 	foreach($all_result as $row)
							 	{	
							 	?>
							 		<tr>
							 			<td><?php echo $row->parent_order_id; ?></td>
							 			<td><?php echo $row->when; ?></td>
							 			<td>
							 				<a href="invoice/<?php echo $row->invoice_url; ?>" target="_blank">View</a>
							 			</td>
						 		<?php }
						 		}
				 		 	?>
						 	</tbody>
						</table>
					</div>
					<hr>
					<div class="" align="right">
						<a class="btn btn-primary" style="background:#bc3526;">Back</a>
					</div>
					

				</div>
			</div>
		</div>
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</body>
</html>