<!DOCTYPE html>
<html>
	<head>
		<title>Bookshelf</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/main.css">
		<link rel="stylesheet" type="text/css" href="assets/bookshelf.css">
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</head>
	<body style="padding:50px;">
		<div class="row">
			<div class="">
				<div class="panel panel-success" align="center">
				  	<!-- Default panel contents -->
				  	<div class="panel-heading" style="font-size:24px;">Bookshelf</div>
				</div>
				<div class="">
				    <!-- Nav tabs -->
				    <ul class="nav nav-pills nav-justified" role="tablist">
				        <!-- <li role="presentation" class="active side-menu">
				            <a href="#available" aria-controls="available" role="tab" data-toggle="tab">Available Books</a>
				        </li> -->
				        <li role="presentation" class="active side-menu">
				            <a href="#currently" aria-controls="currently" role="tab" data-toggle="tab">Currently Reading</a>
				        </li>
				       
				        <li role="presentation" class="side-menu">
				            <a href="#history" aria-controls="history" role="tab" data-toggle="tab">Reading History</a>
				        </li>
				         <li role="presentation" class="side-menu">
				            <a href="#wishlist" aria-controls="wishlist" role="tab" data-toggle="tab">Invoice Details</a>
				        </li>
				    </ul>
				</div>
				<div class="smallmargintop">
				    <div class="tab-content">
				        <!-- <div role="tabpanel" class="tab-pane fade in active" id="available">
				        	<?php //include 'available.php' ?>
				        </div> -->
				        <div role="tabpanel" class="tab-pane fade in active" id="currently">
							<?php include 'currently.php' ?>		        	
				        </div>
				        <div role="tabpanel" class="tab-pane fade in" id="history">
				        	<?php include 'history.php' ?>
				        </div>
				        <div role="tabpanel" class="tab-pane fade in" id="wishlist">
				        	<?php include 'invoice-details.php' ?>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</body>
</html>
