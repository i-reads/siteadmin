<?php 

require_once("config/DB.php");
require_once("config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Return Request</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/main.css">
	</head>
	<body style="padding:50px;">
		<div class="row" id="return_main">
			<h3 style="margin-left:15px;">Return Book</h3>
			<div class="col-md-10">
				<div id="response" style="display:none;font-size:20px;font-weight:800;color:#090;"></div>
				<!-- <div class="authcard fixeddiv" id="authcard" > -->
				<div class="authcard " id="authcard">					
					<div id="invoice_reload">
						<table class="table table-striped">
						 	<thead>
						 		<tr>
						 			<th>Name of the book</th>
						 			<th>Order Date</th>
						 			<th>Initial Pay</th>
						 			<th>Charge</th>
						 			<th>Refund</th>
						 			<th>Action</th>
						 		</tr>
						 	</thead>
						 	<tbody>
						 	<?php 
						 	$sql     = "SELECT return_cart.*, bookshelf_order.* FROM return_cart JOIN bookshelf_order ON return_cart.order_id=bookshelf_order.bookshelf_order_id WHERE return_cart.user_id='$user_id'";
						 	$all_result = $kota_ob->select_return_cart_data_user($sql);
						 	//print_r($all_result);die;
						 	if($all_result>0)
						 	{
							 	$total = 0;
							 	foreach($all_result as $row)
							 	{	
							 		$dt1 = date_create($row->issue_date);
									$dt2 = date_create(date('Y-m-d h:i:s'));
									$diffd = date_diff($dt2,$dt1);
									$diff = $diffd->m;
									if($diffd->d > 0)
									$diff++;
									$mrp = $row->init_pay;
									if($diff>=0 && $diff<=1) { $cost =  (20/100)*$mrp;}
									if($diff>1 && $diff<=2) { $cost =  (25/100)*$mrp;}
									if($diff>2 && $diff<=3) { $cost =  (30/100)*$mrp;}
									if($diff>3 && $diff<=4) { $cost =  (35/100)*$mrp;}
									if($diff>4 && $diff<=6) { $cost =  (40/100)*$mrp;}
									if($diff>6 && $diff<=12) { $cost =  (50/100)*$mrp;}
									$amount_pay = $row->amt_pay+$row->store_pay;
							 	?>
							 		<tr>
							 			<td><?php echo substr($row->title, 0, 20).'....'; ?></td>
							 			<td><?php echo $row->issue_date; ?></td>
							 			<td><?php echo $row->init_pay; ?></td>
							 			<td><?php echo $cost; ?></td>
							 			<td><?php echo $mrp - ceil($cost); ?></td>
							 			<td><?php echo $row->init_pay; ?></td>
							 			<td><p class="btn btn-link" alt="Remove-<?php echo $row->order_id; ?>" title="Remove">Remove</p></td>
							 		</tr>
						 		<?php }
						 		}
				 		 	?>
						 	</tbody>
						</table>
					</div>
					<hr>
					<?php 
					if($all_result>0)
					{
					?>
					<div class="" align="right">
						<button class="btn btn-primary" style="background:#bc3526;" name="return_now1" id="return_now1">Return Now</button>
					</div>
					<?php 
					}
					else
					{
					?>
						<div class="" align="right">
							<a class="btn btn-primary" href="bookshelf.php" style="background:#bc3526;">Add Book</a>
						</div>
					<?php 
					}
					?>

				</div>
			</div>
		</div>
		<script src="assets/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/main.js"></script>
	</body>
</html>
<script type="text/javascript">

$(document).ready(function(){
	$('#invoice_reload').on("click", '.btn-link', function() {

		var details = 	$( this ).attr('alt');
		var split 	=	details.split('-');
		var boid 	=	split[1];
		$.ajax({
		        url: 'services/service-return-cart.php',
		        method: 'POST',
		        data: {boid:boid, action:'delete-return-cart'},
		        dataType: 'json', 
		      }).success(function(resp) {
		      		 if(resp.code==1)
		            {
		                $("#invoice_reload").load(location.href + " #invoice_reload");
		            }
		        });
});
});	

$(document).ready(function(){
	$('#return_main').on("click", '#return_now1', function() {

		var action 		=	"return";
		$.ajax({
		        url: 'services/service-return-book.php',
		        method: 'POST',
		        data: {action:action},
		        dataType: 'json', 
		      }).success(function(resp) {
		            if(resp.code==1)
		            {
		                 //window.location.href = 'return-request.php';
		                 window.location.href = 'return-thankyou.php';
		        	}
		        	else{
		        			$("#response").empty().test("Something went wrong.");
		        	}

		        });
});
});

</script>