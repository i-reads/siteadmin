<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");
$kota_ob    =   new Kota();

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    //Filter POST data for harmful code (sanitize)
    $postArr = array();
    parse_str($_POST['str'], $postArr);
    //print_r($postArr);die;
    $err = '';
    foreach($postArr as $key => $value) 
    {
        $data_filter    = $kota_ob->filter($value);  
        if($data_filter=='invalid inputs')   
        {
            $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Invalid input provided.");
            echo json_encode($jsonResponce);
            die;
        }
        else
        {
            $data[$key]     = $kota_ob->filter($value);
        }                
    }
    $email      = $data['email'];
    $password   = $data['password'];
    $duplicate_sql  =   "select * from user_login where user_email='$email'";

    $result         =   $kota_ob->listing_page($duplicate_sql);

    if(!empty($result))
    {
        $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"User already exists, please use reset password.");
        echo json_encode($jsonResponce);
        die;
    }
    else
    {
        $salt            = $kota_ob->getToken(32);
        $new_pass_hash   = hash("sha256",$password.$salt);
        $RandomString    = $kota_ob->generateRandomString();
        $random_password = Hash('sha256', $RandomString);
        $bookbank_id     = $data['bookbank_id'];
        $sql     = "insert into user_login set user_email='$email', user_password='$new_pass_hash', salt='$salt', hash='$random_password', bookbank_id='$bookbank_id', user_status=1";
        $user_id = $kota_ob->execute_query_with_inserted_id($sql);
        if($user_id)
        {
            ## add records to user_store_credit 

            $user_store_credit = "INSERT INTO user_store_credit SET user_id='$user_id'";
            $kota_ob->execute_query_only($user_store_credit);

            ## add records to user_store_credit_details
            $user_store_credit_details = "INSERT INTO user_store_credit_details SET user_id='$user_id'";
            $kota_ob->execute_query_only($user_store_credit_details);
            
            $name   = explode(" ", $data['name']);
            $fname  = $name[0];
            $lname  = $name[1];
            $query_user_info = "insert into user_info set user_id='$user_id', alternate_email='$email', mobile='$data[phone]', first_name='$fname', last_name='$lname'";
            $kota_ob->execute_query_only($query_user_info);
            $jsonResponce = array( "status" => "Success", "code" => "1", "message"=>"User added successfuly with User ID:- $user_id");
            echo json_encode($jsonResponce);
            die;  
        }
    }
}
?>