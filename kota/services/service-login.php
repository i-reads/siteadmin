<?php
// require_once("../config/database.php");
// require_once("../services/function.php");
require_once("../config/DB.php");
require_once("../config/Kota.php");

$kota_ob    =   new Kota();

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    //Filter POST data for harmful code (sanitize)
    $postArr = array();
    parse_str($_POST['str'], $postArr);
    //print_r($postArr);die;
    $err = '';
    foreach($postArr as $key => $value) 
    {
        $data_filter    = $kota_ob->filter($value);  
        if($data_filter=='invalid inputs')   
        {
            $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Invalid input provided.");
            echo json_encode($jsonResponce);
            die;
        }
        else
        {
            $data[$key]     = $kota_ob->filter($value);
        }                
    }
    $email      = $data['email_log'];
    $user_id    = $data['user_id'];
    $Reso_ID    = $data['Reso_ID'];
    $login_id   =   "";
    if(isset($data['email_log']) && $data['email_log']!="")
    {
        $query      = "select * from user_login where user_email='$email' and user_status in(1,2)";
        $login_id   =   $email;
    }
    else if(isset($data['user_id']) && $data['user_id']!="")
    {
        $query      = "select * from user_login where user_id='$user_id' and user_status in(1,2)";
        $login_id   =   $user_id;
    }
    elseif (isset($data['Reso_ID']) && $data['Reso_ID']!="") 
    {
       $query      = "select * from user_login where bookbank_id='$Reso_ID' and user_status in(1,2)";
       $login_id   =   $Reso_ID;
    }
    $data_rs   =   $kota_ob->check_login($query);
    if(empty($data_rs))
    {
        $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"User doesn't exists.");
        echo json_encode($jsonResponce);
        die;
    }
    else
    {
        $_SESSION['login_id']           =   $login_id;
        $_SESSION['bookbank_user_id']   =   $data_rs[0]->user_id;
        $jsonResponce = array( "status" => "Success", "code" => "1", "message"=>"Success");
        echo json_encode($jsonResponce);
        die;

        // if(checkForPassword($data, $password))
        // {
        //     $_SESSION['login_id']   =   $login_id;
        //     header("location:listing.php");
        //     $jsonResponce = array( "status" => "Success", "code" => "1", "message"=>"Success");
        //     echo json_encode($jsonResponce);
        //     die;
        // }
        // else
        // {
        //     $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Invalid credentials, please try with correct details.");
        //     echo json_encode($jsonResponce);
        //     die;
        // }
    }
}