<?php
//require_once("DB.php");

function filter($data) 
{
	$data = trim(htmlentities(strip_tags($data)));
	if (get_magic_quotes_gpc())
	{
		$data = stripslashes($data);
	}
	$data = mysql_escape_string($data);

	if (strpos(strtolower($data), "union") !== false || strpos(strtolower($data), "select") !== false || strpos(strtolower($data), "update") !== false || strpos(strtolower($data), "delete") !== false) 
	{ 
		$postdata = http_build_query(	array( 'filename' => "unsion_select", 'mobile' => '0', 'name' => '0', 'p1' => $_SERVER['REQUEST_URI'], 'p2' => "1", 'p3' => "1", 'detail' => $data));
		
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		//$data = json_decode(file_get_contents('http://www.indiareads.com/config/errorLog.php', false, $context),true);
		return "invalid inputs";
	}
	else
	{
		return $data;
	}
}
function checkForPassword($data, $input_password)
{
    if($data['new_password'] != null && $data['new_password']!="") 
    {
        if(checkForNewPassword($data['new_password'], $input_password)) 
        {
          
    	}
    } 
    else 
    {
        //print_r($data);die;
        if(checkForOldPassword($data['user_password'], $input_password, $data['salt'])) 
        {
            return true;
        }
    }
    return false;
}
function checkForNewPassword($database_password, $input_password) 
{     
    if(Hash::check($input_password, $database_password)) {
        return true;
    }
    return false;
}

function checkForOldPassword($database_password, $input_password, $salt) 
{
    if(hash("sha256", $input_password.$salt) == $database_password) 
    {
        return true;
    }
    return false;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    for($i=0;$i<$length;$i++)
    {
        $val    = crypto_rand_secure(0,strlen($codeAlphabet));
        $token .= $codeAlphabet[$val];
    }
    return $token;
}
function crypto_rand_secure($min, $max) 
{
    $range = $max - $min;
    if ($range < 0) return $min; // not so random...
    $log = log($range, 2);
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do 
    {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    }
    while($rnd >= $range);
    return $min + $rnd;
}
function generateRandomString($length = 6) 
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function get_user_store_credit()
{

 // $user_id = Input::get('user_id');
    $user_id    =   $_SESSION['bookbank_user_id'];

  //$user_id = Input::get('user_id');
  

    $UserCredit = 0;      
    $selectUserCredit = mysql_query("select * from user_store_credit where user_id='$user_id'");
    $split            = mysql_fetch_assoc($selectUserCredit);             
    if(!empty($selectUserCredit))
    {
      $store_credit = $split['store_credit'];
      $used_credit  = $split['used_credit'];
      if($store_credit !=$used_credit)
      {
        $UserCredit = $store_credit-$used_credit;
      }
    }
      return $UserCredit;
}
function get_user_used_credit($user_id)
{   
    $used_credit = 0;      
    $selectUserCredit = mysql_query("select * from user_store_credit where user_id='$user_id'");
    $selectUserCredit = mysql_fetch_object($selectUserCredit); 
    if(!empty($selectUserCredit))
    {
     
      if(!empty($selectUserCredit->used_credit))
      {
           $used_credit = $selectUserCredit->used_credit;
      }
    }
    return $used_credit;
}
function time_diff($dt1,$dt2){
    $y1 = substr($dt1,0,4);
    $m1 = substr($dt1,5,2);
    $d1 = substr($dt1,8,2);
    $h1 = substr($dt1,11,2);
    $i1 = substr($dt1,14,2);
    $s1 = substr($dt1,17,2);    

    $y2 = substr($dt2,0,4);
    $m2 = substr($dt2,5,2);
    $d2 = substr($dt2,8,2);
    $h2 = substr($dt2,11,2);
    $i2 = substr($dt2,14,2);
    $s2 = substr($dt2,17,2);    

    $r1=date('U',mktime($h1,$i1,$s1,$m1,$d1,$y1));
    $r2=date('U',mktime($h2,$i2,$s2,$m2,$d2,$y2));
    return ($r1-$r2)/(60*60*24*30);

}
function get_parent($oid=null)
{
    if($oid)
    {
        $query = "SELECT p_order_id FROM bookshelf_parent_order WHERE `bookshelf_order_id`= ".$oid."";
        $data = mysql_query($query);  
        $obj = mysql_fetch_object($data);
        if (!empty($obj))
        {
            return $obj->p_order_id;
        }
    }
    return false;         
}
function user_store_credit($user_id=null)
{
    if($user_id)
    {
        $query = "SELECT store_credit FROM user_store_credit WHERE `user_id`= ".$user_id."";
        $data = mysql_query($query);  
        $obj = mysql_fetch_object($data);
        if (!empty($obj->store_credit))
        {
            return $obj->store_credit;
        }
        else
        {
            return 0;
        }
    }
    return false;
}
?>