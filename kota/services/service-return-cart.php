<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($user_id))
{
	if($_POST['action']=='add-return-cart')
	{
		$boid = $kota_ob->check_duplicate_return_cart($_POST['boid']);
		if($boid>0)
		{
			$jsonResponce = array( "status" => "Failed", "code" => "0");
		    echo json_encode($jsonResponce);
		    die;
		}
		else
		{
			$kota_ob->add_return_cart($_POST['boid'], $user_id, $_POST['title']);
		}
	}
	if($_POST['action']=='delete-return-cart')
	{
		$query = "DELETE FROM return_cart WHERE order_id='$_POST[boid]' and user_id='$user_id'";
		$kota_ob->delete_return_cart($query);
	}
	$jsonResponce = array( "status" => "Success", "code" => "1");
    echo json_encode($jsonResponce);
    die;
}