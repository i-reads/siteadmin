<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];


if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($user_id))
{
	$date     = date("Y-m-d H:i:s A");
	$select_ret = "SELECT bookshelf_order.*, return_cart.* FROM return_cart JOIN bookshelf_order On bookshelf_order.bookshelf_order_id= return_cart.order_id WHERE return_cart.user_id='$user_id'";
	$result = $kota_ob->return_cart_update($select_ret);

	//print_r($result);die;
	
	$when			=	date('Y-m-d H:i:s');
	if(!empty($result))
	{
		foreach($result as $info)
		{
			
			$dt1 = date_create($info->issue_date);
			$dt2 = date_create(date('Y-m-d h:i:s'));
			$diffd = date_diff($dt2,$dt1);
			$diff = $diffd->m;
			if($diffd->d > 0)
			$diff++;
			$mrp = $info->init_pay;
			if($diff>=0 && $diff<=1) { $cost =  (20/100)*$mrp;}
			if($diff>1 && $diff<=2) { $cost =  (25/100)*$mrp;}
			if($diff>2 && $diff<=3) { $cost =  (30/100)*$mrp;}
			if($diff>3 && $diff<=4) { $cost =  (35/100)*$mrp;}
			if($diff>4 && $diff<=6) { $cost =  (40/100)*$mrp;}
			if($diff>6 && $diff<=12) { $cost =  (50/100)*$mrp;}
			$get_parent =	$kota_ob->get_parent($info->bookshelf_order_id);


			$refund     =	round($mrp - ceil($cost));
			$user_store_credit = $kota_ob->user_store_credit($user_id);
			// ## insert into user_store_credit_details ##

			$user_store_credit_details = "INSERT INTO user_store_credit_details SET user_id=$user_id, `when`='$when', why_id=8, bookshelf_order_id='$info->bookshelf_order_id', store_credit=$refund, status=1";

			$kota_ob->execute_query_only($user_store_credit_details);

			$user_store_credit = $user_store_credit+$refund;

			$store_credit = "UPDATE  `ireads`.`user_store_credit` SET  `store_credit` =  $user_store_credit WHERE  user_id =$user_id";
			$kota_ob->execute_query_only($store_credit);


            ## END insert into user_store_credit_details ##

            ##update bookshef
            ## update bookshelf_order

			$bookshelf_query 	=	"UPDATE bookshelf SET shelf_type=4 WHERE bookshelf_order_id=$info->bookshelf_order_id and user_id='$user_id'";
			$kota_ob->execute_query_only($bookshelf_query);

            ## update bookshelf_order

			$bookshelf_order 	=	"UPDATE bookshelf_order SET order_status=5, refund=$refund WHERE bookshelf_order_id=$info->bookshelf_order_id";
			$kota_ob->execute_query_only($bookshelf_order);

			## update bookshelf_order_tracking_details
			$bookshelf_order_tracking_details = "UPDATE bookshelf_order_tracking_details SET new_pickup='$when' , returned='$when' where bookshelf_order_id=$info->bookshelf_order_id";

			$kota_ob->execute_query_only($bookshelf_order_tracking_details);

			$ssss = $info->bookshelf_order_id;

		}
		// $delete_return_cart = "DELETE FROM return_cart WHERE user_id='$user_id'";
		// mysql_query($delete_return_cart);

		$jsonResponce = array( "status" => "Success", "code" => "1");
	    echo json_encode($jsonResponce);
	    die;
	}
	else
	{
		$jsonResponce = array( "status" => "Fail", "code" => "1");
	    echo json_encode($jsonResponce);
	    die;
	}
}
?>