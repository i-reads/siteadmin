<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");

$kota_ob    =   new Kota();
if(empty($_SESSION['bookbank_user_id']))
{
    header("location:index.php");
}

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$action        =   $_POST['action'];
    $user_id       =   $_SESSION['bookbank_user_id'];
    if($action=='addto-cart')
    {
        $details 	= 	explode(";", $_POST['details']);
    	$ISBN		=	$details[0];
    	$MRP		=	$details[1];
    	$init_pay	=	$details[2];
        $title      =   $details[3];
    	$author		=	$details[4];
    	$when		=	date('Y-m-d H:i:s');
    	## add to bookshelf table 
    	$check_duplicate_bfs = "SELECT * FROM bookshelf WHERE user_id='$user_id' AND ISBN13='$ISBN' AND shelf_type=3 AND bookshelf_order_id=0 ";
        $result = $kota_ob->listing_page($check_duplicate_bfs);
    	
    	if(empty($result))
    	{
    		$query_bsf = "INSERT INTO  `bookshelf` (`ISBN13`, `when`, `shelf_type`, `title`, `contributor_name1`, `user_id`, `init_pay`, `mrp`) values('$ISBN', '$when', '3', '$title', '$author', '$user_id', '$init_pay', '$init_pay')";
    		$kota_ob->execute_query_only($query_bsf);
    	}
    	## END add to bookshelf table 

    	$check_duplicate 	=	"SELECT * FROM cart where ISBN13='$ISBN' and user_id='$user_id'";
    	$result_duplicate	=	$kota_ob->listing_page($check_duplicate);
    	
    	if(empty($result_duplicate))
    	{
    		$query = "INSERT INTO  `cart` (`user_id`, `when`, `ISBN13`, `mrp`, `initial_pay`, `amt_pay`,`title`,`contributor_name1`) values('$user_id', '$when', '$ISBN', '$MRP', '$init_pay', '$init_pay', '$title', '$author')";
    	}
    	else
    	{
    		$query = "UPDATE cart set `when`='$when'";
    	}
	}
	if($action=='delete-cart')
	{
		if(isset($_POST['invoice']) && $_POST['invoice']!="")
		{
			$ISBN = $_POST['ISBN13'];
		}
		$query = "DELETE FROM cart where ISBN13='$ISBN' AND user_id='$user_id'";
	}

    $result		=	$kota_ob->execute_query_only($query);
    $jsonResponce = array( "status" => "Success", "code" => "1", "message"=>"Success");
    echo json_encode($jsonResponce);
    die;
}