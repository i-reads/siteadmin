<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");

$kota_ob 	=	new Kota();

$user_id =	$_SESSION['bookbank_user_id'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    //Filter POST data for harmful code (sanitize)
    // $postArr = array();
    // parse_str($_POST['str'], $postArr);
    // //print_r($postArr);die;
    $err = '';
    foreach($_POST as $key => $value) 
    {
        $data_filter    = $kota_ob->filter($value);  
        if($data_filter=='invalid inputs')   
        {
            $jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Invalid input provided.");
            echo json_encode($jsonResponce);
            die;
        }
        else
        {
            $data[$key]     = $kota_ob->filter($value);
        }                
    }
    //print_r($data);die;
    if(isset($data['action']) && $data['action']=='subjects')
    {
    	$query  =	"SELECT DISTINCT author FROM books";
    	$result = $kota_ob->listing_page($query);
		$b 		=	'';$b= $b.'<option value="0">'."Narrow by Author".'</option>';
        $a 		=	array();$temp=0;$p=1;$arr=array("."," ","-",";",",");
		foreach($result as $row)
		{

			$a[$temp]=$row->author;
			for($z=0;$z<$temp;$z++)
			{
				$a[$z]	=	str_replace($arr,"",$a[$z]);
				$x		=	$row->author;
				$x		=	str_replace($arr,"",$x);
				$a[$z]	=	strtolower($a[$z]);
				$x		=	strtolower($x);
				$p		=	strcmp($x,$a[$z]);

			}
			if($p!=0)	
			$b = $b.'<option value="'.$row->author.'">'.$row->author.'</option>';	
			$temp++;
		}
		$jsonResponce = array( "status" => "Success", "code" => "1", "option"=>$b);
        echo json_encode($jsonResponce);
        die;

    }
    else if(isset($data['action'])=='filter')
    {
		$cond 	=	"";
		$cond1	=	"";
		if(!empty($data['subjects']))
		{
			$cond = " and category='$data[subjects]'";
		}
		if(!empty($data['author']))
		{
			$cond1 = " and author='$data[author]'";
		}
		$books = "SELECT * FROM books where 1 $cond $cond1";
		$result = $kota_ob->listing_page($books);
		$booklist_div = "";
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$alt 	=	$row->ISBN.';'.$row->mrp.';'.$row->sp.';'.$row->title.';'.$row->author;
				$isbn 	=	$row->ISBN;
				$title 	=	$row->title;
				$mrp 	=	$row->mrp;
				$sp 	=	$row->sp;
				$booklist_div.= "<a href='#' title='$isbn' class='list-group-item' alt='$alt'>
								<p class='inline'>$title</p>
								<p class='inline right'>Rs. <s>$mrp</s>$sp</p>
								</a>";

	    	}
	    }
	    else
	    {
	    	$booklist_div = '';
	    }
    	$jsonResponce = array( "status" => "Success", "code" => "1", "result"=>$booklist_div);
        echo json_encode($jsonResponce);
        die;
    }
    else
    {
    	echo "Invalid inputs";
    }
}

?>