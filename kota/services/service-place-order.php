<?php
require_once("../config/DB.php");
require_once("../config/Kota.php");
require_once("../MPDF57/mpdf.php");

$kota_ob    =   new Kota();

//require_once("pdf-creator.php");

$user_id		=	$_SESSION['bookbank_user_id'];
if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($user_id))
{
	$when			=	date('Y-m-d H:i:s');
	$mode     		=	'';
	$is_paid  		= 	'';
	$status   		= 	'';
	$txnid         	=  	'';
	$response_msg  	=  	'';
	$phone_payu    	=  	'';
	$ordersummarydata1	= 	$kota_ob->listing_page("select * from cart where user_id='$user_id'");
	$ordersummarydata	= 	$kota_ob->listing_page("select * from cart where user_id='$user_id'");
	if(empty($ordersummarydata1))
	{
		$jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Please select a book and place the order.");
	    echo json_encode($jsonResponce);
	    die;
	}
	//print_r($ordersummarydata);die;
	$totalAmount 		= 	$kota_ob->listing_page("select sum(initial_pay) as 'initial_pay', COUNT(*) as 'count' from cart where user_id='$user_id'");
	//print_r($totalAmount);die;
	$count 				=	$totalAmount[0]->count;
	$total_amount		= 	$totalAmount[0]->initial_pay;

    $net_pay 			= 	$kota_ob->listing_page("select sum(amt_pay) as 'amt_pay' from cart where user_id='$user_id'");
    $net_pay			=	$net_pay[0]->amt_pay;
    $availableCredit  	= $kota_ob->get_user_store_credit($user_id);
    if($total_amount<=$availableCredit)
    {
      $availableCredit = $total_amount;
      $net_pay         = 0;
      //$availableCredit = $availableCredit;
    }
    else
    {
      $availableCredit = $availableCredit;
      $net_pay         = $total_amount-$availableCredit;
    } 

	$store_credit_for_bookshelf_order_details  =0;
	$usedCredit 	= 0;
	$check_count  	= 0;
	$added_changes  = 0;
	$poID  			= "";
	$total 			= "";
	$paymentMethod  = '';
	$payment_method = '';
	$price_for_update = '';
	$payment_status   = '';
	$store_pay_total  = '';
	$insert_store_credit = 0;
	//$availableCredit = 500;
	//echo $availableCredit.' , '.$total_amount.'  .  '.$net_pay;die;
	foreach($ordersummarydata as $info)
	{
		//print_r($info);die;
		$initial_pay  = $info->initial_pay;
		$insert_store_credit  =0;
		if($availableCredit>0)
		{
		 
		  $insert_store_credit  = round(($availableCredit-$shiping_minas)*$initial_pay/$total_amount);
		  $store_credit_for_bookshelf_order_details = $store_credit_for_bookshelf_order_details+$insert_store_credit;
		  ## update user_store_credit ##
		  $usedCredit   	= $kota_ob->get_user_used_credit($user_id);
		  $insertUsedCredit = round($usedCredit+$insert_store_credit);
		  ## add shipping to store credit ##
		  $query 	=	"UPDATE user_store_credit SET used_credit='$insertUsedCredit' where user_id='$user_id'";
		  $kota_ob->execute_query_only($query);
		  $store_pay_total  = $store_pay_total+$insert_store_credit;	
		}
		if(!empty($insert_store_credit))
		{
			$amt_pay  = round($initial_pay-$insert_store_credit);
		}
		else
		{
			$amt_pay  = $info->amt_pay;
		}
		$date     			= date("Y-m-d H:i:s A");
		$total    			= $total + $info->initial_pay;
		$payment_status  	= $info->status;
		if($payment_status==1)
		{
			$payment_status = 0;
		}
		else
		{
			$payment_status = 1;
		}
		$ISBN13			=	$info->ISBN13;
		$mrp			=	$info->mrp;
		$payment_option	=	$info->payment_option;
		$contributor_name1 = $info->contributor_name1;

		$bookshelf_order_query = "insert into bookshelf_order SET user_id='$user_id', ISBN13='$ISBN13', mrp='$mrp', init_pay='$initial_pay', store_pay='$insert_store_credit', order_status='4', buy_book_status='$payment_status', amt_pay='$amt_pay', issue_date='$date'";
		
		$query = $kota_ob->execute_query_with_inserted_id($bookshelf_order_query);

		$bookshelf_query = "UPDATE bookshelf SET  ISBN13='$ISBN13', bookshelf_order_id='$query', contributor_name1='$contributor_name1', init_pay='$initial_pay', shelf_type=3, `when`='$date' where user_id='$user_id' and ISBN13='$ISBN13' and bookshelf_order_id=0";
		$kota_ob->execute_query_only($bookshelf_query);

		if(!empty($query))
		{
		  ## prevent multiple entries for bookshelf_order_details ##
		  if($check_count==0)
		  {
		    //echo $info->coupon_code; die;
		    $payment_method  = $info->payment_option;
		    if($payment_method==1)
		    {
		      $payment_status = 0;
		    }
		    else
		    {
		      $payment_status = 1;
		    }
		    //print   $total_amount; exit; 
		    $bookshelf_order_details = "INSERT INTO bookshelf_order_details SET user_id='$user_id', items_count='$count', order_date='$date', price='$total_amount', payment_option='$payment_option', net_pay='$net_pay'";
		   	$poID = $kota_ob->execute_query_with_inserted_id($bookshelf_order_details);
		    
		  }
		$bookshelf_parent_order = "INSERT INTO bookshelf_parent_order SET p_order_id='$poID', bookshelf_order_id='$query'";
		$bookshelf_order_id = $kota_ob->execute_query_with_inserted_id($bookshelf_parent_order);

		  ## insert into user_store_credit_details ##
		  if($insert_store_credit>0)
		  {
			$user_store_credit_details = "INSERT INTO user_store_credit_details SET user_id='$user_id', when='$date', why_id=4, bookshelf_order_id='$query', store_credit='$insert_store_credit', status=2";
			$kota_ob->execute_query_only($user_store_credit_details);
		  }
		  ## END insert into user_store_credit_details ##

		  $bookshelf_order_tracking_details = "INSERT INTO bookshelf_order_tracking_details SET bookshelf_order_id='$query', new_delivery='$date'";
		  $kota_ob->execute_query_only($bookshelf_order_tracking_details);
		  
		}
		$check_count++;
	}
	## update bookshelf_order_details##

	$pdf_creator = $kota_ob->pdf_creator($user_id, $poID);

	$update_tbl = "UPDATE bookshelf_order_details SET store_discount= '$store_pay_total', invoice='$pdf_creator' WHERE p_order_id='$poID' and user_id='$user_id'"; 
	$kota_ob->execute_query_only($update_tbl);
	
	## update bookshelf_order_details##
	
	$empty_cart = "DELETE FROM cart where user_id='$user_id'";
	$kota_ob->execute_query_only($empty_cart);

	$jsonResponce = array( "status" => "Success", "code" => "1", "message"=>"Order has been placed", "pdf_creator"=>$pdf_creator);
    echo json_encode($jsonResponce);
    die;
	
   // echo $availableCredit;
}
else
{
	$jsonResponce = array( "status" => "Failed", "code" => "0", "message"=>"Invalid request.");
    echo json_encode($jsonResponce);
    die;
}     
?>