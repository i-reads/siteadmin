<?php

include_once('../config/config.php');

include_once('../config/functions.php');


$con = connect($config);
$fun_obj = new ireads($con);

include_once('../rent-request/pagination2.php');

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
</head>

<body>
<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>

	 <?php 
    $page_class='more';  
    include_once('../headers/main-header.php');
  ?>
  <div class="container">
		<nav class="sub-head navbar navbar-default">
  <div class="container-fluid">
 
    <ul class="nav navbar-nav">
      <li ><a href="pending-pickup.php">Retail</a></li>
      <li class="active"><a href="corporate-pending-pickup.php">Corporate</a></li>
    </ul>
  </div>
</nav> 
<div class="margintop">
	<form role="form" class="col-md-7 form-inline" method="post">
		<div class="form-group">
		  <label for="date_start">Start Date:</label>
		  <input type="date" class="form-control" name="date_start" id="date_start">
		</div>
		<div class="form-group">
		  <label for="date_end">End Date:</label>
		  <input type="date" class="form-control" name="date_end" id="date_end">
		</div>
	  <input type="submit" name="get_pickup" class="btn btn-danger iread-btn iread-btn-white" value="Get Orders"/>
	</form>
 
      <form class="form-horizontal col-md-5" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label col-sm-3" for="cor_name">Select Corporate:</label>
              <div class="col-sm-5 no-left-pad">
                  <select required class="form-control"  name="cor_name" id="cor_name" >
                   <?php 
                      $res_cor = $fun_obj->get_all_corporates();
                      foreach ($res_cor as $key1) {
                          echo '<option value="'.$key1['company_id'].'">'.$key1['company_name'].'</option>'; 
                      }
                       echo '<option selected>'.$cor_name_v.'</option>'; 
                    ?>
                  </select>
                     
              </div>
              <div class="col-sm-4 no-left-pad">
                  <input type="submit" name="get_pickup1" class="btn btn-danger iread-btn iread-btn-white" value="Submit"/>
                   
              </div>
            </div>
          </div>
      </form> 
	<br> <button class="text-center" id="pi_btn_one">Generate Pickingo Sheet</button> <button class="text-center" id="fedex_one_btn">Generate FedEx Sheet</button> 
  <table class="table table-bordered table-condensed font-new-del margin-top-40">
    <thead>
      <tr>
        <th>Parent Order</th>
        <th>Order id</th>
        <th>User</th>
        <th>Order Date</th>
        <th>Pickup Date</th>
        <th>Issue Date</th>
        <th>Pickup Address</th>
        <th>Price Details</th>
        <th>Sub-Order Price Details</th>
        <th>Payment Mode and Type</th>
        <th>Book Details</th>
        <th>Assigned Book id</th>
        <th>Carrier Details</th>
        <th>Comment</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
   <?php 
   if(isset($_POST['get_pickup']) || isset($_POST['get_pickup1'])){
    if(@$_POST['date_start']!='' && @$_POST['date_end']!=''){
       		echo '<h4 class="text-right">Pending Pickups from '.$_POST['date_start'].' to '.$_POST['date_end'].'</h4>';
    		$Order = $fun_obj->get_bookshelf_order_pending_pickup_corp($_POST['date_start'],$_POST['date_end']);
    }
    if(isset($_POST['cor_name'])){
         $Order = $fun_obj->get_bookshelf_order_pending_pickup_corp_comp($_POST['cor_name']);
  
    }

        $record_count   = count($Order);
         $fedex_one_btn_orders='';
         $fedex_one_btn_orders_pi='';
        foreach ($Order as $key2 ) {
          $check_pincode = $fun_obj->check_pincode_pickingo($key2['pincode']);
          if($check_pincode){
            $fedex_one_btn_orders_pi.=$key2['bookshelf_order_id'].',';
          }else{
             $fedex_one_btn_orders.=$key2['bookshelf_order_id'].',';
          }
           

        }
        // print_r($fedex_one_btn_orders_pi);
     

        echo '<h4 class="text-right">'.$record_count.' pickups are pending</h4><br>';
    		
    	   if($Order) {
    	   foreach ($Order as $key) {
    	     $check_pincode = $fun_obj->check_pincode_pickingo($key['pincode']);

    	   ?>
    	      <tr>
    	      	 
    	        <td><?php echo $key['p_order_id']; ?></td>
    	        <td><?php echo $key['bookshelf_order_id']; ?> <input type="checkbox" name="sub_order[]" value="<?php echo $key['bookshelf_order_id']; ?>">
    	            <?php
    	             if($check_pincode){
    	              echo "Pickingo";
    	             }
    	            ?>
    	        </td>
    	        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
    	        <td><p><?php echo $key['order_date']; ?></p></td>

    	        <td><p><?php echo $key['new_pickup']; ?></p></td>

    	        <td><p><?php echo $key['issue_date']; ?></p></td>
    	        <td><p><?php echo $key['fullname']; ?></p><p><?php echo $key['address_line1']; ?>,</p><p><?php echo $key['address_line2']; ?>, <?php echo $key['city']; ?></p><p><p><?php echo $key['state'] ?> <span><?php echo $key['pincode']; ?></span></p><p id="p7"><?php echo $key['phone']; ?></p></td>
    	        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
    	        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
    	        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
    	        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
    	        <td><?php echo $key['unique_book_id']; ?></a>
    	 
    	          </td>
    	          <td>
    	            <?php
    	              if($key['carrier'] || $key['d_track_id']){
    	                 echo "Carrier:".$key['carrier'];
    	                 echo "<br>Track Id:".$key['d_track_id']."<br>";
    	                 ?>
    	                 <br>
    	                <a  class="pointer-a edit_carrier  ">Edit Carrier</a><br>
    	                
    	                
    	                 <form class="form" role="form" style="display:none;">
    	                    <div class="form-group carrier-select-form">
    	                      <label for="sel1">Select Carrier:</label>
    	                      <select class="form-control" id="sel1">
    	                      <?php
    	                       if($check_pincode){
    	                        ?>
    	                        <option>Pickingo</option>
    	                      <?php
    	                       }
    	                      ?>
    	                        <option>FedEx</option>
    	                        <option>Pickrr</option>
    	                        <option>GoJavas</option>
    	                        <option>Delhivery</option>
    	                        <option>Speed Post</option>
    	                        <option>Self-Logistics</option>
    	                      </select>
    	                    </div>
    	                    <input type="text" name="tracking_id" id="tracking_id" class="track_input" placeholder="Enter Tracking ID">
    	                   
    	                    <input type="hidden" id="parent" value="<?php echo $key['p_order_id'];  ?>">
    	                    <input type="button" value="Process" name="process" class="btn iread-btn iread-btn-white small-process-btn">
    	                  </form> 
    	                 <?php
    	              }
    	              else{
    	              ?>
    	                <form class="form" role="form">
    	                  <div class="form-group carrier-select-form">
    	                    <label for="sel1">Select Carrier:</label>
    	                    <select class="form-control" id="sel1">
    	                      <?php
    	                       if($check_pincode){
    	                        ?>
    	                        <option>Pickingo</option>
    	                      <?php
    	                       }
    	                      ?>
    	                      <option>FedEx</option>
    	                      <option>Pickrr</option>
    	                      <option>GoJavas</option>
    	                      <option>Delhivery</option>
    	                      <option>Speed Post</option>
    	                      <option>Self-Logistics</option>
    	                    </select>
    	                  </div>
    	                  <input type="text" name="tracking_id" id="tracking_id" class="track_input" placeholder="Enter Tracking ID">
    	                   
    	                  <input type="hidden" id="parent" value="<?php echo $key['p_order_id']; ?>">
    	              
    	                  <input type="button" value="Process" name="process" class="btn iread-btn iread-btn-white small-process-btn">
    	                </form> 
    	              <?php 
    	              }?>
    	        </td>
    	        <td>
    		        <div class="form-group">
    	               <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['pickup_comments']; ?></textarea>
    	                <input type="hidden" id="order_id_cmmnt" value="<?php echo $key['bookshelf_order_id']; ?>">
    	                <a class="btn iread-btn iread-btn-white small-save-btn save_comment">Save</a> 
    	                <div id="loader1" class="sp sp-circle loader"></div>
    	            </div>    
    			    </td>
    	        <td> 
    	        	<form  method="POST" role="form" class="margintop">
    	                <input  class="btn btn-danger iread-btn action-btn-2 fedex" name="fedex" value="Fedex Pickup">
    	               <?php
    	                 if($check_pincode){
    	                  ?>
    	                   <input type="submit" class="btn btn-danger iread-btn action-btn-2 margintop pickingo" name="pickingo" value="Pickingo Pickup">
    	               <?php   
    	                 }
    	               ?>
    	               
    	        	</form>
    	        </td>
    	      </tr>

    	      <?php } }
         
    }?>
      	
      
    </tbody>
  </table>

</div>
	</div>
  <?php
/*for pagination*/
$total=$fun_obj->get_bookshelf_order_2_page(6);
include_once('../rent-request/pagination.php');
?>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(8000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});
/*script for  fedex sheet*/
$(".pickingo").click(function() {
    $('.overlay1').show();
       var selected_orders = new Array();
      var n = $("input[type=checkbox]:checked").length;
      if (n > 0){
          $("input[type=checkbox]:checked").each(function(){
              selected_orders.push($(this).val());
          });}
 
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/corporate/ajax/generate_sheet_pickingo.php',
            data: { 
                    selected_orders : selected_orders
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
/*script for  fedex sheet*/
$(".fedex").click(function() {
    $('.overlay1').show();
       var selected_orders = new Array();
      var n = $("input[type=checkbox]:checked").length;
      if (n > 0){
          $("input[type=checkbox]:checked").each(function(){
              selected_orders.push($(this).val());
          });}
 
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/corporate/ajax/generate_sheet.php',
            data: { 
                    selected_orders : selected_orders
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
 

/*script for save comment*/
$(".save_comment").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     var textval=$(this).siblings('textarea').val();
     var order=$(this).siblings('#order_id_cmmnt').val();
     var html='';
     $.ajax({
            type: "POST",

            url: '<?php echo base_url; ?>/corporate/ajax/comment_save_pickup.php',
            data: { textval : textval,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
              $('.loader').hide(); 
           
              html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
              $('body').append(html);
               $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                 $("#i-alert").alert('close');
               });
            }
     });
     return false; 
});
 


// fedex with one btn
$("#fedex_one_btn").click(function() {
    $('.overlay1').show();
   
       // var selected_orders = new Array();
         var  selected_orders= "<?php echo @$fedex_one_btn_orders; ?>";
         var array_Selected = selected_orders.split(',');
         // alert(array_Selected[1]);
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/corporate/ajax/generate_sheet.php',
            data: { 
                    selected_orders : array_Selected
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
$("#pi_btn_one").click(function() {
    $('.overlay1').show();
        var  selected_orders= "<?php echo @$fedex_one_btn_orders_pi; ?>";
         var array_Selected = selected_orders.split(',');
 
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/corporate/ajax/generate_sheet_pickingo.php',
            data: { 
                    selected_orders : array_Selected
                  },
            dataType: 'json',
            success: function(res){ 
                var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","Carrier sheet is generated");
                      window.location.reload();
                   }
            }
     });
     return false; 
});
</script>
<script type="text/javascript">
  $('.edit_carrier').on('click',function(event) {
    $(this).siblings('form').toggle('fast'); 
  });
</script>
	</body>
</html>