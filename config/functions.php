    <?php 
        class Ireads
        {
            
            private $con, $dt, $date_interval;
            
            public function __construct($connection) {
                $this->con = $connection;

                date_default_timezone_set('Asia/Calcutta');
                $this->dt = '2016-02-16';//date('Y-m-d H:i:s');
                $this->date_diff = 2;
                $this->date_limit = "2015-02-01";
                $this->date_interval = 'P'.date('m').'M'.'';

            }

            public $i_msg="";
            function query($query, $bindings, $conn)
            {
                $stmt = $conn->prepare($query);
                $stmt->execute($bindings);

                $results = $stmt->fetchAll();

                return $results ? $results : false;
            }
            function query_update($query, $bindings, $con){
                if($this->con->query($query)) {
                    return true;  
                }
                return false;
            }   
            function mysqli_insert_id()
            {
                return mysqli_insert_id($this->con);
            }  
 
     /*----------------------------------RITESH-----------------------*/
 /*--------------------------------------rent request retail functions----------------------------------*/
     /*-------------------------- ----new delivery--------- ------------------------*/

     /*function for mail to get order*/
        public function get_bookshelf_order($order_id){
            $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.bookshelf_order_id='$order_id'"; 
            $result = $this->query($query, array(), $this->con);
            
            return $result;
        }
         public function get_bookshelf_order_pickup($order_id){
            $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.bookshelf_order_id='$order_id'"; 
            $result = $this->query($query, array(), $this->con);
            
            return $result;
        }
        public function quoteIdent($field) {
      return "`".str_replace("`","``",$field)."`";
  }


                
            /*get all new deliveries ie order status=1*/
        public function get_bookshelf_order_1_page($status){
            $query  ="SELECT count(bookshelf.bookshelf_order_id) as rows FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc
            ";
             $result = $this->query($query, array(), $this->con);
                return $result;
            }
            public function get_bookshelf_order_2_page($status){
            $query  ="SELECT count(bookshelf.bookshelf_order_id) as rows FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc
            ";
            $result = $this->query($query, array(), $this->con);
                return $result;


        }
            public function get_bookshelf_order_1($number,$perpage){
                $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=1 order by bo_details.order_date desc limit $number,$perpage"; 
                $result = $this->query($query, array(), $this->con);
                
                return $result;
            }

            /*get book details from book_library_new */
            public function get_shelf_location($bookid){
                $query2="SELECT * FROM book_library_new WHERE book_library_new.book_id='$bookid'";
                $result2 = $this->query($query2, array(), $this->con); 
                return $result2;
            }
            /*  function to display payment mode from mode value*/
            public function get_payment_mode($mode){
                if($mode==1){
                    return "COD";
                }
                elseif ($mode==2) {
                    return "Online Payment";
                }
                elseif ($mode==3) {
                    return "Paytm Wallet";
                }
                elseif ($mode==4) {
                    return "Pay u Money";
                }
                elseif($mode==5){
                    return "Store Credit";
                }
                 elseif($mode==6){
                    return "Paid Corporate";
                }
                else{
                    return "error";
                }
            }
            /*  function to display payment status from status value*/
            public function get_payment_status($status){
                if($status==0){
                    return "COD/Not Paid";
                }
                elseif($status==1){
                    return "Online/Paid";
                }
                else{
                    return "Failure";
                }

            }
            /*function to display order status from order_status value*/
            public function get_order_status($order_status){
                if($order_status==1){
                    return "New Delivery";
                }
                elseif ($order_status==2) {
                     return "Delivery Processing";
                }
                elseif ($order_status==3) {
                     return "Dispatched";
                }
                elseif ($order_status==4) {
                     return "Delivered";
                }
                elseif ($order_status==5) {
                     return "New Pickup";
                }
                elseif ($order_status==6) {
                     return "Pickup Processing";
                }
                elseif ($order_status==7) {
                     return "Sent to Fedex";
                }
                elseif ($order_status==8) {
                     return "Returned";
                }
                elseif ($order_status==-1) {
                     return "Deleted";
                }
                elseif ($order_status==0) {
                     return "Pending";
                }
                else{
                    return "error";
                }
            }
            /*function to update address */
            public function update_address_delivery($fullname,$address_line1,$address_line2,$city,$state,$pincode,$phone,$addr_id){
                $query = "UPDATE user_address_book SET fullname='$fullname',address_line1='$address_line1',address_line2='$address_line2',city='$city',state='$state',pincode='$pincode',phone='$phone' WHERE address_book_id='$addr_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /* save order comment*/
            public function save_order_comment($textval,$order){
                $query = "UPDATE bookshelf_order SET order_comments='$textval' WHERE  bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*function to get book library id for an isbn,it will return all book ids except the parameter book_id and circulation is not same as locked book*/
            public function get_all_book_library_id($ISBN13,$book_id,$circ){
                $query1 = "SELECT * FROM book_library_new WHERE ISBN13 ='$ISBN13' AND book_id !='$book_id' AND status=1 AND circulation !='$circ'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*function to get book library id for an isbn,it will return all book ids except the parameter book_id and where circulation is same as locked book*/
            public function get_all_book_library_id_same_circulation($ISBN13,$book_id,$circulation){
                $query1 = "SELECT * FROM book_library_new WHERE ISBN13 ='$ISBN13' AND book_id !='$book_id' AND status=1 AND circulation='$circulation'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /* get all books with status 1 in book_library_new for one isbn*/
            public function get_all_book_library_id_isbn($ISBN13){
                $query1 = "SELECT * FROM book_library_new WHERE  status=1 AND ISBN13 ='$ISBN13'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*change locked book */
            public function change_locked_book($book_id,$order,$cur_book_id){
                $query1 = "UPDATE book_library_new,bookshelf_order
                            SET book_library_new.status = (case when book_library_new.book_id = '$book_id' then '2'
                                                                when book_library_new.book_id = '$cur_book_id' then '1'
                                                            end),
                                bookshelf_order.unique_book_id='$book_id'
                            WHERE book_library_new.book_id in ('$book_id', '$cur_book_id') AND bookshelf_order.bookshelf_order_id='$order'";
                $result1 = $this->query_update($query1, array(), $this->con);
                return $result1;
            }
            /*change status to 2 for delivery processing*/
            public function update_order_status_2($order_id){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =2,bookshelf_order_tracking_details.delivery_processing='$date_today' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*change status to -1 for delete*/
            public function update_order_status_1del($order_id,$book_id,$user_id,$ISBN13,$buy){
               $query='';
               if($buy==1){
                    $query = "UPDATE bookshelf_order SET order_status =-1 WHERE bookshelf_order_id='$order_id'";
               }
               else{
                    $query = "UPDATE bookshelf_order,bookshelf SET bookshelf_order.order_status =-1,bookshelf.shelf_type=1 WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf.user_id='$user_id' AND bookshelf.ISBN13='$ISBN13' AND bookshelf.bookshelf_order_id='$order_id'";
                }

                $result = $this->query_update($query,array(),$this->con);
                 
                if($book_id){
                    $query1 = "UPDATE book_library_new SET status =1 WHERE book_id='$book_id'";
                    $result1 = $this->query_update($query1,array(),$this->con);
                }
                 
                return $result;
            }
            /*function to get merchant when isbn is given,*/
            public function get_merchant_with_isbn($isbn){
                $query1 = "SELECT md.merchant_name,ml.supply_price,md.merchant_discount,md.merchant_phone FROM merchant_library ml join merchant_details md on ml.mid=md.merchant_id WHERE ISBN ='$isbn'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*update store credit on cancel order*/
            public function update_refund_oncancel($user,$refund,$order_id,$shipping,$parent){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query1 = "SELECT store_credit from user_store_credit where user_id = '$user'";
                $sc = $this->query($query1,array(),$this->con);
                $credits = $sc[0]['store_credit'];
                $credits += $refund;
                $query = "UPDATE user_store_credit,bookshelf_order,bookshelf_order_details SET bookshelf_order.refund='$refund',bookshelf_order.refund_type=11,user_store_credit.store_credit='$credits',bookshelf_order_details.shipping_charge='$shipping' WHERE bookshelf_order_details.p_order_id='$parent' AND user_store_credit.user_id='$user' AND bookshelf_order.bookshelf_order_id='$order_id'";
                $result1 = $this->query_update($query,array(),$this->con);
                if($result1){
                    $query2 = "INSERT INTO user_store_credit_details (`user_id`,`when`,`why_id`,`bookshelf_order_id`,`store_credit`,`status`) VALUES ('" .$user. "','" .$date_today. "','11','" .$order_id. "','" .$refund. "','2')";
                    $result2 = $this->query_update($query2, array(), $this->con);
                }
                return $result1;
            }
    /*----------- ----delivery processing functions--------- --------*/
            /*get all delivery processing where order status=2*/
            public function get_bookshelf_order_2($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=2 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*change status to 3 for dispatched*/
            public function update_order_status_3($order_id,$date_today){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =3,bookshelf_order.issue_date ='$date_today',bookshelf_order_tracking_details.dispatched='$date_today' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /* change status to 1 for new delivery */
            public function update_order_status_1($order_id){
                $query = "UPDATE bookshelf_order SET order_status =1 WHERE bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*function to save carrier name and tracking id for an order on process button */
            public function update_carrier_track_id($carrier,$track_id,$order){
                $query = "UPDATE bookshelf_order SET carrier ='$carrier',d_track_id='$track_id' WHERE bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*get all books with given isbn from book_library_new*/
            public function get_all_book_library_id_same_isbn($ISBN13){
                $query1 = "SELECT * FROM book_library_new WHERE ISBN13 ='$ISBN13' AND status=1";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*mark book lost as status -1,unassign the unique id for bookshelf_order for an order*/
           public function mark_book_lost_unique_book_unassign($book_id,$order){
                $query = "UPDATE bookshelf_order,book_library_new SET bookshelf_order.unique_book_id ='',book_library_new.status='-1' WHERE bookshelf_order.bookshelf_order_id='$order' AND book_library_new.book_id='$book_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
           }
           /*assign a new book,when book lost button is pressed*/
           public function lock_book_assign_unique_id_on_lost($book_id,$order){
                $query = "UPDATE bookshelf_order,book_library_new SET bookshelf_order.unique_book_id ='$book_id',book_library_new.status='2' WHERE bookshelf_order.bookshelf_order_id='$order' AND book_library_new.book_id='$book_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
           }
           /*get parent order for given order,function is used in carrier save file for checking if orders are from same parent order*/
            public function get_parent_order_for_every_order($order){
                $query1 = "SELECT p_order_id FROM bookshelf_parent_order WHERE bookshelf_order_id ='$order'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*get address from user address book.with address id*/
            public function get_address($address_id){
                $query1 = "SELECT * FROM user_address_book WHERE address_book_id ='$address_id'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*get order isbn with order id*/
            public function get_book_isbn_bookshelf_order($order){
                $query1 = "SELECT ISBN13,init_pay,mrp FROM bookshelf_order WHERE bookshelf_order_id ='$order'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*get title from submastersheet*/
             public function get_title_submastersheet($ISBN13){
                 $query1 = "SELECT title FROM sub_mastersheet WHERE ISBN13 = '".$ISBN13."'";
                 $result = $this->query($query1, array(), $this->con);
                 return $result;
            }
            /*get user email by userid*/
            public function user_email($user_id) {
                $query = "SELECT user_email FROM user_login WHERE user_id = '" . $user_id. "'";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /* insert dispatch and invoice table*/
            public function update_dispatch_invoice($parent,$selected_orders,$prefix_name,$inv){
               if($this->get_dispatch_slip_name($parent,$selected_orders)){
                      $query1 = "UPDATE dispatch_invoice SET dispatch ='$prefix_name',invoice = '$inv' WHERE parent_order_id = '" . $parent. "' AND bookshelf_order_id='$selected_orders'";
                      $result1 = $this->query_update($query1,array(),$this->con);
               }
               else{
                    $query = "INSERT INTO dispatch_invoice(`parent_order_id`,`bookshelf_order_id`,`dispatch`,`invoice`) VALUES ('$parent','$selected_orders','$prefix_name','$inv')";
                    $result = $this->query_update($query, array(), $this->con);
                    $query1="SELECT LAST_INSERT_ID() from dispatch_invoice";
                    $result1 = $this->query($query1, array(), $this->con);
                }
                return $result1;
            }
            /* get dispatch path name from dispatch-invoice*/
            public function get_dispatch_slip_name($parent,$order){
                $query = "SELECT dispatch,invoice FROM dispatch_invoice WHERE parent_order_id = '" . $parent. "' AND bookshelf_order_id='$order'";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*check if parent is present or not,function is used in invoice shiiping cases*/
            public function get_parent_dispatch($parent){
                $query = "SELECT invoice FROM dispatch_invoice WHERE parent_order_id = '" . $parent. "'";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            public function get_delete_slip_name($parent,$order){
                $query = "DELETE FROM dispatch_invoice WHERE parent_order_id = '" . $parent. "' AND bookshelf_order_id='$order'";
                $result = $this->query_update($query, array(), $this->con);
                return $result;
            }
    /*----------------------------- ----dispatched functions--------- -------------------*/
            public function get_bookshelf_order_3($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=3 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*change status to 4 for delivered*/
            public function update_order_status_4($order_id,$pick_address_id,$order_address_id,$parent){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =4,bookshelf_order_tracking_details.delivered='$date_today' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                if($pick_address_id==0){
                    $query1 = "UPDATE bookshelf_order_details SET pick_address_id ='$order_address_id' WHERE p_order_id='$parent'";
                    $result1 = $this->query_update($query1,array(),$this->con);
                }
                return $result;
            }
    /*----------- ----delivered functions--------- --------*/
            public function get_bookshelf_order_4($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=4 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
         // public function get_bookshelf_order_4(){
         //        $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=4 order by bo_details.order_date desc";
         //        $result = $this->query($query, array(), $this->con);
         //        return $result;
         //    }
            /*change status to 5 for generate pickup*/
            public function update_order_status_5($order_id){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =5,bookshelf_order_tracking_details.new_pickup='$date_today' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
     /*----------- ----new pickup requests functions--------- --------*/
            public function get_bookshelf_order_5($number,$perpage){
                $query = "SELECT bookshelf.*,b_track_details.new_pickup, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status=5 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*change status to 6 for processing pickup*/
            public function update_order_status_6($order_id){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa"); 
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =6,bookshelf_order_tracking_details.pickup_processing='$date_today',bookshelf_order.carrier='',bookshelf_order.d_track_id='' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /* save pickup comment*/
            public function save_pickup_comment($textval,$order){
                $query = "UPDATE bookshelf_order SET pickup_comments='$textval' WHERE  bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
    /*----------- ----new pickup requests functions--------- --------*/
            public function get_bookshelf_order_6($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=6 order by bo_details.order_date desc limit $number,$perpage
                ";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*check pincode for pickingo*/
            public function check_pincode_pickingo($pincode){
                $pin= "SELECT * FROM pincodes WHERE vendor_id=105 AND pickup=1 AND pincode='$pincode'";
                $result = $this->query($pin, array(), $this->con);
                return $result;
            }
            /*change status to 7 for fedex*/
            public function update_order_status_7($order_id){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE bookshelf_order,bookshelf_order_tracking_details SET bookshelf_order.order_status =7,bookshelf_order_tracking_details.req_to_fedex='$date_today' WHERE bookshelf_order.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
    /*----------- ----sent to fedex functions--------- --------*/
            public function get_bookshelf_order_7($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email,b_track_details.new_pickup, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status=7 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }    
     // public function get_bookshelf_order_7(){
     //            $query = "SELECT bookshelf.*, user.user_email,b_track_details.new_pickup, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status=7 order by bo_details.order_date desc ";
     //            $result = $this->query($query, array(), $this->con);
     //            return $result;
     //        }
            /*change status to 8 for returned*/
            public function update_order_status_8($order_id,$book_status,$bookid,$user_id,$isbn,$refund,$buy){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $resbook=$this->get_shelf_location($bookid);
                $circulation=$resbook[0]['circulation'];
                $circulation +=1;
                 
                $query1 = "SELECT store_credit from user_store_credit where user_store_credit.user_id = '$user_id'";
                $sc = $this->query($query1,array(),$this->con);
                $credits = $sc[0]['store_credit'];
                $credits += $refund;
                $result='';
                // to be removed
                if($buy==1){
                     $query = "UPDATE bookshelf_order,book_library_new,bookshelf_order_tracking_details,user_store_credit,user_store_credit_details SET user_store_credit.store_credit = '$credits',bookshelf_order.order_status =8,book_library_new.status='$book_status',bookshelf_order_tracking_details.returned='$date_today',user_store_credit_details.status=2 WHERE user_store_credit_details.user_id='$user_id' AND user_store_credit_details.why_id=8 AND user_store_credit_details.bookshelf_order_id='$order_id' AND bookshelf_order.bookshelf_order_id='$order_id' AND book_library_new.book_id='$bookid' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id' AND user_store_credit.user_id = '$user_id'";
                    $result = $this->query_update($query,array(),$this->con);
                }
                else{
                $query = "UPDATE bookshelf_order,book_library_new,bookshelf,bookshelf_order_tracking_details,user_store_credit,user_store_credit_details SET user_store_credit.store_credit = '$credits',bookshelf_order.order_status =8,book_library_new.status='$book_status',book_library_new.circulation='$circulation',bookshelf.shelf_type=4,bookshelf_order_tracking_details.returned='$date_today',user_store_credit_details.status=2 WHERE user_store_credit_details.user_id='$user_id' AND user_store_credit_details.why_id=8 AND user_store_credit_details.bookshelf_order_id='$order_id' AND bookshelf_order.bookshelf_order_id='$order_id' AND book_library_new.book_id='$bookid' AND bookshelf.user_id='$user_id' AND bookshelf.ISBN13='$isbn' AND bookshelf.bookshelf_order_id='$order_id' AND bookshelf_order_tracking_details.bookshelf_order_id='$order_id' AND user_store_credit.user_id = '$user_id'";
                $result = $this->query_update($query,array(),$this->con);
             }
                return $result;
            }
    /*----------- ----returned functions--------- --------*/
            public function get_bookshelf_order_8($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=8 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
     /*----------- ----deleted functions--------- --------*/
            public function get_bookshelf_order_1del($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=-1 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
    /*----------- ----incomplete requests functions--------- --------*/
            public function get_bookshelf_order_0($number,$perpage){
                $query = "SELECT bookshelf.*, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status=0 order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }


          // All category function  

             public function get_supper_cat(){
                $sql = "select * from super_cats";
                $result = $this->query($sql, array(), $this->con);
                return $result;

             }   
             public function get_parent_cat($supper_cat_id){
                $sql = "select parent_id, category from parent_cats_new where super_cat_id = $supper_cat_id";
                $result = $this->query($sql, array(), $this->con);
                return $result;
             }
             public function get_cat1($parent_cat_id){

                $sql = "select cat1_id, category from cats_level1_new where parent_id = $parent_cat_id";
                $result = $this->query($sql, array(), $this->con);
                return $result;
             }

             public function get_cat2($cat1_id){

                $sql = "select cat2_id, category from cats_level2_new where cat1_id = $cat1_id";

                $result = $this->query($sql, array(), $this->con);
                return $result;
             }






/*-----------------------Add book functions---------------------------------------*/
                    /*-------------add to submastersheet---------------*/
            public function add_to_sub_mastersheet($ISBN13,$title,$Author,$ProductForm,$PublisherName,$PublicationPlace,$PublicationCountry,$PublishingStatus,$Price,$TextLaunguage,$ShortDescription,$LongDescription,$AboutAuthor,$img_flag,$save_cat_in_submaster,$parent_cat){
                $if_isbn3 = $this->get_isbn_submastersheet($ISBN13);
                if(!empty($if_isbn3)){
                }
                else{
                    $query = "INSERT INTO sub_mastersheet (`ISBN13`,`title`,`product_form`,`publisher_name`,`publication_place`,`publication_country`,`publishing_status`,`contributor_name1`,`short_desc`,`long_desc`,`author_bio`,`text_language`,`price`,`image`, `category`) VALUES ('" .$ISBN13. "','" .$title. "','" .$ProductForm."','" .$PublisherName. "','" .$PublicationPlace. "','" .$PublicationCountry. "','" .$PublishingStatus. "','" .$Author. "','" .$ShortDescription. "','" .$LongDescription. "','" .$AboutAuthor. "','" .$TextLaunguage. "','" .$Price. "','" .$img_flag. "', '" .$save_cat_in_submaster. "')";
                    $result = $this->query_update($query, array(), $this->con);
                    /*insert into solr master*/
                    $query1 = "INSERT INTO solr_master (`ISBN13`,`title`,`contributor_name1`,`category`,`status`) VALUES ('" .$ISBN13. "','" .$title. "','" .$Author. "', '" .$parent_cat. "','1')";
                    $result1 = $this->query_update($query1, array(), $this->con);
                    return $result;
                }
            }
                /*-----------search isbn in submastersheet------*/
            public function get_isbn_submastersheet($ISBN13){
                 $query1 = "SELECT * FROM sub_mastersheet WHERE ISBN13 = '".$ISBN13."'";
                 $if_isbn = $this->query($query1, array(), $this->con);
                 return $if_isbn;
            }

            // add in ISBN13 book_category_mapping_new

            public function add_category_in_book_category_mapping_new($ISBN13, $super_cat_id, $parent_id, $cat1_id, $cat2_id){
                    $sql = "insert into book_category_mapping_new set ISBN13 = '$ISBN13', super_cat_id = '$super_cat_id', parent_id = '$parent_id', cat1_id = '$cat1_id', cat2_id = '$cat2_id'";

                    $result = $this->query_update($sql, array(), $this->con);
                    return $result;

                  }

               public function update_book_category_mapping_new($ISBN13, $super_cat_id, $parent_id, $cat1_id, $cat2_id){
                    $sql = "update book_category_mapping_new set super_cat_id = '$super_cat_id', parent_id = '$parent_id', cat1_id = '$cat1_id', cat2_id = '$cat2_id' where ISBN13 = '$ISBN13' ";
                    $result = $this->query_update($sql, array(), $this->con);
                    return $result;

               }    
                /*------------uplaod image for submastersheet------------*/
            public function upload_img(){
                $i_msg="";
                if(isset($_FILES["Image"]["name"]) && $_FILES["Image"]["name"]!=""){
                $target_dir = "../add-data/";
                $target_file = $target_dir . basename($_FILES["Image"]["name"]);
                $if_File = basename($_FILES["Image"]["name"]);
               if(empty($if_File)) {
                    $imf_f = 0;
               }
               else {
                    $imf_f = 1;
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    if(isset($_POST["submit"])) {
                        $check = getimagesize($_FILES["Image"]["tmp_name"]);
                        if($check !== false) {
                            $imf_f = 1;
                        } else {
                            $i_msg.= "Image was not uploaded (File is not an image),";
                            $imf_f = 0;
                        }
                    }
                    if (file_exists($target_file)) {
                        $i_msg.= "Image was not uploaded (image with same name already exists),";
                        $imf_f = 0;
                    }
                     if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        $i_msg.= "Image was not uploaded (only JPG, JPEG, PNG & GIF files are allowed),";
                        $imf_f = 0;
                    }
                    if ($imf_f == 0) {
                     
                    } else {
                        if (move_uploaded_file($_FILES["Image"]["tmp_name"], $target_file)) {
                        } else {
                            $i_msg.= "Image was not uploaded,";
                            $imf_f = 0;
                        }
                    }
                }
                return array($imf_f,$i_msg); }else{
                    return 0;
                }
            }
            /*---------update submastersheet--------------*/
            public function update_to_sub_mastersheet($ISBN13,$title,$Author,$ProductForm,$PublisherName,$PublicationPlace,$PublicationCountry,$PublishingStatus,$Price,$TextLaunguage,$ShortDescription,$LongDescription,$AboutAuthor,$img_flag,$save_cat_in_submaster,$parent_cat){
                $if_isbn2 = $this->get_isbn_submastersheet($ISBN13);
                if(!empty($if_isbn2)){

                    $query = "UPDATE sub_mastersheet SET category = '$save_cat_in_submaster',ISBN13='$ISBN13', title='$title', product_form='$ProductForm',publisher_name='$PublisherName',publication_place='$PublicationPlace',publication_country='$PublicationCountry',publishing_status='$PublishingStatus',contributor_name1='$Author',short_desc='$ShortDescription',long_desc='$LongDescription',author_bio='$AboutAuthor',text_language='$TextLaunguage',price='$Price'";
                    if($img_flag!=0){
                      $query.= ",image='$img_flag'";
                    }
                   $query.=" WHERE ISBN13 = '$ISBN13'";
                   // print_r($query);exit;

                    $result = $this->query_update($query, array(), $this->con);
                    $query1 = "UPDATE solr_master SET category = '$parent_cat',title='$title',contributor_name1='$Author' WHERE ISBN13 = '$ISBN13'";
                     $result1 = $this->query_update($query1, array(), $this->con);
                    return $result;
                }
                else{}
            }

            /*add books to library*/
            public function add_to_booklibrary($LibraryBookID,$ISBN13,$InventoryID,$ShelfLocation,$ProcPrice,$Circulation,$CopyDiscount,$Status,$Title){
                $if_isbn4 = $this->get_isbn_submastersheet($ISBN13);
                if(!empty($if_isbn4)){
                    $if_bid3 = $this->get_book_id_booklibrary($LibraryBookID);
                    if(!empty($if_bid3)){
                        $i_msg="Library Book ID already exists";
                    }
                    else{   
                        $query = "INSERT INTO book_library_new (`book_id`,`ISBN13`,`title`,`inv_id`,`shelf_location`,`proc_price`,`circulation`,`copy_discount`,`status`) VALUES ('" .$LibraryBookID. "','" .$ISBN13. "','" .$Title."','" .$InventoryID. "','" .$ShelfLocation. "','" .$ProcPrice. "','" .$Circulation. "','" .$CopyDiscount. "','" .$Status. "')";
                        $result = $this->query_update($query, array(), $this->con);
                         $i_msg="Book is added to Library";
                    }
                }
                else{
                    $i_msg="Book is not present in sub-master sheet.Add first in sub-master and try again";
                }
                return $i_msg;
            }
            /*-------------get bookdetails from booklibrary-----------------*/
            public function get_book_id_booklibrary($LibraryBookID){
                 $query1 = "SELECT * FROM book_library_new WHERE book_id = '".$LibraryBookID."'";
                 $if_bid = $this->query($query1, array(), $this->con);
                 return $if_bid;
            }
            /*--------------update book library----------------*/
            public function update_to_booklibrary($LibraryBookID,$ISBN13,$InventoryID,$ShelfLocation,$ProcPrice,$Circulation,$CopyDiscount,$Status,$Title){
                $if_bid2 = $this->get_book_id_booklibrary($LibraryBookID);
                if(!empty($if_bid2)){

                    $query = "UPDATE book_library_new SET book_id='$LibraryBookID', ISBN13='$ISBN13', title='$Title',inv_id='$InventoryID',shelf_location='$ShelfLocation',proc_price='$ProcPrice',circulation='$Circulation',copy_discount='$CopyDiscount',status='$Status' WHERE book_id = '$LibraryBookID'";
                    $result = $this->query_update($query, array(), $this->con);
                    return $result;
                }
                else{}
            }

            /*--------------------merchant functions----------------------*/
            /*------------add new merchant---------*/
            public function add_merchant($Mer_Name,$Mer_Addres,$Mer_Phone,$Mer_Time,$Mer_Discount){
                $mer_check=$this->get_merchant($Mer_Name); 
                if($mer_check){
                    $i_msg="Merchant ".$Mer_Name." already exists,Go to Update Merchant if you want to update Merchant";
                }
                else{
                    $query = "INSERT INTO merchant_details(`merchant_name`,`merchant_address`,`merchant_phone`,`procurement_time`,`merchant_discount`) VALUES ('$Mer_Name','$Mer_Addres','$Mer_Phone','$Mer_Time','$Mer_Discount')";
                    $result = $this->query_update($query, array(), $this->con);
                    $i_msg="Merchant ".$Mer_Name." is added to Merchant List";
                }
                return $i_msg; 
            }
            /*get  merchant*/
            public function get_merchant($Mer_Name){
                $query1 = "SELECT * FROM merchant_details WHERE merchant_name = '".$Mer_Name."'";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            /*get all  merchant*/
            public function get_all_merchant(){
                $query2 = "SELECT * FROM merchant_details";
                $result2 = $this->query($query2,array(),$this->con);
                return $result2;
            }
            /*---update merchant*/
            public function update_merchant($Mer_Name,$Mer_Addres,$Mer_Phone,$Mer_Time,$Mer_Discount){
                $query = "UPDATE merchant_details SET merchant_address='$Mer_Addres',merchant_phone='$Mer_Phone',procurement_time='$Mer_Time',merchant_discount='$Mer_Discount' WHERE merchant_name = '$Mer_Name'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*add book to merchant*/
            public function add_book_merchant($Mer_Name,$Mer_Isbn,$Mer_Count,$Mer_Price){
                $mer_chk=$this->get_merchant($Mer_Name);
                $mer_id1="";
                foreach ($mer_chk as $id) {
                     $mer_id1=$id['merchant_id'];
                 }
                $query3="SELECT * FROM merchant_library WHERE mid='$mer_id1' AND ISBN = '$Mer_Isbn'";
                $result_p=$this->query($query3, array(), $this->con);
                if(empty($result_p)){
                    $query = "INSERT INTO merchant_library(`mid`,`ISBN`,`count`,`supply_price`) VALUES ('$mer_id1','$Mer_Isbn','$Mer_Count','$Mer_Price')";
                    $result = $this->query_update($query, array(), $this->con);
                    $i_msg="ISBN ".$Mer_Isbn." is added to Merchant Library";
                } 
                else{
                    $i_msg="ISBN".$Mer_Isbn." is already exists for Merchant ".$Mer_Name." ,Please Update ISBN to edit details.";
                }
                return $i_msg;
            }
            /*--get isbn detail from merchant library for isbn---*/ 
            public function get_isbn_merchant($Mer_Id){
                $query1 = "SELECT * FROM merchant_library WHERE mid = '".$Mer_Id."'";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            /*get isbn details for ajax*/
            public function get_isbn_detail_merchant($Mer_Id,$Mer_Isbn1){
                $query1 = "SELECT * FROM merchant_library WHERE mid = '".$Mer_Id."' AND ISBN ='".$Mer_Isbn1."'";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            /*--------update merchant isbn in merchant library---------*/
            public function update_book_merchant($Mer_Id,$Mer_Isbn,$Mer_Count,$Mer_Price){
                $query = "UPDATE merchant_library SET count='$Mer_Count',supply_price='$Mer_Price' WHERE mid = '$Mer_Id' AND ISBN = '$Mer_Isbn'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*delete isbn for merchant from merchant library*/
            public function delete_book_merchant($Mer_Id,$Mer_Isbn){
                $query = "DELETE FROM merchant_library WHERE mid = '".$Mer_Id."' AND ISBN = '".$Mer_Isbn."'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*update isbn from file */
             public function update_from_file_book_merchant($Mer_Name){
                $i_msg="";
                $mer_chk=$this->get_merchant($Mer_Name);
                $mer_id1="";
                foreach ($mer_chk as $id) {
                     $mer_id1=$id['merchant_id'];
                 }
                if(isset($_FILES["file"]["tmp_name"]) && $_FILES["file"]["tmp_name"]!=""){
                    $handle = fopen($_FILES['file']['tmp_name'], "r");
                    fgetcsv($handle);
                    $query31="DELETE FROM merchant_library WHERE mid='$mer_id1'";
                    $result_del = $this->query_update($query31, array(), $this->con);

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        
                        $query3="SELECT * FROM merchant_library WHERE mid='$mer_id1' AND ISBN = '$data[0]'";
                        $result_p=$this->query($query3, array(), $this->con);
                        if(empty($result_p)){
                            $query_u="INSERT INTO merchant_library(`mid`,`ISBN`,`count`,`supply_price`) VALUES ('$mer_id1','$data[0]','$data[1]','$data[2]')";
                            $result = $this->query_update($query_u, array(), $this->con);
                            if($result){
                                 $i_msg.="ISBN ".$data[0]." is added to libary<Br>";
                            }
                            else{
                                $i_msg.="Oops,Something went wrong."; 
                            } 
                        }
                        else{
                            $i_msg.="ISBN ".$data[0]." is already exists.Please edit details in update panel<br>"; 
                        }
                    }
                    fclose($handle);
                }
                else{
                    $i_msg.="Oops,Something went wrong,Please Upload the correct file.";
                }
                return $i_msg;
            }
            /*---------------------wishlist functions----------------------*/
           /*get all company details*/
            public function get_all_corporates(){
                $query = "SELECT * FROM ireads_corp.company_details";
                $result = $this->query($query,array(),$this->con);
                return $result;
            }
            /*gives corporate name from company detail*/
            public function get_one_corporate($cor_id){
                $query = "SELECT company_name FROM ireads_corp.company_details WHERE company_id = '$cor_id'";
                $result = $this->query($query,array(),$this->con);
                return $result;
            }
            /*corporate wishlist*/
            public function get_wishlist_corp($corp_Id){
                $query1="SELECT bookshelf.user_id,bookshelf.ISBN13,login.user_email,sub_mastersheet.title 
                FROM ireads_corp.corporate_user_mapping user join ireads_corp.user_login login on user.user_id=login.user_id join ireads_corp.bookshelf bookshelf on user.user_id = bookshelf.user_id join sub_mastersheet on bookshelf.ISBN13=sub_mastersheet.ISBN13 
                WHERE user.company_id='$corp_Id' AND bookshelf.shelf_type=2 limit 100";
                $result1 = $this->query($query1,array(),$this->con); 
                return $result1;
            }
            /*count number of isbn */
            public function get_count_w($ISBN){
                $query1="SELECT COUNT(ISBN13) AS count_isbn FROM ireads_corp.bookshelf WHERE shelf_type=2 AND ISBN13='$ISBN' GROUP BY ISBN13";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            /*retail wishlist*/
            public function get_wishlist_retail(){
                $query1="SELECT  user.user_id,bookshelf.ISBN13,user.user_email,sub_mastersheet.title
                        FROM user_login user join bookshelf bookshelf on user.user_id=bookshelf.user_id join sub_mastersheet on bookshelf.ISBN13=sub_mastersheet.ISBN13
                        WHERE  NOT EXISTS
                        (SELECT corp.user_id FROM ireads_corp.corporate_user_mapping corp WHERE user.user_id=corp.user_id) 
                        AND bookshelf.shelf_type=2
                        order by bookshelf.when desc LIMIT 30";
                $result1 = $this->query($query1,array(),$this->con); 
                return $result1;
            }
             public function get_wishlist_retail_full(){
                $query1="SELECT  user.user_id,bookshelf.ISBN13,user.user_email,sub_mastersheet.title
                        FROM user_login user join bookshelf bookshelf on user.user_id=bookshelf.user_id join sub_mastersheet on bookshelf.ISBN13=sub_mastersheet.ISBN13
                        WHERE  NOT EXISTS
                        (SELECT corp.user_id FROM ireads_corp.corporate_user_mapping corp WHERE user.user_id=corp.user_id) 
                        AND bookshelf.shelf_type=2";
                $result1 = $this->query($query1,array(),$this->con); 
                return $result1;
            }
            /*count number of isbn retail*/
            public function get_count_w_r($ISBN){
                $query1="SELECT COUNT(ISBN13) AS count_isbn FROM bookshelf WHERE shelf_type=2 AND ISBN13='$ISBN' GROUP BY ISBN13";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            /*-------search isbn from wishlist */
            public function search_wishlist($ISBN13){
                $query2="SELECT  user.user_id,bookshelf.ISBN13,user.user_email,sub_mastersheet.title
                        FROM user_login user join bookshelf bookshelf on user.user_id=bookshelf.user_id join sub_mastersheet on bookshelf.ISBN13=sub_mastersheet.ISBN13
                        WHERE  NOT EXISTS
                        (SELECT corp.user_id FROM ireads_corp.corporate_user_mapping corp WHERE user.user_id=corp.user_id) 
                        AND bookshelf.shelf_type=2 AND bookshelf.ISBN13='$ISBN13'
                        order by bookshelf.when desc";
                $result2 = $this->query($query2,array(),$this->con); 
                return $result2;
            }
    /*---------------------------user panel function---------------------------------*/
            public function check_if_user_email($email){
                $query1="SELECT user_id FROM user_login WHERE user_email='$email'";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            public function update_password($email,$salt,$hash){
                $query = "UPDATE user_login SET salt = '$salt', user_password = '$hash',user_status=1 WHERE user_email='$email'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            public function corp_check_if_user_email($email){
                $query1="SELECT user_id FROM ireads_corp.user_login WHERE user_email='$email'";
                $result1 = $this->query($query1,array(),$this->con);
                return $result1;
            }
            public function corp_update_password($email,$salt,$hash){
                $query = "UPDATE ireads.user_login,ireads_corp.user_login SET ireads.user_login.salt = '$salt', ireads.user_login.user_password = '$hash',ireads.user_login.user_status=1,ireads_corp.user_login.salt = '$salt', ireads_corp.user_login.user_password = '$hash',ireads_corp.user_login.user_status=1 WHERE ireads.user_login.user_email='$email' AND ireads_corp.user_login.user_email='$email'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*store credit functions in user panel*/
            public function check_store_credit($email){
                $result='';
                $check_email=$this->check_if_user_email($email);
                if($check_email){
                     $query2 = "SELECT store_credit,used_credit from user_store_credit where user_id = '".$check_email[0]['user_id']."'";
                     $result2 = $this->query($query2,array(),$this->con);
                     $result=$result2[0]['store_credit']-$result2[0]['used_credit'];
                }
                else{
                    $result="Email does not exists";
                }
                return $result;
            }
            public function update_store_credit($email,$credit){
                $result='';
                $check_email=$this->check_if_user_email($email);
                if($check_email){
                     $query2 = "SELECT store_credit,used_credit from user_store_credit where user_id = '".$check_email[0]['user_id']."'";
                     $result2 = $this->query($query2,array(),$this->con);
                     $updated_credit=$result2[0]['store_credit']+$credit;
                     $query3 = "UPDATE user_store_credit SET store_credit='$updated_credit' where user_id = '".$check_email[0]['user_id']."'";
                     $result3 = $this->query_update($query3,array(),$this->con);
                     if($result3){
                        $cr_new=$updated_credit-$result2[0]['used_credit'];
                        $result="Store credits updated ".$credit." ,New store credits are : ".$cr_new;
                     }
                     else{
                        $result="Oops Something went wrong";
                     }
                     

                }
                else{
                    $result="Email does not exists";
                }
                return $result;
            }

  /*----------------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------------
  ---------------------functions for corporate rent request panel-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  /*function for mail to get order*/
             public function corp_get_bookshelf_order($order_id){
                 $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1, user.user_id FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.bookshelf_order_id='$order_id'"; 
                 $result = $this->query($query, array(), $this->con);
                    //$company_name = $this->get_company_details_with_user_id($result[0]['user_id']);
                 // return $company_name;
                 return $result;
             }
             public function corp_get_bookshelf_order_lib($lib_id){
                 $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1, user.user_id FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status BETWEEN 4 and 7 and bookshelf.unique_book_id='$lib_id'"; 
                 $result = $this->query($query, array(), $this->con);
                    //$company_name = $this->get_company_details_with_user_id($result[0]['user_id']);
                 // return $company_name;
                 return $result;
             }
             public function corp_get_bookshelf_order_pickup($order_id){
                 $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.bookshelf_order_id='$order_id'"; 
                 $result = $this->query($query, array(), $this->con);
                 return $result;
             }

            public function corp_get_bookshelf_order_1_page($status){
                $query  ="SELECT count(bookshelf.bookshelf_order_id) as rows FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            public function corp_get_bookshelf_order_2_page($status){
                $query  ="SELECT count(bookshelf.bookshelf_order_id) as rows FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc";
                $result = $this->query($query, array(), $this->con);
                return $result;


            }
            // public function corp_get_bookshelf_order_status($status,$number,$perpage){
            //     $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc limit $number,$perpage"; 
            //     $result = $this->query($query, array(), $this->con);
                
            //     return $result;
            // }
            public function corp_get_bookshelf_order_status($status,$number,$perpage){
                $query = "SELECT bookshelf.*,user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.order_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 WHERE bookshelf.order_status='$status' order by bo_details.order_date desc limit $number,$perpage"; 
                $result = $this->query($query, array(), $this->con);
                
                return $result;
            }
            /* save order comment*/
            public function corp_save_order_comment($textval,$order){
                $query = "UPDATE ireads_corp.bookshelf_order SET order_comments='$textval' WHERE  ireads_corp.bookshelf_order.bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
             /*change status to -1 for delete*/
            public function corp_update_order_status_1del($order_id,$book_id,$user_id,$ISBN13){
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf SET ireads_corp.bookshelf_order.order_status =-1,ireads_corp.bookshelf.shelf_type=1 WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf.user_id='$user_id' AND ireads_corp.bookshelf.ISBN13='$ISBN13'"; 
                $result = $this->query_update($query,array(),$this->con);
                if($book_id){
                    $query1 = "UPDATE book_library_new SET status =1 WHERE book_id='$book_id'";
                    $result1 = $this->query_update($query1,array(),$this->con);
                }
                return $result;
            }
            /*change status to 2 for delivery processing*/
            public function corp_update_order_status_2($order_id){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =2,ireads_corp.bookshelf_order_tracking_details.delivery_processing='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*change locked book */
            public function corp_change_locked_book($book_id,$order,$cur_book_id){
                $query1 = "UPDATE book_library_new,ireads_corp.bookshelf_order
                            SET book_library_new.status = (case when book_library_new.book_id = '$book_id' then '2'
                                                                when book_library_new.book_id = '$cur_book_id' then '1'
                                                            end),
                                ireads_corp.bookshelf_order.unique_book_id='$book_id'
                            WHERE book_library_new.book_id in ('$book_id', '$cur_book_id') AND ireads_corp.bookshelf_order.bookshelf_order_id='$order'";
                $result1 = $this->query_update($query1, array(), $this->con);
                return $result1;
            }
            /*get address from user address book.with address id*/
            public function corp_get_address($address_id){
                $query1 = "SELECT * FROM ireads_corp.user_address_book WHERE address_book_id ='$address_id'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*----------- ----delivery processing functions--------- --------*/
            /*mark book lost as status -1,unassign the unique id for bookshelf_order for an order*/
            public function corp_mark_book_lost_unique_book_unassign($book_id,$order){
                $query = "UPDATE ireads_corp.bookshelf_order,book_library_new SET ireads_corp.bookshelf_order.unique_book_id ='',book_library_new.status='-1' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order' AND book_library_new.book_id='$book_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*assign a new book,when book lost button is pressed*/
           public function corp_lock_book_assign_unique_id_on_lost($book_id,$order){
                $query = "UPDATE ireads_corp.bookshelf_order,book_library_new SET ireads_corp.bookshelf_order.unique_book_id ='$book_id',book_library_new.status='2' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order' AND book_library_new.book_id='$book_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
           }
            /*get parent order for given order,function is used in carrier save file for checking if orders are from same parent order*/
            public function corp_get_parent_order_for_every_order($order){
                $query1 = "SELECT  p_order_id FROM ireads_corp.bookshelf_parent_order WHERE bookshelf_order_id ='$order'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /*check if parent is present or not,function is used in invoice shiiping cases*/
            public function corp_get_parent_dispatch($parent){
                $query = "SELECT invoice FROM ireads_corp.dispatch_invoice WHERE parent_order_id = '" . $parent. "'";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /* insert dispatch and invoice table*/
            public function corp_update_dispatch_invoice($parent,$selected_orders,$prefix_name,$inv){
               if($this->corp_get_dispatch_slip_name($parent,$selected_orders)){
                      $query1 = "UPDATE ireads_corp.dispatch_invoice SET dispatch ='$prefix_name',invoice = '$inv' WHERE parent_order_id = '" . $parent. "' AND bookshelf_order_id='$selected_orders'";
                      $result1 = $this->query_update($query1,array(),$this->con);
               }
               else{
                    $query = "INSERT INTO ireads_corp.dispatch_invoice(`parent_order_id`,`bookshelf_order_id`,`dispatch`,`invoice`) VALUES ('$parent','$selected_orders','$prefix_name','$inv')";
                    $result = $this->query_update($query, array(), $this->con);
                    $query1="SELECT LAST_INSERT_ID() from ireads_corp.dispatch_invoice";
                    $result1 = $this->query($query1, array(), $this->con);
                }
                return $result1;
            }
            /* get dispatch path name from dispatch-invoice*/
            public function corp_get_dispatch_slip_name($parent,$order){
                $query = "SELECT dispatch,invoice FROM ireads_corp.dispatch_invoice WHERE parent_order_id = '" . $parent. "' AND bookshelf_order_id='$order'";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /*function to save carrier name and tracking id for an order on process button */
            public function corp_update_carrier_track_id($carrier,$track_id,$order){
                $query = "UPDATE ireads_corp.bookshelf_order SET carrier ='$carrier',d_track_id='$track_id' WHERE bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*get order isbn with order id*/
            public function corp_get_book_isbn_bookshelf_order($order){
                $query1 = "SELECT ISBN13,init_pay,mrp,amt_pay FROM ireads_corp.bookshelf_order WHERE bookshelf_order_id ='$order'";
                $result1 = $this->query($query1, array(), $this->con);
                return $result1;
            }
            /* change status to 1 for new delivery */
            public function corp_update_order_status_1($order_id){
                $query = "UPDATE ireads_corp.bookshelf_order SET order_status =1 WHERE bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*change status to 3 for dispatched*/
            public function corp_update_order_status_3($order_id,$date_today){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =3,ireads_corp.bookshelf_order.issue_date ='$date_today',ireads_corp.bookshelf_order_tracking_details.dispatched='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*function to update address */
            public function corp_update_address_delivery($fullname,$address_line1,$address_line2,$city,$state,$pincode,$phone,$addr_id){
                $query = "UPDATE ireads_corp.user_address_book SET fullname='$fullname',address_line1='$address_line1',address_line2='$address_line2',city='$city',state='$state',pincode='$pincode',phone='$phone' WHERE address_book_id='$addr_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*----------------------------- ----dispatched functions--------- -------------------*/
              /*change status to 4 for delivered*/
            public function corp_update_order_status_4($order_id,$pick_address_id,$order_address_id,$parent){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =4,ireads_corp.bookshelf_order_tracking_details.delivered='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                if($pick_address_id==0){
                    $query1 = "UPDATE ireads_corp.bookshelf_order_details SET pick_address_id ='$order_address_id' WHERE p_order_id='$parent'";
                    $result1 = $this->query_update($query1,array(),$this->con);
                }
                return $result;
            }
            /*----------- ----delivered functions--------- --------*/
             /*change status to 5 for generate pickup*/
            public function corp_update_order_status_5($order_id,$addr,$p_order_id){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                 
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_details,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =5,ireads_corp.bookshelf_order_details.pick_address_id='$addr',ireads_corp.bookshelf_order_tracking_details.new_pickup='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_details.p_order_id='$p_order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*----------- ----new pickup requests functions--------- --------*/
            public function corp_get_bookshelf_order_status_pick($status,$number,$perpage){
                $query = "SELECT bookshelf.*,b_track_details.new_pickup, user.user_email, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join ireads_corp.bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status='$status' order by bo_details.order_date desc limit $number,$perpage";
                $result = $this->query($query, array(), $this->con);
                return $result;
            }
            /* save pickup comment*/
            public function corp_save_pickup_comment($textval,$order){
                $query = "UPDATE ireads_corp.bookshelf_order SET pickup_comments='$textval' WHERE  bookshelf_order_id='$order'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*change status to 6 for processing pickup*/
            public function corp_update_order_status_6($order_id){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa"); 
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =6,ireads_corp.bookshelf_order_tracking_details.pickup_processing='$date_today',ireads_corp.bookshelf_order.carrier='',ireads_corp.bookshelf_order.d_track_id='' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
            /*change status to 7 for fedex*/
            public function corp_update_order_status_7($order_id){
                 date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query = "UPDATE ireads_corp.bookshelf_order,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =7,ireads_corp.bookshelf_order_tracking_details.req_to_fedex='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }
   
             /*change status to 8 for returned*/    
            public function corp_update_order_status_8($order_id,$book_status,$bookid,$user_id,$isbn){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query1 = "SELECT * from ireads_corp.bookshelf where ireads_corp.bookshelf.bookshelf_order_id = '$order_id' and ireads_corp.bookshelf.user_id='$user_id' and ireads_corp.bookshelf.ISBN13='$isbn' ";
                $res_b = $this->query($query1,array(),$this->con);
                if($res_b){
                    $query = "UPDATE ireads_corp.bookshelf SET ireads_corp.bookshelf.shelf_type =3 WHERE  ireads_corp.bookshelf.bookshelf_order_id = '$order_id' and ireads_corp.bookshelf.user_id='$user_id' and ireads_corp.bookshelf.ISBN13='$isbn'";
                $result = $this->query_update($query,array(),$this->con);
                }
                else{
                    $query = "INSERT INTO  ireads_corp.bookshelf SET ireads_corp.bookshelf.shelf_type =3, ireads_corp.bookshelf.bookshelf_order_id = '$order_id', ireads_corp.bookshelf.user_id='$user_id', ireads_corp.bookshelf.ISBN13='$isbn'";
                $result = $this->query_update($query,array(),$this->con);
                }

                $resbook=$this->get_shelf_location($bookid);
                $circulation=$resbook[0]['circulation'];
                $circulation +=1;
                $query = "UPDATE ireads_corp.bookshelf_order,book_library_new,ireads_corp.bookshelf,ireads_corp.bookshelf_order_tracking_details SET ireads_corp.bookshelf_order.order_status =8,book_library_new.status='$book_status',book_library_new.circulation='$circulation',ireads_corp.bookshelf.shelf_type=4,ireads_corp.bookshelf_order_tracking_details.returned='$date_today' WHERE ireads_corp.bookshelf_order.bookshelf_order_id='$order_id' AND book_library_new.book_id='$bookid' AND ireads_corp.bookshelf.user_id='$user_id' AND ireads_corp.bookshelf.ISBN13='$isbn' AND ireads_corp.bookshelf.shelf_type=3 AND ireads_corp.bookshelf_order_tracking_details.bookshelf_order_id='$order_id'";
                $result = $this->query_update($query,array(),$this->con);
                return $result;
            }

            /*------------------------ license expiry check-------------------------------*/
            public function license_check(){
                date_default_timezone_set('Asia/Calcutta');
                $date_today=date("Y-m-d h:i:sa");
                $query1 = "SELECT company_details.company_id, company_details.company_name, company_feature_mapping.license_count FROM ireads_corp.company_details JOIN ireads_corp.company_feature_mapping ON company_feature_mapping.company_id = company_details.company_id JOIN ireads_corp.company_membership_mapping ON company_membership_mapping.company_id = company_details.company_id WHERE company_membership_mapping.company_id = company_feature_mapping.company_id AND company_membership_mapping.current_delivery >= company_feature_mapping.license_count AND company_details.company_id = company_membership_mapping.company_id";
                $result = $this->query($query1,array(),$this->con);
                return $result;
            }
            public function order_counts($companyid){
                $results = array();
                $query2 = "SELECT count(bookshelf_order.order_status) FROM ireads_corp.bookshelf_order JOIN ireads_corp.corporate_user_mapping ON corporate_user_mapping.user_id = bookshelf_order.user_id WHERE corporate_user_mapping.company_id = ".$companyid." AND bookshelf_order.order_status = 4";
                $result = $this->query($query2,array(),$this->con);
                $query3 = "SELECT count(bookshelf_order.order_status) FROM ireads_corp.bookshelf_order JOIN ireads_corp.corporate_user_mapping ON corporate_user_mapping.user_id = bookshelf_order.user_id WHERE corporate_user_mapping.company_id = ".$companyid." AND bookshelf_order.order_status IN (1,2,3)";
                $result1 = $this->query($query3,array(),$this->con);
                $results = array_merge($results,$result, $result1);
                return $results;
            }

            public function license_expired_count(){
                $result = $this->license_check();
                $array_filter = array_filter($result, 'strlen');
                $count = count($array_filter);
                return $count;
            }

/*-----------------------pickup pending functions---------------------------------------*/
                public function get_bookshelf_order_pending_pickup($date_start,$date_end){
                    $query = "SELECT  bookshelf.*, user.user_email,b_track_details.new_pickup, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM bookshelf_order bookshelf join user_login user on bookshelf.user_id = user.user_id join bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status=7 AND date(b_track_details.new_pickup) between '$date_start' and '$date_end' order by bo_details.order_date  ";
                    $result = $this->query($query, array(), $this->con);
                    return $result;
                }  
                public function get_bookshelf_order_pending_pickup_corp($date_start,$date_end){
                    $query = "SELECT  bookshelf.*, user.user_email,b_track_details.new_pickup, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join ireads_corp.bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id WHERE bookshelf.order_status=7 AND date(b_track_details.new_pickup) between '$date_start' and '$date_end' order by bo_details.order_date  ";
                    $result = $this->query($query, array(), $this->con);
                    return $result;
                }  
                public function get_bookshelf_order_pending_pickup_corp_comp($comp_id){
                    $query = "SELECT  bookshelf.*, user.user_email,b_track_details.new_pickup, parentorder.p_order_id,address.*,bo_details.*,sub_master.Title,sub_master.contributor_name1 FROM ireads_corp.bookshelf_order bookshelf join ireads_corp.user_login user on bookshelf.user_id = user.user_id join ireads_corp.bookshelf_parent_order parentorder on bookshelf.bookshelf_order_id = parentorder.bookshelf_order_id join ireads_corp.bookshelf_order_details bo_details on parentorder.p_order_id = bo_details.p_order_id join ireads_corp.user_address_book address on bo_details.pick_address_id = address.address_book_id join sub_mastersheet sub_master on bookshelf.ISBN13 = sub_master.ISBN13 join ireads_corp.bookshelf_order_tracking_details b_track_details on bookshelf.bookshelf_order_id = b_track_details.bookshelf_order_id join ireads_corp.corporate_user_mapping as map on bookshelf.user_id=map.user_id WHERE bookshelf.order_status=7 AND map.company_id=$comp_id ";
                    $result = $this->query($query, array(), $this->con);
                    return $result;
                }  
                
 /*--------------------------------verification email-------------------------------------*/
                public function get_unverified_users_corp($cor_id){
                    $query1="SELECT login.user_id,login.user_email FROM ireads_corp.corporate_user_mapping map join ireads_corp.user_login login on map.user_id = login.user_id WHERE map.company_id='$cor_id' AND login.user_status=2 ";
                    $result1 = $this->query($query1,array(),$this->con);
                    return $result1;
                }
                 public function update_newpassword_corp($to_addr,$npass){
                    $query="UPDATE ireads_corp.user_login SET ireads_corp.user_login.new_password = '$npass' WHERE  ireads_corp.user_login.user_email = '$to_addr' ";  
                    $result = $this->query_update($query,array(),$this->con);
                    return $result;
                }
                
/*-------------------------------get users panel functions--------------------------------*/
                public function get_users_corp($cor_id){
                    $query1="SELECT login.user_id,login.user_email,login.user_status FROM ireads_corp.corporate_user_mapping map join ireads_corp.user_login login on map.user_id = login.user_id WHERE map.company_id='$cor_id' AND login.user_status=2 OR map.company_id='$cor_id' AND login.user_status=1";
                    $result1 = $this->query($query1,array(),$this->con);
                    return $result1;
                }
                public function get_users_retail(){
                    $query1="SELECT user_id,user_email,user_status FROM user_login  WHERE user_id NOT IN (SELECT user_id FROM ireads_corp.user_login) limit 10";
                    $result1 = $this->query($query1,array(),$this->con);
                    return $result1;
                }
                
/*--------------------------------procurement management panel-------------------------------------*/
                /*insert into circulation_details table on every return*/
                public function update_circulation_details($book_id,$init_pay,$refund,$issue_date,$pickup_date){
                    $result='';
                    $check_if_proc=$this->check_if_book_in_proc($book_id);
                    if($check_if_proc){
                        $resbook=$this->get_shelf_location($book_id);
                        $circulation=$resbook[0]['circulation'];
                        $circulation +=1;
                         $query = "INSERT INTO circulation_details(`book_id`,`circulation`,`buy_price`,`sell_price`,`buy_date`,`sell_date`) VALUES ('$book_id','$circulation','$refund','$init_pay','$pickup_date','$issue_date')";
                        $result = $this->query_update($query, array(), $this->con);
                        return $result;
                    }
                    else{
                        $result='';
                      return $result;  
                    }
                    
                }
                /*get corporate rent and corp type with user id*/
                public function get_company_details_with_user_id($user_id){
                    $query2="SELECT company.* FROM ireads_corp.corporate_user_mapping company_map join ireads_corp.user_login user on company_map.user_id=user.user_id join ireads_corp.company_details company on company_map.company_id = company.company_id WHERE user.user_id='$user_id' ";
                    $result2 = $this->query($query2, array(), $this->con); 
                    return $result2;
                }
                /*circulation update for corporate*/
                public function update_circulation_details_corp($user_id,$book_id,$init_pay,$refund,$issue_date,$pickup_date){
                    $result='';
                    $check_if_proc=$this->check_if_book_in_proc($book_id);
                    if($check_if_proc){
                        $resbook=$this->get_shelf_location($book_id);
                        $circulation=$resbook[0]['circulation'];
                        $circulation +=1;
                        $company_details=$this->get_company_details_with_user_id($user_id);
                        if($company_details[0]['corp_type'] == 5 && $company_details[0]['rent_type'] == 5){
                             
                             $query = "INSERT INTO circulation_details(`book_id`,`circulation`,`buy_price`,`sell_price`,`buy_date`,`sell_date`) VALUES ('$book_id','$circulation','$refund','$init_pay','$pickup_date','$issue_date')";
                            $result = $this->query_update($query, array(), $this->con);
                        }
                        else{ 
                            $price_corp=$company_details[0]['price']; 
                            $query = "INSERT INTO circulation_details(`book_id`,`circulation`,`buy_price`,`sell_price`,`buy_date`,`sell_date`) VALUES ('$book_id','$circulation','0','$price_corp','$pickup_date','$issue_date')";
                            $result = $this->query_update($query, array(), $this->con);
                        }
                    }
                    else{
                        $result='';
                    }
                    return $result;
                }
                /*check if a library id is in procurement_details or not*/
                public function check_if_book_in_proc($bookid){
                    $query2="SELECT * FROM procurement_details WHERE book_id='$bookid'";
                    $result2 = $this->query($query2, array(), $this->con); 
                    return $result2;
                }
                /*add book to procurement_details table from add books to library*/
                public function add_to_procurement_details($LibraryBookID,$ISBN13,$Mer_Name,$mrp,$discount,$ProcPrice){
                    date_default_timezone_set('Asia/Calcutta');
                    $date_today=date("Y-m-d h:i:sa");
                    $query = "INSERT INTO procurement_details(`book_id`,`ISBN`,`source`,`Mrp`,`discount`,`proc_price`,`when_proc`) VALUES ('$LibraryBookID','$ISBN13','$Mer_Name','$mrp','$discount','$ProcPrice','$date_today')";
                    $result = $this->query_update($query, array(), $this->con);
                    return $result;
                }
                /*update the procurement_details table*/
                public function update_to_procurement_details($LibraryBookID,$ISBN13,$Mer_Name,$mrp,$discount,$ProcPrice){
                    date_default_timezone_set('Asia/Calcutta');
                    $date_today=date("Y-m-d h:i:sa");
                    $query = "UPDATE procurement_details SET book_id='$LibraryBookID', ISBN='$ISBN13', source='$Mer_Name',Mrp='$mrp',discount='$discount',proc_price='$ProcPrice'WHERE book_id = '$LibraryBookID'";
                    $result = $this->query_update($query, array(), $this->con);
                    return $result;
                }
                /*get the books from procurement_details for selected dates*/
                public function get_books_from_procurement($date_start,$date_end){
                    $query = "SELECT  procurement_details.*,merchant_details.merchant_name FROM procurement_details JOIN merchant_details on procurement_details.source = merchant_details.merchant_id WHERE date(procurement_details.when_proc) between '$date_start' and '$date_end' order by procurement_details.when_proc  ";
                    $result = $this->query($query, array(), $this->con);
                    return $result;
                }  

                /*get book details from circulation_details and procurement_details*/
                public function get_details_from_circulation($book_id){
                    $query = "SELECT  procurement_details.*,merchant_details.merchant_name,circulation_details.* FROM circulation_details JOIN procurement_details on circulation_details.book_id = procurement_details.book_id JOIN merchant_details on procurement_details.source = merchant_details.merchant_id WHERE circulation_details.book_id = '$book_id' group by circulation_details.circulation";
                    $result = $this->query($query, array(), $this->con);
                    return $result;
                }  
                /*GET initial payable for an isbn and library id*/
                
                public function get_proc_price_of_book($isbn){
                    $mrp_query = "SELECT merchant_library.supply_price,merchant_details.merchant_discount FROM merchant_library join merchant_details on merchant_library.mid = merchant_details.merchant_id WHERE merchant_library.ISBN = '$isbn'";
                    $mrp_result = $this->query($mrp_query, array(), $this->con);
                    $mrp=0;
                    if($mrp_result){
                        $mrp=$mrp_result[0]['supply_price'];
                    }
                    else{
                        $mrp_query1 = "SELECT price FROM sub_mastersheet WHERE ISBN13 = '$isbn'";
                        $mrp_result1 = $this->query($mrp_query1, array(), $this->con);
                        $mrp=$mrp_result1[0]['price'];
                    }
                    $discount=$mrp_result[0]['merchant_discount'];
                    if(empty($mrp_result[0]['merchant_discount'])){
                        $discount=30;
                    }
                    $proc=($mrp-($mrp*($discount/100)));
                    return $proc;
                }
                 public function get_initial_payable($book_id,$isbn){
                    $mrp_query = "SELECT merchant_library.supply_price,merchant_details.merchant_discount FROM merchant_library join merchant_details on merchant_library.mid = merchant_details.merchant_id WHERE merchant_library.ISBN = '$isbn'";
                    $mrp_result = $this->query($mrp_query, array(), $this->con);
                    if($mrp_result){
                        $mrp=$mrp_result[0]['supply_price'];
                    }
                    else{
                        $mrp_query1 = "SELECT price FROM sub_mastersheet WHERE ISBN13 = '$isbn'";
                        $mrp_result1 = $this->query($mrp_query1, array(), $this->con);
                        $mrp=$mrp_result1[0]['price'];
                    }
                   
                    $resbook=$this->get_shelf_location($book_id);
                    $circulation=$resbook[0]['circulation'];
                    if($circulation>10){
                        $circ_discount=30;
                    }
                    else{
                        $circ_query = "SELECT discounts FROM circulation_discount WHERE circulation = '$circulation'";
                        $circ_result = $this->query($circ_query, array(), $this->con); 
                        $circ_discount=$circ_result[0]['discounts'];
                    }
                    $mer_disc=$mrp_result[0]['merchant_discount'];
                    if(empty($mer_disc)){
                        $mer_disc=0;
                    }
                    $final_mer_disc=$mer_disc/2;
                    $final_discount=$final_mer_disc+$circ_discount;
                    $initial_pay=($mrp-($mrp*($final_discount/100)));
                    return $initial_pay;
                }  
                
                public function get_books_for_every_circulation(){
                    $query="SELECT circulation,count(*) as number FROM book_library_new group by circulation";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                
                public function get_total_books_in_library(){
                    $query="SELECT * FROM book_library_new";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                
                public function get_total_profit(){
                    $query="SELECT count(bookshelf_order_id) as total_orders, SUM(init_pay) as total_init,SUM(refund) as total_refund FROM bookshelf_order WHERE NOT bookshelf_order_id IN (SELECT bookshelf_order_id as total_row FROM bookshelf_order WHERE disc_pay=0 AND amt_pay=0 AND store_pay=0 AND refund=0)";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                public function get_total_profit_corporate(){
                    $query="SELECT bookshelf_order_id  FROM bookshelf_order WHERE disc_pay=0 AND amt_pay=0 AND store_pay=0 AND refund=0";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }

                ###############################################################
                                ##send reminder panel functions##
                ###############################################################
               public function get_orders_to_send_reminder($cor_id){
                       
                        date_default_timezone_set('Asia/Calcutta');
                        $date_chk1 = date('Y-m-d ', strtotime(date("Y-m-d ").' -18 days'));
                        $date_chk2 = date('Y-m-d ', strtotime(date("Y-m-d ").' -28 days'));
                        $date_chk3 = date('Y-m-d ', strtotime(date("Y-m-d ").' -33 days'));
                        $date_chk4 = date('Y-m-d ', strtotime(date("Y-m-d ").' -36 days'));
                         
                         // $date_chk = new DateTime('2017-07-05');
                         // $date_today->modify('-25 days');
                         // $date_chk=$date_today->format('Y-m-d H:i:s');
                         // print_r($date_chk);die;
                    $query="SELECT bo.bookshelf_order_id,bo.order_status,b_track.dispatched,u.user_email FROM ireads_corp.bookshelf_order AS bo join ireads_corp.corporate_user_mapping AS map on bo.user_id=map.user_id join ireads_corp.bookshelf_order_tracking_details AS b_track on bo.bookshelf_order_id = b_track.bookshelf_order_id join ireads_corp.user_login AS u on map.user_id=u.user_id WHERE map.company_id='$cor_id' and bo.order_status>=4 and bo.order_status<=6 and ( date(b_track.dispatched)='$date_chk1'  or date(b_track.dispatched)='$date_chk2'  or date(b_track.dispatched)='$date_chk3'  or date(b_track.dispatched)='$date_chk4')";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
               }

               ###################################################################
               ###################################################################
                                    ##search books panel functions##
               ###################################################################
               ###################################################################
                public function search_isbn_books($isbn){
                    $query="SELECT book_library_new.*,sub_mastersheet.title as book_title FROM book_library_new join sub_mastersheet on book_library_new.ISBN13=sub_mastersheet.ISBN13 WHERE book_library_new.ISBN13 = '$isbn'";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                public function get_order_for_lib_retail($lib){
                    $query="SELECT * FROM bookshelf_order WHERE unique_book_id = '$lib'";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                public function get_order_for_lib_corp($lib){
                    $query="SELECT * FROM ireads_corp.bookshelf_order WHERE unique_book_id = '$lib'";
                    $result = $this->query($query, array(), $this->con); 
                    return $result;
                }
                

/*--------------------------------reissue panel-------------------------------------*/

            ## reissue books function start from here ##

            ## get_company_details ##
            public function get_company_details() 
            {
                $data = $this->query("SELECT company_id, CONCAT(company_name,' - ', company_id) AS name FROM company_details ORDER BY company_name", array(),$this->con);
                if (!empty($data))
                {
                    return $data;
                }
                else{ 
                   return false;
               }
            }
            ## get_company_details ##


            ## to display all the records ##
            public function fetch_border_details($user=null, $oid=null){
                
                if(!$user)
                {
                    $user = $this->user;
                }
                if($oid)
                {
                    $query = "SELECT * FROM ireads_corp.bookshelf_order WHERE bookshelf_order_id = ".$oid." ";
                    $data = $this->query($query, array(),$this->con);
                    if(!empty($data))
                    {
                      return $data[0];
                    }
                }
                return false;
                $query = "";
                $result = $this->query($query,array(),$this->con);
                return $result;
            }
            ## END to display all the records ##

            public function get_bshelf_orders($cid=null) {

            $bshelf_orders = array();

           $query = "SELECT bk.bookshelf_order_id, bk.order_status, cp.user_id, bk.issue_date, cp.company_id FROM ireads_corp.bookshelf_order AS bk INNER JOIN ireads_corp.corporate_user_mapping AS cp ON cp.user_id = bk.user_id WHERE bk.issue_date > '".$this->date_limit."' AND cp.company_id = ".$cid." ORDEr BY bk.issue_date  ";

            $order_data = $this->query($query, array(),$this->con);

            if(!empty($order_data))
            {
                foreach ($order_data as $r) 
                {
                    $result = date_diff(date_create($this->dt), date_create($r['issue_date']));
                    //echo $result->m.">=date_diff: ".$this->date_diff."<br>";
                    if ($result->m >= $this->date_diff && $r['order_status'] >= 3 && $r['order_status'] <= 7) 
                    {
                        // fetch specific boid details
                        array_push($bshelf_orders , $this->fetch_border_details($r['user_id'], $r['bookshelf_order_id']));
                    }
                }   
            }
            
            return $bshelf_orders;
        
            }
            ## END reissue books function start from here ##

            ## convert To Object ##
           public function array_to_object($array) {
            return (object) $array;
            }

            public function array_to_object1($array) {

                $array_new = array();
                foreach($array as $key => $value)
                {
                    $array_new[$key] = $this->array_to_object($value);
                }
                return $array_new;
            }
    // fetch bookshelf_order_tracking details
    public function fetch_bshelf_order_tracking($user=null, $boid=null) {
            if ($user) {
                    if ($boid) {
                            $query  = "SELECT * FROM ireads_corp.bookshelf_order_tracking_details WHERE bookshelf_order_id = ".$boid." ";
                            $data = $this->query($query, array(),$this->con);

                            if (!empty($data))
                            {
                                    return $this->array_to_object($data[0]);
                            }
                    }
            }
            else
            {
                    return false;
            }
    }
    ##to display all the records
    public function fetch_order_details($user=null, $pid=null)
    {
        if ($user) 
        {
            if($pid)
            {
                $query = "SELECT * FROM ireads_corp.bookshelf_order_details WHERE `p_order_id` = $pid";
                $data = $this->query($query, array(),$this->con);
                if (!empty($data))
                {
                     return $this->array_to_object($data[0]);
                }
            }
        }
        return false;
    }
    ## get parent order id 
    public function get_parent($oid=null)
    {
        if($oid)
        {
            $query = "SELECT p_order_id FROM ireads_corp.bookshelf_parent_order WHERE `bookshelf_order_id`= ".$oid."";
            $data = $this->query($query, array(),$this->con);  
         
            if (!empty($data))
            {
                 return $data[0]['p_order_id'];
            }
        }
        return false;         
    }
    ## reissues the specific boid
    public function reissue_bshelf_order($border)
    {
        if ($border)
        {
            
            $date_issue     = date_create($border->issue_date);
            $result         = $date_issue->diff(date_create($this->dt));
            $months_issue   = $result->m;


            $date_issue_new = date('Y-m-d H:i:s', strtotime($border->issue_date.' +'.$months_issue.' months'));

            //echo $border->issue_date.' date_issue_new: '.$date_issue_new."<br>";

            $order_status   = $border->order_status;

            $poid_old             = $this->get_parent($border->bookshelf_order_id);
            $bshelf_order_details = $this->fetch_order_details($border->user_id, $poid_old);

            if($order_status<8 && !empty($bshelf_order_details))
            {
                ## inserting new entries into bshelf_order
                $sql  = "INSERT INTO ireads_corp.bookshelf_order SET user_id=$border->user_id, ISBN13='$border->ISBN13', unique_book_id='$border->unique_book_id', order_status=$border->order_status, inventory_id=$border->inventory_id, mrp=$border->mrp, init_pay=$border->init_pay, amt_pay=$border->amt_pay, store_pay=$border->store_pay, disc_pay=$border->disc_pay, refund=$border->refund, store_refund=$border->store_refund, refund_type=$border->refund_type, issue_date='$date_issue_new', d_track_id='$border->d_track_id', carrier='$border->carrier', carrier_link='$border->carrier_link', order_comments='$border->order_comments'";
               
                $bshelf_order = $this->query_update($sql, array(), $this->con);
                ## getting the new boid
                $boid  = $this->con->lastInsertId();
                ## inserting new entries in bookshelf_order_tracking
                $tracking_details_old = $this->fetch_bshelf_order_tracking($border->user_id, $border->bookshelf_order_id);
                
                if ($order_status >= 4 && $order_status<8)
                {
                    ## New Delivery Date
                    $result_new_del = date_create($tracking_details_old->new_delivery)->diff(date_create($this->dt));
                    $months_new_del = $result_new_del->m;
                    $new_del_date = date('Y-m-d H:i:s', strtotime($tracking_details_old->new_delivery.' +'.$months_new_del.' months'));

                    ## Delivery Processing
                    $result_del_proc = date_create($tracking_details_old->delivery_processing)->diff(date_create($this->dt));
                    $months_del_proc = $result_del_proc->m;
                    $del_proc_date = date('Y-m-d H:i:s', strtotime($tracking_details_old->delivery_processing.' +'.$months_del_proc.' months'));
                    ## Dispatched
                    $result_dispatched = date_create($tracking_details_old->dispatched)->diff(date_create($this->dt));
                    $months_dispatched = $result_dispatched->m;
                    $dispatched_date = date('Y-m-d H:i:s', strtotime($tracking_details_old->dispatched.' +'.$months_dispatched.' months'));

                    ## Delivered
                    $result_del = date_create($tracking_details_old->delivered)->diff(date_create($this->dt));
                    $months_del = $result_del->m;
                    $del_date = date('Y-m-d H:i:s', strtotime($tracking_details_old->delivered.' +'.$months_del.' months'));
                    ## echo $border->bookshelf_order_id;
                    ## echo $order_status;die;
                    if ($order_status == 4)
                    {
                       $order_tracking  =  "INSERT INTO ireads_corp.bookshelf_order_tracking_details(bookshelf_order_id, new_delivery, delivery_processing, dispatched, delivered, new_pickup, pickup_processing, req_to_fedex, returned) VALUES ($boid, '$new_del_date', '$del_proc_date', '$dispatched_date', '$del_date', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
                      $tracking_details_new =  $this->query_update($order_tracking, array(),$this->con);

                    $update_tracking = "UPDATE ireads_corp.bookshelf_order_tracking_details SET new_pickup = '$this->dt', pickup_processing = '$this->dt', req_to_fedex = '$this->dt', returned = '$this->dt' WHERE bookshelf_order_id = ".$border->bookshelf_order_id." ";
                        $this->query_update($update_tracking, array(),$this->con);
                    }

                    if ($order_status == 5)
                    {
                        $order_tracking = "INSERT INTO ireads_corp.bookshelf_order_tracking_details(bookshelf_order_id, new_delivery, delivery_processing, dispatched, delivered, new_pickup, pickup_processing, req_to_fedex, returned) VALUES ($boid, '$new_del_date', '$del_proc_date', '$dispatched_date', '$del_date', '".$this->dt."', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
                        
                        $tracking_details_new = $this->query_update($order_tracking, array(),$this->con);

                        $update_tracking = "UPDATE ireads_corp.bookshelf_order_tracking_details SET pickup_processing = '$this->dt', req_to_fedex = '$this->dt', returned = '$this->dt' WHERE bookshelf_order_id = ".$border->bookshelf_order_id." ";

                        $this->query_update($update_tracking, array(),$this->con);

                    }

                    if ($order_status == 6)
                    {
                       $order_tracking =  "INSERT INTO ireads_corp.bookshelf_order_tracking_details(bookshelf_order_id, new_delivery, delivery_processing, dispatched, delivered, new_pickup, pickup_processing, req_to_fedex, returned) VALUES ($boid, '$new_del_date', '$del_proc_date', '$dispatched_date', '$del_date', '$this->dt', '$this->dt', '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
                        $tracking_details_new = $this->query_update($order_tracking, array(),$this->con);

                       $update_tracking =  "UPDATE ireads_corp.bookshelf_order_tracking_details SET req_to_fedex = '$this->dt', returned = '$this->dt' WHERE bookshelf_order_id = ".$border->bookshelf_order_id." ";

                      $this->query_update($update_tracking, array(),$this->con);
                    }

                    if ($order_status == 7)
                    {
                        
                        $order_tracking = "INSERT INTO ireads_corp.bookshelf_order_tracking_details(bookshelf_order_id, new_delivery, delivery_processing, dispatched, delivered, new_pickup, pickup_processing, req_to_fedex, returned) VALUES ($boid, '$new_del_date', '$del_proc_date', '$dispatched_date', '$del_date', '$this->dt', '$this->dt', '$this->dt', '0000-00-00 00:00:00')";
                        $tracking_details_new = $this->query_update($order_tracking, array(),$this->con);
                        //$tracking_details_new = $this->_db->query();
                        $sql = "UPDATE ireads_corp.bookshelf_order_tracking_details SET delivered = '$this->dt' WHERE bookshelf_order_id = ".$border->bookshelf_order_id." ";
                        $this->query_update($sql, array(),$this->con);
                    }
                

                    ## inserting new entries into bshelf_order_details
                    
                    ## print_r($bshelf_order_details);die;
                    if(!empty($bshelf_order_details->order_date))
                    {
                        $order_date = $bshelf_order_details->order_date;
                    }
                    else
                    {
                        $order_date = date('Y-m-d H:i:s');
                    }
                    $result = date_create($order_date)->diff(date_create($this->dt));
                    $months_date_order = $result->m;
                    $date_order_new = date('Y-m-d H:i:s', strtotime($order_date.' +'.$months_date_order.' months'));
                    $bsf_order = "INSERT INTO ireads_corp.bookshelf_order_details SET 
                                user_id             =   $bshelf_order_details->user_id, 
                                items_count         =   $bshelf_order_details->items_count, 
                                order_date          =   '$date_order_new', 
                                order_address_id    =   '$bshelf_order_details->order_address_id', 
                                pick_address_id     =   '$bshelf_order_details->pick_address_id', 
                                price               =   '$bshelf_order_details->price', 
                                coupon_discount     =   $bshelf_order_details->coupon_discount, 
                                store_discount      =   $bshelf_order_details->store_discount, 
                                shipping_charge     =   $bshelf_order_details->shipping_charge, 
                                cod_charge          =   $bshelf_order_details->cod_charge,
                                net_pay             =   $bshelf_order_details->net_pay, 
                                coupon_code         =   '$bshelf_order_details->coupon_code', 
                                payment_option      =   $bshelf_order_details->payment_option, 
                                payment_status      =   $bshelf_order_details->payment_status, 
                                response_code       =   $bshelf_order_details->response_code, 
                                response_msg        =   '$bshelf_order_details->response_msg', 
                                payment_id          =   '$bshelf_order_details->payment_id', 
                                transaction_id      =   '$bshelf_order_details->transaction_id', 
                                invoice             =   '$bshelf_order_details->invoice', 
                                dispatch            =   '$bshelf_order_details->dispatch'"; 

                    $bshelf_details_data = $this->query_update($bsf_order, array(),$this->con);
                   
                    $poid_new  = $this->con->lastInsertId();

                    ## poid and boid mapping
                    $boid_mapping = "INSERT INTO ireads_corp.bookshelf_parent_order(p_order_id, bookshelf_order_id) VALUES ($poid_new, $boid)";

                    $this->query_update($boid_mapping, array(),$this->con);

                    $update_bo = "UPDATE ireads_corp.bookshelf_order SET order_status = 8, expected_return_date ='".$this->dt."' WHERE bookshelf_order_id = $border->bookshelf_order_id";
                    $this->query_update($update_bo, array(),$this->con);

                    //echo 'New Order id: '.$boid.", Old Order id: ".$border->bookshelf_order_id.' poid_new: '.$poid_new;die;
                    
                }
            }

        }
        else
        {
            return false;
        }
    }
public function insert_paytm_data($customer_id, $MID, $ORDERID, $TXNAMOUNT, $CURRENCY, $TXNID, $GATEWAYNAME,$BANKNAME,$PAYMENTMODE)
    {
        $query = "INSERT INTO ireads_corp. paytm_orders SET customer_id='$customer_id', mid='$MID', order_id='$ORDERID', amount='$TXNAMOUNT', currency='$CURRENCY', transaction_id='$TXNID', bank_name='$BANKNAME', gateway_name='$GATEWAYNAME', payment_mode='$PAYMENTMODE'";
        $this->query_update($query, array(),$this->con);
    }

    #########################################################################################
    ###########################################################################################
    ############################################################################################
    ####################kota functions#############################
    public function get_all_merchant_kota(){
        $query2 = "SELECT * FROM irbbi4yp_bookbank.merchant";
        $result2 = $this->query($query2,array(),$this->con);
        return $result2;
    }
    public function add_to_kota_inven($LibraryBookID,$ISBN13,$ProcPrice,$Mer_Name,$discount,$mrp){

        $query2 = "SELECT * FROM irbbi4yp_bookbank.procurement where library_id='$LibraryBookID'";
        $result2 = $this->query($query2,array(),$this->con);
        if($result2){
            $i_msg="Book ID  ".$LibraryBookID." already exists, try with another one";
        }
        else{
            $query = "INSERT INTO irbbi4yp_bookbank.procurement(`library_id`,`isbn`,`mid`,`mrp`,`pro_price`,`discount`) VALUES ('$LibraryBookID','$ISBN13','$Mer_Name','$mrp','$ProcPrice','$discount')";
            $result = $this->query_update($query, array(), $this->con);
            $i_msg="Book is added successfully";
        }
        return $i_msg; 
            
    }
    public function get_books_by_mer_kota($Mer_Name){
        $query2 = "SELECT * FROM irbbi4yp_bookbank.procurement join irbbi4yp_bookbank.merchant on procurement.mid=merchant.mid   where procurement.mid='$Mer_Name'";
        $result2 = $this->query($query2,array(),$this->con);
        return $result2;
    }
    public function get_books_by_date_kota($date_start,$date_end){
        $query2 = "SELECT * FROM irbbi4yp_bookbank.procurement join irbbi4yp_bookbank.merchant on procurement.mid=merchant.mid   where date(procurement.date) between '$date_start' and '$date_end'";
        $result2 = $this->query($query2,array(),$this->con);
        return $result2;
    }
    public function update_kota_book_price($ISBN13,$mrp,$sell){
        $query1 = "UPDATE irbbi4yp_bookbank.books SET mrp ='$mrp',sp = '$sell' WHERE ISBN = '$ISBN13'";
         $result1 = $this->query_update($query1,array(),$this->con);
         return "Book updated successfully";
    }

    // aman - stats
    public function get_user_details_login($username,$password){
        $query1 = "SELECT * FROM irbbi4yp_bookbank.stat_users where username='$username' AND password='$password'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_allen(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as allen_count FROM irbbi4yp_allen_bookbank.user where created ='$date_today' ";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_bookbank(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as bookbank_count FROM irbbi4yp_bookbank.user where created ='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_cp(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as cp_count FROM irbbi4yp_cp.user where created ='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_delivery(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as delivery_count FROM irbbi4yp_delivery.user where created ='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_landmark(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as landmark_count FROM irbbi4yp_landmark.user where created ='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_motion(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today=date("Y-m-d h:i:sa");
        $query1 = "SELECT COUNT(user_id) as motion_count FROM irbbi4yp_motion.user where created ='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_allen_alone(){
        $query1 = "SELECT COUNT(user_id) as allen_count FROM irbbi4yp_allen_bookbank.user ";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_bookbank_alone(){
        $query1 = "SELECT COUNT(user_id) as bookbank_count FROM irbbi4yp_bookbank.user";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_cp_alone(){
        $query1 = "SELECT COUNT(user_id) as cp_count FROM irbbi4yp_cp.user";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_delivery_alone(){
        $query1 = "SELECT COUNT(user_id) as delivery_count FROM irbbi4yp_delivery.user";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_landmark_alone(){
        $query1 = "SELECT COUNT(user_id) as landmark_count FROM irbbi4yp_landmark.user ";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

    public function get_user_login_count_motion_alone(){
        $query1 = "SELECT COUNT(user_id) as motion_count FROM irbbi4yp_motion.user";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    // function to get total purchase of kota 
    public function get_total_purchase_of_kota(){
        $query1 = "SELECT SUM(pro_price) as total FROM irbbi4yp_bookbank.procurement";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_total_purchase_of_kota_today(){
        date_default_timezone_set('Asia/Calcutta');
        $date_today1=date("Y-m-d h:i:sa");
        $date_today=date('Y-m-d', strtotime($date_today1));
        $query1 = "SELECT SUM(pro_price) as total FROM irbbi4yp_bookbank.procurement WHERE date(date)='$date_today'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_total_sale_of_kota(){
        $query1 = "SELECT SUM(price) as total FROM (SELECT price FROM irbbi4yp_bookbank.issued UNION ALL SELECT price FROM irbbi4yp_motion.issued UNION ALL SELECT price FROM irbbi4yp_landmark.issued UNION ALL SELECT price FROM irbbi4yp_delivery.issued UNION ALL SELECT price FROM irbbi4yp_cp.issued UNION ALL SELECT price FROM irbbi4yp_allen_bookbank.issued) t";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
     public function get_total_sale_of_kota_today(){
          date_default_timezone_set('Asia/Calcutta');
        $date_today1=date("Y-m-d h:i:sa");
       $date_today=date('Y-m-d', strtotime($date_today1));
        $query1 = "SELECT SUM(price) as total FROM (SELECT price FROM irbbi4yp_bookbank.issued WHERE date(issue_date) = '$date_today' UNION ALL SELECT price FROM irbbi4yp_motion.issued WHERE date(issue_date) = '$date_today' UNION ALL SELECT price FROM irbbi4yp_landmark.issued WHERE date(issue_date) = '$date_today' UNION ALL SELECT price FROM irbbi4yp_delivery.issued WHERE date(issue_date) = '$date_today' UNION ALL SELECT price FROM irbbi4yp_cp.issued WHERE date(issue_date) = '$date_today' UNION ALL SELECT price FROM irbbi4yp_allen_bookbank.issued WHERE date(issue_date) = '$date_today') t";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_delivery_between_dates($date_start,$date_end,$corp_flag){
        if($corp_flag==1){
            $query1 = "SELECT bo.bookshelf_order_id as order_id,bo.ISBN13 as isbn,bo.unique_book_id as book_id,bo.user_id as user_id,botd.dispatched as d_date FROM ireads_corp.bookshelf_order_tracking_details as botd join ireads_corp.bookshelf_order as bo on botd.bookshelf_order_id=bo.bookshelf_order_id WHERE date(botd.dispatched) BETWEEN '$date_start' and '$date_end' ";
        }
        if($corp_flag==2){
            $query1 = "SELECT bo.bookshelf_order_id as order_id,bo.ISBN13 as isbn,bo.unique_book_id as book_id,bo.user_id as user_id,botd.dispatched as d_date FROM bookshelf_order_tracking_details as botd join bookshelf_order as bo on botd.bookshelf_order_id=bo.bookshelf_order_id WHERE date(botd.dispatched) BETWEEN '$date_start' and '$date_end' ";
        }
        
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_user_details_login_stats($username,$password){
        $query1 = "SELECT * FROM irbbi4yp_bookbank.stat_users where username='$username' AND password='$password'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
     public function get_number_of_users_all(){
        $query1 = "SELECT count(*) as total FROM user_login";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_revenue_from_table($date_start){
        $query1 = "SELECT * FROM revenue WHERE date(which_year)='$date_start'";
        $result2 = $this->query($query1,array(),$this->con);  
        return $result2;
    }
    public function get_total_sale_of_kota_date($date_start,$date_end){
          
        $query1 = "SELECT SUM(price) as total,count(price) as count FROM (SELECT price FROM irbbi4yp_bookbank.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end' UNION ALL SELECT price FROM irbbi4yp_motion.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end' UNION ALL SELECT price FROM irbbi4yp_landmark.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end' UNION ALL SELECT price FROM irbbi4yp_delivery.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end' UNION ALL SELECT price FROM irbbi4yp_cp.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end' UNION ALL SELECT price FROM irbbi4yp_allen_bookbank.issued WHERE date(issue_date) BETWEEN '$date_start' and '$date_end') t";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    public function get_total_purchase_of_kota_date($date_start,$date_end){
         
        $query1 = "SELECT SUM(pro_price) as total,count(*) as count FROM irbbi4yp_bookbank.procurement WHERE date(date) BETWEEN '$date_start' AND '$date_end'";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }
    
     public function get_revenue_from_table_kota($date_start){
        $query1 = "SELECT * FROM irbbi4yp_bookbank.revenue WHERE date(which_year)='$date_start'";
        $result2 = $this->query($query1,array(),$this->con);  
        return $result2;
    }
    public function get_users_of_kota_date($date_start,$date_end){
          
        $query1 = "SELECT  count(user_id) as count FROM (SELECT user_id FROM irbbi4yp_bookbank.user WHERE created BETWEEN '$date_start' and '$date_end' UNION ALL SELECT user_id FROM irbbi4yp_motion.user WHERE created BETWEEN '$date_start' and '$date_end' UNION ALL SELECT user_id FROM irbbi4yp_landmark.user WHERE created BETWEEN '$date_start' and '$date_end' UNION ALL SELECT user_id FROM irbbi4yp_delivery.user WHERE created BETWEEN '$date_start' and '$date_end' UNION ALL SELECT user_id FROM irbbi4yp_cp.user WHERE created BETWEEN '$date_start' and '$date_end' UNION ALL SELECT user_id FROM irbbi4yp_allen_bookbank.user WHERE created BETWEEN '$date_start' and '$date_end') t";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }


    
    // mahindra
     public function get_mahindra_orders(){
          
        $query1 = "SELECT u.user_email,bo.unique_book_id,sub_master.title,sub_master.long_desc,sub_master.short_desc,bo.order_status FROM ireads_corp.user_login as u join ireads_corp.corporate_user_mapping as cum on u.user_id = cum.user_id join ireads_corp.bookshelf_order as bo on u.user_id=bo.user_id join sub_mastersheet sub_master on bo.ISBN13 = sub_master.ISBN13 WHERE cum.company_id=23 and bo.order_status BETWEEN 2 and 4 LIMIT 5";
        $result2 = $this->query($query1,array(),$this->con);
        return $result2;
    }

}
?>
