 <?php

include_once('../config/config.php');
include_once('../config/functions.php');
$con = connect($config);
global $i_msg;
 $i_msg="";
$fun_obj = new ireads($con);
include_once('../rent-request/pagination2.php');
include_once('../mail/mail.php'); 

 	
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>siteadmin</title>
		<link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="../style.css" rel="stylesheet">
		<script src="../Bootstrap/js/respond.js"></script>
	</head>
	<body>
		<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>
		<?php
		$page_class='more';
		include_once('../headers/main-header.php');
		?>
		<div class="container">
			<nav class="sub-head navbar navbar-default">
				<div class="container-fluid">
					<ul class="nav navbar-nav">
						<li class="active"><a href="get-users.php">Get Users</a></li>
					</ul>
				</div>
			</nav>
			<div class="col-md-12">
				<form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-sm-4" for="cor_name">Select Corporate:</label>
							<div class="col-sm-8 no-left-pad">
								<select required class="form-control"  name="cor_name" id="cor_name" >
									<option value="" selected>Select Corporate</option> 
									<?php
									echo '<option value="9999">Retail</option>';
									$res_cor = $fun_obj->get_all_corporates();
									foreach ($res_cor as $key1) {
									echo '<option value="'.$key1['company_id'].'">'.$key1['company_name'].'</option>';
									}
									
									?>
								</select>
								<div id="loader" class="sp sp-circle loader"></div>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div><hr><Hr>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}else{}
?> 
<script type="text/javascript">
 
$(document).ready(function() {
    $('.loader').hide()
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(10000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});
$("#cor_name").change(function() {
	$('.loader').show();
	 var cor_id = $.trim($('#cor_name').val());
	var html='';
  $.ajax({
    url: '<?php echo base_url; ?>get-users/get-users-ajax.php',
    method: "POST",
    data: {
      cor_id: cor_id
    },
    dataType: 'json',
	   }).success(function(res) {
		    //alert(res.email);
		    if(res=="false"){
		    	$('.loader').hide();
		    	$('#textarea_to').text(html);
		    	html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>No users are unverified/some error</div>';
                 $('body').append(html);
	             $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
	               $("#i-alert").alert('close');
	             });
		    }
		    else{
		   		var arr = [];
                for(var arr1 in res){
                  arr.push(res[arr1]);
                }
                  var csvContent = "data:text/csv;charset=utf-8,";
                  arr.forEach(function(infoArray, index){
                     dataString = infoArray+",";
                     csvContent += index < arr.length ? dataString+ "\n" : dataString;
                  }); 
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "my_data.csv");
                  document.body.appendChild(link);  
                  link.click();   
                  if (typeof(Storage) !== "undefined") {
                      localStorage.setItem("Status","User sheet is generated");
                      window.location.reload();
                   }
		   		$('.loader').hide();
		    }
		   
  	});
});	

 
</script>
 
	</body>
</html>
