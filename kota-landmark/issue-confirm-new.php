<?php

ob_start();

include_once('pages/connect.php');
require_once('tcpdf_include.php');
require_once('tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Indiareads');
$pdf->SetTitle('Indiareads Invoice - Issue');
$pdf->SetSubject('Issue Request Invoice');
$pdf->SetKeywords('Invoice');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 009', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('helvetica','',9);
$pdf->AddPage();


$booksarr = array();
$libidarr = array();
$pricearr = array();

$vall=0;
$finalmsg="";
$totalp = 0;
$date = date('Y-m-d h:i:s.00000');
$invoice_id=$_SESSION['uid'].'_'.$_POST['req1'].'_'.$date;
$in="invoice_details/".$invoice_id.".pdf";
	$a = $_POST['count'];
	$_SESSION['c'] = $a;

$uuid = $_SESSION['uid'];
$query=mysql_query("SELECT * FROM user where user_id = '$uuid'");
$r=mysql_num_rows($query);
if($r!=0)
{
	while($row = mysql_fetch_array($query))
	{
		$fname=$row['fname'];
		$lname=$row['lname'];
		$phone=$row['phone'];
	}
}
	
	for($i=1; $i<=$a; $i++)
	{
			$rid = $_POST['req'.$i];
			$lid = $_POST['lid'.$i];
			if($lid != "")
			{
				$query = mysql_query("SELECT * FROM pending_request WHERE req_id = '{$rid}'");
				$res = mysql_fetch_array($query);
				$uid = $res['user_id'];
				$isbn = $res['ISBN'];
				$price = $res['price'];	
				
				
				
				$check = 0;
				$q = mysql_query("SELECT * FROM library WHERE library_id = '{$lid}'");
				$check = mysql_num_rows($q);	
				if($check == 0)
				{
					$pp = $_POST['pp'.$i];
					$status = 1;
					mysql_query("INSERT INTO library VALUES('$lid','$isbn','$pp','$status')");
				}			
				mysql_query("INSERT INTO issued VALUES ('$rid','$uid','$isbn','$lid','$price','$date','','','')");
				
				
				
				echo mysql_error();
				$vall=1;
				$b_name=mysql_query("SELECT title from books WHERE ISBN = '$isbn'");
				if(mysql_num_rows($b_name));
				{
				$row = mysql_fetch_array($b_name);
				
				array_push($booksarr, $row['title']);
				array_push($libidarr, $lid);
				array_push($pricearr, $price);
				
				$totalp = $totalp + $price;
							
		//		$finalmsg=$finalmsg.'<br><br>Title: '.$row['title'].', Req_id: '.$rid.', ISBN: '.$isbn.', Lib Id: '.$lid.', User Id: '.$uid.', Price: '.$price;
mysql_query("INSERT INTO issued_details VALUES ('$uid','$rid','$title','$date','$in')");				}

				mysql_query("UPDATE library SET status = 2 WHERE library_id = '{$lid}'"); // 1=IN 2=OUT
				echo mysql_error();
				
				mysql_query("DELETE FROM pending_request WHERE req_id = '{$rid}'");
				echo mysql_error();
			}
			
	}
	
	$_SESSION['tot'] = $totalp;
	
	if($vall==1)
	{
	
$finalmsg = library_order_invoice($invoice_id, $date, $fname, $lname, $phone, $booksarr, $libidarr, $pricearr);
$pdf->writeHTML($finalmsg, true, 0 , true);
                                $pdf->lastPage();
                                $f=$invoice_id.".pdf";

                                $pdf->Output('issue_invoice/'.$f, 'F');
                                $pdf->Output('issue_invoice/'.$f, 'I');
                                }
                                
                                
function library_order_invoice($invoice_id, $date, $fname="", $lname="", $phone, $books, $libid, $price)
{
$name=$fname." ".$lname;
$i = 0;
$isbn = "";
$title= "";
$qty = 1;
$mrp = 0;
$disc = 0;
$str="";
$amount = ($mrp*$qty) - $disc;
$sno = 1;
for($x=0;$x<$_SESSION['c'];$x++)
{
if($libid[$x] != "")
{
	$x1=$sno;
	$sno++;
	$str=$str.$x1.". ".$books[$x]." ( ".$libid[$x]." )         - Rs.".$price[$x]."<br>";
}
}
$finalmsg='<!DOCTYPE html>
<html>
<head
<title>Print Invoice</title>
<style>
*{
margin:0;
padding:0;
font-family:Arial;
font-size:10pt;
color:#000;
}
body{
width:100%;
font-family:Arial;
font-size:10pt;
margin:0;
padding:0;
}
.heads
{
	font-size:17px;
	font-weight:bold;
	line-height:22px;
}
.smallheads
{
	font-size:13px;
	font-weight:bold;
	line-height:20px;
}
#dates
{
	position:absolute;
	right:0px;
	width:300px;
	display:inline-block;
	margin-left:500px;
}
</style>
</head>
<body>
Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['uid'].'</h1>
Ph. :'.$phone.' <br>

<h3 class="heads"> Books in Packet : </h3>
<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$_SESSION['tot'].'</h3>

--------------------------------------------------------------------------------------------------------------------------------------------------<br>
Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['uid'].'</h1>
Ph. :'.$phone.' <br>
<h3 class="heads"> Books in Packet : </h3>
<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$_SESSION['tot'].'</h3>

--------------------------------------------------------------------------------------------------------------------------------------------------<br>

Date: '.$date.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice Number:'.$invoice_id.'

<h1 class="heads">'.$name.'<font style="font-size:15px;font-weight:normal";>   User Id:- </font>'.$_SESSION['uid'].'</h1>
Ph. :'.$phone.' <br>
<h3 class="heads"> Books in Packet : </h3>
<h3 class="smallheads">'.$str.'<br>Total: Rs.'.$_SESSION['tot'].'</h3>

</body>
</html>';

return $finalmsg;

}

	
?>
<br/>
Done | <a href="index.php">Back Home</a>
<br/><br/>