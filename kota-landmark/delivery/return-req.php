<?php
include_once('connect.php');

function time_diff($dt1,$dt2){
    $y1 = substr($dt1,0,4);
    $m1 = substr($dt1,5,2);
    $d1 = substr($dt1,8,2);
    $h1 = substr($dt1,11,2);
    $i1 = substr($dt1,14,2);
    $s1 = substr($dt1,17,2);    

    $y2 = substr($dt2,0,4);
    $m2 = substr($dt2,5,2);
    $d2 = substr($dt2,8,2);
    $h2 = substr($dt2,11,2);
    $i2 = substr($dt2,14,2);
    $s2 = substr($dt2,17,2);    

    $r1=date('U',mktime($h1,$i1,$s1,$m1,$d1,$y1));
    $r2=date('U',mktime($h2,$i2,$s2,$m2,$d2,$y2));
    return ($r1-$r2)/(60*60*24*30);

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Return Request</title>
<style>
#addform{
	width:1400px;
	height:500px;	
}
#title{
	width: 350px;
	padding:4px;
	margin:2px 0;
	background-color:#EEE;
	float:left;	
	height:30px;
	overflow:hidden;	
}
#mrp{
	width: 80px;
	padding:4px;
	margin:2px 0;
	height:30px;
	background-color:#EEE;
	float:left;		
}
#discount{
	width: 150px;
	height:30px;
	padding:4px;
	margin:2px 0;
	background-color:#EEE;
	float:left;		
}
#final{
	width: 100px;
	padding:4px;
	height:30px;
	margin:2px 0;
	background-color:#EEE;
	float:left;	
}

</style>
</head>
<body>
<a href="index.php">Back to Home</a><br/><br/>
<center><h1>Return Request</h1></center>
<div id="addform">
	<div id="mrp">Request ID</div>
    <div id="mrp">user ID</div>
    <div id="title">Title</div>
    <div id="discount">Library ID</div>
    <div id="discount">Issue Date</div>
    <div id="mrp">Charges</div>
    <div id="mrp">Refund</div>
    <div id="discount">Preferred Pickup Time</div>
    <div id="discount">Option</div><div style="clear:both"></div>

	<?php 
		$uid = $_SESSION['uid'];
		$query = mysql_query("SELECT * FROM issued WHERE user_id = '{$uid}' AND status = 2");
		while($row = mysql_fetch_array($query))
		{
	?>
    <div id="mrp"><?php echo $row['req_id']; ?></div>
	<div id="mrp"><?php echo $row['user_id']; ?></div>
    <?php
		$isbn = $row['ISBN'];
		$query2 = mysql_query("SELECT title,mrp,sp FROM books WHERE ISBN = '{$isbn}'");
		$res2 = mysql_fetch_array($query2);
		$mrp = $res2['mrp'];
		$sp = $res2['sp'];
	?>
    <div id="title"><?php echo $res2['title']; ?></div>
    <div id="discount"><?php echo $row['library_id']; ?></div>
    <div id="discount"><?php echo $row['issue_date']; ?></div>
    <?php
	$dt1 = date_create($row['issue_date']);
	$dt2 = date_create(date('Y-m-d h:i:s'));
	$diffd = date_diff($dt2,$dt1);
	$diff = $diffd->m;
	if($diffd->d > 0)
		$diff++;
	$price = $row['price'];
	
	if($row['pptime'] == NULL)
	{
		$ptym = date('d-M-Y H:i');
	}
	else
	{
		$ptym = $row['pptime'];
	}
	
/*
	if($diff>=0 && $diff<1) { $cost =  (20/100)*$mrp;}
	if($diff>=1 && $diff<2) { $cost =  (25/100)*$mrp;}
	if($diff>=2 && $diff<3) { $cost =  (30/100)*$mrp;}
	if($diff>=3 && $diff<4) { $cost =  (35/100)*$mrp;}
	if($diff>=4 && $diff<6) { $cost =  (40/100)*$mrp;}
	if($diff>=6 && $diff<12) { $cost =  (50/100)*$mrp;}
*/

	if($diff<=1) { $cost =  (20/100)*$sp;}
	else if($diff>1 && $diff<=3) { $cost =  (30/100)*$sp;}
	else if($diff>3 && $diff<=6) { $cost =  (40/100)*$sp;}
	else if($diff>6 && $diff<=12) { $cost =  (50/100)*$sp;}
	else { $cost = $sp; }
	$cost = ceil($cost);
	$refund = $price - $cost;
    ?>
    <div id="mrp"><?php echo $cost; ?></div>
    <div id="mrp"><?php echo $refund; ?>(<?=$diff?>) </div>
    <div id="discount"><form action="update_ptime.php" method="post"><input type="hidden" name="hrid" value="<?=$row['req_id']?>" /><input type="text" name="ptime" value="<?=$ptym?>" /><input type="submit" name="upd" value="Update" /></form></div>
    <div id="discount"><a href="return-process.php?a=<?php echo $row['req_id'] ?>">Process</a></div><div style="clear:both"></div>
    
<?php } ?>

<center><h1>Return Processing</h1></center>
<div id="addform">
	<div id="mrp">Request ID</div>
    <div id="mrp">user ID</div>
    <div id="title">Title</div>
    <div id="discount">Library ID</div>
    <div id="discount">Issue Date</div>
    <div id="mrp">Charges</div>
    <div id="mrp">Refund</div>
    <div id="discount">Preferred Pickup Time</div>
    <div id="discount">Option</div><div style="clear:both"></div>

	<?php 
		$uid = $_SESSION['uid'];
		$query = mysql_query("SELECT * FROM issued WHERE user_id = '{$uid}'  AND status = 3");
		while($row = mysql_fetch_array($query))
		{
	?>
    <div id="mrp"><?php echo $row['req_id']; ?></div>
	<div id="mrp"><?php echo $row['user_id']; ?></div>
    <?php
		$isbn = $row['ISBN'];
		$query2 = mysql_query("SELECT title,mrp,sp FROM books WHERE ISBN = '{$isbn}'");
		$res2 = mysql_fetch_array($query2);
		$mrp = $res2['mrp'];
		$sp = $res2['sp'];
	?>
    <div id="title"><?php echo $res2['title']; ?></div>
    <div id="discount"><?php echo $row['library_id']; ?></div>
    <div id="discount"><?php echo $row['issue_date']; ?></div>
    <?php
	$dt1 = date_create($row['issue_date']);
	$dt2 = date_create(date('Y-m-d h:i:s'));
	$diffd = date_diff($dt2,$dt1);
	$diff = $diffd->m;
	if($diffd->d > 0)
		$diff++;
	$price = $row['price'];
	
/*
	if($diff>=0 && $diff<1) { $cost =  (20/100)*$mrp;}
	if($diff>=1 && $diff<2) { $cost =  (25/100)*$mrp;}
	if($diff>=2 && $diff<3) { $cost =  (30/100)*$mrp;}
	if($diff>=3 && $diff<4) { $cost =  (35/100)*$mrp;}
	if($diff>=4 && $diff<6) { $cost =  (40/100)*$mrp;}
	if($diff>=6 && $diff<12) { $cost =  (50/100)*$mrp;}
*/

	if($diff<=1) { $cost =  (20/100)*$sp;}
	else if($diff>1 && $diff<=3) { $cost =  (30/100)*$sp;}
	else if($diff>3 && $diff<=6) { $cost =  (40/100)*$sp;}
	else if($diff>6 && $diff<=12) { $cost =  (50/100)*$sp;}
	else { $cost = $sp; }
	$cost = ceil($cost);
	$refund = $price - $cost;
    ?>
    <div id="mrp"><?php echo $cost; ?></div>
    <div id="mrp"><?php echo $refund; ?>(<?=$diff?>) </div>
    <div id="discount"><?=$row['pptime']?></div>
    <div id="discount"><a href="return-confirm.php?a=<?php echo $row['req_id'] ?>&b=<?php echo $cost ?>&c=<?php echo $refund ?>&d=<?php echo $row['library_id'] ?>&e=<?php echo $res2['title'] ?>">Return</a></div><div style="clear:both"></div>
    
<?php } ?>

<center><h1>Returned</h1></center>
<div id="addform">
	<div id="mrp">Request ID</div>
    <div id="mrp">user ID</div>
    <div id="title">Title</div>
    <div id="discount">Library ID</div>
    <div id="discount">Issue Date</div>
    <div id="mrp">Charges</div>
    <div id="mrp">Refund</div>
    <div id="discount">Option</div><div style="clear:both"></div>

	<?php 
		$uid = $_SESSION['uid'];
		$query = mysql_query("SELECT * FROM issued WHERE user_id = '{$uid}' AND status = 4");
		while($row = mysql_fetch_array($query))
		{
	?>
    <div id="mrp"><?php echo $row['req_id']; ?></div>
	<div id="mrp"><?php echo $row['user_id']; ?></div>
    <?php
		$isbn = $row['ISBN'];
		$query2 = mysql_query("SELECT title,mrp,sp FROM books WHERE ISBN = '{$isbn}'");
		$res2 = mysql_fetch_array($query2);
		$mrp = $res2['mrp'];
		$sp = $res2['sp'];
	?>
    <div id="title"><?php echo $res2['title']; ?></div>
    <div id="discount"><?php echo $row['library_id']; ?></div>
    <div id="discount"><?php echo $row['issue_date']; ?></div>
    <?php
	$dt1 = date_create($row['issue_date']);
	$dt2 = date_create(date('Y-m-d h:i:s'));
	$diffd = date_diff($dt2,$dt1);
	$diff = $diffd->m;
	if($diffd->d > 0)
		$diff++;
	$price = $row['price'];
	
/*
	if($diff>=0 && $diff<1) { $cost =  (20/100)*$mrp;}
	if($diff>=1 && $diff<2) { $cost =  (25/100)*$mrp;}
	if($diff>=2 && $diff<3) { $cost =  (30/100)*$mrp;}
	if($diff>=3 && $diff<4) { $cost =  (35/100)*$mrp;}
	if($diff>=4 && $diff<6) { $cost =  (40/100)*$mrp;}
	if($diff>=6 && $diff<12) { $cost =  (50/100)*$mrp;}
*/

	if($diff<=1) { $cost =  (20/100)*$sp;}
	else if($diff>1 && $diff<=3) { $cost =  (30/100)*$sp;}
	else if($diff>3 && $diff<=6) { $cost =  (40/100)*$sp;}
	else if($diff>6 && $diff<=12) { $cost =  (50/100)*$sp;}
	else { $cost = $sp; }
	$cost = ceil($cost);
	$refund = $price - $cost;
    ?>
    <div id="mrp"><?php echo $cost; ?></div>
    <div id="mrp"><?php echo $refund; ?>(<?=$diff?>) </div>
    <div id="discount"></div><div style="clear:both"></div>
    
<?php } ?>


</div>
</body>
</html>