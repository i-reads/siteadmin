<?php

include_once('../connect.php');

$tdt = date('Y-m-d 00:00:00');

$totals = 0;
$totalr = 0;

function fetch_book_name($isbn)
{
	$sel = mysql_query("select title from books where ISBN = ".$isbn."");
	if($arr = mysql_fetch_array($sel))
	{
		return $arr['title'];
	}
	return 'Not Found';
}

function fetch_user_name($uid)
{
	$sel = mysql_query("select fname, lname, phone from user where user_id = ".$uid."");
	if($arr = mysql_fetch_array($sel))
	{
		return $arr['fname'].' '.$arr['lname'].' - '.$arr['phone'].'  ';
	}
	return 'Not Found';
}

if(isset($_REQUEST['filter']))
{
	$date1 = $_REQUEST['datepicker1'];
	$date2 = $_REQUEST['datepicker2'];
	$sort = "";
	$qr = "";
	
	if($date1 != "")
	{
		$qr .= " date(i.issue_date) >= '".$date1."' ";
	}
	
	if($date1 != "" && $date2 != "")
	{
		$sort = $date1." to ".$date2;
		$qr .= " and ";
	}
	else if($date1 != "" && $date2 == "")
	{
		$sort = $date1." to today";
	}
	else if($date1 == "" && $date2 != "")
	{
		$sort = "Till ".$date2;
	}
	else
	{
		$sort = "Overall";
		$qr = " 1";
	}
	
	if($date2 != "")
	{
		$qr .= " date(i.issue_date) <= '".$date2."' ";
	}
}
else
{
	$td = date('Y-m-d');
	$sort = $td;
	$qr = " date(i.issue_date) = '".$td."' ";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Book Bank Stats</title>
<link rel="stylesheet" href="css/default.css" type="text/css">
<script type="text/javascript">
	SyntaxHighlighter.defaults['toolbar'] = false;
        SyntaxHighlighter.all();
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/zebra_datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>

</head>

<body>

<h1>Book Bank Stats | <a href="index.php" style="font-size:16px;">Back</a></h1>

<div id="addform">

<h4> Select Range :  
    <form name="myform" method="post" >
	<div class="main-wrapper">
            <div style="float:left">
                <input id="datepicker1" name="datepicker1" type="text">
            </div>
            <div style="float:left">
        	TO
            </div>
            <div>
                <input id="datepicker2" name="datepicker2" type="text">
            </div>
            
            <input type="submit" id="filter" name="filter" text="Filter" />
        </div>
    </form>
</h4>

<h3> Delivery Stats (<?=$sort?>) </h3>
<?php

$sales = mysql_query("select sum(i.price) from issued i where ".$qr);
if($sale = mysql_fetch_array($sales))
{
	$totals = $sale[0];
}
?>
<h3>Total Sale = <?=$totals?></h3>
<table cellspacing="5" border="1">
	<tr>
		<th> Request ID </th>
		<th> User </th>
		<th> Book </th>
		<th> Library Book-ID </th>
		<th> Issue Date </th>
		<th> Price </th>  
		<th> Invoice </th>
	</tr>
<?php

$reqs = mysql_query("select * from issued i, issued_details id where i.req_id = id.reqid and ".$qr);
while($req = mysql_fetch_array($reqs))
{
	$reqid = $req['req_id'];
	$uid = $req['user_id'];
	$uname = fetch_user_name($uid);
	$isbn = $req['ISBN'];
	$bname = fetch_book_name($isbn);
	$libid = $req['library_id'];
	$idate = $req['issue_date'];
	$price = $req['price'];
	$invpath = $req['invoice'];
	$invfile = explode('/', $invpath);
	$inv = $invfile[1];
	echo '<tr>';
		echo '<td>'.$reqid.'</td>';
		echo '<td>'.$uname.'('.$uid.')</td>';
		echo '<td>'.$bname.'('.$isbn.')</td>';
		echo '<td>'.$libid.'</td>';
		echo '<td>'.$idate.'</td>';
		echo '<td>'.$price.'</td>';
		echo '<td> <a target="_blank" href="../issue_invoice/'.$inv.'" > View/Download </a></td>';
	echo '</tr>';
}
?>

</div>

</body>

</html>