      <?php

include_once('../config/config.php');

include_once('../config/functions.php');
include_once('../mail/corp_mail.php');

global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

if(isset($_POST['return'])){
  $res1 = $fun_obj->corp_update_order_status_8($_POST['order_id'],$_POST['book_status'],$_POST['book_id'],$_POST['user_id'],$_POST['isbn']);
  if($res1){
      /*circulation entery*/
     $res_C=$fun_obj->update_circulation_details_corp($_POST['user_id'],$_POST['book_id'],$_POST['init_pay'],$_POST['refund'],$_POST['issue_date'],$_POST['pickup_date']);
     
    $phone = 0;
    //SMS script
    $msg= 'Your Pickup order with Order ID: '.$_POST["order_id"].' has been returned.';       
    // to replace the space in message with  '%20'
    $str = trim(str_replace(' ', '%20', $msg));
    $user_id = 'IndiaReads';
    $pass= 'ireads@987'; 
    $sender_id = 'INDREA';         
    $mob_no = $_POST['phone'];       
    $priority = 'ndnd';
    $type = 'normal';
    $url="http://bhashsms.com/api/sendmsg.php?user=".$user_id."&pass=".$pass."&sender=".$sender_id."&phone=".$mob_no."&text=".$str."&priority=".$priority."&stype=".$type;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_exec($ch);
    curl_close($ch);
    $result = mailer('Returned',$_POST['order_id'],$fun_obj,$res1);
    $i_msg .= $result.'<br>';
     $i_msg.='Order '.$_POST['order_id'].' is moved to Returned';
     //header("Refresh:0");
  }
  else{
    $i_msg.= "some error";
  } 
}
if(isset($_POST['generate'])){
  $res1 = $fun_obj->corp_update_order_status_5($_POST['order_id']);
  if($res1){
     $i_msg.='Order '.$_POST['order_id'].' is moved to new pickup requests';
     //header("Refresh:0");
  }
  else{
    $i_msg.= "some error";
  } 
}

include_once('../rent-request/pagination2.php');
$Order = $fun_obj->corp_get_bookshelf_order_status_pick(7,$start_from,$num_rec_per_page);
 
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
</head>

<body>
<div class="overlay1"><div id="loader" class="sp sp-circle white-loader"></div><p class="loader-text">Please wait,While we are processing...</p></div>
<?php 
    $page_class='rent-requests';  
    include_once('../headers/main-header.php');
  ?>
  <div class="container">
		<nav class="sub-head navbar navbar-default">
  <div class="container-fluid">
 
    <ul class="nav navbar-nav">
       <!-- <li  ><a href="#">All</a></li> -->
      <li ><a href="../rent-request/rent-requests.php">Retail</a></li>
      <li class="active"><a href="../corporate/rent-requests.php">Corporate</a></li>
    </ul>
  </div>
</nav>
 
 
 
    <ul id="rent-index" class="nav nav-pills">
      <li><a href="#">Stats</a></li>
      <li><a href="deleted.php">Deleted Requests</a></li>
      <li><a href="incomplete.php">Incomplete Requests</a></li> 
      <li ><a href="rent-requests.php">New Delivery Requests</a></li> 
      <li ><a href="delivery-processing.php">Delivery Processing</a></li>
      <li ><a href="dispatched.php">Dispatched</a></li> 
      <li><a href="delivered.php">Delivered</a></li> 
      <li ><a href="new-pickup.php">New Pickup Requests</a></li>
      <li ><a href="pickup-processing.php">Pickup Processing</a></li> 
      <li class="active"><a href="sent-to-fedex.php">Sent to Fedex</a></li> 
      <li><a href="returned.php">Returned</a></li>
        
    </ul>
 
<div class="margintop">
  <table class="table table-bordered table-condensed font-new-del">
    <thead>
      <tr>
      
        <th>Parent Order</th>
        <th>Order id</th>
        <th>User</th>
        <th>Order Date</th>
        <th>Pickup Date</th>
        <th>Issue Date</th>
        <th>Pickup Address</th>
        <th>Price Details</th>
        <th>Sub-Order Price Details</th>
        <th>Payment Mode and Type</th>
        <th>Book Details</th>
        <th>Assigned Book id</th>
        <th>Carrier Details</th>
        <th>Comment</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
   <?php 
   if($Order) {
   foreach ($Order as $key) {
   ?>
      <tr>
      	
        <td><?php echo $key['p_order_id']; ?></td>
        <td><?php echo $key['bookshelf_order_id']; ?></td>
        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
        <td><p><?php echo $key['order_date']; ?></p></td>
         <td><p><?php echo $key['new_pickup']; ?></p></td>
        <td><p><?php echo $key['issue_date']; ?></p></td>
        <td><p><?php echo $key['fullname']; ?></p><p><?php echo $key['address_line1']; ?>,</p><p><?php echo $key['address_line2']; ?>, <?php echo $key['city']; ?></p><p><p><?php echo $key['state'] ?> <span><?php echo $key['pincode']; ?></span></p><p id="p7"><?php echo $key['phone']; ?></p></td>
        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
        <td><?php echo $key['unique_book_id']; ?></a>
 		
        </td>
        <td><?php echo $key['carrier']; ?> <br>Track Id: <?php echo $key['d_track_id'];?><br> </td>
        <td>
	           <div class="form-group">
               <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['pickup_comments']; ?></textarea>
                <input type="hidden" id="order_id_cmmnt" value="<?php echo $key['bookshelf_order_id']; ?>">
                <a class="btn iread-btn iread-btn-white small-save-btn save_comment">Save</a> 
                <div id="loader1" class="sp sp-circle loader"></div>
             </div>     
		    </td>
        <td> 
        	<form action="" method="POST" role="form" class="margintop">
              Please select book status:
              <div class="radio">
                <label><input type="radio" value="1" name="book_status" checked>Inside</label>
              </div>
              <div class="radio">
                <label><input type="radio" value="0" name="book_status" >Damaged</label>
              </div>
              <div class="radio">
                <label><input type="radio" value="-1" name="book_status" >Lost</label>
              </div>
              <input type="hidden" value="<?php echo $key['bookshelf_order_id']; ?>" name="order_id">
              <input type="hidden" value="<?php echo $key['unique_book_id']; ?>" name="book_id">
              <input type="hidden" value="<?php echo $key['user_id']; ?>" name="user_id">
            	<input type="hidden" value="<?php echo $key['ISBN13']; ?>" name="isbn">
               <input type="hidden" value="<?php echo $key['refund']; ?>" name="refund">
              <input type="hidden" value="<?php echo $key['init_pay']; ?>" name="init_pay">
           
              <input type="hidden" value="<?php echo $key['phone']; ?>" name="phone">
              <input type="hidden" value="<?php echo $key['issue_date']; ?>" name="issue_date">
              <input type="hidden" value="<?php echo $key['new_pickup']; ?>" name="pickup_date">
        		<div class="form-group">
        			<input type="submit" class="btn btn-danger iread-btn action-btn-2 " name="return" value="Returned">
        		</div>
        		<div class="form-group">
        			<input type="submit" class="btn btn-danger iread-btn action-btn-2 " name="generate" value="Generate Fedex Pickup">
        		</div>
        	</form>
        </td>
      </tr>

      <?php } } ?>
      	
      
    </tbody>
  </table>

</div>
	</div>
  <?php
/*for pagination*/
$total=$fun_obj->corp_get_bookshelf_order_2_page(7);
include_once('../rent-request/pagination.php');
?>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.loader').hide();
    $('.overlay1').hide();
     if(localStorage.getItem("Status"))
         {     res=localStorage.getItem("Status");
               var htmla='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                $('body').append(htmla);
                localStorage.clear();
         }
    $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
        $("#i-alert").alert('close');
    });
});

 
/*script for save comment*/
$(".save_comment").click(function() {
    $(this).siblings('.loader').toggle('fast'); 
     var textval=$(this).siblings('textarea').val();
     var order=$(this).siblings('#order_id_cmmnt').val();
     var html='';
     $.ajax({
            type: "POST",
            url: '<?php echo base_url; ?>/corporate/ajax/comment_save_pickup.php',
            data: { textval : textval,
                    order : order
                  },
            dataType: 'json',
            success: function(res){  
                                    $('.loader').hide(); 
                                 
                                   html='<div class="alert alert-success i-alert" id="i-alert"><button type="button" class="close cl-btn" data-dismiss="alert">x</button>'+res+'</div>';
                                    $('body').append(html);
                                     $("#i-alert").fadeTo(5000, 500).slideUp(500, function(){
                                       $("#i-alert").alert('close');
                                     });
            }
     });
     return false; 
});
 
</script>
	</body>
</html>