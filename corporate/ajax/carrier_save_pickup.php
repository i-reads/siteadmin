 <?php

include_once('../../config/config.php');
 
include_once('../../config/functions.php');
 
$con = connect($config);
$fun_obj = new ireads($con);
$msg='';
$new_parent='';
$same_parent=1;
$selected_orders=array();
$carrier=$_POST['carrier'];
$parent=$_POST['parent'];
$track_id=$_POST['track_id'];

if(empty($_POST['selected_orders'])){

      $msg.="Please select atleast one order";
}
else{
     $selected_orders=$_POST['selected_orders'];
     /*check if all orders are from same parent order*/
     foreach ($selected_orders as $key1) {
         $every_parent=$fun_obj->corp_get_parent_order_for_every_order($key1);
         foreach ($every_parent as $key2) {$new_parent=$key2['p_order_id'];}
         if($new_parent==$parent){}
         else{$same_parent=0;}
     }
     /*if same parent order then only it will proceed the process*/
     if($same_parent==1){
        /*update tracking id and carrier*/
         $add_orders='';
         $res1='';
        foreach ($selected_orders as $key) {
             $res1= $fun_obj-> corp_update_carrier_track_id($carrier,$track_id,$key);
             $add_orders.="-".$key;
        }
         
        if($res1){
            $msg.=" Carrier ".$carrier." is saved for order ".$add_orders;
        }
        else{
            $msg.="Oops,Something went wrong";
        }
     }
     else{
         $msg.="Please select all orders from same parent order";
     }
       
}


$res[0]=$msg;
 echo json_encode($res[0]);
?>