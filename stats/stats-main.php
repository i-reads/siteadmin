 <?php
 
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);
$msg_err='';
session_start();
if(!isset($_SESSION['username'])){
	header('Location:'.base_url.'/stats/login.php');
}

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>stats</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
    <script src="<?php echo base_url;?>/Bootstrap/js/jquery-2.2.3.min.js"></script>
		 
	<script src="<?php echo base_url;?>/Bootstrap/js/bootstrap.min.js"></script>
     
</head>
<body>
 
<div class="container well">
	<h3 class="text-center margin-top-0"><img src="../headers/logo.png" style="width:140px;margin:0px auto;" class="img-responsive"></h3>
	<h2 class="text-center margin-top-0" style="margin-bottom:-10px;">Stats Noida</h2>
</div>
<div class="container">
      <nav class="sub-head navbar navbar-default">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            
            <li class="active"><a href="../stats/stats-main.php">Noida</a></li>
            <li ><a href="../stats/kota.php">Kota</a></li>
            
          </ul>
        </div>
      </nav>
      <div class="col-md-12">
        <div class="margintop">
        	<form role="form" class="col-md-12 form-inline" method="post">
            <input type="button" id="april" data-from="2017-04-01" data-to="2017-04-30" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="April 2017"/>
            <input type="button" id="may" data-from="2017-05-01" data-to="2017-05-31" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="May 2017"/>
            <input type="button" id="june" data-from="2017-06-01" data-to="2017-06-30" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="June 2017"/>
            <input type="button" id="july" data-from="2017-07-01" data-to="2017-07-31" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="July 2017"/>
            <input type="button" id="august" data-from="2017-08-01" data-to="2017-08-31" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="August 2017"/>
            <input type="button" id="september" data-from="2017-09-01" data-to="2017-09-30" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="September 2017"/>
            <input type="button" id="october" data-from="2017-10-01" data-to="2017-10-31" class="month_wise  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="October 2017"/>
             
          </form>
          <form role="form" class="col-md-12 form-inline" method="post">
          	<h4 class="text-center margintop">OR</h4>
          	<h6 class="text-center">
	            <div class="form-group">
	              <label for="date_start">Start Date:</label>
	              <input type="date" class="form-control" name="date_start" id="date_start">
	            </div>
	            <div class="form-group margin-left-15">
	              <label for="date_end">End Date:</label>
	              <input type="date" class="form-control " name="date_end" id="date_end">
	            </div> 
	            <input type="submit" name="date_wise" id="date_wise" class="  margin-left-15 btn btn-danger iread-btn iread-btn-white" value="Get Books"/>
	        </h6>
          </form>
          <?php 
            if(isset($_POST['date_wise'])){
		              if($_POST['date_start']!='' && $_POST['date_end']!=''){
		              	
          ?>
          <div class="col-md-12 margintop" id="main_col_data" style="border:1.5px solid #555;">
          	<?php 
          		echo '<h4  class="text-center">Stats from '.$_POST['date_start'].' to '.$_POST['date_end'].'</h4>';
          	?>
          	<div class="col-md-6 margintop">
	              <?php
            
	              		
	              
	               		$books = $fun_obj->get_delivery_between_dates($_POST['date_start'],$_POST['date_end'],1);
	 
		                if($books){
		                   $record_count   = count($books);
		                    echo '<h4  class="text-left">Corporate : '.$record_count.' Deliveries</h4>';
		                     }
		                else{
		                 echo '<h4  class="text-left">Corporate : 0 Deliveries</h4>';
		                  }
		                  // retail comment
		                $books1 = $fun_obj->get_delivery_between_dates($_POST['date_start'],$_POST['date_end'],2);
	 
		                if($books1){
		                   $record_count   = count($books1);
		                    echo '<h4  class="text-left">Retail : '.$record_count.' Deliveries</h4>';
		                     }
		                else{
		                  echo '<h4  class="text-left">Retail : 0 Deliveries</h4>';
		                }

		                  // procurement
		                $books2 = $fun_obj->get_books_from_procurement($_POST['date_start'],$_POST['date_end']);
		                // print_r($books2);die;
		                if($books2){
			                $record_count   = count($books2);
			                 $sum_price_b=0;
			                foreach ($books2 as $key => $value) {
			                	 $sum_price_b += $value['proc_price'];
			                }
 
			                echo '<h4 class="text-left">Books procured : '.$record_count.' worth of Rs. '.$sum_price_b.'</h4>';
			            }else{
			            	 echo '<h4 class="text-left">Books procured : 0</h4>';
			            }
	              
               ?>
            </div>
            <div class="col-md-6 margintop">
				 <h4 class="text-left">No. of corporates : 12</h4> 
				 <?php 
				 	$total_users=$fun_obj->get_number_of_users_all();
				 ?>          	
				 <h4 class="text-left">No. of Users : <?php echo $total_users[0]['total'];?></h4>
				 <?php 
				 	$total_revenue=$fun_obj->get_revenue_from_table($_POST['date_start']);
				 	if(!empty($total_revenue[0]['revenue'])){
				 	?>
				 		 <h4 class="text-left">Revenue : <?php echo $total_revenue[0]['revenue'];?></h4> 
				 	<?php
				 	}else{
				 		?>
				 		 <h4 class="text-left">Revenue : 0(try to select month)</h4> 
				 		<?php
				 	}
				 ?>          	
			<br>	          	
            </div>

        </div>
        <button class="btn btn-default margintop" id="print_noida">Print</button>
            <?php 
            	}
	              else{
             		 echo '<h4 class="text-left">Please enter both dates</h4>';
             	 }
              }
            ?>
        </div>
      </div><!-- container ends-->


	
</html>
<script type="text/javascript">
$(document).on('click', '.month_wise', function(){
 var data_from=$(this).attr('data-from');
 var data_to=$(this).attr('data-to');
 $('#date_start').val(data_from);
 $('#date_end').val(data_to);
$('#date_wise').click();
});

  $(document).on('click', '#print_noida', function(){
            var divContents = $("#main_col_data").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
 
</script>