<?php
include_once('../config/config.php');
include_once('../config/functions.php');
global $i_msg;
$i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);
$result2=$fun_obj->get_user_login_count_allen();
$result3=$fun_obj->get_user_login_count_bookbank();
$result4=$fun_obj->get_user_login_count_cp();
$result5=$fun_obj->get_user_login_count_delivery();
$result6=$fun_obj->get_user_login_count_landmark();
$result7=$fun_obj->get_user_login_count_motion();

$result8=$fun_obj->get_user_login_count_allen_alone();
$result9=$fun_obj->get_user_login_count_bookbank_alone();
$result10=$fun_obj->get_user_login_count_cp_alone();
$result11=$fun_obj->get_user_login_count_delivery_alone();
$result12=$fun_obj->get_user_login_count_landmark_alone();
$result13=$fun_obj->get_user_login_count_motion_alone();

$total_purchase=$fun_obj->get_total_purchase_of_kota();
$total_purchase_today=$fun_obj->get_total_purchase_of_kota_today();
$total_sale=$fun_obj->get_total_sale_of_kota();
$total_sale_today=$fun_obj->get_total_sale_of_kota_today();
// print_r($total_sale_today);die;
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>kota-stats</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
    
  </head>
  <body>
    
    <div class="container">
      
      <div class="jumbotron text-center">
        <h1>Kota Stats Panel</h1>
      </div>
      
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h3>Members Stats</h3>
            <p>Total Members = <?php echo $result8[0]['allen_count'] + $result9[0]['bookbank_count'] + $result10[0]['cp_count'] + $result11[0]['delivery_count'] + $result12[0]['landmark_count'] + $result13[0]['motion_count']; ?></p>
            <h4>Today Stats</h4>
            <p>Allen = <?php echo $result2[0]['allen_count']; ?></p>
            <p>Bookbank = <?php echo $result3[0]['bookbank_count']; ?></p>
            <p>CP = <?php echo $result4[0]['cp_count']; ?></p>
            <p>Delivery = <?php echo $result5[0]['delivery_count']; ?></p>
            <p>Landmark = <?php echo $result6[0]['landmark_count']; ?></p>
            <p>Motion = <?php echo $result7[0]['motion_count']; ?></p>
          </div>
          <div class="col-sm-4">
            <h3>Inventory Purchases & Sold</h3>
            <p>Total Purchase = <?php echo $total_purchase[0]['total']; ?></p>
            <p>Total Sale = <?php echo $total_sale[0]['total']; ?></p>
            <h4>Today Stats   </h4>
            <p>Purchase = <?php echo $total_purchase_today[0]['total']; ?></p>
            <p>Sale = <?php echo $total_sale_today[0]['total']; ?></p>
          </div>
          <!-- <div class="col-sm-4">
            <h3>Revenue</h3>
            <p>Total Revenue = </p>
            <h4>Today Stats</h4>
            <p>Members = </p>
            <p>Allen Sale = </p>
            <p>Landmark Sale = </p>
          </div> -->
        </div>
      </div>
      
    </div>
  </body>
</html>