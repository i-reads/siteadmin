 <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

$cor_name_v="-----------------------------select-------------------------";
 
?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='wishlist';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../wishlist/corporate-w.php">Corporate Wishlist</a></li>
        <li ><a href="../wishlist/retail-w.php">Retail Wishlist</a></li>
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-sm-4" for="cor_name">Select Corporate:</label>
              <div class="col-sm-8 no-left-pad">
                  <select required class="form-control"  name="cor_name" id="cor_name" >
                   <?php 
                      $res_cor = $fun_obj->get_all_corporates();
                      foreach ($res_cor as $key1) {
                          echo '<option value="'.$key1['company_id'].'">'.$key1['company_name'].'</option>'; 
                      }
                       echo '<option selected>'.$cor_name_v.'</option>'; 
                    ?>
                  </select>
                   <div id="loader" class="sp sp-circle"></div>
              </div>
            </div>
          </div>
      </form><Br><Br>
      <h4 id="cor_head_name"></h4>
      <table class="table table-hover table-bordered">
	    	<thead>
	      		<tr>
	        		<th>User ID</th>
	        		<th>User Email</th>
	        		<th>ISBN</th>
	        		<th>Title</th>
	        		<th>Count</th>
	      		</tr>
	    	</thead>
	   		 <tbody id="table_res">
	   		 </tbody>
	  </table>
    </div>

</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
	$("#cor_name").change(function() {
		$('#loader').show();
		  var cor_id = $.trim($('#cor_name').val());
 			 html='';
 			 html1='';
 			 $('#cor_head_name').html(html1);
			 $('#table_res').html(html)
		  $.ajax({
		    url: '<?php echo base_url; ?>/wishlist/ajax-wishlist-corp.php',
		    method: "POST",
		    data: {
		      cor_id: cor_id
		    },
		    dataType: 'json',
	  	   }).success(function(res) {
			    
			   if(res.list==null){
			    	html1='No ISBN in wishlist for this corporate';
			    }
			    else{	 
			    	html1+=res.nam.company_name;
			  
			    	for (var i = 0; i < res.list.length; i++) {
			    		 html+='<tr ><td>'+ res.list[i].user_id +'</td><td>'+ res.list[i].user_email +'</td><td>'+ res.list[i].ISBN13 +'</td><td>'+ res.list[i].title +'</td><td>'+ res.count1[i].count_isbn +'</td></tr>';
					 }			    	 
		 
	  			}

			    if(html==''){
			    	$('#table_res').html('No ISBN in wishlist for this corporate');
			    	$("#loader").hide();
			    }
				else{
			 		$('#table_res').append(html);
			 		$('#cor_head_name').html(html1);
			 		$("#loader").hide();
		 		}
	       
	  });
    
	 });	
	   

$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>
