  <?php

include_once('../config/config.php');

include_once('../config/functions.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

$cor_name_v="-----------------------------select-------------------------";
	if(isset($_POST['download'])){
		
		$output = fopen('php://output', 'w');
 
		fputcsv($output, array('User Id', 'User Email', 'ISBN','Title','Count'));
 	$c_Array=array();
		 $res3=$fun_obj->get_wishlist_retail_full();
		 foreach ($res3 as $key3) {
		 	$res2=$fun_obj->get_count_w_r($key3['ISBN13']);
	   		$c_Array=array_merge($c_Array,$res2);
	   		 			 
	   	}
	   	for ($i=0; $i < sizeof($res3); $i++) { 
		 	 fputcsv($output, array($res3[$i]['user_id'],$res3[$i]['user_email'],$res3[$i]['ISBN13'],$res3[$i]['title'],$c_Array[$i]['count_isbn']));
		 }
		 fclose($output);
		 header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=wishlist.csv');
	exit;
	}

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
     
</head>
<body>
 <?php 
    $page_class='wishlist';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
    <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li ><a href="../wishlist/corporate-w.php">Corporate Wishlist</a></li>
        <li class="active"><a href="../wishlist/retail-w.php">Retail Wishlist</a></li>
      </ul>
    </div>
  </nav>

    <div class="col-md-12">
      <form class="form-horizontal" action="" method="POST" role="form" enctype="multipart/form-data">
          <div class="col-md-7">
            <div class="form-group">
              <label class="control-label col-sm-4" for="cor_name">Search ISBN:</label>
              <div class="col-sm-6 no-left-pad">
                  <input   class="form-control" type="text" name="ser_isbn" id="ser_isbn" placeholder="Enter ISBN">
              </div>
              <div class="col-sm-2 no-left-pad">
                   <button type="submit" name="search" class="btn btn-default iread-btn" >Search</button> 
              </div>
                   <div id="loader" class="sp sp-circle"></div>
              
            </div>
          </div>
          <div class="col-md-2 col-md-offset-2"> 
          	  <button type="submit" name="download" class="btn btn-default iread-btn isbn-btn" >Download full wishlist</button>
          </div>
      </form><Br><Br>
      <h4 id="cor_head_name"></h4><Br> 
      <h4>Recently added books to Wishlist</h4>
      <table class="table table-hover table-bordered">
	    	<thead>
	      		<tr>
	        		<th>User ID</th>
	        		<th>User Email</th>
	        		<th>ISBN</th>
	        		<th>Title</th>
	        		<th>Count</th>
	      		</tr>
	    	</thead>
	   		 <tbody id="table_res">
	   		 		<?php 
	   		 		if(isset($_POST['search'])){
						$ISBN13=$_POST['ser_isbn'];
						$res2=$fun_obj->search_wishlist($ISBN13);
						 	foreach ($res2 as $key1) {
		   		 				echo '<tr>';
		   		 				echo '<td>'.$key1['user_id'].'</td>';
		   		 				echo '<td>'.$key1['user_email'].'</td>';
		   		 				echo '<td>'.$key1['ISBN13'].'</td>';
		   		 				echo '<td>'.$key1['title'].'</td>';
		   		 				$res3=$fun_obj->get_count_w_r($key1['ISBN13']);
		   		 				foreach ($res3 as $value1) {
		   		 					 echo '<td>'.$value1['count_isbn'].'</td>';
		   		 				}
		   		 				echo '</tr>';
	   		 				}
					}
					else{
	   		 		 	$res1=$fun_obj->get_wishlist_retail();
	   		 			foreach ($res1 as $key) {
	   		 				echo '<tr>';
	   		 				echo '<td>'.$key['user_id'].'</td>';
	   		 				echo '<td>'.$key['user_email'].'</td>';
	   		 				echo '<td>'.$key['ISBN13'].'</td>';
	   		 				echo '<td>'.$key['title'].'</td>';
	   		 				$res2=$fun_obj->get_count_w_r($key['ISBN13']);
	   		 				foreach ($res2 as $value) {
	   		 					 echo '<td>'.$value['count_isbn'].'</td>';
	   		 				}
	   		 				echo '</tr>';
	   		 			}
	   		 		}
	   		 		?>
	   		 </tbody>
	  </table>
    </div>

</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
<script type="text/javascript">
	$("#cor_name").change(function() {
		$('#loader').show();
		  var cor_id = $.trim($('#cor_name').val());
 			 html='';
 			 html1='';
 			 $('#cor_head_name').html(html1);
			 $('#table_res').html(html)
		  $.ajax({
		    url: '<?php echo base_url; ?>/wishlist/ajax-wishlist-retail.php',
		    method: "POST",
		    data: {
		      cor_id: cor_id
		    },
		    dataType: 'json',
	  	   }).success(function(res) {
			    
			   if(res.list==null){
			    	html1='No ISBN in wishlist for this corporate';
			    }
			    else{	 
			    	html1+=res.nam.company_name;
			  
			    	for (var i = 0; i < res.list.length; i++) {
			    		 html+='<tr ><td>'+ res.list[i].user_id +'</td><td>'+ res.list[i].user_email +'</td><td>'+ res.list[i].ISBN13 +'</td><td>'+ res.list[i].title +'</td><td>'+ res.count1[i].count_isbn +'</td></tr>';
					 }			    	 
		 
	  			}

			    if(html==''){
			    	$('#table_res').html('No ISBN in wishlist for this corporate');
			    	$("#loader").hide();
			    }
				else{
			 		$('#table_res').append(html);
			 		$('#cor_head_name').html(html1);
			 		$("#loader").hide();
		 		}
	       
	  });
    
	 });	
	   

$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
</script>
</html>
