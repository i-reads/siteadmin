    <?php

include_once('../config/config.php');

include_once('../config/functions.php');
include_once('../mail/corp_mail.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

if(isset($_POST['return'])){
  $res1 = $fun_obj->corp_update_order_status_8($_POST['order_id'],$_POST['book_status'],$_POST['book_id'],$_POST['user_id'],$_POST['isbn']);
  if($res1){
      /*circulation entery*/
       date_default_timezone_set('Asia/Calcutta');
                    $date_today=date("Y-m-d h:i:sa");
     $res_C=$fun_obj->update_circulation_details_corp($_POST['user_id'],$_POST['book_id'],$_POST['init_pay'],$_POST['refund'],$_POST['issue_date'],$date_today);
     
    $phone = 0;
    //SMS script
    $msg= 'Your Pickup order with Order ID: '.$_POST["order_id"].' has been returned.';       
    // to replace the space in message with  '%20'
    $str = trim(str_replace(' ', '%20', $msg));
    $user_id = 'IndiaReads';
    $pass= 'ireads@987'; 
    $sender_id = 'INDREA';         
    $mob_no = $_POST['phone'];       
    $priority = 'ndnd';
    $type = 'normal';
    $url="http://bhashsms.com/api/sendmsg.php?user=".$user_id."&pass=".$pass."&sender=".$sender_id."&phone=".$mob_no."&text=".$str."&priority=".$priority."&stype=".$type;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_exec($ch);
    curl_close($ch);
    $result = mailer('Returned',$_POST['order_id'],$fun_obj,$res1);
    $i_msg .= $result.'<br>';
     $i_msg.='Order '.$_POST['order_id'].' is moved to Returned';
     //header("Refresh:0");
  }
  else{
    $i_msg.= "some error";
  } 
}
 


  

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>
 <?php 
    $page_class='search';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
	    <div class="container-fluid">
	      <ul class="nav navbar-nav">
	            <li ><a href="../search/search.php">Search Orders</a></li>
	        <li  class="active"><a href="../search/search_books.php">Search Books</a></li>
 
	      </ul>
	    </div>
  	</nav>	
	<div class="col-md-4 col-md-offset-1">	
	 <h3>Search ISBN</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="text" name="isbn" required class="form-control" id="isbn" placeholder="Enter ISBN">
	    </div>
	 
	    <button type="submit" name="search" class="btn btn-danger iread-btn iread-btn-white">Search</button>
	  </form>
	 

	 <?php

		if(isset($_POST['search'])){ 
			$Order=$fun_obj->search_isbn_books($_POST['isbn']);

		if($Order){
				?>
			<h3>Total books for <?php echo $_POST['isbn'];?> is :<?php echo count($Order); ?></h3>
			<table class="table table-bordered table-condensed font-new-del">
		    <thead>
		      <tr>
		      	 
		        <th>Library ID</th>
		        <th>Status</th>
		        <th>Location</th>
		        <th>Title</th>
		      </tr>
		    </thead>
		    <tbody>
		   <?php 
		   if($Order) {
		   	$flag=0;
		   foreach ($Order as $key) {
		   	if(($key['status'])==1){
		   		$flag=1;

		   ?>
		      <tr>
		        <td><?php echo $key['book_id']; ?></td>
		        <td>
		        	<?php
		        	if(($key['status'])==1){
                          echo  '<small style="color:#64DD17;">Inside</small>';
                      }
                      elseif(($key['status'])==2){
                          echo  '<small style="color:#DD2C00;">Outside</small>';
                      }
                      elseif(($key['status'])==-1){
                          echo  '<small style="color:#d50000;">Lost</small>';
                      }
                      elseif(($key['status'])==0){
                          echo  '<small style="color:#d50000;">Damaged</small>';
                      }
                      else{
                        echo  "";
                      }
                      ?>
		        </td>
		        <td><p><?php echo $key['shelf_location']; ?></p></td>
		        <td><p><?php echo $key['book_title']; ?></p></td>
		      </tr>
		      <?php }
		       }

		  	} ?>	
		    </tbody>
		  </table>
	 <?php
	  if($flag==0){ echo '<h4>Not a single book in library <i class="material-icons">sentiment_very_dissatisfied</i></h4>';}
	  			?><table class="table table-bordered table-condensed font-new-del margintop">
		    <thead>
		      <tr>
		        <th>Library ID</th>
		        <th>Status</th>
		        <th>Location</th>
		        <th>Title</th>
		      </tr>
		    </thead>
		    <tbody>
		   <?php 
		   if($Order) {
		   	$flag=0;
		   foreach ($Order as $key) {
		   	if(($key['status'])!=1){
		   		$flag=1;

		   ?>
		      <tr>
		        <td><?php echo $key['book_id']; ?></td>
		        <td>
		        	<?php
		        	if(($key['status'])==1){
                          echo  '<small style="color:#64DD17;">Inside</small>';
                      }
                      elseif(($key['status'])==2){
                          echo  '<small style="color:#DD2C00;">Outside</small>';
                      }
                      elseif(($key['status'])==-1){
                          echo  '<small style="color:#d50000;">Lost</small>';
                      }
                      elseif(($key['status'])==0){
                          echo  '<small style="color:#d50000;">Damaged</small>';
                      }
                      else{
                        echo  "";
                      }
                      ?>
		        </td>
		        <td><p><?php echo $key['shelf_location']; ?></p></td>
		        <td><p><?php echo $key['book_title']; ?></p></td>
		      </tr>
		      <?php }
		       }

		  	} ?>	
		    </tbody>
		  </table>
		  <?php
		}
		else{
			$i_msg.='ISBN not found <i class="material-icons">sentiment_very_dissatisfied</i>';
		}
	}
	 ?>
	 </div>
	<div class="col-md-4 col-md-offset-2">
	 <h3>Search Library ID from orders</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="text" name="lib"  class="form-control" id="lib" placeholder="Enter Library ID">
	    </div>

	 	<input type="hidden" value="1" name="corporate"/>
	    <button type="submit" name="search1" class="btn btn-danger iread-btn iread-btn-white">Search</button>
	  </form>
 
	</div>
	  <?php

		if(isset($_POST['search1'])){ 
			
			if(!empty($_POST['lib'])){
				$Order=$fun_obj->get_order_for_lib_retail($_POST['lib']);
				$Order1=$fun_obj->get_order_for_lib_corp($_POST['lib']);
				// $status=$fun_obj->get_order_status($Order[0]['order_status']);
				$lib_chk=$fun_obj->get_book_id_booklibrary($_POST['lib']);
			}
			
			if($Order){
	?>
	<div class="col-md-6 col-md-offset-6">
		<h3>Library ID : <?php echo $_POST['lib']; ?> is in : 
			<?php if(($lib_chk[0]['status'])==1){
                          echo  '<small style="color:#64DD17;">Library</small>';
                      }
                      elseif(($lib_chk[0]['status'])==2){
                          echo  '<small style="color:#DD2C00;">Circulation</small>';
                      }
                      elseif(($lib_chk[0]['status'])==-1){
                          echo  '<small style="color:#d50000;">Lost</small>';
                      }
                      elseif(($lib_chk[0]['status'])==0){
                          echo  '<small style="color:#d50000;">Damaged</small>';
                      }
                      else{
                        echo  "";
                      } ?>
			  </h3>
		<H3>Retail</H3>
	 <table class="table table-bordered table-condensed font-new-del ">
	    <thead>
	      <tr>
	        <th>Order id</th>
	        <th>User id</th>
	        <th>Issue Date</th>
	        <th>Order status</th>
	      </tr>
	    </thead>
	    <tbody>
	   <?php 
	   if($Order) {
	   foreach ($Order as $key) {
	   ?>
	      <tr>
	       
	        <td><?php echo $key['bookshelf_order_id']; ?></td>
	        <td><?php echo $key['user_id']; ?></td>
	        
	        <td><p><?php echo $key['issue_date']; ?></p></td>
	       
	        <td><p><?php echo $fun_obj->get_order_status($key['order_status']); ?></p></td>
	        
	        
	         
	      </tr>

	      <?php } } ?>
	      	
	      
	    </tbody>
	  </table>
	  <H3>Corporate</H3>
	 <table class="table table-bordered table-condensed font-new-del ">
	    <thead>
	      <tr>
	        <th>Order id</th>
	        <th>User id</th>
	        <th>Issue Date</th>
	        <th>Order status</th>
	      </tr>
	    </thead>
	    <tbody>
	   <?php 
	   if($Order1) {
	   foreach ($Order1 as $key) {
	   ?>
	      <tr>
	       
	        <td><?php echo $key['bookshelf_order_id']; ?></td>
	        <td><?php echo $key['user_id']; ?></td>
	        
	        <td><p><?php echo $key['issue_date']; ?></p></td>
	       
	        <td><p><?php echo $fun_obj->get_order_status($key['order_status']); ?></p></td>
	        
	        
	         
	      </tr>

	      <?php } } ?>
	      	
	      
	    </tbody>
	  </table>
	 <?php
		}
		else{
			$i_msg.="Library ID does not exists";
		}
		}
	 ?>
	</div>
	 
	
 

<div class="col-md-12"><hr></div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
 
<script type="text/javascript"> 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
 
</script>
</html>
