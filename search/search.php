    <?php

include_once('../config/config.php');

include_once('../config/functions.php');
include_once('../mail/corp_mail.php');
global $i_msg;
 $i_msg="";
$con = connect($config);
$fun_obj = new ireads($con);

if(isset($_POST['return'])){
  $res1 = $fun_obj->corp_update_order_status_8($_POST['order_id'],$_POST['book_status'],$_POST['book_id'],$_POST['user_id'],$_POST['isbn']);
  if($res1){
      /*circulation entery*/
       date_default_timezone_set('Asia/Calcutta');
                    $date_today=date("Y-m-d h:i:sa");
     $res_C=$fun_obj->update_circulation_details_corp($_POST['user_id'],$_POST['book_id'],$_POST['init_pay'],$_POST['refund'],$_POST['issue_date'],$date_today);
     
    $phone = 0;
    //SMS script
    $msg= 'Your Pickup order with Order ID: '.$_POST["order_id"].' has been returned.';       
    // to replace the space in message with  '%20'
    $str = trim(str_replace(' ', '%20', $msg));
    $user_id = 'IndiaReads';
    $pass= 'ireads@987'; 
    $sender_id = 'INDREA';         
    $mob_no = $_POST['phone'];       
    $priority = 'ndnd';
    $type = 'normal';
    $url="http://bhashsms.com/api/sendmsg.php?user=".$user_id."&pass=".$pass."&sender=".$sender_id."&phone=".$mob_no."&text=".$str."&priority=".$priority."&stype=".$type;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_exec($ch);
    curl_close($ch);
    $result = mailer('Returned',$_POST['order_id'],$fun_obj,$res1);
    $i_msg .= $result.'<br>';
     $i_msg.='Order '.$_POST['order_id'].' is moved to Returned';
     //header("Refresh:0");
  }
  else{
    $i_msg.= "some error";
  } 
}
 


  

?>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>siteadmin-Add books</title>
    <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="../Bootstrap/js/respond.js"></script>
 
</head>
<body>
 <?php 
    $page_class='search';  
    include_once('../headers/main-header.php');
  ?>
<div class="container">
	<nav class="sub-head navbar navbar-default">
	    <div class="container-fluid">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="../search/search.php">Search Orders</a></li>
	        <li  ><a href="../search/search_books.php">Search Books</a></li>
 
	      </ul>
	    </div>
  	</nav>	
	<div class="col-md-4 col-md-offset-1">	
	 <h3>Search Order Retail</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="text" name="order" required class="form-control" id="order" placeholder="Enter order">
	    </div>
	 
	    <button type="submit" name="search" class="btn btn-danger iread-btn iread-btn-white">Search</button>
	  </form>
	 

	 <?php

		if(isset($_POST['search'])){ 
			$Order=$fun_obj->get_bookshelf_order($_POST['order']);

			$status=$fun_obj->get_order_status($Order[0]['order_status']);
	
		if($Order){
				?>
			<h3>Order <?php echo $_POST['order']; ?> is in : <?php echo $status; ?></h3>
			<table class="table table-bordered table-condensed font-new-del">
		    <thead>
		      <tr>
		      	 
		        <th>Parent Order</th>
		        <th>Order id</th>
		        <th>User</th>
		        <th>Order Date</th>
		   
		        <th>Issue Date</th>
		        <th>Order Address</th>
		        <th>Pickup Address ID</th>
		        <th>Price Details</th>
		        <th>Sub-Order Price Details</th>
		        <th>Payment Mode and Type</th>
		        <th>Refund Details</th>
		        <th>Book Details</th>
		        <th>Assigned Book id</th>
		         
		        <th>Comment</th>
		      
		      </tr>
		    </thead>
		    <tbody>
		   <?php 
		   if($Order) {
		   foreach ($Order as $key) {
		   ?>
		      <tr>
		       
		        <td><?php echo $key['p_order_id']; ?></td>
		        <td><?php echo $key['bookshelf_order_id']; ?></td>
		        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
		        <td><p><?php echo $key['order_date']; ?></p></td>
		       
		        <td><p><?php echo $key['issue_date']; ?></p></td>
		        
		        <td><p><?php echo $key['fullname']; ?></p><p><?php echo $key['address_line1']; ?>,</p><p><?php echo $key['address_line2']; ?>, <?php echo $key['city']; ?></p><p><p><?php echo $key['state'] ?> <span><?php echo $key['pincode']; ?></span></p><p id="p7"><?php echo $key['phone']; ?></p></td>
		        <td><p><?php echo $key['pick_address_id']; ?></p></td>
		        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
		        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
		        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
		        <td><p>Cost:<?php echo $key['cost']; ?></p>  <p>Refund: <?php echo $key['refund']; ?></p><p>Store Refund:  <?php echo $key['store_refund']; ?></p><p>Bonus Refund:  </p><p>Refund Type:<?php echo $key['refund_type']; ?></p><p>Generated By:<?php echo $key['refund_generate']; ?></p></td>
		        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
		        <td><?php echo $key['unique_book_id']; ?></a>
		          
		        </td>
		        <td>
		        <div class="form-group">
	               <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['pickup_comments']; ?></textarea>
	          
	            </div>    
			    </td>
		         
		      </tr>

		      <?php } } ?>
		      	
		      
		    </tbody>
		  </table>
	 <?php
		}
		else{
			$i_msg.="Order does not exists";
		}
	}
	 ?>
	  <?php

		if(isset($_POST['search1'])){ 
			if(!empty($_POST['order1'])){
				$Order=$fun_obj->corp_get_bookshelf_order($_POST['order1']);
				$status=$fun_obj->get_order_status($Order[0]['order_status']);
			}
			if(!empty($_POST['lib'])){
				$Order=$fun_obj->corp_get_bookshelf_order_lib($_POST['lib']);
				$status=$fun_obj->get_order_status($Order[0]['order_status']);
			}
			
			if($Order){
	?>
		<h3>Order <?php echo $_POST['order1']; ?> is in : <?php echo $status; ?></h3>
	 <table class="table table-bordered table-condensed font-new-del">
	    <thead>
	      <tr>
	      	 
	        <th>Parent Order</th>
	        <th>Order id</th>
	        <th>User</th>
	        <th>Order Date</th>
	   
	        <th>Issue Date</th>
	        <th>Order Address</th>
	        <th>Pickup Address ID</th>
	        <th>Price Details</th>
	        <th>Sub-Order Price Details</th>
	        <th>Payment Mode and Type</th>
	        <th>Refund Details</th>
	        <th>Book Details</th>
	        <th>Assigned Book id</th>
	         
	        <th>Comment</th>
	        <th>Action</th>
	      
	      </tr>
	    </thead>
	    <tbody>
	   <?php 
	   if($Order) {
	   foreach ($Order as $key) {
	   ?>
	      <tr>
	       
	        <td><?php echo $key['p_order_id']; ?></td>
	        <td><?php echo $key['bookshelf_order_id']; ?></td>
	        <td><p class="p-small"><?php echo $key['user_id']; ?></p> <p class="p-small"><?php echo $key['user_email']; ?></p></td>
	        <td><p><?php echo $key['order_date']; ?></p></td>
	       
	        <td><p><?php echo $key['issue_date']; ?></p></td>
	        
	        <td><p><?php echo $key['fullname']; ?></p><p><?php echo $key['address_line1']; ?>,</p><p><?php echo $key['address_line2']; ?>, <?php echo $key['city']; ?></p><p><p><?php echo $key['state'] ?> <span><?php echo $key['pincode']; ?></span></p><p id="p7"><?php echo $key['phone']; ?></p></td>
	        <td><p><?php echo $key['pick_address_id']; ?></p></td>
	        <td><p>Mrp:<?php echo $key['price']; ?></p>  <p>Discount: <?php echo $key['coupon_discount']; ?></p><p>Coupon Code:  <?php echo $key['coupon_code']; ?></p><p>Store Pay: <?php echo $key['store_discount']; ?></p><p>Shipping:<?php echo $key['shipping_charge']; ?></p><p>Amount Paid:<?php echo $key['net_pay']; ?></p></td>
	        <td><p>Mrp:<?php echo $key['mrp']; ?></p><p>Init Pay:<?php echo $key['init_pay']; ?></p>  <p>Discount: <?php echo $key['disc_pay']; ?></p> <p>Store Pay: <?php echo $key['store_pay']; ?></p><p>Amount Paid:<?php echo $key['amt_pay']; ?></p></td>
	        <td><p>Mode: <?php echo $fun_obj->get_payment_mode($key['payment_option']); ?></p><p>Status: <?php echo $fun_obj->get_payment_status($key['payment_status']); ?></p></td>
	        <td><p>Cost:<?php echo $key['cost']; ?></p>  <p>Refund: <?php echo $key['refund']; ?></p><p>Store Refund:  <?php echo $key['store_refund']; ?></p><p>Bonus Refund:  </p><p>Refund Type:<?php echo $key['refund_type']; ?></p><p>Generated By:<?php echo $key['refund_generate']; ?></p></td>
	        <td><p><?php echo $key['Title']; ?></p><p>By : <span><?php echo $key['contributor_name1']; ?></span></p><p>Isbn: <?php echo $key['ISBN13']; ?></p></td>
	        <td><?php echo $key['unique_book_id']; ?></a>
	          
	        </td>
	        <td>
	        <div class="form-group">
               <textarea class="form-control" rows="5" id="comment" name="comment" class="comment"><?php echo $key['pickup_comments']; ?></textarea>
          
            </div>    
		    </td>
		    <?php if(@$_POST['corporate']==1){ 
		     ?><td> 
        	<form action="" method="POST" role="form" class="margintop">
              Please select book status:
              <div class="radio">
                <label><input type="radio" value="1" name="book_status" checked>Inside</label>
              </div>
              <div class="radio">
                <label><input type="radio" value="0" name="book_status" >Damaged</label>
              </div>
              <div class="radio">
                <label><input type="radio" value="-1" name="book_status" >Lost</label>
              </div>
              <input type="hidden" value="<?php echo $key['bookshelf_order_id']; ?>" name="order_id">
              <input type="hidden" value="<?php echo $key['unique_book_id']; ?>" name="book_id">
              <input type="hidden" value="<?php echo $key['user_id']; ?>" name="user_id">
            	<input type="hidden" value="<?php echo $key['ISBN13']; ?>" name="isbn">
               <input type="hidden" value="<?php echo $key['refund']; ?>" name="refund">
              <input type="hidden" value="<?php echo $key['init_pay']; ?>" name="init_pay">
           
              <input type="hidden" value="<?php echo $key['phone']; ?>" name="phone">
              <input type="hidden" value="<?php echo $key['issue_date']; ?>" name="issue_date">
              
        		<div class="form-group">
        			<input type="submit" class="btn btn-danger iread-btn action-btn-2 " name="return" value="Returned">
        		</div>
        		<div class="form-group">
        			<input type="submit" class="btn btn-danger iread-btn action-btn-2 " name="generate" value="Generate Fedex Pickup">
        		</div>
        	</form>
        </td>
	         <?php }?>
	      </tr>

	      <?php } } ?>
	      	
	      
	    </tbody>
	  </table>
	 <?php
		}
		else{
			$i_msg.="Order does not exists";
		}
		}
	 ?>
	</div>
	<div class="col-md-4 col-md-offset-2">
	 <h3>Search Order Corporate</h3>
	  <form role="form" method="post">
	    <div class="form-group">
	      <input type="text" name="order1"  class="form-control" id="order1" placeholder="Enter order">
	    </div>
	      <h4 class="text-center">Or</h4>
	    <div class="form-group">
	      <input type="text" name="lib"  class="form-control" id="lib" placeholder="Enter Library ID">
	    </div>

	 	<input type="hidden" value="1" name="corporate"/>
	    <button type="submit" name="search1" class="btn btn-danger iread-btn iread-btn-white">Search</button>
	  </form>
 
	</div>
 

<div class="col-md-12"><hr></div>
</div>
<?php  
if(!empty($i_msg)){
echo '<div class="alert alert-success i-alert" id="i-alert">
    <button type="button" class="close cl-btn" data-dismiss="alert">x</button>
   '.$i_msg.'  
</div>';
}
else{
 
}
?>
<!-- container ends-->
	</body>
 
<script type="text/javascript"> 
$(document).ready(function() {
	 $("#loader").hide();
  $("#i-alert").fadeTo(4000, 500).slideUp(500, function(){
      $("#i-alert").alert('close');
  });
});
 
</script>
</html>
