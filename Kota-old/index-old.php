<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home - Book Bank</title>

</head>
<body>
<div style="width:100%; height: 60px;">
	<img height="50px" style="margin-right:20px;" src="images/resonance-logo.jpg" />
	<img height="50px" src="images/indiareads-logo.png" />	
</div>
<h1>Book Bank Home</h1>

<h2>Add</h2>
<a href="add-user.php">Add New User</a><br/>
<a href="add_pccp_user.php">Add New PCCP User</a><br/>
<a href="add-books.php">Add New Book (Display, Price & Discount Information)</a><br/>
<a href="add-to-library.php">Add to Library (My Inventory)</a><br/>

<h2>Order</h2>
<a href="login.php">Place Request</a><br/>
<a href="view-req.php">View Request</a><br/>
<a href="issue-login.php">Issue Book</a><br/>
<a href="return-login.php">Return Book</a><br/>

<h2>Database</h2>
<a href="view-user.php">View User Details</a><br/>
<a href="user-edit.php">Edit User</a><br/>
<a href="lib-edit.php">Edit Library</a><br/>
<a href="edit-db.php">Edit Books</a><br/>
<a href="procurement.php">Procurement Requirements</a><br/>
</body>
</html>