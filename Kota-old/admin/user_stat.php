<?php

include_once('../pages/connect.php');

if(isset($_REQUEST['filter']))
{
	$date1 = $_REQUEST['datepicker1'];
	$date2 = $_REQUEST['datepicker2'];
	$sort = "";
	$qr = "";
	
	if($date1 != "")
	{
		$qr .= " date(u.created) >= '".$date1."' ";
	}
	
	if($date1 != "" && $date2 != "")
	{
		$sort = $date1." to ".$date2;
		$qr .= " and ";
	}
	else if($date1 != "" && $date2 == "")
	{
		$sort = $date1." to today";
	}
	else if($date1 == "" && $date2 != "")
	{
		$sort = "Till ".$date2;
	}
	else
	{
		$sort = "Overall";
		$qr = " 1";
	}
	
	if($date2 != "")
	{
		$qr .= " date(u.created) <= '".$date2."' ";
	}
}
else
{
	$td = date('Y-m-d');
	$sort = $td;
	$qr = " date(created) = '".$td."' ";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Book Bank Stats</title>
<link rel="stylesheet" href="css/default.css" type="text/css">
<script type="text/javascript">
	SyntaxHighlighter.defaults['toolbar'] = false;
        SyntaxHighlighter.all();
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/zebra_datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>

</head>

<body>

<h1>Book Bank Stats | <a href="index.php" style="font-size:16px;">Back</a></h1>

<div id="addform">

<h4> Select Range :  
    <form name="myform" method="post" >
	<div class="main-wrapper">
            <div style="float:left">
                <input id="datepicker1" name="datepicker1" type="text">
            </div>
            <div style="float:left">
        	TO
            </div>
            <div>
                <input id="datepicker2" name="datepicker2" type="text">
            </div>
            
            <input type="submit" id="filter" name="filter" text="Filter" />
        </div>
    </form>
</h4>

<h3> Users Stats (<?=$sort?>) </h3>
<?php

$sales = mysql_query("select count(*) from user u where ".$qr);
if($sale = mysql_fetch_array($sales))
{
	$totals = $sale[0];
}
?>
<h3>Total Sale = <?=$totals?></h3>
<table cellspacing="5" border="1">
	<tr>
		<th> Email ID </th>
		<th> User ID </th>
		<th> Name </th>
		<th> Address </th>
		<th> Phone </th>
		<th> Created Date </th>  
	</tr>
<?php

$reqs = mysql_query("select * from user u where ".$qr." order by u.created");
while($req = mysql_fetch_array($reqs))
{
	$reqid = $req['reso_id'];
	$uid = $req['user_id'];
	$uname = $req['fname'].' '.$req['lname'];
	$addr = $req['address'];
	$phone = $req['phone'];
	$cdate = $req['created'];
	echo '<tr>';
		echo '<td>'.$reqid.'</td>';
		echo '<td>'.$uid.'</td>';
		echo '<td>'.$uname.'</td>';
		echo '<td>'.$addr.'</td>';
		echo '<td>'.$phone.'</td>';
		echo '<td>'.$cdate.'</td>';
	echo '</tr>';
}
?>

</div>

</body>

</html>