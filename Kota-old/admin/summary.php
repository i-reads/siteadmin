<?php

include_once('../pages/connect.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Book Bank Stats</title>
<link rel="stylesheet" href="css/default.css" type="text/css">
<script type="text/javascript">
	SyntaxHighlighter.defaults['toolbar'] = false;
        SyntaxHighlighter.all();
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/zebra_datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>

</head>

<body>

<h1>Book Bank Stats | <a href="index.php" style="font-size:16px;">Back</a></h1>

<div id="addform">

<h3> Summary </h3>

<table cellpadding="10" cellspacing="2" border="1">
	<tr>
		<th> Date </th>
		<th> Total Sale </th>
		<th> Total Refund </th>
		<th> Total Users </th>
	</tr>
<?php
$start = '2014-01-01';
$td = date('Y-m-d');
$dt = $td;
while($dt >= $start)
{
	$disp = date('d M Y, D', strtotime($dt));
	$sales = mysql_query("select sum(price) from issued where date(issue_date) = '".$dt."' ");
	if($sale = mysql_fetch_array($sales))
	{
		$totals = $sale[0]; 
	}
	
	$refs = mysql_query("select sum(refund) from issued where date(return_date) = '".$dt."' ");
	if($ref = mysql_fetch_array($refs))
	{
		$totalr = $ref[0]; 
	}
	
	$users = mysql_query("select count(*) from user where date(created) = '".$dt."' and user_status = 2 ");
	if($user = mysql_fetch_array($users))
	{
		$totalu = $user[0]; 
	}
	?>
		<tr>
			<th> <?=$disp?></th>
			<th> <?=$totals?> </th>
			<th> <?=$totalr?> </th>
			<th> <?=$totalu?> </th>
		</tr>
<?php
	$dt = date('Y-m-d', strtotime("-1 day", strtotime($dt)));
}
?>
</table>

</div>

</body>

</html>