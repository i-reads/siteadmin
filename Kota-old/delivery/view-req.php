<?php header( "refresh:30; url=view-req.php" );
	
include_once('connect.php');

if(isset($_GET['id']))
{
	$id = $_GET['id'];
	mysql_query("DELETE FROM pending_request WHERE req_id='{$id}'");
	header('location:view-req.php');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Current Requests</title>
<style>
table{
	background-color:#EEE;
}

</style>
</head>
<body>
<center><h2>Pending Requests</h2></center>
<table cellspacing="10" cellpadding="3">
	<tr>
	<th>Request ID</th> 
    <th>Email-ID</th>
    <th>user ID</th>
    <th>Title</th>
    <th>Date</th>
    <th>Invoice</th>
    <th>Issue Books</th>
    <th>Operation</th>
	</tr>
	<?php 
		$query = mysql_query("SELECT * FROM pending_request order by date desc");
		while($row = mysql_fetch_array($query))
		{
	?>
	<tr>
    <td><?php echo $row['req_id']; ?></td>
    <?php
    	$uid = $row['user_id'];
    	$q1 = mysql_query("SELECT * FROM user WHERE user_id = '{$uid}'");
    	$r1 = mysql_fetch_array($q1);
    ?>
    <td><?php echo $r1['reso_id']; ?></td>
	<td id="mrp"><?php echo $row['user_id']; ?></td >
    <td>
    <?php
    	$isbn = $row['ISBN']; 
    	$query2 = mysql_query("SELECT title FROM books WHERE ISBN = '{$isbn}'");
	$res2 = mysql_fetch_array($query2); 
	echo $res2['title'];	 
    ?>
    </td>
    <td><?php echo $row['date']; ?></td>
    <td><a target="_blank" href="invoice/<?=$row['invoice']?>" >Invoice</a></td>
    <td><a href="issue-login.php?uid=<?=$row['user_id']?>&type=<?=$r1['reference']?>">Issue Books</a></td>
    <td><a href="view-req.php?id=<?php echo $row['req_id']; ?>">Delete</a></td>
    </tr>
<?php } ?>
</table>

<center><h2>Delivery Processing</h2></center>
<table cellspacing="10" cellpadding="3">
	<tr>
	<th>Request ID</th> 
    <th>Email-ID</th>
    <th>user ID</th>
    <th>Title</th>
    <th>Date</th>
	</tr>
	<?php 
		$query = mysql_query("SELECT * FROM issued where return_date = '0000-00-00 00:00:00.000000' AND status = 1 order by issue_date desc");
		while($row = mysql_fetch_array($query))
		{
	?>
	<tr>
    <td><?php echo $row['req_id']; ?></td>
    <?php
    	$uid = $row['user_id'];
    	$q1 = mysql_query("SELECT * FROM user WHERE user_id = '{$uid}'");
    	$r1 = mysql_fetch_array($q1);
    ?>
    <td><?php echo $r1['reso_id']; ?></td>
	<td id="mrp"><?php echo $row['user_id']; ?></td >
    <td>
    <?php
    	$isbn = $row['ISBN']; 
    	$query2 = mysql_query("SELECT title FROM books WHERE ISBN = '{$isbn}'");
	$res2 = mysql_fetch_array($query2); 
	echo $res2['title'];	 
    ?>
    </td>
    <td><?php echo $row['issue_date']; ?></td>
    </tr>
<?php } ?>
</table>

<center><h2>Delivered</h2></center>
<table cellspacing="10" cellpadding="3">
	<tr>
	<th>Request ID</th> 
    <th>Email-ID</th>
    <th>user ID</th>
    <th>Title</th>
    <th>Date</th>
	</tr>
	<?php 
		$query = mysql_query("SELECT * FROM issued where return_date = '0000-00-00 00:00:00.000000' AND status = 2 order by issue_date desc");
		while($row = mysql_fetch_array($query))
		{
	?>
	<tr>
    <td><?php echo $row['req_id']; ?></td>
    <?php
    	$uid = $row['user_id'];
    	$q1 = mysql_query("SELECT * FROM user WHERE user_id = '{$uid}'");
    	$r1 = mysql_fetch_array($q1);
    ?>
    <td><?php echo $r1['reso_id']; ?></td>
	<td id="mrp"><?php echo $row['user_id']; ?></td >
    <td>
    <?php
    	$isbn = $row['ISBN']; 
    	$query2 = mysql_query("SELECT title FROM books WHERE ISBN = '{$isbn}'");
	$res2 = mysql_fetch_array($query2); 
	echo $res2['title'];	 
    ?>
    </td>
    <td><?php echo $row['issue_date']; ?></td>
    </tr>
<?php } ?>
</table>

</body>
</html>