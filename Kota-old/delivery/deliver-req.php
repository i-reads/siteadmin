<?php
include_once('connect.php');

function time_diff($dt1,$dt2){
    $y1 = substr($dt1,0,4);
    $m1 = substr($dt1,5,2);
    $d1 = substr($dt1,8,2);
    $h1 = substr($dt1,11,2);
    $i1 = substr($dt1,14,2);
    $s1 = substr($dt1,17,2);    

    $y2 = substr($dt2,0,4);
    $m2 = substr($dt2,5,2);
    $d2 = substr($dt2,8,2);
    $h2 = substr($dt2,11,2);
    $i2 = substr($dt2,14,2);
    $s2 = substr($dt2,17,2);    

    $r1=date('U',mktime($h1,$i1,$s1,$m1,$d1,$y1));
    $r2=date('U',mktime($h2,$i2,$s2,$m2,$d2,$y2));
    return ($r1-$r2)/(60*60*24*30);

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Return Request</title>
<style>
#addform{
	width:1400px;
	height:500px;	
}
#title{
	width: 400px;
	padding:4px;
	margin:2px 0;
	background-color:#EEE;
	float:left;	
	height:30px;
	overflow:hidden;	
}
#mrp{
	width: 100px;
	padding:4px;
	margin:2px 0;
	height:30px;
	background-color:#EEE;
	float:left;		
}
#discount{
	width: 150px;
	height:30px;
	padding:4px;
	margin:2px 0;
	background-color:#EEE;
	float:left;		
}
#final{
	width: 100px;
	padding:4px;
	height:30px;
	margin:2px 0;
	background-color:#EEE;
	float:left;	
}

</style>
</head>
<body>
<a href="index.php">Back to Home</a><br/><br/>
<center><h1>Delivery Request</h1></center>
<div id="addform">
	<div id="mrp">Request ID</div>
    <div id="mrp">user ID</div>
    <div id="title">Title</div>
    <div id="discount">Library ID</div>
    <div id="discount">Issue Date</div>
    <div id="mrp">Price</div>
    <div id="discount">Option</div>
    <div style="clear:both"></div>
<form action="" method="post">
	<?php 
		$uid = $_SESSION['uid'];
		$query = mysql_query("SELECT * FROM issued WHERE user_id = '{$uid}' AND  return_date = '0000-00-00 00:00:00.000000' AND status = 1");
		while($row = mysql_fetch_array($query))
		{
	?>
    <div id="mrp"><?php echo $row['req_id']; ?></div>
	<div id="mrp"><?php echo $row['user_id']; ?></div>
    <?php
		$isbn = $row['ISBN'];
		$query2 = mysql_query("SELECT title,mrp,sp FROM books WHERE ISBN = '{$isbn}'");
		$res2 = mysql_fetch_array($query2);
		$mrp = $res2['mrp'];
		$sp = $res2['sp'];
	?>
    <div id="title"><?php echo $res2['title']; ?></div>
    <div id="discount"><?php echo $row['library_id']; ?></div>
    <div id="discount"><?php echo $row['issue_date']; ?></div>
    <div id="mrp"><?php echo $row['price']; ?></div>
    <div id="discount"><a href="deliver-process.php?a=<?php echo $row['req_id'] ?>">Delivered</a><br/><a href="deliver-delete.php?a=<?php echo $row['req_id'] ?>">Delete</a></div>
        <div style="clear:both"></div>
    
<?php } ?>
    <div style="clear:both"></div><br/>
<center><h1>Delivered</h1></center>
<div id="addform">
	<div id="mrp">Request ID</div>
    <div id="mrp">user ID</div>
    <div id="title">Title</div>
    <div id="discount">Library ID</div>
    <div id="discount">Issue Date</div>
    <div id="mrp">Price</div>
    <div style="clear:both"></div>
<form action="return-confirm.php" method="post">
	<?php 
		$uid = $_SESSION['uid'];
		$query = mysql_query("SELECT * FROM issued WHERE user_id = '{$uid}' AND  return_date = '0000-00-00 00:00:00.000000' AND status = 2");
		while($row = mysql_fetch_array($query))
		{
	?>
    <div id="mrp"><?php echo $row['req_id']; ?></div>
	<div id="mrp"><?php echo $row['user_id']; ?></div>
    <?php
		$isbn = $row['ISBN'];
		$query2 = mysql_query("SELECT title,mrp,sp FROM books WHERE ISBN = '{$isbn}'");
		$res2 = mysql_fetch_array($query2);
		$mrp = $res2['mrp'];
		$sp = $res2['sp'];
	?>
    <div id="title"><?php echo $res2['title']; ?></div>
    <div id="discount"><?php echo $row['library_id']; ?></div>
    <div id="discount"><?php echo $row['issue_date']; ?></div>
    <div id="mrp"><?php echo $row['price']; ?></div>
        <div style="clear:both"></div>
    
<?php } ?>

</div>
</body>
</html>