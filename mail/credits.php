<?php

  require_once __DIR__ . '/../init.php';

  function add_store_credits($user,$credits,$why)
    {
      DB::getInstance()->insert('user_store_credit_details',array('user_id' => $user, 'when' => date('Y-m-d H:i:s'), 'why' => $why,'store_credit' => $credits));
      
      $prev=DB::getInstance()->query('SELECT store_credit from user_store_credit where user_id=?',array($user));

      if($prev->count())
      {
        $new_total=($prev->first()->store_credit)+$credits;

        DB::getInstance()->update('user_store_credit','user_id',$user,array('store_credit' => $new_total));
      }
      else
      {
        DB::getInstance()->insert('user_store_credit',array('user_id'=>$user,'store_credit'=>$credits));
      }
    }


?>