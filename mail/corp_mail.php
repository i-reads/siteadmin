<?php

	function sendMail($to,$subject,$body)
	{
		if(!class_exists('PHPMailer',false))
		{
			require_once __DIR__ . '/PHPMailer-master/class.phpmailer.php';
		}

		$mail = new PHPMailer;

		$mail->ClearAddresses();
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'smtp.mandrillapp.com';	    		  			// Specify main and backup server
		$mail->Port = 587;
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'info@indiareads.com';                        // SMTP username
		$mail->Password = 'ApZTwA6thDlb5hi8zFUMFA';                     // SMTP password
		$mail->SMTPSecure = 'tls';                           	        // Enable encryption, 'ssl' also accepted
		$mail->From = 'info@indiareads.com';
		$mail->FromName = 'IndiaReads Online Library';
		$mail->addAddress($to);               							// Name is optional

		$mail->WordWrap = 50;                              				// Set word wrap to 50 characters

		$mail->isHTML(true);                                  			// Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body= $body;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
		   return false;
		   exit;
		}
		else
			return true;
	}

	function order_ids($case, $orders, $fun_obj){

		$result = '';
		$company_name = '';
		$useremail_res = $fun_obj->corp_get_bookshelf_order($orders[0]);
		$useremail = $useremail_res[0]["user_email"];
		$company_name_res = $fun_obj->get_company_details_with_user_id($useremail_res[0]['user_id']);
		$company_name = $company_name_res[0]['company_name'];
		$body = mailbody($case,$orders,$fun_obj,0,$company_name);
		sendMail($useremail,'[INDIAREADS] Order '.$case, $body);
		$result = 'Mail sent for Order '.$case.'<br>';
		return $result;
	}
	
	function mailer($case, $order_id, $fun_obj, $credits=0){

		$result = '';
		$res = $fun_obj->corp_get_bookshelf_order($order_id);
		$company_name_res = $fun_obj->get_company_details_with_user_id($res[0]['user_id']);
		$company_name = $company_name_res[0]['company_name'];
		sendMail($res[0]['user_email'],'[INDIAREADS] Order '.$case, mailbody($case,$res,$fun_obj,$credits,$company_name));
		$result = 'Order '.$order_id.' '.$case;
		return $result;
	}

	function mailbody($status,$res, $fun_obj,$credits=0,$company_name){

		if($company_name == 'inautix'){
			$str = " using your iNautix email id in a Chrome browser";
		}
		else{
			$str = "";
		}

		if($status == 'Deleted'){
			$maildeleted = '<table style="color:#000;padding:20px;max-width:600px;font-size: 16px;line-height: 1.2em;letter-spacing: 0.02em">
						<tbody>
							<tr>
								<td><a target="_blank" href="http://indiareads.com/"><img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									This is a confirmation email that your order has been cancelled.<br><br> Your cancelled order details are as follows
								</td>
							</tr>
							<tr>
								<td>
									<p>Order ID: '.$res[0]["bookshelf_order_id"].'</p>
								</td>
							</tr>
							<tr>
							    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
							        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
							        '.$res[0]['title'].'<br/>By '.$res[0]["contributor_name1"].'
							        <p>ISBN : '.$res[0]["ISBN13"].'</p>
							    </td>
							</tr>
							<tr>
								<td>
									<p>Reading just got bigger and better. <br/>Now books are affordable like never before. <br/><a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> lets you read books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<b>Here is how we make reading so easy for you.</b>
									<li>Just login into <a style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com">'.$company_name.'.indiareads.com</a> account'.$str.', browse books and add them to your <a  style="color: #bc3526;text-decoration: none;" target="_blank" href="http://indiareads.com/bookshelf?tabs=Bookshelf" style="color: #bc3526;text-decoration: none;">Bookshelf</a></li>
									<li>Select the books you want to read and place order</li>
									<li>Books are delivered at your address</li>
									<li>Done reading? Return the book from the <a  style="color: #bc3526;text-decoration: none;" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf">Currently Reading</a> section of the bookshelf. <a target="_blank" href="http://'.$company_name.'.indiareads.com/" style="color: #bc3526;text-decoration: none;"> IndiaReads</a> will connect and pick the books</li>
								</td>
							</tr>
							';
								if(($company_name) != 'hdfc')
								{
					$maildeleted.=	'<tr>
								<td>
									<p>
										<b>Note:</b> You will be able to read only one book at a time. To order a book, please ensure you have returned all previously ordered books. Also, you can keep a book for a period of 30 days';
										if($company_name == "inautix")
										{ 
											$maildeleted.= ', beyond which <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;"> IndiaReads</a> will charge a late fee.' ; 
										} 
									$maildeleted.= '</p>
								</td>
							</tr>
							';
						}
						$maildeleted.=	'<tr>
								<td><br>Need Assistance?&nbsp;<a target="_blank" href="http://'.$company_name.'.indiareads.com/contactus">Contact us</a> any time.</td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us at
									customercare@indiareads.com <br>Phone : (0120)-419-4999 <br>Alternate No. : (0120)-420-6323
								</td>
							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';
					return $maildeleted;
		}
		else if($status == 'Dispatched'){

			$result = array();
			$pay = array();

			$parentid = $fun_obj->corp_get_bookshelf_order($res[0]);
			$trackid = $parentid[0]["d_track_id"];
			$tablerows = '';
			foreach ($res as $key) {
				$result[$key] = $fun_obj->corp_get_bookshelf_order($key);
				$tablerows .= '<tr>
								    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
								        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$result[$key][0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
								       	'.$result[$key][0]["title"].'<br/>By '.$result[$key][0]["contributor_name1"].'
								       	<p>ISBN: '.$result[$key][0]["ISBN13"].'</p>
								    </td>
								</tr>';
			}
			$maildispatched = '<table style="color:#000;padding:20px;max-width:600px;font-size: 16px;line-height: 1.2em;letter-spacing: 0.02em">
						<tbody>
							<tr>
								<td><a target="_blank" href="http://indiareads.com/"><img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									We are glad to inform you that we have <b>dispatched</b> your book. <br>You can track you order using the following details.<br>
										<ul>
											<li>Tracking ID: '.$trackid.'</li>
										</ul>
										<br>Your order details are as follows
								</td>
							</tr>
							</tr>
							<tr style="background-color:#bc3526;color:#ffffff;">
							    <td align="center" style="padding:5px;width:300px;">Item</td>
							</tr>'.$tablerows.'
							<tr>
								<td>
									<p>Reading just got bigger and better. <br/>Now books are affordable like never before. <br/><a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> lets you read books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<b>Here is how we make reading so easy for you.</b>
									<li>Just login into <a style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com">'.$company_name.'.indiareads.com</a> account'.$str.', browse books and add them to your <a  style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf" style="color: #bc3526;text-decoration: none;">Bookshelf</a></li>
									<li>Select the books you want to read and place order</li>
									<li>Books are delivered at your address</li>
									<li>Done reading? Return the book from the <a  style="color: #bc3526;text-decoration: none;" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf">Currently Reading</a> section of the bookshelf. <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> will connect and pick the books</li>
								</td>
							</tr>
									';
										if(($company_name) != 'hdfc')
										{
							$maildispatched.=	'<tr>
										<td>
											<p>
												<b>Note:</b> You will be able to read only one book at a time. To order a book, please ensure you have returned all previously ordered books. Also, you can keep a book for a period of 30 days';
												if($company_name == "inautix")
												{ 
													$maildispatched.= ', beyond which <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;"> IndiaReads</a> will charge a late fee.' ; 
												} 
											$maildispatched.= '</p>
										</td>
									</tr>
									';
								}

							$maildispatched.= '<tr>
								<td><br>Need Assistance?&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://'.$company_name.'.indiareads.com/contactus">Contact us</a> any time.</td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us at
									customercare@indiareads.com <br>Phone : (0120)-419-4999 <br>Alternate No. : (0120)-420-6323
								</td>
							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';
					return $maildispatched;

		}
		else if($status == 'Delivered'){

			$maildelivered =  '<table style="color:#000;padding:20px;max-width:600px;font-size: 16px;line-height: 1.2em;letter-spacing: 0.02em">
						<tbody>
							<tr>
								<td><a target="_blank" href="http://indiareads.com/"><img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
									<br>
									We are happy to inform you that you book has been <b>delivered</b>. <br>This is a confirmation email that you have received the order.<br><br> Your order details are as follows
								</td>
							</tr>
							<tr style="background-color:#bc3526;color:#ffffff;">
							    <td align="center" style="padding:5px;width:300px;">Item</td>
							</tr>
								<tr>
								    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
								        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
								        '.$res[0]['title'].'<br/>By '.$res[0]["contributor_name1"].'
								        <p>ISBN : '.$res[0]["ISBN13"].'</p>
								    </td>
							  	</tr>
						  			<tr>
						  				<td>
						  					<p>Reading just got bigger and better. <br/>Now books are affordable like never before. <br/><a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> lets you read books and return them after reading.<br/></p>
						  				</td>
						  			</tr>
						  			<tr>
						  				<td>
						  					<b>Here is how we make reading so easy for you.</b>
						  					<li>Just login into <a style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com">'.$company_name.'.indiareads.com</a> account'.$str.', browse books and add them to your <a  style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf" style="color: #bc3526;text-decoration: none;">Bookshelf</a></li>
						  					<li>Select the books you want to read and checkout through your cart</li>
						  					<li>Books are delivered at your address</li>
						  					<li>Done reading? Return the book from the <a  style="color: #bc3526;text-decoration: none;" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf">Currently Reading</a> section of the bookshelf. <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> will connect and pick the books</li>
						  				</td>
						  			</tr>
						  					';
						  						if(($company_name) != 'hdfc')
						  						{
						  			$maildelivered.=	'<tr>
						  						<td>
						  							<p>
						  								<b>Note:</b> You will be able to read only one book at a time. To order a book, please ensure you have returned all previously ordered books. Also, you can keep a book for a period of 30 days';
						  								if($company_name == "inautix")
						  								{ 
						  									$maildelivered.= ', beyond which <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;"> IndiaReads</a> will charge a late fee.' ; 
						  								} 
						  							$maildelivered.= '</p>
						  						</td>
						  					</tr>
						  					';
						  				}
						  			$maildelivered .= '<tr>
						  				<td><br>Need Assistance?&nbsp;<a target="_blank" href="http://indiareads.com/contactus">Contact us</a> any time.</td>
						  			</tr>
						  			<tr>
						  				<td>
						  					Or, <br> Write to us at
						  					customercare@indiareads.com <br>Phone : (0120)-419-4999 <br>Alternate No. : (0120)-420-6323
						  				</td>
						  			</tr>
						  			<br>
						  			<tr>
						  				<td>
						  					<p>Happy Reading!<br>Team IndiaReads.</p>
						  				</td>
						  			</tr>
						  		</tbody>
						  	</table>';
						  	return $maildelivered;
		}
		else if($status == 'Returned'){
				$totalpay = $res[0]['amt_pay'] + $res[0]['store_pay'];
				$rent = $totalpay - $res[0]['refund'];
				$mailreturned = '<table style="color:#000;padding:20px;max-width:600px;font-size: 16px;line-height: 1.2em;letter-spacing: 0.02em">
						<tbody>
							<tr>
								<td><a target="_blank" href="http://indiareads.com/"><img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/logo.png" alt="INDIAREADS"></a></td>
							</tr>
								<tr>
									<td><br>Hello, Reader!</td>
								</tr>
								<tr>
									<td>
								
										Your order has been <b>returned</b>. <br>We hope the book was an awesome read for you.
									</td>
								</tr>
									<tr style="background-color:#bc3526;color:#ffffff;">
									    <td align="center" style="padding:5px;width:300px;">Order ID:</td>
									</tr>
									<tr>
									    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
									        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
									        '.$res[0]['title'].'<br/>By '.$res[0]["contributor_name1"].'
									        <p>ISBN : '.$res[0]["ISBN13"].'</p></td>
									</tr>
									<tr>
										<td>
											<p>Reading just got bigger and better. <br/>Now books are affordable like never before. <br/><a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> lets you read books and return them after reading.<br/></p>
										</td>
									</tr>
									<tr>
										<td>
											<b>Here is how we make reading so easy for you.</b>
											<li>Just login into <a style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com">'.$company_name.'.indiareads.com</a> account'.$str.', browse books and add them to your <a  style="color: #bc3526;text-decoration: none;" target="_blank" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf" style="color: #bc3526;text-decoration: none;">Bookshelf</a></li>
											<li>Select the books you want to read and checkout through your cart</li>
											<li>Books are delivered at your address</li>
											<li>Done reading? Return the book from the <a  style="color: #bc3526;text-decoration: none;" href="http://'.$company_name.'.indiareads.com/bookshelf?tabs=Bookshelf">Currently Reading</a> section of the bookshelf. <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;">IndiaReads</a> will connect and pick the books</li>
										</td>
									</tr>
											';
												if(($company_name) != 'hdfc')
												{
									$mailreturned.=	'<tr>
												<td>
													<p>
														<b>Note:</b> You will be able to read only one book at a time. To order a book, please ensure you have returned all previously ordered books. Also, you can keep a book for a period of 30 days';
														if($company_name == "inautix")
														{ 
															$mailreturned.= ', beyond which <a target="_blank" href="http://indiareads.com/" style="color: #bc3526;text-decoration: none;"> IndiaReads</a> will charge a late fee.' ; 
														} 
													$mailreturned.= '</p>
												</td>
											</tr>
											';
										}
									$mailreturned .= '
									<tr>
										<td><br>Need Assistance?&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://indiareads.com/contactus">Contact us</a> any time.</td>
									</tr>
									<tr>
										<td>
											Or, <br> Write to us at
											customercare@indiareads.com <br>Phone : (0120)-419-4999 <br>Alternate No. : (0120)-420-6323
										</td>
									</tr>
									<br>
									<tr>
										<td>
											<p>Happy Reading!<br>Team IndiaReads.</p>
										</td>
									</tr>
								</tbody>
							</table>';

							return $mailreturned;
		}
		else{

			print_r('');
		}

	}
?>