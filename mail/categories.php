<?php
        function breadcrumbs($id)
            {
                global $data;
                $current = $data[$id];
                $parent_id = $current["parent_id"] === NULL ? "NULL" : $current["parent_id"];
                $parents = array();
                $parents[] = $current['name'];
                while (isset($data[$parent_id])) 
                {
                    $current = $data[$parent_id];
                    $parent_id = $current["parent_id"] === NULL ? "NULL" : $current["parent_id"];
                    $parents[] = $current["name"];
                }

                return implode(" > ", array_reverse($parents));

            } 

        function get_child_nodes2($parent_id)
        {
            $children = array();
            global $data, $index;
            $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
            if (isset($index[$parent_id])) {
                foreach ($index[$parent_id] as $id) 
                {
                    $children[] = $id;
                    $children = array_merge($children, get_child_nodes2($id));
                }
            }
            return $children;
        }

        function get_child_nodes1($parent_id, &$children)
        {
            global $data, $index;
            $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
            if (isset($index[$parent_id])) {
                foreach ($index[$parent_id] as $id) 
                {
                    $children[] = $id;
                    get_child_nodes1($id, $children);
                }
            }
        }

        function display_child_nodes($parent_id, $level)
        {
            global $data, $index;
            $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
            if (isset($index[$parent_id])) {
                foreach ($index[$parent_id] as $id) 
                {
                    echo str_repeat("-", $level) . $data[$id]["name"] . "<br>";
                    display_child_nodes($id, $level + 1);
                }
            }
        } 


        function get_path($node)
         { 
            $result = DB::getInstance()->query('SELECT parent_id FROM book_categories WHERE name='.$node.''); 

            $row = $result->results(); 

            print_r($result->first());

            $path = array(); 

            if ($row['parent_id']!=NULL) 
            { 

                $path[] = $row['parent_id']; 

                $path = array_merge(get_path($row['parent_id']), $path); 

            } 
            return $path; 

        } 
    
?>