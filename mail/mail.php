<?php

	function sendMail($to,$subject,$body)
	{
		if(!class_exists('PHPMailer',false))
		{
			require_once __DIR__ . '/PHPMailer-master/class.phpmailer.php';
		}

		$mail = new PHPMailer;

		$mail->ClearAddresses();
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'smtp.mandrillapp.com';	    		  			// Specify main and backup server
		$mail->Port = 587;
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'info@indiareads.com';                        // SMTP username
		$mail->Password = 'ApZTwA6thDlb5hi8zFUMFA';                     // SMTP password
		$mail->SMTPSecure = 'tls';                           	        // Enable encryption, 'ssl' also accepted
		$mail->From = 'info@indiareads.com';
		$mail->FromName = 'IndiaReads Online Library';
		$mail->addAddress($to);               							// Name is optional

		$mail->WordWrap = 50;                              				// Set word wrap to 50 characters

		$mail->isHTML(true);                                  			// Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body= $body;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
		   return false;
		   exit;
		}
		else
			return true;
	}

	function order_ids($case, $orders, $fun_obj){

		$result = '';
		$useremail = $fun_obj->get_bookshelf_order($orders[0]);
		$useremail = $useremail[0]["user_email"];
		$body = mailbody($case,$orders,$fun_obj);
		sendMail($useremail,'[INDIAREADS] Order '.$case, $body);
		$result = 'Mail sent for Order '.$case.'<br>';
		return $result;
	}
	
	function mailer($case, $order_id, $fun_obj, $credits=0){

		$result = '';

		$res = $fun_obj->get_bookshelf_order($order_id);
		sendMail($res[0]['user_email'],'[INDIAREADS] Order '.$case, mailbody($case,$res,$fun_obj,$credits));
		$result = 'Order '.$order_id.' '.$case;

		return $result;

	}

	function mailbody($status,$res, $fun_obj,$credits=0){
		$trackid = 0;

		if($status == 'Deleted'){
			return '<table>
						<tbody style="font-size: 18px;">
							<tr>
								<td><a href="#"><img src="http://www.indiareads.com/images/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									Your order has been deleted. <br>This is a confirmation email that we have deleted your order.<br><br> Your order has been listed below.
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 18px;">
							<tr style="background-color:#bc3526;color:#ffffff;">
							    <!-- <td align="center">Sub-order</td> -->
							    <td align="center" style="padding:5px;width:300px;">Order ID</td>
							    <td align="center" style="padding:5px;width:300px;">Item</td>
							</tr>
							<tr>
								<td align="center" style="border: 1px solid #c0c0c0;width:300px;">'.$res[0]["bookshelf_order_id"].'</td>
							    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
							        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
							        '.$res[0]["title"].'<br/>By '.$res[0]["contributor_name1"].'
							        <p>ISBN : '.$res[0]["ISBN13"].'</p>
							    </td>
							    
							</tr>
					</table>
					<table style="font-size: 18px;">
						<tbody>
							<tr>
								<td>
								<br><br>
								<p>Please check your user account for store credits refund if applicable.</p>
								<br><br>
									<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you rent books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<h4>Here is how we make reading so easy for you.</h4>
									<li>Just Login, Browse Books and Add them to your Bookshelf</li>
									<li>Select the Books you want to rent and checkout through our Rental Cart</li>
									<li>Make the Initial Payment via Store Credit, Online Payments, COD or internet wallets</li>
									<li>Books are Delivered at your Address</li>
									<li>Done Reading ? Request to return the books</li>
									<li>Rent is Charged and Refund is made to Store Credit and Bank Account.</li>
								</td>
							</tr>
							<tr>
								<td><p>Need Assistance?<br><a href="#">Contact us</a> any time.</p></td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us @
									customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-4206323
								</td>

							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';
		}
		else if($status == 'Dispatched'){
			$result = array();
			$pay = array();

			$parentid = $fun_obj->get_bookshelf_order($res[0]);
			$parent = $parentid[0]['p_order_id'];
			$trackid = $parentid[0]["d_track_id"];
			$storecredits = $parentid[0]["store_discount"];
			$coupondisc = 0;
			$tablerows = '';
			$storecredits=0;
			foreach ($res as $key) {
				$result[$key] = $fun_obj->get_bookshelf_order($key);
				$tablerows .= '<tr>
									<td align="center" style="border: 1px solid #c0c0c0;width:300px;">'.$key.'</td>
								    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
								        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$result[$key][0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
								       	'.$result[$key][0]["title"].'<br/>By '.$result[$key][0]["contributor_name1"].'
								       	<p>ISBN: '.$result[$key][0]["ISBN13"].'</p>
								    </td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$result[$key][0]["init_pay"].'</td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$result[$key][0]["mrp"].'</td>
								</tr>';
				$initpay += $result[$key][0]["init_pay"];
				$totalmrp += $result[$key][0]["mrp"];
				$coupondisc += $result[$key][0]["disc_pay"];
			}
			array_push($pay, $initpay, $totalmrp);
			$ship_flag = 0;
			$check_parent = $fun_obj->get_parent_dispatch($parent);
			if($check_parent){
				$ship_flag = 0;
			}
			else{
				$ship_flag = 50;
			}
			$finalpay = $pay[0] + $ship_flag - $storecredits - $coupondisc;
			return '<table>
						<?php print_r($parentid[0]["d_track_id"]);exit;?>
						<tbody style="font-size: 18px;">
							<tr>
								<td><a href="#"><img src="http://www.indiareads.com/images/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									We are glad to inform you that we have <b>dispatched</b> your book(s). <br>You can track you order using the following details.<br>
										<ul>
											<li>Carrier: '.$parentid[0]['carrier'].'</li>
											<li>Tracking ID: '.$trackid.'</li>
										</ul>
									<br>In case of any query please <a href="#">contact us</a>.<br><br> Your order has been listed below.
								</td>
							</tr>
						</tbody></table>
							<table style="font-size: 18px;">
								<tr style="background-color:#bc3526;color:#ffffff;">
								    <!-- <td align="center">Sub-order</td> -->
								    <td align="center" style="padding:5px;width:300px;">Order ID</td>
								    <td align="center" style="padding:5px;width:300px;">Item</td>
								    <td align="center" style="padding:5px;">Initial Payable</td>
								    <td align="center" style="padding:5px;">MRP</td>
								</tr>
									'.$tablerows.'
							</table>
							<table style="font-size: 18px;"><tbody>
							<tr>
								<td>
								<br><br>
									<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you rent books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<h4>Here is how we make reading so easy for you.</h4>
									<li>Just Login, Browse Books and Add them to your Bookshelf</li>
									<li>Select the Books you want to rent and checkout through our Rental Cart</li>
									<li>Make the Initial Payment via Store Credit, Online Payments, COD or internet wallets</li>
									<li>Books are Delivered at your Address</li>
									<li>Done Reading ? Request to return the books</li>
									<li>Rent is Charged and Refund is made to Store Credit and Bank Account.</li>
								</td>
							</tr>
							<tr>
								<td><p>Need Assistance?<br><a href="#">Contact us</a> any time.</p></td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us @
									customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-4206323
								</td>

							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';

		}
		else if($status == 'Delivered'){

			return '<table>
						<tbody style="font-size: 18px;">
							<tr>
								<td><a href="#"><img src="http://www.indiareads.com/images/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									We are happy to inform you the we have <b>delivered</b> your book(s). <br>This is a confirmation email that you have received the order.<br><br>In case of any query please <a href="#">contact us</a>.<br><br> Your order has been listed below.
								</td>
							</tr>
						</tbody></table>
							<table style="font-size: 18px;">
								<tr style="background-color:#bc3526;color:#ffffff;">
								    <!-- <td align="center">Sub-order</td> -->
								    <td align="center" style="padding:5px;width:300px;">Order ID</td>
								    <td align="center" style="padding:5px;width:300px;">Item</td>
								    <td align="center" style="padding:5px;">MRP</td>
								    <td align="center" style="padding:5px;">Initial Payable</td>
								</tr>
								<!-- this tr has to be repeated -->
								<tr>
									<td align="center" style="border: 1px solid #c0c0c0;width:300px;">'.$res[0]["bookshelf_order_id"].'</td>
								    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
								        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
								        '.$res[0]['title'].'<br/>By '.$res[0]["contributor_name1"].'
								        <p>ISBN : '.$res[0]["ISBN13"].'</p>
								    </td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$res[0]["mrp"].'</td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$res[0]["init_pay"].'</td>
								</tr>
							</table>
							<table style="font-size: 18px;"><tbody>
							<tr>
								<td>
								<br><br>
									<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you rent books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<h4>Here is how we make reading so easy for you.</h4>
									<li>Just Login, Browse Books and Add them to your Bookshelf</li>
									<li>Select the Books you want to rent and checkout through our Rental Cart</li>
									<li>Make the Initial Payment via Store Credit, Online Payments, COD or internet wallets</li>
									<li>Books are Delivered at your Address</li>
									<li>Done Reading ? Request to return the books</li>
									<li>Rent is Charged and Refund is made to Store Credit and Bank Account.</li>
								</td>
							</tr>
							<tr>
								<td><p>Need Assistance?<br><a href="#">Contact us</a> any time.</p></td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us @
									customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-4206323
								</td>

							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';
		}
		else if($status == 'Returned'){
			$totalpay = $res[0]['amt_pay'] + $res[0]['store_pay'];
			$rent = $totalpay - $res[0]['refund'];
			return '<table>
						<tbody style="font-size: 18px;">
							<tr>
								<td><a href="#"><img src="http://www.indiareads.com/images/logo.png" alt="INDIAREADS"></a></td>
							</tr>
							<tr>
								<td><br>Hello, Reader!</td>
							</tr>
							<tr>
								<td>
								<br>
									Your order has been returned. <br>We hope the book was an awesome read for you.
								</td>
							</tr>
						</tbody></table>
							<table style="font-size: 18px;">
								<br>
								<tr style="background-color:#bc3526;color:#ffffff;">
								    <!-- <td align="center">Sub-order</td> -->
								    <td align="center" style="padding:5px;width:300px;">Order ID:</td>
								    <td align="center" style="padding:5px;width:300px;">Item</td>
								    <td align="center" style="padding:5px;">Total Amount Paid by You</td>
								    <td align="center" style="padding:5px;">Total Rent</td>
								    <td align="center" style="padding:5px;">Total Refund</td>
								</tr>
								<!-- this tr has to be repeated -->
								<tr>
									<td align="center" style="border: 1px solid #c0c0c0;width:300px;">'.$res[0]["bookshelf_order_id"].'</td>
								    <td align="center" style="border: 1px solid #c0c0c0;width:300px;">
								        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/'.$res[0]["ISBN13"].'.jpg" style="margin-left:10px;max-height:85px;min-height:85px;width:55px"> <br>
								        '.$res[0]['title'].'<br/>By '.$res[0]["contributor_name1"].'
								        <p>ISBN : '.$res[0]["ISBN13"].'</p>
								    </td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$totalpay.'</td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$rent.'</td>
								    <td align="center" style="padding:5px; border: 1px solid #c0c0c0;"> Rs '.$res[0]["refund"].'</td>
								</tr>
							</table>
							<table style="font-size: 18px;"><tbody>
							<tr>
								<td>
								<br><br>
									<p>Reading Just got Bigger and Better. <br/>Now Books are affordable like never before. <br/>IndiaReads lets you rent books and return them after reading.<br/></p>
								</td>
							</tr>
							<tr>
								<td>
									<h4>Here is how we make reading so easy for you.</h4>
									<li>Just Login, Browse Books and Add them to your Bookshelf</li>
									<li>Select the Books you want to rent and checkout through our Rental Cart</li>
									<li>Make the Initial Payment via Store Credit, Online Payments, COD or internet wallets</li>
									<li>Books are Delivered at your Address</li>
									<li>Done Reading ? Request to return the books</li>
									<li>Rent is Charged and Refund is made to Store Credit and Bank Account.</li>
								</td>
							</tr>
							<tr>
								<td><p>Need Assistance?<br><a href="#">Contact us</a> any time.</p></td>
							</tr>
							<tr>
								<td>
									Or, <br> Write to us @
									customercare@indiareads.com <br>Toll Free : 1800-103-7323 <br>Alternate No. : 0120-4206323
								</td>

							</tr>
							<br>
							<tr>
								<td>
									<p>Happy Reading!<br>Team IndiaReads.</p>
								</td>
							</tr>
						</tbody>
					</table>';
		}
		else{

			print_r('');
		}

	}
?>
